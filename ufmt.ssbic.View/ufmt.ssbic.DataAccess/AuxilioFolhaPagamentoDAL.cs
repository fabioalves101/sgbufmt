﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.DataAccess
{
    public class AuxilioFolhaPagamentoDAL
    {

        public void Excluir(int auxiliadoUID, int folhaPagamentoUID)
        {
            var contexto = Contexto.GetContextInstance();
            contexto.ExecuteStoreCommand("delete from AuxilioFolhaPagamento where auxiliadoUID = " + auxiliadoUID + " and folhaPagamentoUID = " + folhaPagamentoUID);
            contexto.SaveChanges();
        }

        public void AtualizarVinculoInstitucional(int folhaPagamentoUID)
        {
            var contexto = Contexto.GetContextInstance();
            contexto.VerificaAlteracaoVinculoAux(folhaPagamentoUID);
        }
    }
}