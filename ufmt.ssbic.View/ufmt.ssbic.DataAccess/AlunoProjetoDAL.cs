﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ufmt.ssbic.DataAccess
{
    public class AlunoProjetoDAL
    {
        public List<vwAlunosProjetoResponsavel> GetAlunosProjeto(int projetoUID)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                List<vwAlunosProjetoResponsavel> registros = (
                    from c in contexto.vwAlunosProjetoResponsavel
                    where c.projetoUID == projetoUID                    
                    select c).ToList<vwAlunosProjetoResponsavel>();
                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        

        public vwAluno GetAluno(Int64 registroMatricula)
        {
            var contexto = Contexto.GetContextInstance();
            vwAluno aluno = (
                from a in contexto.vwAluno
                where a.Matricula == registroMatricula
                select a
                ).FirstOrDefault<vwAluno>();

            return aluno;
        }

        public AlunoProjeto SaveAlunoProjeto(AlunoProjeto objAlunoProjeto, int operacao)
        {
            var contexto = Contexto.GetContextInstance();

            AlunoProjeto alunoProjeto = new AlunoProjeto();
            alunoProjeto.alunoProjetoUID = objAlunoProjeto.alunoProjetoUID;
            alunoProjeto.membroUID = objAlunoProjeto.membroUID;
            alunoProjeto.projetoUID = objAlunoProjeto.projetoUID;
            alunoProjeto.registroAluno = objAlunoProjeto.registroAluno;
            alunoProjeto.pessoaMembroUID = objAlunoProjeto.pessoaMembroUID;
            
            if (operacao == 1)
            {
                contexto.AddObject("AlunoProjeto", alunoProjeto);
            }
            else
            {
                contexto.AlunoProjeto.Attach(alunoProjeto);
            }

            contexto.SaveChanges();

            return alunoProjeto;
        }

        public bool DeleteAlunoProjeto(AlunoProjeto objAlunoProjeto)
        {
            try
            {
                AlunoProjeto alunoProjeto = new AlunoProjeto();
                alunoProjeto.alunoProjetoUID = objAlunoProjeto.alunoProjetoUID;
                alunoProjeto.registroAluno = objAlunoProjeto.registroAluno;
                alunoProjeto.membroUID = objAlunoProjeto.membroUID;
                alunoProjeto.projetoUID = objAlunoProjeto.projetoUID;
                alunoProjeto.pessoaMembroUID = objAlunoProjeto.pessoaMembroUID;

                var contexto = Contexto.GetContextInstance();

                contexto.AlunoProjeto.Attach(alunoProjeto);
                contexto.AlunoProjeto.DeleteObject(alunoProjeto);
                contexto.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AlunoProjeto GetAlunoProjeto(int alunoProjetoUID)
        {
            var contexto = Contexto.GetContextInstance();

            AlunoProjeto alunoProjeto = (
                from c in contexto.AlunoProjeto
                where c.alunoProjetoUID == alunoProjetoUID
                select c
                ).FirstOrDefault<AlunoProjeto>();

            return alunoProjeto;
        }

        public AlunoProjeto GetAlunoProjeto(Int64 matricula)
        {
            var contexto = Contexto.GetContextInstance();

            String registroAluno = matricula.ToString();

            AlunoProjeto alunoProjeto = (
                from c in contexto.AlunoProjeto
                where c.registroAluno == registroAluno
                select c
                ).FirstOrDefault<AlunoProjeto>();

            return alunoProjeto;
        }

        public List<vwAlunosProjetoResponsavel> GetAlunosProjetoMembro(int pessoaMembroUID)
        {
            var contexto = Contexto.GetContextInstance();

            List<vwAlunosProjetoResponsavel> alunosProjeto = (
                from c in contexto.vwAlunosProjetoResponsavel
                where c.pessoaMembroUID == pessoaMembroUID
                select c
                ).ToList<vwAlunosProjetoResponsavel>();

            return alunosProjeto;
        }

        public List<AlunoProjeto> GetAlunosPorProjeto(int projetoUID)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                List<AlunoProjeto> registros = (
                    from c in contexto.AlunoProjeto
                    where c.projetoUID == projetoUID
                    select c).ToList<AlunoProjeto>();
                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }
        
    }
}
