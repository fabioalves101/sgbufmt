﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace ufmt.ssbic.DataAccess
{
    public class BolsistaFolhaPagamentoDAL
    {
        public void Save(BolsistaFolhaPagamento bolsistaFolhaPagamento)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();

                contexto.AddObject("BolsistaFolhaPagamento", bolsistaFolhaPagamento);
                contexto.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public BolsistaFolhaPagamento GetBolsistaFolhaPagamentoPorBolsista(int bolsistaUID)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();

                return (from c in contexto.BolsistaFolhaPagamento
                           where c.bolsistaUID == bolsistaUID
                           select c).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<BolsistaFolhaPagamento> GetBolsistasFolhaPagamento(int bolsistaUID, int folhaPagamentoUID)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();

                return (from c in contexto.BolsistaFolhaPagamento
                        where c.bolsistaUID == bolsistaUID && 
                              c.folhaPagamentoUID == folhaPagamentoUID
                        select c).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public BolsistaFolhaPagamento GetBolsistaFolhaPagamento(int bolsistaUID, int folhaPagamentoUID)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();

                return (from c in contexto.BolsistaFolhaPagamento
                        where c.bolsistaUID == bolsistaUID &&
                              c.folhaPagamentoUID == folhaPagamentoUID 
                        orderby c.ativo descending
                        select c).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void AtualizarSituacaoBolsistaFolhaPagamento(int bolsistaUID, int folhaPagamentoUID, bool ativo, String justificativa)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();
                BolsistaFolhaPagamento bfp =  
                    (from c in contexto.BolsistaFolhaPagamento
                        where c.bolsistaUID == bolsistaUID &&
                              c.folhaPagamentoUID == folhaPagamentoUID 
                        orderby c.ativo descending
                        select c).FirstOrDefault();

                bfp.ativo = ativo;
                bfp.justificativaSituacao = justificativa;
                int i = contexto.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<BolsistaFolhaPagamento> GetBolsistasPorFolhaPagamento(int folhaPagamentoUID, bool somenteAtivos)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();

                return (from c in contexto.BolsistaFolhaPagamento
                        where 
                            c.folhaPagamentoUID == folhaPagamentoUID && 
                            c.ativo == somenteAtivos
                        select c).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public BolsistaFolhaPagamento GetBolsistaUltimaFolhaPagamento(int bolsistaUID)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();

                return (from c in contexto.BolsistaFolhaPagamento
                            join f in contexto.FolhaPagamento
                            on c.folhaPagamentoUID equals f.folhaPagamentoUID
                        where
                            c.bolsistaUID == bolsistaUID
                            orderby f.dataFim descending
                        select c).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void AtualizarVinculoInstitucionalBolsistas(int folhaPagamentoUID)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();
                contexto.VerificaAlteracaoVinculo(folhaPagamentoUID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
