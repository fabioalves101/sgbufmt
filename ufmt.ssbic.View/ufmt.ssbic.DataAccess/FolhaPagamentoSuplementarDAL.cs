﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace ufmt.ssbic.DataAccess
{
    public class FolhaPagamentoSuplementarDAL
    {
        public FolhaPagamentoSuplementar GetFolhaPagamentoSuplementar(int folhaPagamentoSuplementarUID)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                FolhaPagamentoSuplementar registro =
                    (from i in contexto.FolhaPagamentoSuplementar where i.folhaSuplementarUID == folhaPagamentoSuplementarUID select i)
                    .FirstOrDefault<FolhaPagamentoSuplementar>();

                return registro;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }
        
        public List<FolhaPagamentoSuplementar> GetFolhaPagamentoSuplementarProcessoSeletivo(int processoSeletivoUID)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();
                return (from c in contexto.FolhaPagamentoSuplementar where c.processoSeletivoUID == processoSeletivoUID select c).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void SalvarFolhaPagamentoSuplementar(int processoSeletivoUID, DateTime dataInicio, DateTime dataFim)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();
                FolhaPagamentoSuplementar folha = new FolhaPagamentoSuplementar()
                {
                    processoSeletivoUID = processoSeletivoUID,
                    dataInicio = dataInicio,
                    dataFim = dataFim,
                    fechada = false
                };

                contexto.FolhaPagamentoSuplementar.AddObject(folha);
                contexto.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public FolhaPagamento GetUltimaFolhaPagamento(int processoSeletivoUID)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();
                return (from c in contexto.FolhaPagamento
                        where c.processoSeletivoUID == processoSeletivoUID
                        orderby c.dataFim descending
                        select c).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public BolsistaFolhaSuplementar GetAlunoEmFolhaSuplementar(string cpf, int folhaSuplementarUID)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();
                return (from c in contexto.BolsistaFolhaSuplementar
                        where c.cpf == cpf && c.folhaSuplementarUID == folhaSuplementarUID
                        select c).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
