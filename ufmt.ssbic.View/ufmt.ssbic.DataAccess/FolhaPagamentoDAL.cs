﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace ufmt.ssbic.DataAccess
{
    public class FolhaPagamentoDAL
    {
        public List<FolhaPagamento> GetRegistros(int? processoSeletivoUID)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                List<FolhaPagamento> registros = (from i in contexto.FolhaPagamento orderby i.dataInicio descending where i.processoSeletivoUID == processoSeletivoUID select i).ToList<FolhaPagamento>();

                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public List<FolhaPagamento> FindRegistros(int? processoSeletivoUID, DateTime dataInicio)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                List<FolhaPagamento> registros = 
                    (from i in contexto.FolhaPagamento orderby i.dataInicio descending where i.processoSeletivoUID == processoSeletivoUID && i.dataInicio == dataInicio select i).ToList<FolhaPagamento>();

                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public FolhaPagamento GetFolhaPagamento(int folhaPagamentoUID)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                FolhaPagamento registro =
                    (from i in contexto.FolhaPagamento where i.folhaPagamentoUID == folhaPagamentoUID select i).FirstOrDefault<FolhaPagamento>();

                return registro;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public FolhaPagamento SalvarFolhaPagamento(FolhaPagamento objFolha, int operacao)
        {
            var contexto = Contexto.GetContextInstance();
            try
            {
                FolhaPagamento folha = new FolhaPagamento();
                folha.folhaPagamentoUID = objFolha.folhaPagamentoUID;
                folha.processoSeletivoUID = objFolha.processoSeletivoUID;
                folha.dataInicio = objFolha.dataInicio;
                folha.dataFim = objFolha.dataFim;
                folha.fechada = objFolha.fechada;
                folha.etapa1 = objFolha.etapa1;
                folha.etapa2 = objFolha.etapa2;
                folha.etapa3 = objFolha.etapa3;
                folha.parecerE2 = objFolha.parecerE2;
                folha.parecerE3 = objFolha.parecerE3;
                folha.suplementar = objFolha.suplementar;

                if (operacao == 1)
                {
                    contexto.AddObject("FolhaPagamento", folha);
                }
                else
                {
                    contexto.FolhaPagamento.Attach(folha);
                    contexto.ObjectStateManager.ChangeObjectState(folha, EntityState.Modified);
                }
                contexto.SaveChanges();

                return folha;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível atualizar os registros.", ex);
            }

        }

        public List<FolhaPagamento> GetFolhasPagamentoAtivas()
        {
            try
            {
                var contexto = Contexto.GetContextInstance();
                return (from c in contexto.FolhaPagamento 
                        where c.etapa1 == 3 && c.etapa2 == 3 && c.etapa3 == 0
                        select c).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
       

        public IList<FolhaPagamento> GetFolhaPagamentoProcessoSeletivo(int processoSeletivoUID)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();
                return (from c in contexto.FolhaPagamento where c.processoSeletivoUID == processoSeletivoUID select c).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void GerarNovaFolhaPagamento(int processoSeletivoUID, DateTime dataInicio, DateTime dataFim)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();
                contexto.TransfereAlunosFolha(processoSeletivoUID, dataInicio, dataFim);

                List<FolhaPagamento> folhas = contexto.FolhaPagamento.Where(fp => fp.processoSeletivoUID == processoSeletivoUID).ToList();
                
                contexto.TransfereAlunosAuxilio(folhas.LastOrDefault().folhaPagamentoUID, processoSeletivoUID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public FolhaPagamento GetUltimaFolhaPagamento(int processoSeletivoUID)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();
                return (from c in contexto.FolhaPagamento
                        where c.processoSeletivoUID == processoSeletivoUID
                        orderby c.dataFim descending
                        select c).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
