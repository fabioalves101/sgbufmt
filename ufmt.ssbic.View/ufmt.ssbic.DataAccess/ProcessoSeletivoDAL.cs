﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;

namespace ufmt.ssbic.DataAccess
{
    public class ProcessoSeletivoDAL
    {

        public List<vwProcessoSeletivo> GetRegistros(int? instrumentoSelecaoUID)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                List<vwProcessoSeletivo> registros = (from i in contexto.vwProcessoSeletivo where i.instrumentoSelecaoUID == instrumentoSelecaoUID select i).ToList<vwProcessoSeletivo>();

                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public vwProcessoSeletivo GetProcessoSeletivo(int processoSeletivoUID)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                return (from c in contexto.vwProcessoSeletivo where c.processoSeletivoUID == processoSeletivoUID select c).FirstOrDefault<vwProcessoSeletivo>();

            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar o registro.", exception);
            }
        }

        public ProcessoSeletivo GetProcessoSeletivoPorId(int processoSeletivoUID)
        {
            var contexto = Contexto.GetContextInstance();
            try
            {
                return (from c in contexto.ProcessoSeletivo
                        where c.processoSeletivoUID == processoSeletivoUID
                        select c).FirstOrDefault();
                

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ProcessoSeletivo GetProcesso(int instrumentoSelecaoUID)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                return (from c in contexto.ProcessoSeletivo where c.instrumentoSelecaoUID == instrumentoSelecaoUID select c).FirstOrDefault();

            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar o registro.", exception);
            }
        }

        public List<vwProcessoSeletivo> FindRegistros(int? instrumentoSelecaoUID, string criterio)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                List<vwProcessoSeletivo> registros = (from c in contexto.vwProcessoSeletivo where c.instrumentoSelecaoUID == instrumentoSelecaoUID && c.descricao.Contains(criterio) select c).ToList<vwProcessoSeletivo>();
                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public int Salvar(ProcessoSeletivo ps, int operacao)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {

                var novoRegistro = new ProcessoSeletivo
                {
                    processoSeletivoUID = ps.processoSeletivoUID,
                    instrumentoSelecaoUID = ps.instrumentoSelecaoUID,
                    titulo = ps.titulo,
                    descricao = ps.descricao,
                    dataAplicacao = ps.dataAplicacao,
                    dataResultado = ps.dataResultado
                };

                if (operacao == 1)
                {
                    contexto.AddObject("ProcessoSeletivo", novoRegistro);
                }
                else
                {
                    contexto.ProcessoSeletivo.Attach(novoRegistro);
                    contexto.ObjectStateManager.ChangeObjectState(novoRegistro, EntityState.Modified);
                }

                contexto.SaveChanges();
                return novoRegistro.processoSeletivoUID;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível salvar os registros.", exception);

            }
        }

        public bool Excluir(int processoSeletivoUID)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();
                contexto.DeleteObject((from c in contexto.ProcessoSeletivo where c.processoSeletivoUID == processoSeletivoUID select c).SingleOrDefault());
                contexto.SaveChanges();
                return true;

            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível excluir os registros.", exception);

            }
        }

        

    }
}
