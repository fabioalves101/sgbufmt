﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ufmt.ssbic.DataAccess
{
    public class AuxilioDAL
    {
        public List<Auxilio> GetAuxilios()
        {
            var contexto = Contexto.GetContextInstance();

            return contexto.Auxilio.ToList();

        }

        public Auxilio GetAuxilio(int auxilioUID)
        {
            var contexto = Contexto.GetContextInstance();
            return contexto.Auxilio.Where(a => a.auxilioUID == auxilioUID).FirstOrDefault();
        }

        public List<AuxilioAluno> GetAuxiliosAuxiliado(int auxiliadoUID)
        {
            var contexto = Contexto.GetContextInstance();
            return contexto.AuxilioAluno.Where(a => a.auxiliadoUID == auxiliadoUID).ToList();
        }

        public Auxiliado GetAuxiliado(int auxiliadoUID)
        {
            var contexto = Contexto.GetContextInstance();
            return contexto.Auxiliado.Where(a => a.auxiliadoUID == auxiliadoUID).FirstOrDefault();
        }
    }
}
