﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace ufmt.ssbic.DataAccess
{
    public class AuxiliadoDAL
    {
        public void Salvar(Auxiliado auxiliado)
        {
            var contexto = Contexto.GetContextInstance();
            contexto.Auxiliado.AddObject(auxiliado);
            contexto.SaveChanges();
        }


        public List<Auxiliado> GetAuxiliados(int folhaPagamentoUID)
        {
            var contexto = Contexto.GetContextInstance();

            return 
            (from a in contexto.Auxiliado 
             join afp in contexto.AuxilioFolhaPagamento on
                a.auxiliadoUID equals afp.auxiliadoUID
            where afp.folhaPagamentoUID == folhaPagamentoUID
                 select a
                 ).ToList();
             
            
                


                //contexto.Auxiliado.Where(
                //    a => a.AuxilioFolhaPagamento.FirstOrDefault().folhaPagamentoUID == folhaPagamentoUID
                //    ).ToList();
        }

        public vwAluno GetAlunoAuxiliado(int auxiliadoUID)
        {
            var contexto = Contexto.GetContextInstance();

            return 
                (from a in contexto.Auxiliado
                    join al in contexto.vwAluno on a.cpf equals al.CPF
                    where a.auxiliadoUID == auxiliadoUID
                    select al).FirstOrDefault();
        }

        public Auxiliado GetAuxiliado(int auxiliadoUID)
        {
            var contexto = Contexto.GetContextInstance();

            return
                (from a in contexto.Auxiliado                 
                 where a.auxiliadoUID == auxiliadoUID
                 select a).FirstOrDefault();
        }

        public void Atualizar(Auxiliado auxiliado)
        {
            var contexto = Contexto.GetContextInstance();
            contexto.ExecuteStoreCommand("delete from AuxilioAluno where auxiliadoUID = "+auxiliado.auxiliadoUID+";");
            contexto.SaveChanges();

            List<AuxilioAluno> listNewAuxilioAluno = new List<AuxilioAluno>();
            foreach (AuxilioAluno auxilioAluno in auxiliado.AuxilioAluno)
            {
                AuxilioAluno newAuxilioAluno = new AuxilioAluno()
                {
                    auxiliadoUID = auxiliado.auxiliadoUID,
                    auxilioUID = auxilioAluno.auxilioUID                    
                };

                listNewAuxilioAluno.Add(newAuxilioAluno);
            }           
            
            foreach(AuxilioAluno auxilioAluno in listNewAuxilioAluno)
            {
                contexto.AuxilioAluno.AddObject(auxilioAluno);
            }

            Auxiliado objAuxiliado = new Auxiliado()
            {
                agencia = auxiliado.agencia,
                auxiliadoUID = auxiliado.auxiliadoUID,
                bancoUID = auxiliado.bancoUID,
                cpf = auxiliado.cpf,
                email = auxiliado.email,
                numeroConta = auxiliado.numeroConta,
                registroAluno = auxiliado.registroAluno
            };

            contexto.Auxiliado.Attach(objAuxiliado);
            contexto.ObjectStateManager.ChangeObjectState(objAuxiliado, EntityState.Modified);
            contexto.SaveChanges();

        }

        public List<AuxilioFolhaPagamento> GetAuxiliadosFolhaPagamento(int folhaPagamentoUID, bool somenteAtivos)
        {
            var contexto = Contexto.GetContextInstance();

            List<AuxilioFolhaPagamento> auxiliados = new List<AuxilioFolhaPagamento>();
            if (somenteAtivos)
            {
                auxiliados = 
                    contexto.AuxilioFolhaPagamento
                    .Where(a => a.folhaPagamentoUID == folhaPagamentoUID)
                    .Where(a => a.ativo == somenteAtivos)
                    .ToList();
            }
            else
            {
                auxiliados = 
                contexto.AuxilioFolhaPagamento.Where(a => a.folhaPagamentoUID == folhaPagamentoUID).ToList();
            }

            return auxiliados;       
        }

        public void Excluir(int auxiliadoUID)
        {
            var contexto = Contexto.GetContextInstance();
            
            contexto.Auxiliado.DeleteObject(
                contexto.Auxiliado.Where(a => a.auxiliadoUID == auxiliadoUID).FirstOrDefault()
            );
            contexto.SaveChanges();
        }

        public List<Auxiliado> GetAuxiliados(int folhaPagamentoUID, string nome)
        {
            var contexto = Contexto.GetContextInstance();

            return
            (from a in contexto.Auxiliado
             join afp in contexto.AuxilioFolhaPagamento on
                a.auxiliadoUID equals afp.auxiliadoUID 
             join al in contexto.vwAluno on 
                a.cpf equals al.CPF
             where afp.folhaPagamentoUID == folhaPagamentoUID && al.Nome.Contains(nome)
             select a
                 ).ToList();
             
        }

        public AuxilioFolhaPagamento GetAuxilioFolhaPagamento(string cpf, int folhaPagamentoUID)
        {
            var contexto = Contexto.GetContextInstance();

            return 
            (from afp in contexto.AuxilioFolhaPagamento
             join a in contexto.Auxiliado on
                 afp.auxiliadoUID equals a.auxiliadoUID
             join al in contexto.vwAluno on
                 a.cpf equals al.CPF
             where afp.folhaPagamentoUID == folhaPagamentoUID && al.CPF == cpf
             select afp).FirstOrDefault();
        }

       
    }
}
