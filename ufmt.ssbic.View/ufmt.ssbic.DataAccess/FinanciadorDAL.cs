﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;

namespace ufmt.ssbic.DataAccess
{
    public class FinanciadorDAL
    {

        public List<vwFinanciador> GetRegistros(int? instrumentoSelecaoUID)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                List<vwFinanciador> registros = (from i in contexto.vwFinanciador where i.instrumentoSelecaoUID == instrumentoSelecaoUID select i).ToList<vwFinanciador>();

                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public List<Financiador> GetTipoFinanciador()
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                List<Financiador> registros = (from i in contexto.Financiador select i).ToList<Financiador>();

                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public vwFinanciador GetFinanciador(int financiadorInstrumentoSelecaoUID)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                return (from c in contexto.vwFinanciador where c.financiadorInstrumentoSelecaoUID == financiadorInstrumentoSelecaoUID select c).FirstOrDefault<vwFinanciador>();

            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar o registro.", exception);
            }
        }

        public List<vwFinanciador> FindRegistros(int? instrumentoSelecaoUID, string criterio)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                List<vwFinanciador> registros = (from c in contexto.vwFinanciador where c.instrumentoSelecaoUID == instrumentoSelecaoUID && c.descricao.Contains(criterio) select c).ToList<vwFinanciador>();
                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public bool Salvar(FinanciadorInstrumentoSelecao fis, int operacao)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {

                var novoRegistro = new FinanciadorInstrumentoSelecao
                {
                    financiadorInstrumentoSelecaoUID = fis.financiadorInstrumentoSelecaoUID,
                    instrumentoSelecaoUID = fis.instrumentoSelecaoUID,
                    financiadorUID = fis.financiadorUID,
                    quantidadeBolsas = fis.quantidadeBolsas,
                    quantidadeRestantes = fis.quantidadeRestantes,
                    valorBolsa = fis.valorBolsa,
                    tipoOferta = fis.tipoOferta,
                    cursoUID = fis.cursoUID

                };

                if (operacao == 1)
                {
                    contexto.AddObject("FinanciadorInstrumentoSelecao", novoRegistro);
                }
                else
                {
                    contexto.FinanciadorInstrumentoSelecao.Attach(novoRegistro);
                    contexto.ObjectStateManager.ChangeObjectState(novoRegistro, EntityState.Modified);
                }

                contexto.SaveChanges();
                return true;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível salvar os registros.", exception);

            }
        }

        public bool Excluir(int financiadorInstrumentoSelecaoUID)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();
                contexto.DeleteObject((from c in contexto.FinanciadorInstrumentoSelecao where c.financiadorInstrumentoSelecaoUID == financiadorInstrumentoSelecaoUID select c).SingleOrDefault());
                contexto.SaveChanges();
                return true;

            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível excluir os registros.", exception);

            }
        }

        public bool Salvar(Financiador objfinanciador, int operacao)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                var novoRegistro = new Financiador
                {
                    financiadorUID = objfinanciador.financiadorUID,
                    descricao = objfinanciador.descricao,
                    bancoUID = objfinanciador.bancoUID
                };

                if (operacao == 1)
                {
                    contexto.AddObject("Financiador", novoRegistro);
                }
                else
                {
                    contexto.Financiador.Attach(novoRegistro);
                    contexto.ObjectStateManager.ChangeObjectState(novoRegistro, EntityState.Modified);
                }

                contexto.SaveChanges();
                return true;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível salvar os registros.", exception);

            }
        }



        public FinanciadorInstrumentoSelecao GetFinanciadorInstrumentoSelecao(int p)
        {
            var contexto = Contexto.GetContextInstance();
            return (from c in contexto.FinanciadorInstrumentoSelecao where c.instrumentoSelecaoUID == p select c).FirstOrDefault(); 
        }
    }
}
