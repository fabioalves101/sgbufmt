﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Objects;

namespace ufmt.ssbic.DataAccess
{
    public class ProjetoDAL
    {
        public void SaveProjeto(Projeto objProjeto, int operacao)
        {
            var contexto = Contexto.GetContextInstance();

            if (operacao == 1)
            {
                contexto.AddObject("Projeto", objProjeto);
            }
            else
            {
                Projeto projeto = new Projeto();
                projeto.projetoUID = objProjeto.projetoUID;
                projeto.registroCap = objProjeto.registroCap;
                projeto.anoCap = objProjeto.anoCap;
                projeto.titulo = objProjeto.titulo;
                projeto.inicio = objProjeto.inicio;
                projeto.fim = objProjeto.fim;
                projeto.areaConhecimentoUID = objProjeto.areaConhecimentoUID;
                projeto.produtoTecnologico = objProjeto.produtoTecnologico;

                int opConvenio = 1;
                if (objProjeto.Convenio.Count != 0)
                {
                    Convenio convenio = objProjeto.Convenio.FirstOrDefault<Convenio>();
                    projeto.Convenio.Add(convenio);
                    if(convenio.convenioUID != 0)
                        opConvenio = 2;
                }

                int opFinanciamento = 1;
                if (objProjeto.Financiamento.Count != 0)
                {                    
                    Financiamento financiamento = objProjeto.Financiamento.FirstOrDefault<Financiamento>();
                    projeto.Financiamento.Add(financiamento);
                    if (financiamento.financiamentoUID != 0)
                        opFinanciamento = 2;
                }

                int opInteracao = 1;
                if (objProjeto.InteracaoProjetoInstituicao.Count != 0)
                {
                    InteracaoProjetoInstituicao interacao = objProjeto.InteracaoProjetoInstituicao.FirstOrDefault<InteracaoProjetoInstituicao>();
                    projeto.InteracaoProjetoInstituicao.Add(interacao);
                    if (interacao.interacaoProjetoInstituicaoUID != 0)
                        opFinanciamento = 2;
                }
                

                SaveConvenio(projeto.Convenio.FirstOrDefault<Convenio>(), opConvenio);
                SaveFinanciamento(projeto.Financiamento.FirstOrDefault<Financiamento>(), opFinanciamento);
                SaveInteracaoProjetoInstituicao(projeto.InteracaoProjetoInstituicao.FirstOrDefault<InteracaoProjetoInstituicao>(), opInteracao);

                contexto.Projeto.Attach(projeto);
                contexto.ObjectStateManager.ChangeObjectState(projeto, EntityState.Modified);       
            }

            contexto.SaveChanges();
        }

        public void SaveConvenio(Convenio objConvenio, int operacao)
        {
            if (objConvenio != null)
            {
                var contexto = Contexto.GetContextInstance();

                Convenio convenio = new Convenio();
                convenio.convenioUID = objConvenio.convenioUID;
                convenio.projetoUID = objConvenio.projetoUID;
                convenio.convenio1 = objConvenio.convenio1;

                if (operacao == 1)
                {
                    contexto.Convenio.AddObject(convenio);
                }
                else
                {
                    contexto.Convenio.Attach(convenio);
                }
                contexto.SaveChanges();
            }
        }

        public void SaveFinanciamento(Financiamento objFinanciamento, int operacao)
        {
            if (objFinanciamento != null)
            {
                var contexto = Contexto.GetContextInstance();
                Financiamento financiamento = new Financiamento();
                financiamento.financiamentoUID = objFinanciamento.financiamentoUID;
                financiamento.projetoUID = objFinanciamento.projetoUID;
                financiamento.financiador = objFinanciamento.financiador;
                financiamento.valor = objFinanciamento.valor;

                if (operacao == 1)
                {
                    contexto.AddObject("Financiamento", financiamento);
                }
                else
                {
                    contexto.Financiamento.Attach(financiamento);
                    contexto.ObjectStateManager.ChangeObjectState(financiamento, EntityState.Modified);
                }

                contexto.SaveChanges();
            }
        }

        public void DeleteFinanciamento(Financiamento objFinanciamento)
        {
            Financiamento financiamento = new Financiamento();
            financiamento.financiamentoUID = objFinanciamento.financiamentoUID;
            financiamento.projetoUID = objFinanciamento.Projeto.projetoUID;
            financiamento.financiador = objFinanciamento.financiador;
            financiamento.valor = objFinanciamento.valor;

            var contexto = Contexto.GetContextInstance();
            contexto.Financiamento.Attach(financiamento);
            contexto.Financiamento.DeleteObject(financiamento);            
            contexto.SaveChanges();
        }

        public void DeleteInteracao(InteracaoProjetoInstituicao objInteracao)
        {
            InteracaoProjetoInstituicao interacao = new InteracaoProjetoInstituicao();
            interacao.interacaoProjetoInstituicaoUID = objInteracao.interacaoProjetoInstituicaoUID;
            interacao.projetoUID = objInteracao.projetoUID;
            interacao.tipoInstituicao = objInteracao.tipoInstituicao;

            var contexto = Contexto.GetContextInstance();
            contexto.InteracaoProjetoInstituicao.Attach(interacao);
            contexto.InteracaoProjetoInstituicao.DeleteObject(interacao);
            contexto.SaveChanges();
        }

        public void DeleteConvenio(Convenio objConvenio)
        {
            Convenio convenio = new Convenio();
            convenio.convenioUID = objConvenio.convenioUID;
            convenio.projetoUID = objConvenio.Projeto.projetoUID;
            convenio.convenio1 = objConvenio.convenio1;

            var contexto = Contexto.GetContextInstance();
            contexto.Convenio.Attach(convenio);
            contexto.Convenio.DeleteObject(convenio);            
            contexto.SaveChanges();

            contexto.Refresh(RefreshMode.StoreWins, contexto.Convenio);
        }

        
        public IList<vwProjetosCoordenador> GetProjetosCoordenador(String titulo, String coordenador)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                IList<vwProjetosCoordenador> projetosCoordenador = (
                        from p in contexto.vwProjetosCoordenador
                        where p.titulo.Contains(titulo) && p.coordenador.Contains(coordenador)
                        select p
                    ).ToList<vwProjetosCoordenador>();
                    
                    //contexto.vwProjetosCoordenador.ToList<vwProjetosCoordenador>();
                
                return projetosCoordenador;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public Projeto GetProjeto(long projetoUID)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                Projeto projeto = contexto.Projeto.Where(p => p.projetoUID == projetoUID).First<Projeto>();                
                return projeto;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public void DeleteProjeto(Projeto objProjeto)
        {
            Projeto projeto = new Projeto();
            projeto.projetoUID = objProjeto.projetoUID;
            projeto.registroCap = objProjeto.registroCap;
            projeto.anoCap = objProjeto.anoCap;
            projeto.titulo = objProjeto.titulo;
            projeto.inicio = objProjeto.inicio;
            projeto.fim = objProjeto.fim;

            var contexto = Contexto.GetContextInstance();
            contexto.Projeto.Attach(projeto);
            contexto.Projeto.DeleteObject(projeto);
            contexto.SaveChanges();
        }

        public Projeto GetUltmoProjeto()
        {
            var contexto = Contexto.GetContextInstance();

            Projeto ultimoProjeto = contexto.Projeto.OrderByDescending(u => u.projetoUID).Take(1).FirstOrDefault<Projeto>();

            return ultimoProjeto;
        }

        public List<AreaConhecimento> GetAreasConhecimento()
        {
            var contexto = Contexto.GetContextInstance();

            List<AreaConhecimento> areasConhecimento = contexto.AreaConhecimento.ToList<AreaConhecimento>();

            return areasConhecimento;
        }

        public List<vwProjetosMembro> GetProjetosMembro(String cpf)
        {
            var contexto = Contexto.GetContextInstance();

            List<vwProjetosMembro> projetosMembro = (
                    from c in contexto.vwProjetosMembro
                    where c.cpf == cpf
                    select c).ToList<vwProjetosMembro>();

            return projetosMembro;
        }

        public void SaveInteracaoProjetoInstituicao(InteracaoProjetoInstituicao objInteracao, int operacao)
        {
            if (objInteracao != null)
            {
                var contexto = Contexto.GetContextInstance();
                InteracaoProjetoInstituicao interacao = new InteracaoProjetoInstituicao();
                interacao.interacaoProjetoInstituicaoUID = objInteracao.interacaoProjetoInstituicaoUID;
                interacao.projetoUID = objInteracao.projetoUID;
                interacao.tipoInstituicao = objInteracao.tipoInstituicao;
                
                if (operacao == 1)
                {
                    contexto.AddObject("InteracaoProjetoInstituicao", interacao);
                }
                else
                {
                    contexto.InteracaoProjetoInstituicao.Attach(interacao);
                    contexto.ObjectStateManager.ChangeObjectState(interacao, EntityState.Modified);
                }

                contexto.SaveChanges();
            }
        }

        public void SaveArquivoProjeto(ArquivoProjeto objarquivo)
        {
            if (objarquivo != null)
            {
                var contexto = Contexto.GetContextInstance();
                ArquivoProjeto arquivo = new ArquivoProjeto();
                arquivo.arquivoProjetoUID = objarquivo.arquivoProjetoUID;
                arquivo.nomeArquivo = objarquivo.nomeArquivo;
                arquivo.projetoUID = objarquivo.projetoUID;
                arquivo.descricao = objarquivo.descricao;

                contexto.AddObject("ArquivoProjeto", arquivo);                

                contexto.SaveChanges();
            }
        }

        public List<ArquivoProjeto> GetArquivosProjeto(int projetoUID)
        {
            var contexto = Contexto.GetContextInstance();

            List<ArquivoProjeto> arquivosProjeto = (
                    from c in contexto.ArquivoProjeto
                    where c.projetoUID == projetoUID
                    select c).ToList<ArquivoProjeto>();

            return arquivosProjeto;
        }

        public ArquivoProjeto GetArquivoProjeto(int arquivoProjetoUID)
        {
            var contexto = Contexto.GetContextInstance();

            ArquivoProjeto arquivoProjeto = (
                    from c in contexto.ArquivoProjeto
                    where c.arquivoProjetoUID == arquivoProjetoUID
                    select c).FirstOrDefault<ArquivoProjeto>();

            return arquivoProjeto;
        }



        public void DeleteArquivoProjeto(ArquivoProjeto objArquivoProjeto)
        {
            ArquivoProjeto arquivoProjeto = new ArquivoProjeto();
            arquivoProjeto.arquivoProjetoUID = objArquivoProjeto.arquivoProjetoUID;
            arquivoProjeto.projetoUID = objArquivoProjeto.projetoUID;
            arquivoProjeto.nomeArquivo = objArquivoProjeto.nomeArquivo;
            arquivoProjeto.descricao = objArquivoProjeto.descricao;
            
            var contexto = Contexto.GetContextInstance();
            contexto.ArquivoProjeto.Attach(arquivoProjeto);
            contexto.ArquivoProjeto.DeleteObject(arquivoProjeto);
            contexto.SaveChanges();

            contexto.Refresh(RefreshMode.StoreWins, contexto.ArquivoProjeto);
        }


    }
}
