﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ufmt.ssbic.DataAccess
{
    public class BolsistaFolhaSuplementarDAL
    {
        public void Salvar(BolsistaFolhaSuplementar bfs)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();
                contexto.BolsistaFolhaSuplementar.AddObject(bfs);
                contexto.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public List<BolsistaFolhaSuplementar> GetListBolsistaFolhaSuplementar(int folhaSuplementarUID)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();
                return contexto.BolsistaFolhaSuplementar.Where(b => b.folhaSuplementarUID == folhaSuplementarUID).ToList();
            }
            catch
            {
                throw;
            }
        }

        public void Excluir(int bolsistaFolhaSuplementarUID)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();
                var bolsista = (from b in contexto.BolsistaFolhaSuplementar
                                where b.bolsistaSuplementarUID == bolsistaFolhaSuplementarUID
                                select b).FirstOrDefault();
                contexto.DeleteObject(bolsista);
                contexto.SaveChanges();
            }
            catch
            {
                throw;
            }
        }
    }
}
