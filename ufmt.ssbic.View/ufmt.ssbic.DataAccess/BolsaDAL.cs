﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace ufmt.ssbic.DataAccess
{
    public class BolsaDAL
    {
        public List<Bolsa> GetRegistros(int? proreitoriaUID)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                List<Bolsa> registros = (from c in contexto.Bolsa where c.proReitoriaUID == proreitoriaUID select c).ToList<Bolsa>();
                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }


        public List<vwBolsaProcessoSeletivo> GetBolsaProcessoSeletivo(int? processoSeletivoUID)
        {
            var contexto = Contexto.GetContextInstance();
            try
            {
                List<vwBolsaProcessoSeletivo> registros = (from c in contexto.vwBolsaProcessoSeletivo where c.processoSeletivoUID == processoSeletivoUID select c).ToList<vwBolsaProcessoSeletivo>();

                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }


        public FinanciadorInstrumentoSelecao GetTotalBolsaRestante(int financiadorInstrumentoSelecaoUID)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                return (from c in contexto.FinanciadorInstrumentoSelecao
                        where c.financiadorInstrumentoSelecaoUID == financiadorInstrumentoSelecaoUID
                        select c).FirstOrDefault<FinanciadorInstrumentoSelecao>();

            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar o registro.", exception);
            }
        }

        //public List<FinanciadorInstrumentoSelecao> GetBolsaProcessoSeletivo(int? instrumentoSelecaoUID)
        //{
        //    var contexto = Contexto.GetContextInstance();
        //    try
        //    {
        //        List<FinanciadorInstrumentoSelecao> registros = (from c in contexto.FinanciadorInstrumentoSelecao where c.instrumentoSelecaoUID == instrumentoSelecaoUID select c).ToList<FinanciadorInstrumentoSelecao>();

        //        return registros;
        //    }
        //    catch (Exception exception)
        //    {
        //        throw new Exception("Não foi possível recuperar os registros.", exception);
        //    }
        //}


        public Bolsa GetBolsa(int bolsaUID)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                return (from c in contexto.Bolsa where c.bolsaUID == bolsaUID select c).FirstOrDefault<Bolsa>();

            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar o registro.", exception);
            }
        }

        public List<Bolsa> FindRegistros(int? proreitoriaUID, string criterio)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                List<Bolsa> registros = (from c in contexto.Bolsa where c.proReitoriaUID == proreitoriaUID && c.descricao.Contains(criterio) select c).ToList<Bolsa>();
                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public bool Salvar(Bolsa bolsa, int operacao)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {

                var novoRegistro = new Bolsa
                {
                    bolsaUID = bolsa.bolsaUID,
                    descricao = bolsa.descricao,
                    detalhe = bolsa.detalhe,
                    proReitoriaUID = bolsa.proReitoriaUID,
                    dataRegistro = DateTime.Now
                };

                if (operacao == 1)
                {
                    contexto.AddObject("Bolsa", novoRegistro);
                }
                else
                {
                    contexto.Bolsa.Attach(novoRegistro);
                    contexto.ObjectStateManager.ChangeObjectState(novoRegistro, EntityState.Modified);
                }

                contexto.SaveChanges();
                return true;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);

            }
        }

        public bool Excluir(int bolsaUID)
        {
            try {
                var contexto = Contexto.GetContextInstance();
                contexto.DeleteObject((from c in contexto.Bolsa where c.bolsaUID == bolsaUID select c).SingleOrDefault());
                contexto.SaveChanges();
                return true;

            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);

            }
        }

        public Proreitoria GetPrograma(string programa)
        {
            var contexto = Contexto.GetContextInstance();
            try
            {
                Proreitoria registros = (from c in contexto.Proreitoria where c.descricao.Contains(programa) select c).FirstOrDefault<Proreitoria>();
                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);

            }

        }


    }
}
