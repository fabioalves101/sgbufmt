﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace ufmt.ssbic.DataAccess
{
    public class AlunoDAL
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nome"></param>
        /// <param name="matricula"></param>
        /// <returns></returns>
        public List<vwAluno> FindRegistros(string nome, decimal matricula)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                List<vwAluno> registros = null;
                if(!String.IsNullOrEmpty(nome))
                   registros = (from c in contexto.vwAluno where c.Nome.Contains(nome) select c).ToList<vwAluno>();
                else
                    registros = (from c in contexto.vwAluno where c.Matricula == matricula select c).ToList<vwAluno>();

                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }


        public vwAlunoSiga GetAluno(decimal matricula)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                vwAlunoSiga registro = (from c in contexto.vwAlunoSiga where c.Matricula == matricula select c).FirstOrDefault();

                return registro;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public vwAluno GetAlunoMatriculado(decimal matricula)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                vwAluno registro = (from c in contexto.vwAluno where c.Matricula == matricula select c).FirstOrDefault();

                return registro;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }


        public vwAlunoSiga GetAlunoPorCPF(string cpf)
        {
            var contexto = Contexto.GetContextInstance();
            return contexto.vwAlunoSiga.Where(a => a.CPF.Equals(cpf)).FirstOrDefault();
        }
    }
}
