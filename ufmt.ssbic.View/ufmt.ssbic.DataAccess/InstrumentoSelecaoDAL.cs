﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.Objects;

namespace ufmt.ssbic.DataAccess
{
    public class InstrumentoSelecaoDAL
    {
        public List<vwInstrumentoSelecao> GetRegistros(int? proreitoriaUID)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                //var registros = from i in contexto.InstrumentoSelecao join t in contexto.TipoInstrumentoSelecao 
                //                on i.tipoInstrumentoSelecaoUID equals t.tipoInstrumentoSelecaoUID
                //                    select new { 
                //                       InstrumentoSelecaoUID = i.instrumentoSelecaoUID, 
                //                       InstrumentoSelecao = i.descricao, 
                //                       ProReitoriaUID = i.proreitoriaUID, 
                //                       Tipo = t.descricao 
                //                    };


                List<vwInstrumentoSelecao> registros = (from i in contexto.vwInstrumentoSelecao where i.proreitoriaUID == proreitoriaUID select i).ToList<vwInstrumentoSelecao>();

                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public List<TipoInstrumentoSelecao> GetTipoInstrumentoSelecao()
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                List<TipoInstrumentoSelecao> registros = (from i in contexto.TipoInstrumentoSelecao select i).ToList<TipoInstrumentoSelecao>();

                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public vwInstrumentoSelecao GetInstrumentoSelecao(int instrumentoSelecaoUID)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                return (from c in contexto.vwInstrumentoSelecao where c.instrumentoSelecaoUID == instrumentoSelecaoUID select c).FirstOrDefault<vwInstrumentoSelecao>();

            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar o registro.", exception);
            }
        }

        public InstrumentoSelecao GetInstrumento(int instrumentoSelecaoUID)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                return (from c in contexto.InstrumentoSelecao where c.instrumentoSelecaoUID == instrumentoSelecaoUID select c).FirstOrDefault();

            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar o registro.", exception);
            }
        }

        public InstrumentoSelecao GetObjInstrumentoSelecao(int instrumentoSelecaoUID)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                return (from c in contexto.InstrumentoSelecao where c.instrumentoSelecaoUID == instrumentoSelecaoUID select c).FirstOrDefault<InstrumentoSelecao>();

            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar o registro.", exception);
            }
        }


        public List<vwInstrumentoSelecao> FindRegistros(int? proreitoriaUID, string criterio)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                List<vwInstrumentoSelecao> registros = (from c in contexto.vwInstrumentoSelecao where c.proreitoriaUID == proreitoriaUID && c.descricao.Contains(criterio) select c).ToList<vwInstrumentoSelecao>();
                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public int Salvar(InstrumentoSelecao instrumentoSelecao, int operacao)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {

                var novoRegistro = new InstrumentoSelecao
                {
                    instrumentoSelecaoUID = instrumentoSelecao.instrumentoSelecaoUID,
                    tipoInstrumentoSelecaoUID = instrumentoSelecao.tipoInstrumentoSelecaoUID,
                    descricao = instrumentoSelecao.descricao,
                    proreitoriaUID = instrumentoSelecao.proreitoriaUID,
                    bolsaUID = instrumentoSelecao.bolsaUID,
                    situacao = instrumentoSelecao.situacao

                };

                if (operacao == 1)
                {
                    contexto.AddObject("InstrumentoSelecao", novoRegistro);
                }
                else
                {
                    contexto.InstrumentoSelecao.Attach(novoRegistro);
                    contexto.ObjectStateManager.ChangeObjectState(novoRegistro, EntityState.Modified);
                }

                contexto.SaveChanges();

                int instanceid = 0;
                var iselecaoUID = (from c in contexto.InstrumentoSelecao orderby c.instrumentoSelecaoUID
                                       descending select new { c.instrumentoSelecaoUID }).Take(1);
                foreach (var isUID in iselecaoUID)
                {
                    instanceid = isUID.instrumentoSelecaoUID;
                }


                return instanceid;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);

            }
        }

        public bool Excluir(int instrumentoSelecaoUID)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();
                contexto.DeleteObject((from c in contexto.InstrumentoSelecao where c.instrumentoSelecaoUID == instrumentoSelecaoUID select c).SingleOrDefault());
                contexto.SaveChanges();
                return true;

            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível excluir os registros.", exception);

            }
        }

        public List<vwInstrumentoSelecaoBolsa> FindRegistroISB(int? proreitoriaUID, String criterio, long? cursoUID)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();
                List<vwInstrumentoSelecaoBolsa> registros =
                    (from c in contexto.vwInstrumentoSelecaoBolsa
                     where (!proreitoriaUID.HasValue || c.proreitoriaUID == proreitoriaUID) 
                            && c.descricao.Contains(criterio)
                            && (!cursoUID.HasValue || c.cursoUID == cursoUID) 

                     select c).ToList<vwInstrumentoSelecaoBolsa>();
                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível selecionar os registros.", exception);
            }
        }


        public List<vwInstrumentoSelecaoBolsa> GetInstrumentosPorResponsavel(int? proreitoriaUID, long? cursoUID, InstrumentoSelecaoResponsavel responsavelIS)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();
                List<vwInstrumentoSelecaoBolsa> registros = null;
                if (proreitoriaUID.HasValue)
                {
                    registros =
                        (from c in contexto.vwInstrumentoSelecaoBolsa
                         where c.proreitoriaUID == proreitoriaUID
                                && (!cursoUID.HasValue || c.cursoUID == cursoUID)
                                || c.responsavelUID == responsavelIS.usuarioUID
                                && (!cursoUID.HasValue || c.cursoUID == cursoUID)                                
                         select c).ToList<vwInstrumentoSelecaoBolsa>();
                }
                else
                {
                    registros =
                        (from c in contexto.vwInstrumentoSelecaoBolsa
                         where (!cursoUID.HasValue || c.cursoUID == cursoUID)
                               && c.responsavelUID == responsavelIS.usuarioUID
                         select c).ToList<vwInstrumentoSelecaoBolsa>();
                }
                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível selecionar os registros.", exception);
            }
        }

        public List<vwInstrumentoSelecaoBolsa> GetInstrumentosPorResponsavelPorCriterio(int? proreitoriaUID, String criterio, long? cursoUID, InstrumentoSelecaoResponsavel responsavelIS)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();
                List<vwInstrumentoSelecaoBolsa> registros = null;
                if (proreitoriaUID.HasValue)
                {
                    registros =
                        (from c in contexto.vwInstrumentoSelecaoBolsa
                         where c.proreitoriaUID == proreitoriaUID
                                && c.descricao.Contains(criterio)
                                && (!cursoUID.HasValue || c.cursoUID == cursoUID)
                                || c.responsavelUID == responsavelIS.usuarioUID
                                && (!cursoUID.HasValue || c.cursoUID == cursoUID)
                                && c.descricao.Contains(criterio)
                         select c).ToList<vwInstrumentoSelecaoBolsa>();
                }
                else
                {
                    registros =
                        (from c in contexto.vwInstrumentoSelecaoBolsa
                         where (!cursoUID.HasValue || c.cursoUID == cursoUID)
                               && c.descricao.Contains(criterio)
                               && c.responsavelUID == responsavelIS.usuarioUID
                         select c).ToList<vwInstrumentoSelecaoBolsa>();
                }
                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível selecionar os registros.", exception);
            }
        }

        public List<vwInstrumentoSelecaoBolsa> GetInstrumentosProcessosBolsas(int? proreitoriaUID, long? cursoUID)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();
                List<vwInstrumentoSelecaoBolsa> registros =
                    (from c in contexto.vwInstrumentoSelecaoBolsa
                     where (!proreitoriaUID.HasValue || c.proreitoriaUID == proreitoriaUID)
                            && (!cursoUID.HasValue || c.cursoUID == cursoUID) 
                     select c).ToList<vwInstrumentoSelecaoBolsa>();
                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível selecionar os registros.", exception);
            }
        }

        public bool SaveInstrumentoSelecaoResponsavel(InstrumentoSelecaoResponsavel ISResponsavel)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();
                var novoRegistro = new InstrumentoSelecaoResponsavel
                {
                    instrumentoSelecaoUID = ISResponsavel.instrumentoSelecaoUID,
                    usuarioUID = ISResponsavel.usuarioUID,
                    cursoUID = ISResponsavel.cursoUID
                };

                contexto.AddObject("InstrumentoSelecaoResponsavel", novoRegistro);

                contexto.SaveChanges();
                return true;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível salvar o registro. ", exception);
            }
        }

        public List<vwInstrumentoSelecaoResponsavel> GetResponsaveisPorInstrumentoSelecao(int instrumentoSelecaoUID)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();
                List<vwInstrumentoSelecaoResponsavel> registros =
                    (from c in contexto.vwInstrumentoSelecaoResponsavel
                     where c.instrumentoSelecaoUID == instrumentoSelecaoUID
                     select c).ToList<vwInstrumentoSelecaoResponsavel>();
                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível selecionar os registros.", exception);
            }
        }

        public InstrumentoSelecaoResponsavel GetResponsavelPorInstrumentoSelecao(long usuarioUID)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();
                InstrumentoSelecaoResponsavel registro =
                    (from c in contexto.InstrumentoSelecaoResponsavel
                     where c.usuarioUID == usuarioUID
                     select c).FirstOrDefault<InstrumentoSelecaoResponsavel>();
                return registro;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível selecionar os registros.", exception);
            }
        }

        public bool ExcluirReponsavel(int id)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();
                contexto.DeleteObject((from c in contexto.InstrumentoSelecaoResponsavel where c.id == id select c).SingleOrDefault());
                contexto.SaveChanges();
                return true;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível excluir os registros.", exception);

            }
        }

        public void SaveArquivoInstrumentoSelecao(ArquivoInstrumentoSelecao objarquivo)
        {
            if (objarquivo != null)
            {
                var contexto = Contexto.GetContextInstance();
                ArquivoInstrumentoSelecao arquivo = new ArquivoInstrumentoSelecao();
                arquivo.arquivoInstrumentoSelecaoUID = objarquivo.arquivoInstrumentoSelecaoUID;
                arquivo.nomeArquivo = objarquivo.nomeArquivo;
                arquivo.instrumentoSelecaoUID = objarquivo.instrumentoSelecaoUID;
                arquivo.descricao = objarquivo.descricao;

                contexto.AddObject("ArquivoInstrumentoSelecao", arquivo);

                contexto.SaveChanges();
            }
        }



        public ArquivoInstrumentoSelecao GetArquivoInstrumentoSelecao(int instrumentoSelecaoUID)
        {
            var contexto = Contexto.GetContextInstance();

            ArquivoInstrumentoSelecao arquivoInstrumentoSelecao = (
                    from c in contexto.ArquivoInstrumentoSelecao
                    where c.instrumentoSelecaoUID == instrumentoSelecaoUID
                    select c).FirstOrDefault<ArquivoInstrumentoSelecao>();

            return arquivoInstrumentoSelecao;
        }



        public void DeleteArquivoIs(ArquivoInstrumentoSelecao objArquivoIS)
        {
            ArquivoInstrumentoSelecao arquivoProjeto = new ArquivoInstrumentoSelecao();
            arquivoProjeto.arquivoInstrumentoSelecaoUID = objArquivoIS.arquivoInstrumentoSelecaoUID;
            arquivoProjeto.instrumentoSelecaoUID = objArquivoIS.instrumentoSelecaoUID;
            arquivoProjeto.nomeArquivo = objArquivoIS.nomeArquivo;
            arquivoProjeto.descricao = objArquivoIS.descricao;

            var contexto = Contexto.GetContextInstance();
            contexto.ArquivoInstrumentoSelecao.Attach(arquivoProjeto);
            contexto.ArquivoInstrumentoSelecao.DeleteObject(arquivoProjeto);
            contexto.SaveChanges();

            contexto.Refresh(RefreshMode.StoreWins, contexto.ArquivoInstrumentoSelecao);
        }


        public List<InstrumentoSelecao> GetListInstrumentoSelecaoPorPrograma(int programaUID)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();

                return (from c in contexto.InstrumentoSelecao
                 where c.proreitoriaUID == programaUID
                 select c).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
