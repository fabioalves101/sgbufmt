﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ufmt.ssbic.DataAccess
{
    public class AuxiliadoAlunoSiga
    {
        private int auxiliadoUID;

        public int AuxiliadoUID
        {
            get { return auxiliadoUID; }
            set { auxiliadoUID = value; }
        }
        private Auxiliado auxiliado;

        public Auxiliado Auxiliado
        {
            get { return auxiliado; }
            set { auxiliado = value; }
        }
        private List<AuxilioAluno> listAuxilioAluno;

        public List<AuxilioAluno> ListAuxilioAluno
        {
            get { return listAuxilioAluno; }
            set { listAuxilioAluno = value; }
        }
        private AuxilioFolhaPagamento auxilioFolhaPagamento;

        public AuxilioFolhaPagamento AuxilioFolhaPagamento
        {
            get { return auxilioFolhaPagamento; }
            set { auxilioFolhaPagamento = value; }
        }
        private vwAlunoSiga aluno;

        public vwAlunoSiga Aluno
        {
            get { return aluno; }
            set { aluno = value; }
        }


    }
}
