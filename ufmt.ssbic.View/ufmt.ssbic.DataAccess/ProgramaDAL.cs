﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ufmt.ssbic.DataAccess
{
    public class ProgramaDAL
    {
        public Proreitoria GetPrograma(int programaUID)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();
                
                return (from c in contexto.Proreitoria 
                        where c.proreitoriaUID == programaUID 
                        select c).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Proreitoria> GetListPrograma()
        {
            try
            {
                return Contexto.GetContextInstance().Proreitoria.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
