﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ufmt.ssbic.DataAccess
{
    public class BolsistaAuxilioFolhaPagamentoDAL
    {
        public void Salvar(BolsistaAuxilioFolhaPagamento bolsistaAuxilioFolhaPagamento)
        {
            var contexto = Contexto.GetContextInstance();
            contexto.BolsistaAuxilioFolhaPagamento.AddObject(bolsistaAuxilioFolhaPagamento);
            contexto.SaveChanges();
        }

        public void Limpar()
        {
            var contexto = Contexto.GetContextInstance();
            contexto.ExecuteStoreCommand("delete from BolsistaAuxilioFolhaPagamento");
            contexto.SaveChanges();
        }


        public List<BolsistaAuxilioFolhaPagamento> GetBolsistasAuxilioFolhaPagamento(int folhaPagamentoUID)
        {
            var contexto = Contexto.GetContextInstance();
            return contexto.BolsistaAuxilioFolhaPagamento.Where(b => b.folhaPagamentoUID == folhaPagamentoUID).ToList();
        }
    }
}
