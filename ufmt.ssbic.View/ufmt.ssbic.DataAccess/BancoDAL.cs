﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace ufmt.ssbic.DataAccess
{
    public class BancoDAL
    {
        public List<vwBanco> GetBanco()
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                List<vwBanco> registros = (from c in contexto.vwBanco select c).ToList<vwBanco>();
                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }



        public vwBanco GetBanco(int bancoUID)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                return (from c in contexto.vwBanco where c.bancoUID == bancoUID select c).FirstOrDefault<vwBanco>();

            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar o registro.", exception);
            }
        }

        public Banco GetBancoPorId(int bancoUID)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                return (from c in contexto.Banco where c.bancoUID == bancoUID select c).FirstOrDefault();

            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar o registro.", exception);
            }
        }


        public Banco GetBanco(string codigoFebraban)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                int codigo = int.Parse(codigoFebraban);
                return (from c in contexto.Banco where c.numero == codigo select c).FirstOrDefault();
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar o registro.", exception);
            }
        }
    }
}
