﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ufmt.ssbic.DataAccess
{
    public class AuxilioAlunoDAL
    {

        public void Excluir(int auxiliadoUID)
        {
            var contexto = Contexto.GetContextInstance();
            contexto.ExecuteStoreCommand("delete from AuxilioAluno where auxiliadoUID = " + auxiliadoUID);
            contexto.SaveChanges();
        }
    }
}
