﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Transactions;
using System.Data.Objects;


namespace ufmt.ssbic.DataAccess
{
    public class ResponsavelDAL
    {
        public Responsavel GetResponsavel(int responsavelUID)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                return (from c in contexto.Responsavel where c.responsavelUID == responsavelUID select c).FirstOrDefault<Responsavel>();

            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar o registro.", exception);
            }
        }

        public List<Responsavel> FindRegistros(string matricula, string criterio)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                List<Responsavel> registros = (from c in contexto.Responsavel where c.matricula == matricula && c.email.Contains(criterio) select c).ToList<Responsavel>();
                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public bool Salvar(Responsavel responsavel)
        {
            var contextoBolsita = Contexto.GetContextInstance();

            try
            {

                contextoBolsita.AddToResponsavel(responsavel);
                contextoBolsita.SaveChanges(SaveOptions.DetectChangesBeforeSave);
                return true;

            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);

            }

        }

        public bool Atualizar(Responsavel responsavel)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                contexto.Responsavel.Attach(responsavel);
                contexto.ObjectStateManager.ChangeObjectState(responsavel, EntityState.Modified);

                contexto.SaveChanges();

                return true;

            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);

            }

        }
        
        public bool Excluir(int responsavelUID)
        {

                var contexto = Contexto.GetContextInstance();

                try
                {
                    contexto.DeleteObject((from c in contexto.Responsavel where c.responsavelUID == responsavelUID select c).SingleOrDefault());
                    contexto.SaveChanges();
                    return true;
                }

                catch (Exception exception)
                {
                    throw new Exception("Não foi possível excluir o registro.", exception);

                }


        }


    }
}
