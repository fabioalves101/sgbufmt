﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace ufmt.ssbic.DataAccess
{
    public class MonitorDAL
    {
        idbufmtEntities contexto;
        public MonitorDAL()
        {
            this.contexto = Contexto.GetContextInstance();
        }

        public int Save(Monitor objMonitor)
        {
            try
            {
                var monitor = new Monitor
                {
                    cursoUID = objMonitor.cursoUID,
                    disciplinaUID = objMonitor.disciplinaUID,
                    coordenadorUID = objMonitor.coordenadorUID,
                    orientadorUID = objMonitor.orientadorUID,
                    bolsistaUID = objMonitor.bolsistaUID,
                    remunerada = objMonitor.remunerada,
                    periodoAtuacao = objMonitor.periodoAtuacao,
                    ativo = false,
                    processoSeletivoUID = objMonitor.processoSeletivoUID,
                    obs = objMonitor.obs
                };

                this.contexto.AddObject("Monitor", monitor);
                this.contexto.SaveChanges();

                return monitor.monitorUID;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<vwMonitorAluno> GetVwMonitores(String rga, int processoSeletivoUID, long? cursoUID)
        {
            try
            {
                List<vwMonitorAluno> monitores = null;
                if (cursoUID == 0)
                {
                    monitores = (from c in contexto.vwMonitorAluno
                                 where
                                     c.processoSeletivoUID == processoSeletivoUID &&
                                     c.Matricula.Contains(rga)
                                 select c).ToList<vwMonitorAluno>();
                }
                else
                {
                    monitores = (from c in contexto.vwMonitorAluno
                                 where
                                     c.processoSeletivoUID == processoSeletivoUID &&
                                     c.cursoUID == cursoUID &&
                                     c.Matricula.Contains(rga)
                                 select c).ToList<vwMonitorAluno>();
                }
                return monitores;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<vwMonitorAluno> GetVwMonitores(bool status, int processoSeletivoUID, long? cursoUID)
        {
            try
            {
                List<vwMonitorAluno> monitores = null;
                if (cursoUID == 0)
                {
                    monitores = (from c in contexto.vwMonitorAluno
                                 where
                                     c.processoSeletivoUID == processoSeletivoUID &&
                                     c.status == status
                                 select c).ToList<vwMonitorAluno>();
                }
                else
                {
                    monitores = (from c in contexto.vwMonitorAluno
                                 where
                                     c.processoSeletivoUID == processoSeletivoUID &&
                                     c.cursoUID == cursoUID &&
                                     c.status == status
                                 select c).ToList<vwMonitorAluno>();
                }
                return monitores;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<vwMonitorAluno> GetVwMonitores(bool status, String rga, int processoSeletivoUID, long? cursoUID)
        {
            try
            {
                List<vwMonitorAluno> monitores = null;
                if (cursoUID == 0)
                {
                    monitores = (from c in contexto.vwMonitorAluno
                                 where
                                     c.processoSeletivoUID == processoSeletivoUID &&
                                     c.status == status &&
                                     c.Matricula.Contains(rga)
                                 select c).ToList<vwMonitorAluno>();
                }
                else
                {
                    monitores = (from c in contexto.vwMonitorAluno
                                 where
                                     c.processoSeletivoUID == processoSeletivoUID &&
                                     c.cursoUID == cursoUID &&
                                     c.status == status &&
                                     c.Matricula.Contains(rga)
                                 select c).ToList<vwMonitorAluno>();
                }
                return monitores;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<vwMonitorAluno> GetVwMonitoresPorRemuneracao(bool remunerado, String rga, int processoSeletivoUID, 
            long? cursoUID, bool ativo, string periodoAtuacao)
        {
            try
            {
                List<vwMonitorAluno> monitores = null;
                if (cursoUID == 0)
                {
                    if (String.IsNullOrEmpty(rga))
                    {
                        if (periodoAtuacao.Equals("0"))
                        {
                            monitores = (from c in contexto.vwMonitorAluno
                                         where
                                             c.processoSeletivoUID == processoSeletivoUID &&
                                             c.remunerada == remunerado &&
                                             c.ativo == ativo && 
                                             c.status == true
                                         select c).ToList<vwMonitorAluno>();
                        }
                        else
                        {
                            monitores = (from c in contexto.vwMonitorAluno
                                         where
                                             c.processoSeletivoUID == processoSeletivoUID &&
                                             c.remunerada == remunerado &&
                                             c.ativo == ativo &&
                                             c.periodoAtuacao == periodoAtuacao &&
                                             c.status == true
                                         select c).ToList<vwMonitorAluno>();
                        }
                    }
                    else
                    {
                        if (periodoAtuacao.Equals("0"))
                        {
                            monitores = (from c in contexto.vwMonitorAluno
                                         where
                                             c.processoSeletivoUID == processoSeletivoUID &&
                                             c.remunerada == remunerado &&
                                             c.ativo == ativo &&
                                             c.Matricula.Contains(rga) &&
                                             c.status == true
                                         select c).ToList<vwMonitorAluno>();
                        }
                        else
                        {
                            monitores = (from c in contexto.vwMonitorAluno
                                         where
                                             c.processoSeletivoUID == processoSeletivoUID &&
                                             c.remunerada == remunerado &&
                                             c.ativo == ativo &&
                                             c.Matricula.Contains(rga) &&
                                             c.periodoAtuacao == periodoAtuacao &&
                                             c.status == true
                                         select c).ToList<vwMonitorAluno>();
                        }
                    }
                }
                else
                {
                    if (periodoAtuacao.Equals("0"))
                    {
                        monitores = (from c in contexto.vwMonitorAluno
                                     where
                                         c.processoSeletivoUID == processoSeletivoUID &&
                                         c.cursoUID == cursoUID &&
                                         c.remunerada == remunerado &&
                                         c.ativo == ativo &&
                                         c.Matricula.Contains(rga) &&
                                             c.status == true
                                     select c).ToList<vwMonitorAluno>();
                    }
                    else
                    {
                        monitores = (from c in contexto.vwMonitorAluno
                                     where
                                         c.processoSeletivoUID == processoSeletivoUID &&
                                         c.cursoUID == cursoUID &&
                                         c.remunerada == remunerado &&
                                         c.ativo == ativo &&
                                         c.Matricula.Contains(rga) &&
                                         c.periodoAtuacao == periodoAtuacao &&
                                             c.status == true
                                     select c).ToList<vwMonitorAluno>();
                    }
                }
                return monitores;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<vwMonitorAluno> GetVwMonitores(int processoSeletivoUID, long? cursoUID)
        {
            try
            {
                List<vwMonitorAluno> monitores = null;
                if (cursoUID == 0)
                {
                    monitores = (from c in contexto.vwMonitorAluno
                                 where
                                     c.processoSeletivoUID == processoSeletivoUID
                                 select c).ToList<vwMonitorAluno>();
                }
                else
                {
                    monitores = (from c in contexto.vwMonitorAluno
                                 where
                                     c.processoSeletivoUID == processoSeletivoUID &&
                                     c.cursoUID == cursoUID
                                 select c).ToList<vwMonitorAluno>();
                }
                return monitores;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Monitor GetMonitor(int monitorUID)
        {
            try
            {
                return (from c in contexto.Monitor where c.monitorUID == monitorUID select c).FirstOrDefault<Monitor>();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Delete(int monitorUID)
        {
            try
            {
                contexto.DeleteObject((from c in contexto.Monitor where c.monitorUID == monitorUID select c).FirstOrDefault<Monitor>());
                contexto.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int Update(Monitor objMonitor)
        {
            try
            {
                using (idbufmtEntities con = new idbufmtEntities())
                {
                    var monitor = new Monitor
                    {
                        monitorUID = objMonitor.monitorUID,
                        coordenadorUID = objMonitor.coordenadorUID,
                        orientadorUID = objMonitor.orientadorUID,
                        bolsistaUID = objMonitor.bolsistaUID,
                        remunerada = objMonitor.remunerada,
                        periodoAtuacao = objMonitor.periodoAtuacao,
                        ativo = objMonitor.ativo,

                        cursoUID = objMonitor.cursoUID,
                        disciplinaUID = objMonitor.disciplinaUID,
                        inicio = objMonitor.inicio,
                        final = objMonitor.final,
                        processoSeletivoUID = objMonitor.processoSeletivoUID,
                        obs = objMonitor.obs,
                        status = objMonitor.status
                    };

                    con.Monitor.Attach(monitor);
                    con.ObjectStateManager.ChangeObjectState(monitor, EntityState.Modified);
                    con.SaveChanges();

                    return monitor.monitorUID;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Monitor GetMonitorPorBolsista(int bolsistaUID)
        {
            var contexto = Contexto.GetContextInstance();
            try
            {
                Monitor m = (from c in contexto.Monitor where c.bolsistaUID == bolsistaUID select c).FirstOrDefault();
                return m;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Monitor> GetMonitoresPorBolsista(int bolsistaUID)
        {
            var contexto = Contexto.GetContextInstance();
            try
            {
                List<Monitor> list = 
                    (from c in contexto.Monitor 
                     where c.bolsistaUID == bolsistaUID 
                     select c).ToList();
                
                return list;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
