﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace ufmt.ssbic.DataAccess
{
    public class MembroDAL
    {
        public PessoaMembro SaveCoordenador(PessoaMembro objPessoaMembro, int operacao)
        {
            var contexto = Contexto.GetContextInstance();

            PessoaMembro pessoaMembro = new PessoaMembro();
            pessoaMembro.pessoaMembroUID = objPessoaMembro.pessoaMembroUID;
            pessoaMembro.pessoaUID = objPessoaMembro.pessoaUID;
            pessoaMembro.nome = objPessoaMembro.nome;
            pessoaMembro.instituicao = objPessoaMembro.instituicao;
            pessoaMembro.registro = objPessoaMembro.registro;
            pessoaMembro.tipoTitulacaoUID = objPessoaMembro.tipoTitulacaoUID;
            pessoaMembro.cpf = objPessoaMembro.cpf;

            if (operacao == 1)
            {
                contexto.AddObject("PessoaMembro", pessoaMembro);
            }
            else
            {
                contexto.PessoaMembro.Attach(pessoaMembro);                
            }

            contexto.SaveChanges();

            return pessoaMembro;
        }

        public void SaveMembro(Membro objMembro, int operacao)
        {
            var contexto = Contexto.GetContextInstance();

            Membro membro = new Membro();
            membro.membroUID = objMembro.membroUID;
            membro.pessoaMembroUID = objMembro.pessoaMembroUID;
            membro.projetoUID = objMembro.projetoUID;
            membro.funcaoProjetoUID = objMembro.funcaoProjetoUID;
            membro.cargaHoraria = objMembro.cargaHoraria;

            if (operacao == 1)
            {
                contexto.AddObject("Membro", membro);
            }
            else
            {
                contexto.Membro.Attach(membro);
                contexto.ObjectStateManager.ChangeObjectState(membro, EntityState.Modified);
            }

            contexto.SaveChanges();
        }

        public PessoaMembro SavePessoaMembro(PessoaMembro objPessoaMembro, int operacao)
        {
            var contexto = Contexto.GetContextInstance();

            PessoaMembro pessoaMembro = new PessoaMembro();
            pessoaMembro.pessoaMembroUID = objPessoaMembro.pessoaMembroUID;
            pessoaMembro.pessoaUID = objPessoaMembro.pessoaUID;
            pessoaMembro.nome = objPessoaMembro.nome;
            pessoaMembro.instituicao = objPessoaMembro.instituicao;
            pessoaMembro.registro = objPessoaMembro.registro;
            pessoaMembro.tipoTitulacaoUID = objPessoaMembro.tipoTitulacaoUID;
            pessoaMembro.cpf = objPessoaMembro.cpf;

            if (operacao == 1)
            {
                contexto.AddObject("PessoaMembro", pessoaMembro);
            }
            else
            {
                contexto.PessoaMembro.Attach(pessoaMembro);
            }

            contexto.SaveChanges();

            return pessoaMembro;
        }

        public PessoaMembro GetCoordenadorProjeto(long projetoUID)
        {
            var contexto = Contexto.GetContextInstance();


            var coordenador = (
                from p in contexto.PessoaMembro
                from m in contexto.Membro
                where p.pessoaMembroUID == m.pessoaMembroUID
                   && m.projetoUID == projetoUID
                   && m.funcaoProjetoUID == 1
                select p
                ).FirstOrDefault<PessoaMembro>();
                

            return coordenador;
        }

        public PessoaMembro GetPessoaMembro(long pessoaUID)
        {
            var contexto = Contexto.GetContextInstance();

            return contexto.PessoaMembro.Where(p => p.pessoaUID == pessoaUID).FirstOrDefault<PessoaMembro>();
        }

        public PessoaMembro GetPessoaMembro(int pessoaMembroUID)
        {
            var contexto = Contexto.GetContextInstance();

            return contexto.PessoaMembro.Where(p => p.pessoaMembroUID == pessoaMembroUID).FirstOrDefault<PessoaMembro>();
        }

        public PessoaMembro GetPessoaMembro(String cpf)
        {
            var contexto = Contexto.GetContextInstance();

            return contexto.PessoaMembro.Where(p => p.cpf == cpf).FirstOrDefault<PessoaMembro>();
        }

        public List<PessoaMembro> GetPessoaMembroPorNome(String nome)
        {
            var contexto = Contexto.GetContextInstance();

            List<PessoaMembro> pessoasMembro =               
                                (
                                    from p in contexto.PessoaMembro
                                    where p.nome.Contains(nome)
                                    select p
                                ).ToList<PessoaMembro>();

            return pessoasMembro;
        }

        public PessoaMembro GetPessoaMembroByMembro(int membroUID)
        {
            var contexto = Contexto.GetContextInstance();

            PessoaMembro pessoaMembro = (from c in contexto.PessoaMembro
                                         from m in contexto.Membro
                                         where m.membroUID == membroUID && c.pessoaMembroUID == m.pessoaMembroUID
                                         select c).FirstOrDefault<PessoaMembro>();
            return pessoaMembro;
        }

        public Membro GetMembroCoordenador(long projetoUID)
        {
            var contexto = Contexto.GetContextInstance();

           return contexto.Membro.Where(
               m => m.projetoUID == projetoUID && m.funcaoProjetoUID == 1).FirstOrDefault<Membro>();
        }

        public List<PessoaMembro> GetMembrosProjeto(int projetoUID, int funcaoProjetoUID)
        {
            var contexto = Contexto.GetContextInstance();

            List<PessoaMembro> membros = (
                    from p in contexto.PessoaMembro
                    from m in contexto.Membro
                    where p.pessoaMembroUID == m.pessoaMembroUID 
                       && m.projetoUID == projetoUID
                       && m.funcaoProjetoUID == funcaoProjetoUID
                    select p
                ).ToList<PessoaMembro>();

            return membros;
        }

        public List<PessoaMembro> GetMembrosProjeto(int projetoUID)
        {
            var contexto = Contexto.GetContextInstance();

            List<PessoaMembro> membros = (
                    from p in contexto.PessoaMembro
                    from m in contexto.Membro
                    where p.pessoaMembroUID == m.pessoaMembroUID
                       && m.projetoUID == projetoUID
                    select p
                ).ToList<PessoaMembro>();

            return membros;
        }

        public bool DeleteMembro(Membro objMembro)
        {
            try
            {
                Membro membro = new Membro();
                membro.membroUID = objMembro.membroUID;
                membro.pessoaMembroUID = objMembro.pessoaMembroUID;
                membro.projetoUID = objMembro.projetoUID;
                membro.funcaoProjetoUID = objMembro.funcaoProjetoUID;
                membro.cargaHoraria = objMembro.cargaHoraria;

                var contexto = Contexto.GetContextInstance();

                contexto.Membro.Attach(membro);
                contexto.Membro.DeleteObject(membro);
                contexto.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Membro GetMembro(int pessoaMembroUID, int projetoUID)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();

                Membro membro = (
                        from m in contexto.Membro
                        where m.pessoaMembroUID == pessoaMembroUID && m.projetoUID == projetoUID
                        select m
                    ).FirstOrDefault<Membro>();

                return membro;
            }
            catch
            {
                throw;
            }
        }

        public List<PessoaMembro> GetMembros(int funcaoProjetoUID)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();

                List<PessoaMembro> pessoaMembro = (
                        from pm in contexto.PessoaMembro
                        from m in contexto.Membro
                        where pm.pessoaMembroUID == m.pessoaMembroUID
                        && m.funcaoProjetoUID == funcaoProjetoUID
                        select pm
                    ).ToList<PessoaMembro>();

                return pessoaMembro;
            }
            catch 
            {
                throw;
            }
        }


    }
}
