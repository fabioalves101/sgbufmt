﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ufmt.ssbic.DataAccess
{
    public static class Contexto
    {
        private static idbufmtEntities contexto;
        public static idbufmtEntities GetContextInstance()
        {
            contexto = new idbufmtEntities();
            return contexto;
        }

    }
}
