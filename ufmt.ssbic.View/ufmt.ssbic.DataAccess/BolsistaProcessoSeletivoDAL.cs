﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Transactions;
using System.Data.Objects;


namespace ufmt.ssbic.DataAccess
{
    public class BolsistaProcessoSeletivoDAL
    {

        public bool Salvar(BolsistaProcessoSeletivo bolsistaps, Bolsista bolsista, Responsavel res, int financiadorInstrumentoSelecaoUID, int folhaPagamentoUID)
        {           
            var contextoBolsita = Contexto.GetContextInstance();
            var contextoResponsavel = Contexto.GetContextInstance();
            var contextoBolsitaProcessoSeletivo = Contexto.GetContextInstance();
            var contextoFinanciadorInstrumentoSelecao = Contexto.GetContextInstance();
            var contextoBolsistaFolhaPagamento = Contexto.GetContextInstance();
            int BDBolsistaUID = 0;
            try
            {

                using (TransactionScope transaction = new TransactionScope())
                {
                    
                    //Verificar se já existe o cadastro de bolsista, caso sim, não salvar
                    BDBolsistaUID = (from c in contextoBolsita.Bolsista
                              where c.registroAluno == bolsista.registroAluno
                              select c.bolsistaUID).FirstOrDefault();
                    
                    //Verificar se já existe cadastro de responsavel, caso sim, não salvar
                    int BDResponsavelUID = (from c in contextoBolsita.Responsavel
                                       where c.matricula == res.matricula
                                       select c.responsavelUID).FirstOrDefault();

                    if (BDBolsistaUID == 0)
                    {
                        contextoBolsita.AddToBolsista(bolsista);
                        contextoBolsita.SaveChanges(SaveOptions.DetectChangesBeforeSave);
                        //Último ID do bolsista inserido
                        BDBolsistaUID = bolsista.bolsistaUID;
                    }
                    
                    if (BDResponsavelUID == 0)
                    {

                        contextoResponsavel.AddToResponsavel(res);
                        contextoResponsavel.SaveChanges(SaveOptions.DetectChangesBeforeSave);
                        //Último ID do responsável inserido
                        BDResponsavelUID = res.responsavelUID;
                    }
                    
                    var novoBolsistaProcessoSeletivo = new BolsistaProcessoSeletivo
                    {
                       
                        //Todas Pró-reitorias
                        bolsistaUID = BDBolsistaUID,
                        responsavelUID = BDResponsavelUID,
                        processoSeletivoUID = bolsistaps.processoSeletivoUID,
                        situacao = bolsistaps.situacao,

                        //Procev
                        horaAtvSegMat = bolsistaps.horaAtvSegMat,
                        horaAtvTerMat = bolsistaps.horaAtvTerMat,
                        horaAtvQuaMat = bolsistaps.horaAtvQuaMat,
                        horaAtvQuiMat = bolsistaps.horaAtvQuiMat,
                        horaAtvSexMat = bolsistaps.horaAtvSexMat,
                        horaAtvSabMat = bolsistaps.horaAtvSabMat,

                        horaAtvSegVesp = bolsistaps.horaAtvSegVesp,
                        horaAtvTerVesp = bolsistaps.horaAtvTerVesp,
                        horaAtvQuaVesp = bolsistaps.horaAtvQuaVesp,
                        horaAtvQuiVesp = bolsistaps.horaAtvQuiVesp,
                        horaAtvSexVesp = bolsistaps.horaAtvSexVesp,
                        horaAtvSabVesp = bolsistaps.horaAtvSabVesp,

                        horaAtvSegNot = bolsistaps.horaAtvSegNot,
                        horaAtvTerNot = bolsistaps.horaAtvTerNot,
                        horaAtvQuaNot = bolsistaps.horaAtvQuaNot,
                        horaAtvQuiNot = bolsistaps.horaAtvQuiNot,
                        horaAtvSexNot = bolsistaps.horaAtvSexNot,
                        horaAtvSabNot = bolsistaps.horaAtvSabNot,
                        numSigProj = bolsistaps.numSigProj,
                        tituloSigProj = bolsistaps.tituloSigProj,
                        //Propeq
                        numProjetoPropeq = bolsistaps.numProjetoPropeq

                    };


                    var novoBolsistaFolhaPagamento = new BolsistaFolhaPagamento
                    {
                        bolsistaUID = BDBolsistaUID,
                        folhaPagamentoUID = folhaPagamentoUID,
                        pagamentoAutorizado = false,
                        pagamentoRecusado = false,
                        ativo = true

                    };

                    contextoBolsitaProcessoSeletivo.AddObject("BolsistaProcessoSeletivo", novoBolsistaProcessoSeletivo);
                    contextoBolsitaProcessoSeletivo.SaveChanges(SaveOptions.DetectChangesBeforeSave);
                    contextoFinanciadorInstrumentoSelecao.spSetQuantidadeBolsaRestante(financiadorInstrumentoSelecaoUID);
                    
                    contextoBolsistaFolhaPagamento.AddObject("BolsistaFolhaPagamento", novoBolsistaFolhaPagamento);
                    contextoBolsistaFolhaPagamento.SaveChanges(SaveOptions.DetectChangesBeforeSave);

                    transaction.Complete();
                    contextoBolsita.AcceptAllChanges();
                    contextoResponsavel.AcceptAllChanges();
                    contextoBolsitaProcessoSeletivo.AcceptAllChanges();
                    contextoFinanciadorInstrumentoSelecao.AcceptAllChanges();
                    contextoBolsistaFolhaPagamento.AcceptAllChanges();

                    
                }

                if (BDBolsistaUID != 0)
                {
                    
                    Bolsista bolsistaExistente = contextoBolsita.Bolsista.Where(b => b.bolsistaUID == BDBolsistaUID).FirstOrDefault();
                    
                    idbufmtEntities newContext = Contexto.GetContextInstance();

                    newContext.ExecuteStoreCommand("update Bolsista set bancoUID = {0}, email = {1}, agencia = {2}, numeroConta = {3} where bolsistaUID = {4}", 
                        bolsista.bancoUID, bolsista.email, bolsista.agencia, bolsista.numeroConta, bolsistaExistente.bolsistaUID);

                    newContext.SaveChanges();
                }
                return true;

            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);

            }
            finally {


                contextoBolsita.Dispose();
                contextoResponsavel.Dispose();
                contextoBolsitaProcessoSeletivo.Dispose();
                contextoFinanciadorInstrumentoSelecao.Dispose();
                contextoBolsistaFolhaPagamento.Dispose();
            }
        }


        public bool Excluir(int id)
        {

                var contexto = Contexto.GetContextInstance();

                try
                {
                    contexto.DeleteObject((from c in contexto.BolsistaProcessoSeletivo where c.id == id select c).SingleOrDefault());
                    contexto.SaveChanges();
                    return true;
                }

                catch (Exception exception)
                {
                    throw new Exception("Não foi possível excluir o registro.", exception);

                }


        }

        public BolsistaProcessoSeletivo GetBolsistaProcessoSeletivo(int bolsistaUID, int processoSeletivoUID)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                List<BolsistaProcessoSeletivo> registros = (from c in contexto.BolsistaProcessoSeletivo
                                                      where c.bolsistaUID == bolsistaUID
                                                            && c.processoSeletivoUID == processoSeletivoUID
                                                      select c).ToList();

                BolsistaProcessoSeletivo bps = null;
                if(registros.Count > 0)
                    bps = registros.Last();

                return bps;
            }

            catch (Exception exception)
            {
                throw new Exception("Não foi possível selecionar o registro.", exception);

            }
        }

        public void AtualizarSituacaoBolsita(BolsistaProcessoSeletivo objBPS)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                BolsistaProcessoSeletivo bolsistaPS = new BolsistaProcessoSeletivo();
                bolsistaPS.id = objBPS.id;
                bolsistaPS.situacao = objBPS.situacao;
                bolsistaPS.bolsistaUID = objBPS.bolsistaUID;
                bolsistaPS.responsavelUID = objBPS.responsavelUID;
                bolsistaPS.processoSeletivoUID = objBPS.processoSeletivoUID;

                //Procev
                bolsistaPS.horaAtvSegMat = objBPS.horaAtvSegMat;
                bolsistaPS.horaAtvTerMat = objBPS.horaAtvTerMat;
                bolsistaPS.horaAtvQuaMat = objBPS.horaAtvQuaMat;
                bolsistaPS.horaAtvQuiMat = objBPS.horaAtvQuiMat;
                bolsistaPS.horaAtvSexMat = objBPS.horaAtvSexMat;
                bolsistaPS.horaAtvSabMat = objBPS.horaAtvSabMat;

                bolsistaPS.horaAtvSegVesp = objBPS.horaAtvSegVesp;
                bolsistaPS.horaAtvTerVesp = objBPS.horaAtvTerVesp;
                bolsistaPS.horaAtvQuaVesp = objBPS.horaAtvQuaVesp;
                bolsistaPS.horaAtvQuiVesp = objBPS.horaAtvQuiVesp;
                bolsistaPS.horaAtvSexVesp = objBPS.horaAtvSexVesp;
                bolsistaPS.horaAtvSabVesp = objBPS.horaAtvSabVesp;

                bolsistaPS.horaAtvSegNot = objBPS.horaAtvSegNot;
                bolsistaPS.horaAtvTerNot = objBPS.horaAtvTerNot;
                bolsistaPS.horaAtvQuaNot = objBPS.horaAtvQuaNot;
                bolsistaPS.horaAtvQuiNot = objBPS.horaAtvQuiNot;
                bolsistaPS.horaAtvSexNot = objBPS.horaAtvSexNot;
                bolsistaPS.horaAtvSabNot = objBPS.horaAtvSabNot;
                bolsistaPS.numSigProj = objBPS.numSigProj;
                bolsistaPS.tituloSigProj = objBPS.tituloSigProj;
                //Propeq
                bolsistaPS.numProjetoPropeq = objBPS.numProjetoPropeq;

                contexto.BolsistaProcessoSeletivo.Attach(bolsistaPS);
                contexto.ObjectStateManager.ChangeObjectState(bolsistaPS, EntityState.Modified);
                contexto.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível atualizar o registro.", ex);
            }
        }


        public List<BolsistaProcessoSeletivo> GetListBolsistaProcessoSeletivo(int bolsistaUID, int processoSeletivoUID)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();

                return (from c in contexto.BolsistaProcessoSeletivo
                        where c.bolsistaUID == bolsistaUID && c.processoSeletivoUID == processoSeletivoUID
                        select c).ToList();
            }
            catch
            {
                throw;
            }
        }
    }
}
