﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;

namespace ufmt.ssbic.DataAccess
{
    public class CursoProcessoSeletivoDAL
    {
        idbufmtEntities contexto;
        public CursoProcessoSeletivoDAL()
        {
            this.contexto = Contexto.GetContextInstance();
        }

        public void Save(CursoProcessoSeletivo cursoProc)
        {
            try
            {
                var cursoPS = new CursoProcessoSeletivo
                {
                    cursoProcessoSeletivoUID = cursoProc.cursoProcessoSeletivoUID,
                    cursoUID = cursoProc.cursoUID,
                    processoSeletivoUID = cursoProc.processoSeletivoUID,
                    vagas = cursoProc.vagas
                };


                this.contexto.AddObject("CursoProcessoSeletivo", cursoPS);
                this.contexto.SaveChanges();
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);

            }
        }

        public void Delete(int cursoProcUID)
        {
            try
            {
                contexto.DeleteObject((from c in contexto.CursoProcessoSeletivo where c.cursoProcessoSeletivoUID == cursoProcUID select c).SingleOrDefault());
                contexto.SaveChanges();                
            }

            catch (Exception exception)
            {
                throw new Exception("Não foi possível excluir o registro.", exception);

            }
        }

        public List<vwCursoProcessoSeletivo> GetRegistros(int processoSeletivoUID)
        {
            try
            {
                List<vwCursoProcessoSeletivo> registros = (from c in this.contexto.vwCursoProcessoSeletivo where c.processoSeletivoUID == processoSeletivoUID select c).ToList<vwCursoProcessoSeletivo>();
                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public CursoProcessoSeletivo GetRegistro(int cursoProcUID)
        {
            try
            {
                CursoProcessoSeletivo registro = (from c in this.contexto.CursoProcessoSeletivo where c.cursoProcessoSeletivoUID == cursoProcUID select c).FirstOrDefault<CursoProcessoSeletivo>();
                return registro;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }
    }
}
