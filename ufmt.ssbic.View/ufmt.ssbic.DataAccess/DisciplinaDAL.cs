﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ufmt.ssbic.DataAccess
{
    public class DisciplinaDAL
    {
        private idbufmtEntities contexto;
        public DisciplinaDAL()
        {
            this.contexto = new idbufmtEntities();
        }
        public List<DisciplinasAprovadasEntity> GetDisciplinasAprovadas(String rga, double media)
        {
            List<DisciplinasAprovadasEntity> disciplinas = this.contexto.getDisciplinasAprovadas(rga, media).ToList<DisciplinasAprovadasEntity>();
            return disciplinas;
        }
    }
}
