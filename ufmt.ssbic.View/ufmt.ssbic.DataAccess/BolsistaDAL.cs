﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Transactions;
using System.Data.Objects;
using System.Data.Objects.SqlClient;


namespace ufmt.ssbic.DataAccess
{
    public class BolsistaDAL
    {
        public List<vwBolsista> GetRegistros(int processoSeletivoUID)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                List<vwBolsista> registros = (from c in contexto.vwBolsista where c.processoSeletivoUID == processoSeletivoUID select c).ToList<vwBolsista>();
                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public vwBolsista GetBolsista(int processoSeletivoUID)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                return (from c in contexto.vwBolsista where c.processoSeletivoUID == processoSeletivoUID select c).FirstOrDefault<vwBolsista>();

            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar o registro.", exception);
            }
        }

        public Bolsista GetBolsista(String matricula)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                return (from c in contexto.Bolsista where c.registroAluno == matricula select c).FirstOrDefault<Bolsista>();

            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar o registro.", exception);
            }
        }

      

        public Bolsista GetBolsistaPorId(int bolsistaUID)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                return (from c in contexto.Bolsista where c.bolsistaUID == bolsistaUID select c).FirstOrDefault<Bolsista>();

            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar o registro.", exception);
            }
        }



        public List<vwBolsista> FindRegistros(int? processoSeletivoUID, string criterio)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                List<vwBolsista> registros = (from c in contexto.vwBolsista where c.processoSeletivoUID == processoSeletivoUID && c.registroAluno.Contains(criterio) select c).ToList<vwBolsista>();
                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }


        public bool Salvar(Bolsista bolsista)
        {
            var contextoBolsita = Contexto.GetContextInstance();

            try
            {
                contextoBolsita.AddToBolsista(bolsista);
                contextoBolsita.SaveChanges(SaveOptions.DetectChangesBeforeSave);
                return true;

            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);

            }

        }

        public int SalvarBolsista(Bolsista objBolsista)
        {
            var contextoBolsita = Contexto.GetContextInstance();

            try
            {
                var bolsista = new Bolsista
                {
                    bolsistaUID = objBolsista.bolsistaUID,
                    agencia = objBolsista.agencia,
                    bancoUID = objBolsista.bancoUID,
                    email = objBolsista.email,
                    numeroConta = objBolsista.numeroConta,
                    registroAluno = objBolsista.registroAluno,
                    cpf = objBolsista.cpf
                };

                contextoBolsita.AddObject("Bolsista", bolsista);
                contextoBolsita.SaveChanges();

                return bolsista.bolsistaUID;

            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);

            }

        }

        public bool AtualizarBolsista(Bolsista objBolsista)
        {
            var contexto = Contexto.GetContextInstance();
            try
            {

                Bolsista bolsista = new Bolsista();
                bolsista.bolsistaUID = objBolsista.bolsistaUID;
                bolsista.registroAluno = objBolsista.registroAluno;
                bolsista.email = objBolsista.email;
                bolsista.numeroConta = objBolsista.numeroConta;
                bolsista.bancoUID = objBolsista.bancoUID;
                bolsista.agencia = objBolsista.agencia;
                bolsista.cpf = objBolsista.cpf;

                contexto.Bolsista.Attach(bolsista);
                contexto.ObjectStateManager.ChangeObjectState(bolsista, EntityState.Modified);
                contexto.SaveChanges();
               
                return true;

            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível atualizar os registros.", exception);

            }
 

        }



        public int Salvar(Bolsista bolsista, BolsistaProcessoSeletivo bps)
        {
            var contextoBolsita = Contexto.GetContextInstance();
            var contextoBolsitaProcessoSeletivo = Contexto.GetContextInstance();
            
            try
            {

                using (TransactionScope transaction = new TransactionScope())
                {

                    contextoBolsita.AddToBolsista(bolsista);
                    contextoBolsita.SaveChanges(SaveOptions.DetectChangesBeforeSave);
                    
                    var novoBolsistaProcessoSeletivo = new BolsistaProcessoSeletivo
                    {
                        //Último ID inserido
                        bolsistaUID = bolsista.bolsistaUID,
                        situacao = bps.situacao,
                        processoSeletivoUID = bps.processoSeletivoUID
                    };

                    contextoBolsitaProcessoSeletivo.AddObject("BolsistaProcessoSeletivo", novoBolsistaProcessoSeletivo);
                    contextoBolsitaProcessoSeletivo.SaveChanges(SaveOptions.DetectChangesBeforeSave);

                    transaction.Complete();
                    contextoBolsita.AcceptAllChanges();
                    contextoBolsitaProcessoSeletivo.AcceptAllChanges();

                    return bolsista.bolsistaUID;


                }
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);

            }
            finally {


                contextoBolsita.Dispose();
                contextoBolsitaProcessoSeletivo.Dispose();
            }
        }

        //public bool Excluir(int bolsistaUID, int processoSeletivoUID)
        //{

        //        var contexto = Contexto.GetContextInstance();
        //        object BolsistaExcluido;

        //        EntityKey BolsistaUID =
        //            new EntityKey("idbufmtEntities.BolsistaProcessoSeletivo",
        //                "BolsistaUID", bolsistaUID);

        //        if (contexto.TryGetObjectByKey(BolsistaUID, out BolsistaExcluido))
        //            {

        //                try
        //                {
        //                    contexto.DeleteObject((from c in contexto.Bolsista where c.bolsistaUID == bolsistaUID select c).SingleOrDefault());
        //                    contexto.SaveChanges();
        //                    return true;
        //                }   
        //                catch (OptimisticConcurrencyException ex)
        //                {
        //                    throw new InvalidOperationException(string.Format(
        //                        "O registro código '{0}' não pode ser excluído.<br />"
        //                        + "Verifique se o objeto já foi excluído anteriormente.\n",
        //                        BolsistaUID.EntityKeyValues[0].Value), ex);
        //                }

        //                catch (Exception exception)
        //                {
        //                    throw new Exception("Não foi possível excluir o registro.", exception);

        //                }

        //            }
        //            else
        //            {
        //                throw new InvalidOperationException(string.Format(
        //                    "O registro código '{0}' não foi encontrado. <br />"
        //                    + "Tenha certeza que o registro existe no banco de dados.<br />",
        //                    BolsistaUID.EntityKeyValues[0].Value));
        //            }
           

        //}



        public bool Excluir(int bolsistaUID, int processoSeletivoUID)
        {

                var contexto = Contexto.GetContextInstance();

                try
                {
                    contexto.DeleteObject((from c in contexto.BolsistaProcessoSeletivo where c.bolsistaUID == bolsistaUID && c.processoSeletivoUID==processoSeletivoUID && c.situacao == 0 select c).SingleOrDefault());
                    contexto.SaveChanges();
                    return true;
                }

                catch (Exception exception)
                {
                    throw new Exception("Não foi possível excluir o registro.", exception);

                }

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="rga"></param>
        /// <param name="situacao">1 - Bolsita ativo     2 - bolsista cancelado</param>
        /// <returns></returns>
        public int? VerificaRGA(string rga, int situacao)
        {
            var contexto = Contexto.GetContextInstance();
            //TODO: Granvando repetido quando usuário é excluído
            try
            {
                int? bolsistaUID = (from c in contexto.vwBolsista where c.registroAluno == rga && c.situacao == situacao select c.bolsistaUID).FirstOrDefault();
                return bolsistaUID;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar o registro.", exception);
            }

        }

        public bool VerificaBolsistaAtivo(string rga)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();

                BolsistaFolhaPagamento bfp =
                    (from c in contexto.BolsistaFolhaPagamento
                     join fp in contexto.FolhaPagamento
                         on c.folhaPagamentoUID equals fp.folhaPagamentoUID
                     join b in contexto.Bolsista
                         on c.bolsistaUID equals b.bolsistaUID
                     where b.registroAluno == rga 
                        orderby fp.dataFim descending
                     select c).FirstOrDefault();



                if (bfp != null)
                {
                    if (bfp.FolhaPagamento.ProcessoSeletivo.InstrumentoSelecao.situacao.HasValue)
                    {
                        if (bfp.FolhaPagamento.ProcessoSeletivo.InstrumentoSelecao.situacao.Value)
                        {
                            return bfp.ativo;                            
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return bfp.ativo;
                    }
                } 
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            /*
            var contexto = Contexto.GetContextInstance();

            try
            {
                Bolsista bolsista =
                    (from b in contexto.Bolsista
                     join bps in contexto.BolsistaProcessoSeletivo
                         on b.bolsistaUID equals bps.bolsistaUID
                     join p in contexto.ProcessoSeletivo
                         on bps.processoSeletivoUID equals p.processoSeletivoUID
                     join i in contexto.InstrumentoSelecao
                         on p.instrumentoSelecaoUID equals i.instrumentoSelecaoUID
                     where
                         b.registroAluno == rga &&
                         bps.situacao.Value == 1
                     select b).FirstOrDefault();

                if (bolsista != null)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
             * 
             * */
        }

        public List<vwBolsistasFolhaPagamento> GetBolsistasFolhaPagamento(int folhaPagamentoUID)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();
                List<vwBolsistasFolhaPagamento> registros =
                    (from c in contexto.vwBolsistasFolhaPagamento
                     where c.folhaPagamentoUID == folhaPagamentoUID  
                        orderby c.Nome ascending
                     select c 
                    ).ToList<vwBolsistasFolhaPagamento>();

                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível selecionar os registros", exception);
            }
        }

        public List<BolsistaFolhaPagamento> GetBolsistasFolhaPagamentoPorId(int folhaPagamentoUID)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();
                List<BolsistaFolhaPagamento> registros =
                    (from bfp in contexto.BolsistaFolhaPagamento
                        join bps in contexto.BolsistaProcessoSeletivo
                            on bfp.bolsistaUID equals bps.bolsistaUID
                        where bfp.folhaPagamentoUID == folhaPagamentoUID && 
                              bps.situacao == 1                     
                     select bfp
                    ).ToList<BolsistaFolhaPagamento>();

                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível selecionar os registros", exception);
            }
        }

        public List<vwBolsistasFolhaPagamento> GetBolsistasFolhaPagamentoPorNome(int folhaPagamentoUID, String nome)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();
                List<vwBolsistasFolhaPagamento> registros =
                    (from c in contexto.vwBolsistasFolhaPagamento
                     where c.folhaPagamentoUID == folhaPagamentoUID &&
                            c.Nome.Contains(nome)
                     orderby c.Nome ascending
                     select c
                    ).ToList<vwBolsistasFolhaPagamento>();
                
                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível selecionar os registros", exception);
            }
        }

        public IList<Bolsista> GetBolsistasPorFolhaPagamento(int folhaPagamentoUID)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();
                
                var registros = (from c in contexto.vwBolsistasFolhaPagamento 
                        join b in contexto.Bolsista 
                            on c.bolsistaUID equals b.bolsistaUID
                        where c.folhaPagamentoUID == folhaPagamentoUID && c.situacao == 1
                        orderby c.Nome ascending
                        select b
                            );

                return registros.ToList();

                /*
                     var registros =
                        (from b in contexto.Bolsista.Include("BolsistaFolhaPagamento").Include("Banco")
                            join bfp in contexto.BolsistaFolhaPagamento
                                on b.bolsistaUID equals bfp.bolsistaUID
                            join bps in contexto.BolsistaProcessoSeletivo
                                on b.bolsistaUID equals bps.bolsistaUID
                            where bfp.folhaPagamentoUID == folhaPagamentoUID && bps.situacao == 1
                         select b);
                    */
            
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível selecionar os registros", exception);
            }
        }

        public List<Bolsista> GetBolsistasPorFolhaPagamentoSuplementar(int folhaPagamentoUID)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();

                var registros = (from c in contexto.vwBolsistasFolhaPagamento
                                 join b in contexto.Bolsista
                                     on c.bolsistaUID equals b.bolsistaUID
                                 where c.folhaPagamentoUID == folhaPagamentoUID && c.situacao == 1
                                    && c.pagamentoRecusado == true 
                                 orderby c.Nome ascending
                                 select b
                            );
                /*
                List<Bolsista> registros =
                    (from b in contexto.Bolsista.Include("BolsistaFolhaPagamento").Include("Banco")
                     join bfp in contexto.BolsistaFolhaPagamento
                         on b.bolsistaUID equals bfp.bolsistaUID
                     join bps in contexto.BolsistaProcessoSeletivo
                         on b.bolsistaUID equals bps.bolsistaUID 
                     join a in contexto.vwAluno
                         on decimal.Parse(b.registroAluno) equals a.Matricula
                     where bfp.folhaPagamentoUID == folhaPagamentoUID && bps.situacao == 1 
                            && bfp.pagamentoRecusado == true     
                     orderby a.Nome ascending
                     select b).ToList();
                */
                return registros.ToList();
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível selecionar os registros", exception);
            }
        }

        public List<vwBolsistasFolhaPagamento> GetBolsistasFolhaPagamentoSuplementar(int folhaPagamentoUID)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();
                List<vwBolsistasFolhaPagamento> registros =
                    (from c in contexto.vwBolsistasFolhaPagamento
                     where c.folhaPagamentoUID == folhaPagamentoUID && 
                             c.situacao == 1 && 
                             c.pagamentoRecusado == true && 
                             c.ativo == true
                     orderby c.Nome ascending
                     select c
                    ).ToList<vwBolsistasFolhaPagamento>();

                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível selecionar os registros", exception);
            }
        }

        public List<vwBolsistasFolhaPagamento> GetBolsistasFolhaPagamentoSuplementarPorNome(int folhaPagamentoUID, String nome)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();
                List<vwBolsistasFolhaPagamento> registros =
                    (from c in contexto.vwBolsistasFolhaPagamento
                     where  c.folhaPagamentoUID == folhaPagamentoUID && 
                            c.situacao == 1 &&
                            c.pagamentoRecusado == true && 
                            c.Nome.Contains(nome) && 
                            c.ativo == true
                     orderby c.Nome ascending
                     select c
                    ).ToList<vwBolsistasFolhaPagamento>();

                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível selecionar os registros", exception);
            }
        } 

        public BolsistaFolhaPagamento GetBolsistaFolhaPagamento(int bolsistaUID, int folhaPagamentoUID)
        {
            try
            {
                var contexto = Contexto.GetContextInstance();
                BolsistaFolhaPagamento registro =
                    (from c in contexto.BolsistaFolhaPagamento
                     where c.folhaPagamentoUID == folhaPagamentoUID && c.bolsistaUID == bolsistaUID
                     select c
                    ).FirstOrDefault<BolsistaFolhaPagamento>();

                return registro;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível selecionar os registros", exception);
            }
        }

        public void SalvarBolsistaFolhaPagamento(BolsistaFolhaPagamento objBolsista, int operacao)
        {
            var contexto = Contexto.GetContextInstance();
            try
            {
                BolsistaFolhaPagamento bolsista = new BolsistaFolhaPagamento();
                bolsista.bolsistaFolhaPagamentoUID = objBolsista.bolsistaFolhaPagamentoUID;
                bolsista.folhaPagamentoUID = objBolsista.folhaPagamentoUID;
                bolsista.bolsistaUID = objBolsista.bolsistaUID;
                bolsista.pagamentoAutorizado = objBolsista.pagamentoAutorizado;
                bolsista.pagamentoRecusado = objBolsista.pagamentoRecusado;
                bolsista.parecer = objBolsista.parecer;
                bolsista.ativo = objBolsista.ativo;                     

                if (operacao == 1)
                {
                    contexto.AddObject("BolsistaFolhaPagamento", bolsista);
                }
                else
                {
                    contexto.BolsistaFolhaPagamento.Attach(bolsista);
                    contexto.ObjectStateManager.ChangeObjectState(bolsista, EntityState.Modified);
                }
                contexto.SaveChanges();

            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível atualizar os registros.", ex);
            }
        }

        public void HomologarBolsistas1Semestre(int? bolsistaUID, int processoSeletivoUID, int folhaPagamentoUID)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                if(bolsistaUID.HasValue) 
                    contexto.Homologacao1Semestre(bolsistaUID.Value, processoSeletivoUID, folhaPagamentoUID);
                else
                    contexto.Homologacao1Semestre(null, processoSeletivoUID, folhaPagamentoUID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public void HomologarBolsistas2Semestre(int? bolsistaUID, int processoSeletivoUID, int folhaPagamentoUID)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                if (bolsistaUID.HasValue)
                    contexto.Homologacao2Semestre(bolsistaUID.Value, processoSeletivoUID, folhaPagamentoUID);
                else
                    contexto.Homologacao2Semestre(null, processoSeletivoUID, folhaPagamentoUID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public void AlterarVinculoBolsista(int bolsistaUID)
        {
            var contexto = Contexto.GetContextInstance();
            try
            {
                contexto.Historico(bolsistaUID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void HomologarBolsistaIndividual(int bolsistaUID, int processoSeletivoUID, int folhaPagamentoUID, int semestre)
        {
            var contexto = Contexto.GetContextInstance();

            try
            {
                if(semestre == 2)
                    contexto.HomologacaoIndividual2Semestre(bolsistaUID, processoSeletivoUID, folhaPagamentoUID);                
                else if(semestre == 3)
                    contexto.HomologacaoIndividual1Semestre(bolsistaUID, processoSeletivoUID, folhaPagamentoUID);                

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public BolsistaFolhaPagamento GetFolhaPagamentoBolsistaAtivo(string rga)
        {
            var contexto = Contexto.GetContextInstance();

            BolsistaFolhaPagamento bfp =
                (from c in contexto.BolsistaFolhaPagamento
                 join fp in contexto.FolhaPagamento
                     on c.folhaPagamentoUID equals fp.folhaPagamentoUID
                 join b in contexto.Bolsista
                     on c.bolsistaUID equals b.bolsistaUID
                 where b.registroAluno == rga
                 orderby fp.dataFim descending
                 select c).FirstOrDefault();

            return bfp;
        }
    }
}
