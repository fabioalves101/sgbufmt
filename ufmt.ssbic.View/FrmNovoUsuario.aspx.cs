﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;

namespace ufmt.ssbic.View
{
    public partial class FrmNovoUsuario : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Image ImageTopo = (Image)Page.Master.FindControl("ImageTopo");
            ImageTopo.ImageUrl = "~/webapp/images/bolsa_topo.png";
        }

        protected void btSalvar_Click(object sender, ImageClickEventArgs e)
        {
            AutenticacaoBO autenticacaoBO = new AutenticacaoBO();
            try
            {
                if (autenticacaoBO.criaUsuario(txtcpf.Text, txtregistro.Text, 
                    DateTime.Parse(txtdatanascimento.Text), txtnomemae.Text, txtemail.Text, 
                    int.Parse(ddlProreitoria.SelectedValue)))
                {
                    PanelFormulario.Visible = false;
                    panelAviso.Visible = true;
                    lblAlerta.Text = "Usuário Criado com Sucesso.";
                }
                else
                {                    
                    panelAviso.Visible = true;
                    lblAlerta.Text = "Erro ao criar o usuário.";
                }
            }
            catch (Exception ex)
            {
                panelAviso.Visible = true;                
                lblAlerta.Text = ex.Message;
            }
        }
    }
}