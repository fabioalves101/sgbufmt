﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.sig.entity;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.View.controles
{
    public partial class UCBuscarServidor : System.Web.UI.UserControl
    {
        public String labelBuscaServidor;

        public Servidor servidor;
        private PessoaBO pessoaBO;

        public UCBuscarServidor()
        {
            this.pessoaBO = new PessoaBO();
        }
                
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(labelBuscaServidor))
                lblBuscaServidor.Text = labelBuscaServidor;            
        }

        protected void PreencherGridview(String criterio, String tipoCriterio, bool visible)
        {
            IList<Servidor> servidores = this.pessoaBO.GetPessoas(criterio, tipoCriterio);

            gdrListaServidores.DataSource = servidores;
            pnlListaServidores.Visible = visible;
            gdrListaServidores.DataBind();            
        }


        protected void gdrListaServidores_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridView gridview = (GridView)sender;
            Int64 servidorUID = int.Parse(gridview.SelectedValue.ToString());

            Pessoa pessoa = this.pessoaBO.GetPessoa(servidorUID);
            this.servidor = this.pessoaBO.GetServidorUnico(servidorUID);

            lblnome.Text = pessoa.Nome;
            
            lblmatricula.Text = this.servidor.Registro;
            hdservidor.Value = this.servidor.ServidorUID.ToString();    

            pnlInfoServidor.Visible = true;
            pnlListaServidores.Visible = false;
            pnlServidor.Visible = false;
        }

        protected void lnkNovaBusca_Click(object sender, EventArgs e)
        {
            pnlServidor.Visible = true;
        }

        
        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtBuscaServidor.Text))
                PreencherGridview(txtBuscaServidor.Text, ddlBuscaServidor.SelectedValue, true);
            else
                throw new Exception("Preencha os dados para realizar a busca");
        }

        public void SetServidor(long servidorUID)
        {
            hdservidor.Value = servidorUID.ToString();
        }

        public Servidor GetServidor()
        {
            try
            {
                return this.pessoaBO.GetServidorUnico(long.Parse(hdservidor.Value));
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível selecionar o servidor.");
            }
        }

        protected void gdrListaServidores_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gdrListaServidores.PageIndex = e.NewPageIndex;
            PreencherGridview(txtBuscaServidor.Text, ddlBuscaServidor.SelectedValue, true);
        }
    }
}