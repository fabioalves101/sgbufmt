﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;
using ufmt.sig.entity;

namespace ufmt.ssbic.View.controles
{
    public partial class UCBuscarAluno : System.Web.UI.UserControl
    {
        public Aluno aluno;

        public UCBuscarAluno()
        {
            this.aluno = new Aluno();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        
        protected void PreencherGridAluno()
        {

            AlunoBO objBO = new AlunoBO();
            int totalRegistros = 0;
            List<vwAluno> registros = null;
            if (rblTipoConsultaAluno.SelectedValue.Equals("0"))
                registros = objBO.FindRegistros(txtAluno.Text, 0);
            else
                registros = objBO.FindRegistros(null, decimal.Parse(txtAluno.Text));

            gdrAlunos.DataSource = registros;
            gdrAlunos.DataBind();
            totalRegistros = registros.Count;
            PanelResultadoConsulta.Visible = true;
            PanelResultadoConsulta.Visible = true;
            lblTotalAlunos.Text = totalRegistros.ToString();


        }
        protected void gdrAlunos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gdrAlunos.PageIndex = e.NewPageIndex;
            PreencherGridAluno();
        }

        protected void gdrAlunos_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "adicionar")
            {
                string[] dados = e.CommandArgument.ToString().Split(',');

                if (dados.Length > 8)
                {
                    this.aluno.Matricula = decimal.Parse(dados[0]);
                    this.aluno.Nome = dados[1];
                    this.aluno.CPF = dados[2];
                    this.aluno.Email = dados[3];
                    this.aluno.Curso = dados[4] + " - " + dados[5];
                    this.aluno.Campus = dados[8];
                    this.aluno.CodCurso = int.Parse(dados[4]);
                    this.aluno.CodCampus = byte.Parse(dados[7]);
                }
                else
                {
                    this.aluno.Matricula = decimal.Parse(dados[0]);
                    this.aluno.Nome = dados[1];
                    this.aluno.CPF = dados[2];
                    this.aluno.Email = dados[3];
                    this.aluno.Curso = dados[4] + " - " + dados[5];
                    this.aluno.Campus = dados[7];
                    this.aluno.CodCurso = int.Parse(dados[4]);
                    this.aluno.CodCampus = byte.Parse(dados[6]);
                }
                hdmatricula.Value = this.aluno.Matricula.ToString();

                PanelEtapa1.Visible = false;

                pnlDadosAluno.Visible = true;
                lblNome.Text = this.aluno.Nome;
                lblCpf.Text = this.aluno.CPF;
            }
        }

                

        protected void Button1_Click(object sender, EventArgs e)
        {
            PreencherGridAluno();
        }

        protected void rblTipoConsultaAluno_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblTipoConsultaAluno.Items[1].Selected)
                REVAluno.Enabled = true;
            else
                REVAluno.Enabled = false;
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {

        }

        protected void lnkNovaBusca_Click(object sender, EventArgs e)
        {
            PanelEtapa1.Visible = true;
            pnlDadosAluno.Visible = false;
        }

        public Aluno GetAluno()
        {
            try
            {
                AlunoBO alunoBO = new AlunoBO();
                vwAluno vwaluno = alunoBO.GetAlunoMatriculado(decimal.Parse(hdmatricula.Value));

                Aluno aluno = new Aluno();
                aluno.Campus = vwaluno.Campus;
                aluno.CodCampus = vwaluno.CodCampus;
                aluno.CodCurso = vwaluno.CodCurso;
                aluno.CPF = vwaluno.CPF;
                aluno.Curso = vwaluno.Curso;
                aluno.Email = vwaluno.Email;
                aluno.Matricula = vwaluno.Matricula;
                aluno.Nome = vwaluno.Nome;
                aluno.Tipo = vwaluno.Tipo;
                aluno.CodigoFebrabram = vwaluno.CodigoFebrabram;
                aluno.Agencia = vwaluno.Agencia;
                aluno.ContaCorrente = vwaluno.ContaCorrente;

                return aluno;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível selecionar o aluno.");
            }
        }
    }
}