﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;
using ufmt.sig.entity;

namespace ufmt.ssbic.View.controles
{
    public partial class UCDadosMonitor : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["permissao"].ToString().Contains("MONITORIA"))
            {
                if (!IsPostBack)
                {
                    Monitor monitor = new Monitor();
                    if (String.IsNullOrEmpty(Request.QueryString["monitorUID"]))
                        monitor = this.GetMonitorPorBolsista();
                    else
                        monitor = this.GetMonitor();

                    PessoaBO pessoaBO = new PessoaBO();
                    Servidor orientador = null;
                    if (monitor.orientadorUID.HasValue)
                    {
                        orientador = pessoaBO.GetServidorUnico(monitor.orientadorUID.Value);
                        lblOrientador.Text = orientador.Pessoa.Nome;
                        UCBuscarOrientador.servidor = orientador;
                        UCBuscarOrientador.SetServidor(orientador.ServidorUID);
                    }
                    else
                    {
                        lblOrientador.Text = "Não definido";
                    }

                    Servidor coordenador = null;
                    if (monitor.coordenadorUID.HasValue)
                    {
                        coordenador = pessoaBO.GetServidorUnico(monitor.coordenadorUID.Value);
                        lblCoordenador.Text = coordenador.Pessoa.Nome;
                        UCBuscarCoordenador.servidor = coordenador;
                        UCBuscarCoordenador.SetServidor(coordenador.ServidorUID);
                    }
                    else
                    {
                        lblCoordenador.Text = "Não definido";
                    }


                    if (monitor.remunerada.Value)
                        rdoModalidade.SelectedValue = "2";
                    else
                        rdoModalidade.SelectedValue = "1";

                    if (monitor.periodoAtuacao.Equals("1"))
                        rdoPeriodo.SelectedValue = "1";
                    else
                    {
                        if (monitor.periodoAtuacao.Equals("2"))
                            rdoPeriodo.SelectedValue = "2";
                        else
                            rdoPeriodo.SelectedValue = "3";
                    }




                    if (monitor.status.HasValue)
                    {
                        if (monitor.status.Value)
                            rdoStatus.SelectedValue = "1";
                        else
                            rdoStatus.SelectedValue = "0";
                    }
                    else
                        rdoStatus.SelectedValue = "0";


                    if (monitor.inicio.HasValue)
                        txtInicio.Text = monitor.inicio.Value.ToShortDateString();

                    if (monitor.final.HasValue)
                        txtFinal.Text = monitor.final.Value.ToShortDateString();

                    BolsistaBO bolsistaBO = new BolsistaBO();
                    Bolsista bolsista = bolsistaBO.GetBolsistaPorId(monitor.bolsistaUID.Value);


                    rdoStatus.SelectedValue = this.VerificaSituacao(bolsista, monitor).ToString();

                    AlunoBO alunoBO = new AlunoBO();
                    vwAlunoSiga vwaluno = alunoBO.GetAluno(decimal.Parse(bolsista.registroAluno));

                    Aluno aluno = new Aluno();
                    aluno.Campus = vwaluno.Campus;
                    aluno.CodCampus = vwaluno.CodCampus;
                    aluno.CodCurso = vwaluno.CodCurso;
                    aluno.CPF = vwaluno.CPF;
                    aluno.Curso = vwaluno.Curso;
                    aluno.Email = vwaluno.Email;
                    aluno.Matricula = vwaluno.Matricula;
                    aluno.Nome = vwaluno.Nome;
                    aluno.Tipo = vwaluno.Tipo;

                    UCDadosBancariosAluno1.aluno = aluno;
                    UCDadosBancariosAluno1.Agencia = bolsista.agencia;
                    UCDadosBancariosAluno1.Conta = bolsista.numeroConta;

                    if (bolsista.bancoUID.HasValue)
                        UCDadosBancariosAluno1.BancoUID = bolsista.bancoUID.Value;



                    UCDadosBancariosAluno1.SetDadosBancarios();


                    CursoBO cursoBO = new CursoBO();
                    ufmt.sig.entity.MatrizDisciplinar matriz = cursoBO.GetMatrizDisciplinar(int.Parse(monitor.disciplinaUID.Value.ToString()));

                    UCBuscarDisciplinas1.rga = aluno.Matricula.ToString();
                    UCBuscarDisciplinas1.media = 5.0;
                    UCBuscarDisciplinas1.nomeCurso = aluno.Curso;

                    ((Panel)UCBuscarDisciplinas1.FindControl("pnlGridview")).Visible = false;
                    ((Panel)UCBuscarDisciplinas1.FindControl("pnlDisciplina")).Visible = true;

                    ((Label)UCBuscarDisciplinas1.FindControl("lblCodigoDiscplina")).Text = matriz.CodigoExterno.Value.ToString();
                    ((Label)UCBuscarDisciplinas1.FindControl("lblNomeDisciplina")).Text = matriz.Nome;

                    ((HiddenField)UCBuscarDisciplinas1.FindControl("hdMatrizDisciplinarUID")).Value = matriz.MatrizDisciplinarUID.ToString();
                    UCBuscarDisciplinas1.disciplina = matriz;

                    BolsistaController controller = new BolsistaController();
                    BolsistaFolhaPagamento bfp = controller.GetBolsistaPorFolhaPagamento(monitor.bolsistaUID.Value, GetFolhaPagamentoUID());

                    if (bfp != null)
                    {
                        if (bfp.ativo)
                        {
                            rdoStatus.SelectedValue = "1";
                        }
                        else
                        {
                            rdoStatus.SelectedValue = "0";
                        }
                        TextJustificativaSituacao.Text = bfp.justificativaSituacao;
                    }
                    else
                    {
                        rdoStatus.SelectedValue = "0";
                    }

                    

                }
            }
        }
        protected int GetMonitorUID()
        {
            if (String.IsNullOrEmpty(Request.QueryString["monitorUID"]))
            {
                int bolsistaUID = int.Parse(Request.QueryString["bolsistaUID"]);
                int processoSeletivoUID = int.Parse(Request.QueryString["processoSeletivoUID"]);

                return new MonitorBO().GetMonitorPorBolsistaProcessoSeletivo(bolsistaUID, processoSeletivoUID).monitorUID;
            }
            else
            {
                return int.Parse(Request.QueryString["monitorUID"]);
            }
        }

        protected Monitor GetMonitorPorBolsista()
        {
            int bolsistaUID = int.Parse(Request.QueryString["bolsistaUID"]);
            int processoSeletivoUID = int.Parse(Request.QueryString["processoSeletivoUID"]);

            return new MonitorBO().GetMonitorPorBolsistaProcessoSeletivo(bolsistaUID, processoSeletivoUID);
        }

        private Monitor GetMonitor()
        {
            int monitorUID = int.Parse(Request.QueryString["monitorUID"]);
            return new MonitorBO().GetMonitor(monitorUID);
        }

        protected void lnkOrientador_Click(object sender, EventArgs e)
        {
            pnlDadosOrientador.Visible = false;

            UCBuscarOrientador.labelBuscaServidor = "Buscar Orientador";
            pnlOrientador.Visible = true;
        }

        protected void lnkCoordenador_Click(object sender, EventArgs e)
        {
            pnlDadosCoordenador.Visible = false;

            UCBuscarCoordenador.labelBuscaServidor = "Buscar Coordenador";
            pnlCoordenador.Visible = true;
        }

        private int VerificaSituacao(Bolsista bolsista, Monitor monitor)
        {
            if (monitor.remunerada.Value)
            {
                BolsistaProcessoSeletivoBO bpsBO = new BolsistaProcessoSeletivoBO();
                BolsistaProcessoSeletivo bps =
                    bpsBO.GetBolsistaProcessoSeletivo(bolsista.bolsistaUID, GetProcessoSeletivoUID());

                if(bps.situacao.HasValue)
                    return bps.situacao.Value;
                else
                    return 0;
            }
            else
            {
                if (monitor.ativo.HasValue)
                    return Convert.ToInt32(monitor.ativo.Value);
                else
                    return 0;
            }
        }

        private int GetProcessoSeletivoUID()
        {
            return int.Parse(Request.QueryString["processoSeletivoUID"]);
        }

        private int GetFolhaPagamentoUID()
        {
            return int.Parse(Request.QueryString["folhaPagamentoUID"]);
        }

        protected void btAtualizar_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(Request.QueryString["monitorUID"]))
            {
                this.AtualizarDadosMonitor();
            }
            else
            {
                if (rdoModalidade.SelectedValue.Equals("2"))
                {
                    MdlFolhaPagamento.Show();
                }
                else
                {
                    this.AtualizarDadosMonitor();
                }
            }
        }

        private void AtualizarDadosMonitor()
        {
            try
            {
                BolsistaBO bolsistaBO = new BolsistaBO();

                Monitor monitor = new Monitor();
                if (String.IsNullOrEmpty(Request.QueryString["monitorUID"]))
                    monitor = this.GetMonitorPorBolsista();
                else
                    monitor = this.GetMonitor();

                Servidor orientador = null;
                try
                {
                    orientador = UCBuscarOrientador.GetServidor();
                }
                catch (Exception ex)
                {
                    orientador = new Servidor();
                    orientador.ServidorUID = monitor.orientadorUID.Value;
                }

                Servidor coordenador = null;
                try
                {
                    coordenador = UCBuscarCoordenador.GetServidor();
                }
                catch (Exception ex)
                {
                    coordenador = new Servidor();
                    coordenador.ServidorUID = monitor.coordenadorUID.Value;
                }

                Monitor newMonitor = new Monitor();
                newMonitor.cursoUID = monitor.cursoUID;
                newMonitor.bolsistaUID = monitor.bolsistaUID;

                int disciplinaUID = int.Parse(((HiddenField)UCBuscarDisciplinas1.FindControl("hdMatrizDisciplinarUID")).Value);

                newMonitor.disciplinaUID = disciplinaUID;
                if (rdoModalidade.SelectedItem.Value == "2")
                {
                    newMonitor.remunerada = true;
                }
                else
                {                                        
                    newMonitor.remunerada = false;
                    
                    //CASO MUDE O VINCULO DE REMUNERADO PARA VOLUNTÁRIO
                    if (monitor.remunerada.Value)
                    {
                        if(monitor.bolsistaUID.HasValue)
                            bolsistaBO.AlterarVinculoMonitoria(monitor.bolsistaUID.Value);
                    }
                }

                if (rdoPeriodo.SelectedItem.Value == "1")
                    newMonitor.periodoAtuacao = "1";
                else
                {
                    if (rdoPeriodo.SelectedItem.Value == "2")
                        newMonitor.periodoAtuacao = "2";
                    else
                        newMonitor.periodoAtuacao = "3";
                }

                if (!String.IsNullOrEmpty(txtInicio.Text))
                    newMonitor.inicio = Convert.ToDateTime(txtInicio.Text);
                if (!String.IsNullOrEmpty(txtFinal.Text))
                    newMonitor.final = Convert.ToDateTime(txtFinal.Text);
                newMonitor.ativo = false;
                newMonitor.processoSeletivoUID = monitor.processoSeletivoUID;
                newMonitor.obs = monitor.obs;

                
                Bolsista bolsista = bolsistaBO.GetBolsistaPorId(monitor.bolsistaUID.Value);


                if (rdoStatus.SelectedItem.Value == "1")
                    newMonitor.ativo = true;
                else
                    newMonitor.ativo = false;

                //CASO SEJA REMUNERADO, ATUALIZA REGISTRO DE BOLSISTAPROCESSOSELETIVO
                if (String.IsNullOrEmpty(Request.QueryString["monitorUID"]))
                    AtualizarBolsistaProcessoSeletivo(bolsista);

                bolsista.agencia = UCDadosBancariosAluno1.Agencia;
                bolsista.numeroConta = UCDadosBancariosAluno1.Conta;

                TextBox txtEmail = (TextBox)UCDadosBancariosAluno1.FindControl("txtEmailAluno");
                bolsista.email = txtEmail.Text;

                if (UCDadosBancariosAluno1.BancoUID != 0)
                    bolsista.bancoUID = UCDadosBancariosAluno1.BancoUID;
                else
                    bolsista.bancoUID = null;

                AlunoBO alunoBO = new AlunoBO();
                vwAlunoSiga vwaluno = alunoBO.GetAluno(decimal.Parse(bolsista.registroAluno));

                bolsista.registroAluno = vwaluno.Matricula.ToString();

                MonitorBO monitorBO = new MonitorBO();

                newMonitor.status = true;
                monitorBO.Atualizar(GetMonitorUID(), newMonitor, orientador, coordenador);
                bolsistaBO.AtualizarBolsista(bolsista.bolsistaUID, bolsista.email, bolsista.numeroConta, bolsista.bancoUID, bolsista.agencia);

                Response.Redirect(hdUrlRedirect.Value, false);

            }
            catch (Exception ex)
            {
                PanelMsgResultado.Visible = true;
                lblMsgResultado.Text = "Não foi possível atualizar o registro. " + ex.Message;
            }
        }

        private void AtualizarBolsistaProcessoSeletivo(Bolsista bolsista)
        {
            /*

            BolsistaProcessoSeletivoBO bpsBO = new BolsistaProcessoSeletivoBO();
            BolsistaProcessoSeletivo bps = bpsBO.GetBolsistaProcessoSeletivo(bolsista.bolsistaUID,
                                                                            GetProcessoSeletivoUID());
            
            if (rdoStatus.SelectedItem.Value == "1")
                bps.situacao = 1;
            else
                bps.situacao = 0;



            bpsBO.AtualizarSituacaoBolsista(bolsista.bolsistaUID, GetProcessoSeletivoUID(), GetFolhaPagamentoUID(), bps.situacao.Value);
             */

            int situacao = 0;
            if (rdoStatus.SelectedItem.Value == "1")
                situacao = 1;

            BolsistaController controller = new BolsistaController();
            controller.AtualizarSituacaoBolsista(bolsista.bolsistaUID, GetFolhaPagamentoUID(), situacao, TextJustificativaSituacao.Text);
        }

        private void Homologar(int folhaPagamentoUID)
        {
            BolsistaBO bolsistaBO = new BolsistaBO();

            Monitor monitor = this.GetMonitor();
            Bolsista bolsista = bolsistaBO.GetBolsistaPorId(monitor.bolsistaUID.Value);

            string semestre = "0";
            if(bolsista.Monitor.FirstOrDefault().periodoAtuacao.Equals("1") || 
                bolsista.Monitor.FirstOrDefault().periodoAtuacao.Equals("3"))
            {
                semestre = "1";
            } else 
                if(bolsista.Monitor.FirstOrDefault().periodoAtuacao.Equals("2")) 
                {
                    semestre = "2";
                }

            bolsistaBO.HomologarBolsistas(bolsista.bolsistaUID, GetProcessoSeletivoUID(), folhaPagamentoUID, semestre);
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            MdlFolhaPagamento.Hide();
        }

        protected void LinkButtonFechar_Click(object sender, EventArgs e)
        {
            MdlFolhaPagamento.Hide();
        }

        protected void grdFolhasPagamento_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            this.AtualizarDadosMonitor();

            int folhaPagamentoUID = int.Parse(e.CommandArgument.ToString());
            this.Homologar(folhaPagamentoUID);
        }
    }
}