﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCBuscarDisciplinas.ascx.cs" Inherits="ufmt.ssbic.View.controles.UCBuscarDisciplinas" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<style>
.mGrid { 
    width: 100%; 
    background-color: #fff; 
    margin: 5px 0 10px 0; 
    border: solid 1px #525252; 
    border-collapse:collapse; 
}
.mGrid td { 
    padding: 2px; 
    border: solid 1px #c1c1c1; 
    color: #717171; 
}
.mGrid th { 
    padding: 4px 2px; 
    color: #fff; 
    background: #424242 url(../../media/images/grd_head.png) repeat-x top; 
    border-left: solid 1px #525252; 
    font-size: 0.9em; 
}
.mGrid .alt { background: #fcfcfc url(../../media/images/grd_alt.png) repeat-x top; }
.mGrid .pgr { background: #424242 url(../../media/images/grd_pgr.png) repeat-x top; }
.mGrid .pgr table { margin: 5px 0; }
.mGrid .pgr td { 
    border-width: 0; 
    padding: 0 6px; 
    border-left: solid 1px #666; 
    font-weight: bold; 
    color: #fff; 
    line-height: 12px; 
 }   
.mGrid .pgr a { color: #666; text-decoration: none; }
.mGrid .pgr a:hover { color: #000; text-decoration: none; }
.minibutton {
    background: url("../../media/images/minibutton_matrix.png") no-repeat scroll 0 0 transparent;
    border: medium none;
    color: #333333 !important;
    cursor: pointer;
    display: inline-block;
    font-size: 11px;
    font-weight: bold;
    height: 23px;
    margin-bottom: 5px;
    overflow: visible;
    padding: 0 0 0 3px;
    text-decoration: none !important;
    text-shadow: 1px 1px 0 #FFFFFF;
    white-space: nowrap;
}
.containerFormulario { margin-bottom:10px; margin-top:10px; padding-left:10px;} 
</style>

<div class="containerFormulario">
<div style="background-color:#cccccc;font-weight:bold;padding:10px;width:98%">
        SELECIONE A DISCIPLINA&nbsp;
        </div>
        <p>
            Curso: <asp:Label ID="lblCurso" runat="server"></asp:Label>
        </p>
<asp:Panel ID="pnlGridview" runat="server">

<asp:GridView ID="grdMatrizes" runat="server" AutoGenerateColumns="False" CssClass="mGrid"
    DataSourceID="ObjectDataSource1" Width="100%" 
    DataKeyNames="codigoDisciplina" onrowcommand="grdMatrizes_RowCommand">
    <Columns>
        <asp:TemplateField HeaderText="Selecionar" ItemStyle-Width="150px" ItemStyle-HorizontalAlign="Center">
            <ItemTemplate>
                <asp:LinkButton ID="LinkButton1" runat="server" 
                    CommandArgument='<%# Eval("codigoDisciplina") %>' 
                    CommandName="adicionar" CssClass="minibutton" onclick="LinkButton1_Click">
                    <span>
                        <img src="../../media/images/select.png" alt="" />
                        Selecionar
                    </span>
                </asp:LinkButton>
            </ItemTemplate>

<ItemStyle HorizontalAlign="Center" Width="150px"></ItemStyle>
        </asp:TemplateField>
        <asp:BoundField DataField="nomeDisciplina" HeaderText="Nome" 
            SortExpression="nomeDisciplina" />
        <asp:BoundField DataField="codigoDisciplina" 
            HeaderText="Código da Disciplina" 
            SortExpression="codigoDisciplina" ItemStyle-Width="100px"  
            ItemStyle-HorizontalAlign="Center" >
<ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
        </asp:BoundField>
        <asp:BoundField DataField="nota" HeaderText="Nota" ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center"
            SortExpression="nota" >
<ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
        </asp:BoundField>
    </Columns>
</asp:GridView>
<asp:HiddenField ID="hdRga" runat="server" />
<asp:HiddenField ID="hdMedia" runat="server" />
<asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
    SelectMethod="GetDisciplinasAprovadas" 
    TypeName="ufmt.ssbic.Business.CursoBO">
    <SelectParameters>
        <asp:ControlParameter ControlID="hdRga" Name="rga" 
            PropertyName="Value" Type="String" />
        <asp:ControlParameter ControlID="hdMedia" Name="media" 
            PropertyName="Value" Type="Double" />
    </SelectParameters>
</asp:ObjectDataSource>
</asp:Panel>

<asp:Panel ID="pnlDisciplina" runat="server" Visible="false">
<p>
Código:<asp:Label ID="lblCodigoDiscplina" runat="server"></asp:Label>
</p>
<p>
Disciplina:<asp:Label ID="lblNomeDisciplina" runat="server"></asp:Label>
</p>
<p>
    <asp:LinkButton ID="lnkNovaBusca" runat="server" onclick="lnkNovaBusca_Click">Selecionar outra disciplina</asp:LinkButton>
</p>
    <asp:HiddenField ID="hdMatrizDisciplinarUID" runat="server" />
</asp:Panel>
</div>
</ContentTemplate>
</asp:UpdatePanel>

