﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.sig.entity;

namespace ufmt.ssbic.View.controles
{
    public partial class UCBuscarDisciplinas : System.Web.UI.UserControl
    {
        public String rga;
        public double media;
        public String nomeCurso;

        public MatrizDisciplinar disciplina;        
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(rga))
            {
                this.SetDados(rga, media, nomeCurso);
            }
        }

        public void SetDados(String rga, double media, String nomeCurso)
        {
            this.rga = rga;
            this.media = media;
            this.nomeCurso = nomeCurso;
            this.LoadDados();
        }

        public void LoadDados()
        {
            if (!String.IsNullOrEmpty(rga) & media != 0)
            {
                hdRga.Value = this.rga;
                hdMedia.Value = this.media.ToString();

                lblCurso.Text = nomeCurso;

                grdMatrizes.DataBind();
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {

        }

        protected void grdMatrizes_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int codigoDisciplina = int.Parse(e.CommandArgument.ToString());
                
                CursoBO cursoBO = new CursoBO();
                ufmt.ssbic.DataAccess.MatrizDisciplinar matriz = cursoBO.GetDisciplina(codigoDisciplina);
                if (matriz != null)
                {
                    pnlGridview.Visible = false;
                    pnlDisciplina.Visible = true;

                    lblCodigoDiscplina.Text = matriz.codigoExterno.ToString();
                    lblNomeDisciplina.Text = matriz.nome;

                    hdMatrizDisciplinarUID.Value = matriz.matrizDisciplinarUID.ToString();
                    disciplina = getMatrizDisciplinar();
                }
                else
                {
                    throw new Exception("Disciplina não existe no banco de dados atualizado.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void lnkNovaBusca_Click(object sender, EventArgs e)
        {
            pnlGridview.Visible = true;
            pnlDisciplina.Visible = false;
        }


        public MatrizDisciplinar getMatrizDisciplinar()
        {            
            CursoBO cursoBO = new CursoBO();
            MatrizDisciplinar matrizDisciplinar = cursoBO.GetMatrizDisciplinar(long.Parse(hdMatrizDisciplinarUID.Value));

            return matrizDisciplinar;
        }

        


    }
}