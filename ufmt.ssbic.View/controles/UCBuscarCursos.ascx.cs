﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.sig.entity;

namespace ufmt.ssbic.View.controles
{
    public partial class UCBuscarCursos : System.Web.UI.UserControl
    {
        public Curso curso;


        protected bool ehLetra(char caracter)
        {
            if ((caracter >= 'A' && caracter <= 'Z') || (caracter >= 'a' && caracter <= 'z'))
                return true;
            else
                return false;
        }

        protected bool ehDigito(char caracter)
        {
            if (caracter >= '0' && caracter <= '9')
                return true;
            else
                return false;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btBuscar_Click(object sender, EventArgs e)
        {
            if ((opcaoDropdown.SelectedValue == "Código") && (ehDigito(txtBuscaCurso.Text[0])))
            {
                GridView1.DataSourceID = "ObjectDataSource3";
                GridView1.DataBind();
            }
            else
                GridView1.DataSourceID = "ObjectDataSource2";
            GridView1.DataBind();
            
        }

        protected void ddlCampus_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridView1.DataBind();
        }

        protected void btMostrarTodos_Click(object sender, EventArgs e)
        {
            txtBuscaCurso.Text = "";
            GridView1.DataBind();
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "selecionar")
            {
                long cursoUID = long.Parse(e.CommandArgument.ToString());
                
                hdCursoUID.Value = cursoUID.ToString();
                this.curso = this.GetCurso();

                pnlBusca.Visible = false;

                pnlDados.Visible = true;
                lblCampus.Text = ddlCampus.SelectedItem.Text;
                lblCurso.Text = this.curso.Nome;
            }
        }

        public Curso GetCurso()
        {
            if (String.IsNullOrEmpty(hdCursoUID.Value))
                throw new Exception("Nenhum curso foi selecionado");

            CursoBO cursoBO = new CursoBO();
            return cursoBO.GetCurso(long.Parse(hdCursoUID.Value));            
        }

        public bool TemCurso()
        {
            return !String.IsNullOrEmpty(hdCursoUID.Value);
        }

        protected void lnkSelecionar_Click(object sender, EventArgs e)
        {

        }

        protected void lnkNovaBusca_Click(object sender, EventArgs e)
        {
            pnlDados.Visible = false;
            pnlBusca.Visible = true;
        }
    }
}