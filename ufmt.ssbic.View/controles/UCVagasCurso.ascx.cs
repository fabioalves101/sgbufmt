﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.sig.entity;
using System.Collections;

namespace ufmt.ssbic.View.controles
{
    public partial class UCVagasCurso : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["vagasPorCurso"] != null)
            {
                List<CursoQtdeVagas> vagasPorCurso = (List<CursoQtdeVagas>)Session["vagasPorCurso"];
                grdVagas.DataSource = vagasPorCurso;
                grdVagas.DataBind();
            }
        }

        protected void btAdicionar_Click(object sender, EventArgs e)
        {


            if (!String.IsNullOrEmpty(txtVagas.Text) && (int.Parse(txtVagas.Text) > 0))
            {
                
                if (UCBuscarCursos1.TemCurso())
                {
                    Curso curso = UCBuscarCursos1.GetCurso();
                    int quantidadeVagas = int.Parse(txtVagas.Text);
                    this.SetSession(curso, quantidadeVagas);
                }
                else
                {
                    PanelMsgResultado.Visible = true;
                    lblMsgResultado.Text = "Não há curso selecionado. Selecione um curso.";
                }
            }
            else
            {
                PanelMsgResultado.Visible = true;
                lblMsgResultado.Text = "Número de vagas inválido. Preencha corretamente";
            }
        }

        public void SetSession(Curso curso, int qtdeVagas)
        {
            CursoQtdeVagas cursoQtdeVagas = new CursoQtdeVagas();
            cursoQtdeVagas.curso = curso;
            
            cursoQtdeVagas.qtdeVagas = qtdeVagas;
            List<CursoQtdeVagas> vagasPorCurso = null;

            if (Session["vagasPorCurso"] != null)
            {
                vagasPorCurso = (List<CursoQtdeVagas>) Session["vagasPorCurso"];
                vagasPorCurso.Add(cursoQtdeVagas);
            }
            else
            {
                vagasPorCurso = new List<CursoQtdeVagas>();
                vagasPorCurso.Add(cursoQtdeVagas);
                
                Session["vagasPorCurso"] = vagasPorCurso;
                grdVagas.DataSource = vagasPorCurso;
            }

            grdVagas.DataSource = vagasPorCurso;
            grdVagas.DataBind();
        }

        //Retorna a quantidade de rows na grdvagas. Pra testar antes da pessoa clicar em salva.
        public int getRows()
        {
            return grdVagas.Rows.Count;
        }

        protected void grdVagas_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "remover")
            {
                long cursoUID = long.Parse(e.CommandArgument.ToString());
                List<CursoQtdeVagas> vagasPorCurso = (List<CursoQtdeVagas>)Session["vagasPorCurso"];

                foreach (CursoQtdeVagas cursoqtde in vagasPorCurso)
                {
                    if (cursoqtde.curso.CursoUID == cursoUID)
                    {
                        vagasPorCurso.Remove(cursoqtde);
                        break;
                    }
                }

                grdVagas.DataBind();
            }
        }

        protected void grdVagas_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {
            grdVagas.DataBind();
        }
    }

    class CursoQtdeVagas
    {
        public Curso curso { get; set;}
        public int qtdeVagas { get; set;}
    }
}