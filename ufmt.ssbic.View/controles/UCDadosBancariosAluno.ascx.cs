﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.View.controles
{
    public partial class UCDadosBancariosAluno : System.Web.UI.UserControl
    {
        public Aluno aluno;

        private int bancoUID;

        public int BancoUID
        {
            get { return int.Parse(ddlBanco.SelectedValue); }
            set { bancoUID = value; }
        }
        private String agencia;

        public String Agencia
        {
            get { return txtAgencia.Text; }
            set { agencia = value; }
        }
        private String conta;

        public String Conta
        {
            get { return txtContaCorrente.Text; }
            set { conta = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {          
            
            if (aluno != null)
            {
                lblNomeAluno.Text = aluno.Nome;
                lblMatricula.Text = aluno.Matricula.ToString();
                lblCurso.Text = aluno.Curso;
                lblCpf.Text = aluno.CPF;
                lblCampus.Text = aluno.Campus;
                txtEmailAluno.Text = aluno.Email;
            }           
        }

        public void SetAluno(Aluno objAluno)
        {
            this.aluno = objAluno;

            lblNomeAluno.Text = aluno.Nome;
            lblMatricula.Text = aluno.Matricula.ToString();
            lblCurso.Text = aluno.Curso;
            lblCpf.Text = aluno.CPF;
            lblCampus.Text = aluno.Campus;
            txtEmailAluno.Text = aluno.Email;    
        }

        public void SetDadosBancarios()
        {
            txtAgencia.Text = agencia;
            txtContaCorrente.Text = conta;
            if (bancoUID != 0)
                ddlBanco.SelectedValue = bancoUID.ToString();
            else
                ddlBanco.SelectedValue = "0";
        }
        
    }
}