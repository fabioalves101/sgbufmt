﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCBreadcrumbFinanceiro.ascx.cs" Inherits="ufmt.ssbic.View.controles.UCBreadcrumbFinanceiro" %>
<asp:Panel ID="pnlBreadCrumb" CssClass="breadcrumb" runat="server">
        <asp:HyperLink ID="hpInstrumentoSelecao" runat="server">Instrumentos de Seleção</asp:HyperLink>
        <asp:HyperLink ID="hpFolhaPagamento" runat="server">&nbsp;&gt;&gt; Folhas de Pagamento</asp:HyperLink>
         
        <asp:HyperLink ID="hpBolsista" runat="server">&nbsp;&gt;&gt; Bolsistas</asp:HyperLink>
</asp:Panel>