﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCDadosMonitor.ascx.cs" Inherits="ufmt.ssbic.View.controles.UCDadosMonitor" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register src="UCBuscarServidor.ascx" tagname="UCBuscarServidor" tagprefix="uc1" %>

<%@ Register src="UCDadosBancariosAluno.ascx" tagname="UCDadosBancariosAluno" tagprefix="uc2" %>

<%@ Register src="UCBuscarDisciplinas.ascx" tagname="UCBuscarDisciplinas" tagprefix="uc3" %>

<div id="containerMensagens">
    <asp:Panel ID="PanelMsgResultado" runat="server" Visible="false">
        <asp:Label ID="lblMsgResultado" runat="server"></asp:Label>
    </asp:Panel>
        <div>
            <fieldset>
                <legend>Professor Orientador:</legend>
                <asp:Panel ID="pnlDadosOrientador" runat="server">
                    <p>
                    <asp:Label ID="lblOrientador" runat="server"></asp:Label>
                    </p>
                    <p>
                    <asp:LinkButton ID="lnkOrientador" CausesValidation="false" runat="server" onclick="lnkOrientador_Click">Alterar</asp:LinkButton>
                    </p>
                </asp:Panel>
                <asp:Panel ID="pnlOrientador" runat="server" Visible="false">
                    <uc1:UCBuscarServidor ID="UCBuscarOrientador" runat="server" />
                    
                    
                </asp:Panel>
            </fieldset>
        </div>
        <div>
            <fieldset>
                <legend>Coordenador:</legend>
                <asp:Panel ID="pnlDadosCoordenador" runat="server">
                    <p>
                    <asp:Label ID="lblCoordenador" runat="server"></asp:Label>
                    </p>
                    <p>
                    <asp:LinkButton ID="lnkCoordenador" CausesValidation="false" runat="server" onclick="lnkCoordenador_Click">Alterar</asp:LinkButton>
                    </p>
                </asp:Panel>
                <asp:Panel ID="pnlCoordenador" runat="server" Visible="false">
                    <uc1:UCBuscarServidor ID="UCBuscarCoordenador" runat="server" />
                    
                </asp:Panel>

            </fieldset>
        </div>
        <div>
        <fieldset>
                <legend>Modalidade:</legend>
                <asp:RadioButtonList ID="rdoModalidade" runat="server" 
                    RepeatDirection="Horizontal" Width="500px">
                    <asp:ListItem Value="1">Voluntária</asp:ListItem>
                    <asp:ListItem Value="2">Remunerada</asp:ListItem>
                </asp:RadioButtonList>
            </fieldset>
        </div>
        <div>
            <fieldset>
                <legend>Período:</legend>
                <asp:RadioButtonList ID="rdoPeriodo" runat="server" 
                    RepeatDirection="Horizontal" Width="300px">
                    <asp:ListItem Value="1">1° Semestre</asp:ListItem>
                    <asp:ListItem Value="2">2° Semestre</asp:ListItem>
                    <asp:ListItem Value="3">Anual</asp:ListItem>
                </asp:RadioButtonList>
            </fieldset>
        </div>
        <div>
            <fieldset>
                <legend>Atuação:</legend>
                <p>
                Início: 
                    <asp:TextBox ID="txtInicio" runat="server"></asp:TextBox>
                </p>
                <p>
                Final: 
                    <asp:TextBox ID="txtFinal" runat="server"></asp:TextBox>
                </p>
                
            </fieldset>
        </div>

        <div>

            <uc2:UCDadosBancariosAluno ID="UCDadosBancariosAluno1" runat="server" />

        </div>
        <div>

            <asp:Panel ID="pnlDisciplinas" runat="server" Visible="true">
                <uc3:UCBuscarDisciplinas ID="UCBuscarDisciplinas1" runat="server" />
            

            
            </asp:Panel>
            
        </div>
        <div>
            <fieldset>
                <legend>Situação:</legend>
                <p>
                <asp:RadioButtonList ID="rdoStatus" runat="server" 
                    RepeatDirection="Horizontal" Width="300px">
                    <asp:ListItem Value="1">Ativo</asp:ListItem>
                    <asp:ListItem Value="0">Inativo</asp:ListItem>
                </asp:RadioButtonList>
                </p>
                <p>
                Justificativa de Situação:<br />
                    <asp:TextBox ID="TextJustificativaSituacao" runat="server" TextMode="MultiLine" 
                    Height="69px" Width="424px"></asp:TextBox>
                </p>
            </fieldset>
        </div>
        <div style="text-align:center">
            <asp:Button ID="btAtualizar" runat="server" Text="Atualizar" Height="30px" 
                onclick="btAtualizar_Click" Width="193px" />
        
        </div>

</div>
<asp:HiddenField ID="hdUrlRedirect" runat="server" />

    <asp:modalpopupextender 
                ID="MdlFolhaPagamento"
                BehaviorID="behavior"
                runat="server"
                TargetControlID="lnkModal"
                PopupControlID="pnlModal"
                BackgroundCssClass="modalBackground"
                DropShadow="True"
                DynamicServicePath="" 
                Enabled="True"
        
        />


        <asp:HyperLink ID="lnkModal" runat="server" Visible="true" Text=""  />

 <asp:Panel ID="pnlModal" runat="server" CssClass="modalPopup">
 <div id="ContainerDetalhes" style="width:800px">
          <div id="MenuDetalhes" style="background-color:White;padding-left:5px;padding-bottom:5px;height:40px">
            <div style="width:650px; float:left; height: 32px;">
                <h2 style="height: 35px">Escolha a folha de pagamento</h2>
            </div>
            <div style="float:left;width:130px;text-align:right; padding-top:10px;">
            <asp:ImageButton ID="ImageButton1" runat="server" 
                    ImageUrl="~/images/close24.png" 
                    OnclientClick="$find('behavior').hide(); return false;" ToolTip="Fechar Janela" 
                    ImageAlign="AbsMiddle" onclick="ImageButton1_Click" />
                &nbsp;<asp:LinkButton ID="LinkButtonFechar" runat="server" Visible="true" 
                    Text="Fechar" onclick="LinkButtonFechar_Click"   />
            </div>
            </div>
     <asp:GridView ID="grdFolhasPagamento" runat="server" AutoGenerateColumns="False" 
              DataSourceID="odsFolhaPagamento" Width="100%" 
              onrowcommand="grdFolhasPagamento_RowCommand">
         <Columns>
             <asp:TemplateField HeaderText="Selecionar">
                <ItemStyle Width="80px" />
                <ItemTemplate>
                    <asp:LinkButton ID="lnkHomologarTodos" runat="server" CausesValidation="False" 
                                            CommandArgument='<%# Eval("folhaPagamentoUID") %>' 
                                            CommandName="homologar" CssClass="minibutton">
                                            <span>
                                                <img src="~/images/select.png" alt="" />
                                                Selecionar
                                            </span>
                    </asp:LinkButton>
                </ItemTemplate>
             </asp:TemplateField>
             <asp:BoundField DataField="dataInicio" HeaderText="Início" 
                 SortExpression="dataInicio" />
             <asp:BoundField DataField="dataFim" HeaderText="Fim" SortExpression="dataFim" />
         </Columns>
     </asp:GridView>
     <asp:ObjectDataSource ID="odsFolhaPagamento" runat="server" 
              SelectMethod="GetFolhaPagamentoProcessoSeletivo" 
              TypeName="ufmt.ssbic.Business.FolhaPagamentoBO">
         <SelectParameters>
             <asp:QueryStringParameter Name="processoSeletivoUID" 
                 QueryStringField="processoSeletivoUID" Type="Int32" />
         </SelectParameters>
          </asp:ObjectDataSource>
     <asp:HiddenField ID="hdBolsistaUID" runat="server" />
</div>
 </asp:Panel>

