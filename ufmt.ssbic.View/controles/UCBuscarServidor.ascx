﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCBuscarServidor.ascx.cs" Inherits="ufmt.ssbic.View.controles.UCBuscarServidor" %>
<style>
 .containerFormulario { margin-bottom:10px; margin-top:10px; padding-left:10px;} 
 .center { text-align:center }
</style>

    <div id="containerMensagens">
                <asp:Panel ID="PanelMsgResultadoUC" runat="server" Visible="false">
                    <asp:Label ID="lblMsgResultadoUC" runat="server"></asp:Label>
                </asp:Panel>
    </div>    

    <div class="containerFormulario">
        <asp:Panel ID="pnlInfoServidor" runat="server" Visible="false">
        <fieldset>
            <legend>Servidor Selecionado:</legend>        
        <p>
            Nome: <asp:Label ID="lblnome" runat="server" Text="Label"></asp:Label>
        </p>
        
        <p>
            Matrícula: <asp:Label ID="lblmatricula" runat="server" Text="Label"></asp:Label>        
            <asp:HiddenField ID="hdservidor" runat="server" />
        </p>
        <p>
            <asp:LinkButton CausesValidation="false" ID="lnkNovaBusca" runat="server" onclick="lnkNovaBusca_Click">Realizar Nova Busca</asp:LinkButton>
        </p>
        </fieldset>
        </asp:Panel>

    <asp:Panel ID="pnlServidor" runat="server">

    <div id="divConsultaServidor" runat="server">
            <asp:Panel ID="pnlBusca" runat="server">
        
            <asp:Label ID="lblBuscaServidor" runat="server" 
                Text="Localizar Servidor" style="font-weight: 700"></asp:Label>       
            <br />
            <asp:TextBox ID="txtBuscaServidor" runat="server" Width="400px"></asp:TextBox>
            &nbsp;<asp:DropDownList ID="ddlBuscaServidor" runat="server">
                <asp:ListItem>Nome</asp:ListItem>
                <asp:ListItem>CPF</asp:ListItem>
                <asp:ListItem Value="siape">Registro SIAPE</asp:ListItem>
            </asp:DropDownList>
            <asp:Button ID="btnBuscar" OnClick="btnBuscar_Click" runat="server" CausesValidation="false"
                Text="Buscar" />
            &nbsp;</asp:Panel>
<asp:Panel ID="pnlListaServidores" runat="server" Visible="False">
        Selecione o Servidor.
            <asp:GridView ID="gdrListaServidores" runat="server" 
                AutoGenerateColumns="False"
                AllowPaging="True" Width="100%"
                CssClass="mGrid"
                PagerStyle-CssClass="pgr"
                AlternatingRowStyle-CssClass="alt" DataKeyNames="ServidorUID" 
                onselectedindexchanged="gdrListaServidores_SelectedIndexChanged" onpageindexchanging="gdrListaServidores_PageIndexChanging"
                >            
                <AlternatingRowStyle CssClass="alt" />
                <Columns>                    
                    <asp:TemplateField HeaderText="Selecionar" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="120px">
                        <ItemTemplate>
                            <asp:LinkButton CssClass="minibutton" ID="LinkButton1" runat="server" CausesValidation="False" 
                                            CommandName="select">
                            <span>
                                <img src="../../media/images/select.png" alt="" />
                                Selecionar
                            </span>
                            </asp:LinkButton>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="120px" />
                    </asp:TemplateField>
                    
                    <asp:BoundField DataField="Registro" HeaderText="Registro SIAPE" ItemStyle-HorizontalAlign="Center" 
                        SortExpression="Registro" ItemStyle-Width="120px">
                    <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                        Nome do Servidor
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='  <%# Eval("Pessoa.Nome")%>'></asp:Label>
                        
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle CssClass="pgr" />
            </asp:GridView>
            
        </asp:Panel>        
    </div>    
    </asp:Panel>
    </div>
   