﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ufmt.ssbic.View.controles
{
    public partial class UCBreadcrumbFinanceiro : System.Web.UI.UserControl
    {
        public int? instrumentoSelecaoUID;
        public int? folhaPagamentoUID;

        protected void Page_Load(object sender, EventArgs e)
        {
            hpInstrumentoSelecao.NavigateUrl = "../webapp/financeiro/FrmInstrumentosSelecao.aspx";

            if (instrumentoSelecaoUID.HasValue)
            {
                hpFolhaPagamento.NavigateUrl = "../webapp/financeiro/FrmFolhasPagamento.aspx?is=" + instrumentoSelecaoUID;               
            } else
                hpFolhaPagamento.Visible = false;

            if (folhaPagamentoUID.HasValue)
            {
                hpBolsista.NavigateUrl = "../webapp/financeiro/FrmBolsistas.aspx?fp=" + folhaPagamentoUID;                
            } else
                hpBolsista.Visible = false;
        }
    }
}