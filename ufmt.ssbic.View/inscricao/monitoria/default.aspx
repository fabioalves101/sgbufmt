﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="ufmt.ssbic.View.inscricao.monitoria._default" %>
<%@ Register src="../../controles/UCBuscarServidor.ascx" tagname="UCBuscarServidor" tagprefix="uc1" %>
<%@ Register src="../../controles/UCBuscarAluno.ascx" tagname="UCBuscarAluno" tagprefix="uc2" %>
<%@ Register src="../../controles/UCDadosBancariosAluno.ascx" tagname="UCDadosBancariosAluno" tagprefix="uc3" %>
<%@ Register src="../../controles/UCBuscarDisciplinas.ascx" tagname="UCBuscarDisciplinas" tagprefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<h1>
    FICHA DE CADASTRAL DE MONITORIA
</h1>
    <asp:Panel ID="pnlAlerta" runat="server" CssClass="pnlAviso" Visible="false">
        <asp:Label ID="lblAlerta" runat="server"></asp:Label>
    </asp:Panel>


    <fieldset>
        <legend>Coordenador</legend>
        <uc1:UCBuscarServidor ID="UCBuscarServidor1" runat="server" />
    </fieldset>
    
    <fieldset>
        <legend>Orientador</legend>
        <uc1:UCBuscarServidor ID="UCBuscarServidor2" runat="server" />
    </fieldset> 
    <fieldset>
        <legend>Identificação do Aluno</legend>
        <uc2:UCBuscarAluno ID="UCBuscarAluno1" runat="server" />
        
        

        <asp:Panel ID="pnlDadosBancarios" runat="server" Visible="false">  
            <uc4:UCBuscarDisciplinas ID="UCBuscarDisciplinas1" runat="server" />
          
            <uc3:UCDadosBancariosAluno ID="UCDadosBancariosAluno1" runat="server" />        
        </asp:Panel>
    </fieldset>
    
    <fieldset>
        <legend>Modalidade de Monitoria</legend>
        <asp:RadioButtonList ID="rdoModalidade" runat="server" 
            RepeatDirection="Horizontal" Width="500px">
            <asp:ListItem Value="1">Voluntária</asp:ListItem>
            <asp:ListItem Value="2">Remunerada</asp:ListItem>
        </asp:RadioButtonList>
    </fieldset>
    <fieldset>
        <legend>Período de Atividade</legend>
        <asp:RadioButtonList ID="rdoPeriodo" runat="server" 
            RepeatDirection="Horizontal" Width="300px">
            <asp:ListItem Value="1">1° Semestre</asp:ListItem>
            <asp:ListItem Value="2">2° Semestre</asp:ListItem>
            <asp:ListItem Value="3">Anual</asp:ListItem>
        </asp:RadioButtonList>
    </fieldset>

    <fieldset>
    <legend>Observações</legend>
        <asp:TextBox ID="obsTextBox" runat="server" class="login" Height="130px" TextMode="MultiLine" Width="797px"></asp:TextBox>
    </fieldset>

    <asp:Panel ID="pnlButton" runat="server" Height="42px" HorizontalAlign="Center">
        <br />
        <asp:Button ID="btContinuar" runat="server" Text="Continuar" Height="29px" 
            onclick="btContinuar_Click" Width="205px" />
    </asp:Panel>
</asp:Content>