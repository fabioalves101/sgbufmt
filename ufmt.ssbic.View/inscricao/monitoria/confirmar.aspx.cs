﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.DataAccess;
using ufmt.sig.entity;
using ufmt.ssbic.Business;
using Stimulsoft.Report;
using Stimulsoft.Report.Web;


namespace ufmt.ssbic.View.inscricao.monitoria
{
    public partial class confirmar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int processoSeletivoUID = int.Parse(Request.QueryString["processo"]);

            Aluno aluno = (Aluno)Session["aluno"];
            Servidor coordenador = (Servidor)Session["coordenador"];
            Servidor orientador = (Servidor)Session["orientador"];
            int modalidade = int.Parse(Session["modalidade"].ToString());
            int periodo = int.Parse(Session["periodo"].ToString());
            int bancoUID = int.Parse(Session["banco"].ToString());
            String agencia = Session["agencia"].ToString();
            String conta = Session["conta"].ToString();
            String obs = Session["obs"].ToString();

            ufmt.sig.entity.MatrizDisciplinar disciplina = (ufmt.sig.entity.MatrizDisciplinar)Session["disciplina"];

            lblCampus.Text = aluno.Campus;
            lblData.Text = DateTime.Today.ToShortDateString();
            lblCurso.Text = aluno.Curso;

            lblCoordenador.Text = coordenador.Pessoa.Nome;
            lblMailCoordenador.Text = coordenador.Pessoa.Mail;

            lblOrientador.Text = orientador.Pessoa.Nome;
            lblMailOrientador.Text = orientador.Pessoa.Mail;

            lblNomeMonitor.Text = aluno.Nome;
            lblSemestre.Text = "Não informado";
            lblDataNascimento.Text = "Não informado";
            lblMailMonitor.Text = aluno.Email;
            lblMatricula.Text = aluno.Matricula.ToString();
            lblCpf.Text = aluno.CPF;

            String strModalidade = "Voluntária";

            if (modalidade != 1)
                strModalidade = "Remunerada";

            lblModalidade.Text = strModalidade;

            String strPeriodo = "Semestral";

            if (periodo == 1)
                strPeriodo = "1° Semestre";
            else
                if (periodo == 2)
                    strPeriodo = "2° Semestre";
                else
                    strPeriodo = "Anual";


            lblPeriodo.Text = strPeriodo;

            lblConta.Text = conta;
            lblAgencia.Text = agencia;
            lblBanco.Text = GetNomeBanco(bancoUID);

            lblCodDisciplina.Text = disciplina.CodigoExterno.ToString();
            lblDisciplina.Text = disciplina.Nome;
            obsLabel.Text = obs;
        }

        public String GetNomeBanco(int bancoUID)
        {
            try
            {
                BancoBO bancoBO = new BancoBO();

                if (bancoUID != 0)
                    return bancoBO.GetBanco(bancoUID).banco;
                else
                    return "Banco não informado";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void btConfirmar_Click(object sender, EventArgs e)
        {
            int processoSeletivoUID = int.Parse(Request.QueryString["processo"]);

            Aluno aluno = (Aluno)Session["aluno"];
            Servidor coordenador = (Servidor)Session["coordenador"];
            Servidor orientador = (Servidor)Session["orientador"];
            int modalidade = int.Parse(Session["modalidade"].ToString());
            int periodo = int.Parse(Session["periodo"].ToString());
            int bancoUID = int.Parse(Session["banco"].ToString());
            String agencia = Session["agencia"].ToString();
            String conta = Session["conta"].ToString();
            String obs = Session["obs"].ToString();

            ufmt.sig.entity.MatrizDisciplinar disciplina = (ufmt.sig.entity.MatrizDisciplinar)Session["disciplina"];

            //try
            //{
            //    MonitorBO monitorBo = new MonitorBO();
            //    int uid = monitorBo.Salvar(coordenador, orientador, modalidade, periodo, bancoUID, agencia, conta, aluno, disciplina, processoSeletivoUID);
            //    relatorios(uid);
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception(ex.Message);
            //}

            
                MonitorBO monitorBo = new MonitorBO();
                int uid = monitorBo.Salvar(coordenador, orientador, modalidade, periodo, bancoUID, agencia, conta, aluno, disciplina, processoSeletivoUID, obs);
                relatorios(uid);
            
        }

        private void relatorios(int uid)
        {
            //try
            //{
            //    StiReport report = new StiReport();
            //    report.Load(AppDomain.CurrentDomain.BaseDirectory + "reports\\fichaMonitoria.mrt");
            //    report.Compile();

            //    report.CompiledReport.DataSources["DSFicha"].Parameters["@uid"].ParameterValue = uid;
            //    StiReportResponse.ResponseAsPdf(this, report, true);
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception(ex.Message);
            //}

            
                StiReport report = new StiReport();
                report.Load(AppDomain.CurrentDomain.BaseDirectory + "reports\\fichaMonitoria.mrt");
                report.Compile();

                report.CompiledReport.DataSources["DSFicha"].Parameters["@uid"].ParameterValue = uid;
                StiReportResponse.ResponseAsPdf(this, report, true);
            
        }
    }
}