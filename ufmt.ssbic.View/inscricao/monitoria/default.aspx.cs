﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.View.controles;
using ufmt.sig.entity;
using ufmt.ssbic.DataAccess;
using ufmt.ssbic.Business;

namespace ufmt.ssbic.View.inscricao.monitoria
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            #region Controla Datas

            DateTime DataHoraInicio = DateTime.Parse(System.Configuration.ConfigurationManager.AppSettings["dataInicio"]);
            DateTime DataHoraBloqueio = DateTime.Parse(System.Configuration.ConfigurationManager.AppSettings["dataBloqueio"]);

            DateTime DataHoraAtual = DateTime.Now;

            if (DataHoraAtual < DataHoraInicio)
            {
                Response.Redirect("FrmManutencao.aspx");
            }

            if (DataHoraAtual > DataHoraBloqueio)
            {
                Response.Redirect("FrmManutencao.aspx");
            }

            #endregion

            int processoSeletivoUID = int.Parse(Request.QueryString["processo"]);

            UCBuscarServidor1.labelBuscaServidor = "Buscar Coordenador";
            UCBuscarServidor2.labelBuscaServidor = "Buscar Orientador";
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (UCBuscarAluno1.aluno.CPF != null)
            {
                UCDadosBancariosAluno1.aluno = UCBuscarAluno1.aluno;
                pnlDadosBancarios.Visible = true;
                UCDadosBancariosAluno1.SetAluno(UCBuscarAluno1.aluno);
                UCBuscarDisciplinas1.SetDados(UCBuscarAluno1.aluno.Matricula.ToString(), 5, UCBuscarAluno1.aluno.Curso);
            }
        }

        protected void btContinuar_Click(object sender, EventArgs e)
        {
            int processoSeletivoUID = int.Parse(Request.QueryString["processo"]);
            try
            {
                this.Validar();

                Servidor coordenador = UCBuscarServidor1.GetServidor();
                Servidor orientador = UCBuscarServidor2.GetServidor();
                Aluno aluno = UCBuscarAluno1.GetAluno();

                String agencia = UCDadosBancariosAluno1.Agencia;
                int bancoUID = UCDadosBancariosAluno1.BancoUID;
                String conta = UCDadosBancariosAluno1.Conta;

                ufmt.sig.entity.MatrizDisciplinar disciplina = UCBuscarDisciplinas1.getMatrizDisciplinar();

                int modalidade = int.Parse(rdoModalidade.SelectedValue);
                int periodo = int.Parse(rdoPeriodo.SelectedValue);
                string obs = obsTextBox.Text;

                Session["coordenador"] = coordenador;
                Session["orientador"] = orientador;
                Session["aluno"] = aluno;
                Session["disciplina"] = disciplina;
                Session["modalidade"] = modalidade;
                Session["periodo"] = periodo;
                Session["banco"] = bancoUID.ToString();
                Session["agencia"] = agencia;
                Session["conta"] = conta;
                Session["obs"] = obs;

                Response.Redirect("confirmar.aspx?processo=" + processoSeletivoUID);
            }
            catch (Exception ex)
            {
                lblAlerta.Text = "Verifique o preenchimento dos dados. " + ex.Message;
                pnlAlerta.Visible = true;
                
            }
        }

        public void Validar()
        {
            try 
            {
                //try
                //{
                //    if (!VerificaCurso())
                //        throw new Exception("O aluno não está matriculado em um curso com demanda de monitores");
                //}
                //catch (Exception ex)
                //{
                //    throw new Exception(ex.Message);
                //}

                try
                {
                    if (UCBuscarServidor1.GetServidor() == null)
                        throw new Exception("Coordenador não foi selecionado");
                }
                catch (Exception ex)
                {
                    throw new Exception("Coordenador não foi selecionado");
                }
                try 
                {
                    if(UCBuscarServidor2.GetServidor() == null)
                        throw new Exception("Orientador não foi selecionado");
                }
                catch (Exception ex)
                {
                    throw new Exception("Orientador não foi selecionado");
                }

                try
                {
                    if (UCBuscarAluno1.GetAluno() == null)
                        throw new Exception("Monitor não foi selecionado");
                }
                catch (Exception ex)
                {
                    throw new Exception("Monitor não foi selecionado");
                }

                if(String.IsNullOrEmpty(rdoModalidade.SelectedValue))
                    throw new Exception("A modalidade não foi selecionada");

                if (String.IsNullOrEmpty(rdoPeriodo.SelectedValue))
                    throw new Exception("O período não foi selecionado");

                if (rdoModalidade.SelectedValue.Equals("2"))
                {
                    if(String.IsNullOrEmpty(UCDadosBancariosAluno1.Agencia))
                        throw new Exception("O número da agência não foi preenchido");

                    if (String.IsNullOrEmpty(UCDadosBancariosAluno1.Conta))
                        throw new Exception("O número da conta não foi preenchido");
                }

            } catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }

        //public bool VerificaCurso()
        //{
        //    int processoSeletivoUID = int.Parse(Request.QueryString["processo"]);
        //    CursoProcessoSeletivoBO cursoPSBO = new CursoProcessoSeletivoBO();
        //    List<vwCursoProcessoSeletivo> cursosPS = cursoPSBO.GetCursosProcessoSeletivo(processoSeletivoUID);

        //    Aluno aluno = UCBuscarAluno1.GetAluno();
        //    bool isCurso = false;

        //    foreach (vwCursoProcessoSeletivo curso in cursosPS)
        //    {
        //        if (curso.codigoExterno == aluno.CodCurso)
        //        {
        //            isCurso = true;
        //        }
        //    }

        //    return isCurso;
        //}

    }
}