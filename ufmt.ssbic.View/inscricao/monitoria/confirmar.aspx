﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="confirmar.aspx.cs" Inherits="ufmt.ssbic.View.inscricao.monitoria.confirmar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table border="1" width="800px" cellpadding="10">
    <tr>
        <td><strong>Campus:</strong> 
            <asp:Label ID="lblCampus" runat="server"></asp:Label>
        </td>
        <td><strong>Data: 
            </strong> 
            <asp:Label ID="lblData" runat="server"></asp:Label>
        </td>    
    </tr>
    <tr>
        <td colspan="2">
            <strong>Curso:</strong>
            <asp:Label ID="lblCurso" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td><strong>Coordenador:
        </strong>
        <asp:Label ID="lblCoordenador" runat="server"></asp:Label>
        </td>
        <td>
            <strong>E-mail:
            </strong>
            <asp:Label ID="lblMailCoordenador" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <strong>Professor Orientador:</strong> 
            <asp:Label ID="lblOrientador" runat="server"></asp:Label>
        </td>
        <td>
            <strong>E-mail:</strong>
            <asp:Label ID="lblMailOrientador" runat="server"></asp:Label>
        </td>
    </tr>
    </table>
    <br />
    <table border="1" width="800px" cellpadding="10">
    <tr>
        <td colspan="3"><strong>IDENTIFICAÇÃO DO ALUNO</strong></td> 
    </tr>
    <tr>
        <td colspan="2">
            <strong>Nome do Monitor:</strong>
            <asp:Label ID="lblNomeMonitor" runat="server"></asp:Label>
        </td>
        <td>
            Semestre que está cursando:
            <asp:Label ID="lblSemestre" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <strong>Data Nascimento:</strong>
            <asp:Label ID="lblDataNascimento" runat="server"></asp:Label>
        </td>
        <td colspan="2">
            <strong>E-mail:</strong>
            <asp:Label ID="lblMailMonitor" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <strong>Nº Matrícula:</strong>
            <asp:Label ID="lblMatricula" runat="server"></asp:Label>
        </td>
        <td colspan="2">
            <strong>CPF:</strong>
            <asp:Label ID="lblCpf" runat="server"></asp:Label>
        </td>
    </tr>
    </table>
    <br />
    <table border="1" width="800px" cellpadding="10">
    <tr>
        <td colspan="3"><strong>DADOS BANCÁRIOS</strong></td>
    </tr>
    <tr>
        <td><strong>Banco:</strong>
        <asp:Label ID="lblBanco" runat="server"></asp:Label>
        </td>
        <td>
            <strong>Agência:</strong>
            <asp:Label ID="lblAgencia" runat="server"></asp:Label>
        </td>
        <td>
            <strong>C/Corrente:</strong>
            <asp:Label ID="lblConta" runat="server"></asp:Label>
        </td>
    </tr>
</table>
<br />
<table border="1" width="800px" cellpadding="10">
    <tr>
        <td colspan="2"><strong>DADOS DA DISCIPLINA</strong></td>
    </tr>
    <tr>
    <td>
        <strong>Disciplina: </strong>
        <asp:Label ID="lblDisciplina" runat="server"></asp:Label>
    </td>
    <td>
        <strong>Código: </strong>
        <asp:Label ID="lblCodDisciplina" runat="server"></asp:Label>
    </td>
    </tr>
</table>

<br />
<table border="1" width="800px" cellpadding="10">
    <tr>
        <td colspan="2"><strong>MODALIDADE MONITORIA</strong></td>
    </tr>
    <tr>
        <td>
            <strong>Modalidade:</strong> 
            <asp:Label ID="lblModalidade" runat="server"></asp:Label>
        </td>
        <td>
            <strong>Período:
            </strong>
            <asp:Label ID="lblPeriodo" runat="server"></asp:Label>
        </td>
    </tr>
</table>
<br />
<table border="1" width="800px" cellpadding="10">
    <tr>
        <td colspan="2"><strong>Observações</strong></td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="obsLabel" runat="server"></asp:Label>
        </td>
    </tr>
</table>
<div style="width:800px;">
<p>
    <strong>OBS: A conferência dos dados fornecidos pelos alunos é de responsabilidade do coordenador e do professor orientador; 
Os dados bancários, obrigatoriamente, deveráo ser do próprio aluno.
</strong>
</p>
</div>

<div>
    <fieldset style="text-align:center">
        <asp:Button ID="btConfirmar" runat="server" Text="Confirmar" Height="34px" 
            onclick="btConfirmar_Click" Width="253px" />
    </fieldset>
</div>
</asp:Content>
