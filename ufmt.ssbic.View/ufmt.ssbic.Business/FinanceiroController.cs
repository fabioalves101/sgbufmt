﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.Business
{
    public class FinanceiroController
    {
        public IList<InstrumentoSelecao> GetInstrumentoSelecaoFolhaPagamentoAtiva(int? programaUID)
        {
            try
            {
                InstrumentoSelecaoBO isBO = new InstrumentoSelecaoBO();
                return isBO.GetInstrumentoSelecaoFolhaPagamentoAtiva(programaUID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IList<FolhaPagamento> GetFolhasPagamento(int instrumentoSelecaoUID)
        {
            try
            {
                InstrumentoSelecaoBO isBO = new InstrumentoSelecaoBO();
                InstrumentoSelecao instrumento = isBO.GetInstrumento(instrumentoSelecaoUID);

                ProcessoSeletivoBO procBO = new ProcessoSeletivoBO();
                ProcessoSeletivo processo = procBO.GetProcesso(instrumento.instrumentoSelecaoUID);

                FolhaPagamentoBO folhaBO = new FolhaPagamentoBO();
                return folhaBO.GetFolhaPagamentoProcessoSeletivo(processo.processoSeletivoUID);
                
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IList<Bolsista> GetBolsistas(int folhaPagamentoUID, bool suplementar)
        {
            try
            {
                BolsistaBO bolsistaBO = new BolsistaBO();

                IList<Bolsista> list = bolsistaBO.GetBolsistasPorFolhaPagamento(folhaPagamentoUID, suplementar);


                BolsistaFolhaPagamentoBO bfpBO = new BolsistaFolhaPagamentoBO();

                IList<Bolsista> newListBolsista = new List<Bolsista>();

                foreach (Bolsista bolsista in list)
                {
                    if (bfpBO.VerificarSituacao(bolsista.bolsistaUID, folhaPagamentoUID))
                    {
                        newListBolsista.Add(bolsista);
                    }                    
                }

                return newListBolsista;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public vwAlunoSiga GetAlunoPorMatricula(decimal matricula)
        {
            try
            {
                AlunoBO alunoBO = new AlunoBO();
                return alunoBO.GetAluno(matricula);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);                     
            }
        }

        public Banco GetBanco(int bancoUID)
        {
            try
            {
                BancoBO bancoBO = new BancoBO();
                return bancoBO.GetBancoPorId(bancoUID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public InstrumentoSelecao GetInstrumentoSelecaoPorFolhaPagamento(int folhaPagamentoUID)
        {
            try
            {
                FolhaPagamentoBO folhaBO = new FolhaPagamentoBO();
                return folhaBO.GetFolhaPagamento(folhaPagamentoUID).ProcessoSeletivo.InstrumentoSelecao;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void PagamentoNaoAutorizado(int bolsistaUID, int folhaPagamentoUID, String parecer)
        {
            try
            {
                BolsistaBO objBO = new BolsistaBO();
                objBO.RecusarPagamento(bolsistaUID, folhaPagamentoUID, parecer);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void PagamentoRealizado(int bolsistaUID, int folhaPagamentoUID)
        {
            try
            {
                BolsistaBO bolsistaBO = new BolsistaBO();
                bolsistaBO.AtualizarPagamento(bolsistaUID, folhaPagamentoUID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool VerificaPagamentoNaoRealizado(int bolsistaUID, int folhaPagamentoUID)
        {
            try
            {
                BolsistaBO bolsistaBO = new BolsistaBO();
                BolsistaFolhaPagamento bolsistaFP = bolsistaBO.GetBolsistaFolhaPagamento(bolsistaUID, folhaPagamentoUID);

                bool pagamentoNaoRealizado = false;
                if (bolsistaFP.pagamentoRecusado)
                {
                    pagamentoNaoRealizado = true;                    
                }

                return pagamentoNaoRealizado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool VerificaFolhaSuplementar(int folhaPagamentoUID)
        {
            try
            {
                FolhaPagamentoBO folhaBO = new FolhaPagamentoBO();
                FolhaPagamento folha = folhaBO.GetFolhaPagamento(folhaPagamentoUID);

                if (folha.suplementar.HasValue && folha.suplementar.Value)
                    return true;
                else
                    return false;
            } 
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void FecharFolhaPagamento(int folhaPagamentoUID)
        {
            try
            {
                FolhaPagamentoBO folhaBO = new FolhaPagamentoBO();
                FolhaPagamento folha = folhaBO.GetFolhaPagamento(folhaPagamentoUID);

                folha.fechada = true;
                                
                BolsistaBO bolsistaBO = new BolsistaBO();

                IList<Bolsista> listBolsistas = new List<Bolsista>();
                listBolsistas = bolsistaBO.GetBolsistasPorFolhaPagamento(folhaPagamentoUID, false);

                if (this.VerificaBolsistasFolhaSuplementar(listBolsistas, folhaPagamentoUID))
                {   
                    folha.suplementar = true;                 
                }

                folhaBO.Atualizar(folha);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private bool VerificaBolsistasFolhaSuplementar(IList<Bolsista> listBolsistas, int folhaPagamentoUID)
        {
            try
            {
                BolsistaBO bolsistaBO = new BolsistaBO();

                bool isSuplementar = false;
                foreach (Bolsista bolsista in listBolsistas)
                {
                    BolsistaFolhaPagamento bfp = bolsistaBO.GetBolsistaFolhaPagamento(bolsista.bolsistaUID, 
                                                                                        folhaPagamentoUID);

                    if (bfp.pagamentoRecusado)
                    {
                        isSuplementar = true;
                        break;                        
                    }
                }

                return isSuplementar;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool VerificaFolhaPagamentoFechada(int folhaPagamentoUID)
        {
            try
            {
                FolhaPagamento folha = this.GetFolhaPagamento(folhaPagamentoUID);

                if (folha.fechada.HasValue && folha.fechada.Value)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected FolhaPagamento GetFolhaPagamento(int folhaPagamentoUID)
        {
            try
            {
                FolhaPagamentoBO folhaBO = new FolhaPagamentoBO();
                FolhaPagamento folha = folhaBO.GetFolhaPagamento(folhaPagamentoUID);
                return folha;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public BolsistaFolhaPagamento GetBolsistaFolhaPagamentoPorBolsista(int bolsistaUID)
        {
            try
            {
                BolsistaFolhaPagamentoBO bfpBO = new BolsistaFolhaPagamentoBO();
                BolsistaFolhaPagamento bfp = bfpBO.GetBolsistaFolhaPagamentoPorBolsista(bolsistaUID);
                return bfp;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Proreitoria> GetListPrograma()
        {
            try
            {
                ProgramaBO programaBO = new ProgramaBO();
                return programaBO.GetListPrograma();
            }
            catch
            {
                throw;
            }
        }

        public List<BolsistaFolhaPagamento> GetBolsistaFolhaPagamentoPorFolhaPagamento(int folhaPagamentoUID)
        {
            try
            {
                BolsistaFolhaPagamentoBO bfpBO = new BolsistaFolhaPagamentoBO();
                List<BolsistaFolhaPagamento> listBfp = 
                    bfpBO.GetBolsistasPorFolhaPagamento(folhaPagamentoUID, true);

                return listBfp;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        
        public void AtualizarEtapaFinanceiro(int folhaPagamentoUID, int situacao)
        {
            try
            {
                FolhaPagamentoBO fpBO = new FolhaPagamentoBO();
                fpBO.AtualizarEtapaFinanceiro(folhaPagamentoUID, situacao);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ProcessoSeletivo GetProcessoSeletivoPorInstrumentoSelecao(int instrumentoSelecaoUID)
        {
            try
            {
                ProcessoSeletivoBO pbo = new ProcessoSeletivoBO();
                return pbo.GetProcessoSeletivoPorInstrumentoSelecao(instrumentoSelecaoUID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
