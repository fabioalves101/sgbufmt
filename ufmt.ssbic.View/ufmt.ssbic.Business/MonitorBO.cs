﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ufmt.ssbic.DataAccess;
using ufmt.sig.entity;

namespace ufmt.ssbic.Business
{
    public class MonitorBO
    {
        MonitorDAL monitorDAL;

        public MonitorBO()
        {
            this.monitorDAL = new MonitorDAL();
        }

        public int Salvar(Servidor coordenador, Servidor orientador, int modalidade, int periodo, int bancoUID, String agencia, String conta, Aluno aluno, ufmt.sig.entity.MatrizDisciplinar disciplina, int processoSeletivoUID, String obs)
        {
            BolsistaDAL bolsistaDAL = new BolsistaDAL();
            Bolsista bolsista = bolsistaDAL.GetBolsista(aluno.Matricula.ToString());

            int bolsistaUID = 0;
            if (bolsista == null)
            {
                bolsista = new Bolsista();
                bolsista.agencia = agencia;
                bolsista.numeroConta = conta;
                bolsista.registroAluno = aluno.Matricula.ToString();
                
                if(bancoUID != 0)
                    bolsista.bancoUID = bancoUID;

                bolsista.email = aluno.Email;
                bolsistaUID = bolsistaDAL.SalvarBolsista(bolsista);
            }
            else
            {
                bolsistaUID = bolsista.bolsistaUID;

                if (!String.IsNullOrEmpty(conta))
                {
                    bolsista.numeroConta = conta;
                }

                if (!String.IsNullOrEmpty(agencia))
                {
                    bolsista.agencia = agencia;
                }

                if (!String.IsNullOrEmpty(bancoUID.ToString()))
                {
                    if (bancoUID != 0)
                        bolsista.bancoUID = bancoUID;
                }

                bolsistaDAL.AtualizarBolsista(bolsista);
            }


            Monitor monitor = new Monitor();
            monitor.bolsistaUID = bolsistaUID;

            CursoBO cursoBO = new CursoBO();
            Curso curso = cursoBO.GetCursoPorCodCurso(aluno.CodCurso, aluno.CodCampus);
            monitor.cursoUID = curso.CursoUID;

            monitor.disciplinaUID = disciplina.MatrizDisciplinarUID;

            if (modalidade != 1)
                monitor.remunerada = true;
            else
                monitor.remunerada = false;

            if (periodo == 1)
                monitor.periodoAtuacao = "1";
            else
                if (periodo == 2)
                    monitor.periodoAtuacao = "2";
                else
                    monitor.periodoAtuacao = "A";

            monitor.orientadorUID = orientador.ServidorUID;
            monitor.coordenadorUID = coordenador.ServidorUID;
            monitor.processoSeletivoUID = processoSeletivoUID;
            monitor.obs = obs;

            int uid = this.monitorDAL.Save(monitor);
            return uid;
        }

        public void Excluir(int monitorUID)
        {
            try
            {
                this.monitorDAL.Delete(monitorUID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<vwMonitorAluno> GetMonitores(String rga, int processoSeletivoUID, long? cursoUID, String status)
        {
            try
            {
                if (!String.IsNullOrEmpty(rga))
                {
                    if (!String.IsNullOrEmpty(status))
                    {
                        return this.monitorDAL.GetVwMonitores(true, rga, processoSeletivoUID, cursoUID);
                    }
                    else
                    {
                        return this.monitorDAL.GetVwMonitores(rga, processoSeletivoUID, cursoUID);
                    }
                }
                else
                {
                     if (!String.IsNullOrEmpty(status))
                     {
                          return this.monitorDAL.GetVwMonitores(true, processoSeletivoUID, cursoUID);
                     }
                     else
                     {
                          return this.monitorDAL.GetVwMonitores(processoSeletivoUID, cursoUID);
                     }
                }
                                  
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Monitor GetMonitor(int monitorUID)
        {
            try
            {
                return this.monitorDAL.GetMonitor(monitorUID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int Atualizar(int monitorUID, Servidor orientador, Servidor coordenador)
        {
            try
            {
                Monitor objMonitor = this.monitorDAL.GetMonitor(monitorUID);

                if (objMonitor != null)
                {
                    objMonitor.monitorUID = monitorUID;
                    objMonitor.orientadorUID = orientador.ServidorUID;
                    objMonitor.coordenadorUID = coordenador.ServidorUID;

                    return this.monitorDAL.Update(objMonitor);
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int Atualizar(int monitorUID, Monitor monitor, Servidor orientador, Servidor coordenador)
        {
            try
            {
                Monitor objMonitor = this.monitorDAL.GetMonitor(monitorUID);

                if (objMonitor != null)
                {
                    objMonitor.monitorUID = monitorUID;
                    objMonitor.orientadorUID = orientador.ServidorUID;
                    objMonitor.coordenadorUID = coordenador.ServidorUID;
                    objMonitor.cursoUID = monitor.cursoUID;
                    objMonitor.bolsistaUID = monitor.bolsistaUID;
                    objMonitor.disciplinaUID = monitor.disciplinaUID;
                    objMonitor.remunerada = monitor.remunerada;
                    objMonitor.periodoAtuacao = monitor.periodoAtuacao;
                    objMonitor.inicio = monitor.inicio;
                    objMonitor.final = monitor.final;
                    objMonitor.ativo = monitor.ativo;
                    objMonitor.processoSeletivoUID = monitor.processoSeletivoUID;
                    objMonitor.obs = monitor.obs;
                    objMonitor.status = monitor.status;
                    return this.monitorDAL.Update(objMonitor);
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Monitor GetMonitorPorBolsistaProcessoSeletivo(int bolsistaUID, int processoSeletivoUID)
        {
            try
            {
                BolsistaProcessoSeletivo bps = new BolsistaProcessoSeletivoBO()
                                                .GetBolsistaProcessoSeletivo(bolsistaUID, processoSeletivoUID);


                return this.GetMonitorPorBolsista(bps.bolsistaUID.Value);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Monitor GetMonitorPorBolsista(int bolsistaUID)
        {
            try
            {
                return this.monitorDAL.GetMonitorPorBolsista(bolsistaUID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<vwMonitorAluno> GetVwMonitoresPorRemuneracao(bool remunerado, String rga,
                                                        int processoSeletivoUID, long? cursoUID, 
                                                        bool ativo, string periodoAtuacao)
        {
            try
            {
                return this.monitorDAL.GetVwMonitoresPorRemuneracao(remunerado, rga, processoSeletivoUID, cursoUID,
                                                                    ativo, periodoAtuacao);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        internal List<Monitor> GetMonitoresPorBolsista(int bolsistaUID)
        {
            try
            {
                return this.monitorDAL.GetMonitoresPorBolsista(bolsistaUID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
