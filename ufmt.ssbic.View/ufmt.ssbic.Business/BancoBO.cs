﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.Business
{
    public class BancoBO
    {
        private BancoDAL bancoDAL;
        public BancoBO()
        {
            this.bancoDAL = new BancoDAL();
        }
             
        public List<vwBanco> GetBancos()
        {
            try {
            return this.bancoDAL.GetBanco();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public vwBanco GetBanco(int bancoUID)
        {
            try
            {
                return this.bancoDAL.GetBanco(bancoUID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Banco GetBancoPorId(int bancoUID)
        {
            try
            {
                return this.bancoDAL.GetBancoPorId(bancoUID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Banco GetBanco(string codigoFebraban)
        {
            try
            {
                return this.bancoDAL.GetBanco(codigoFebraban);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
