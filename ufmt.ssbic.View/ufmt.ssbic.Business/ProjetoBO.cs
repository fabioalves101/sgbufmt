﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.Business
{
    public enum TipoInteracaoInstituicao
    {
        PRIVADA = 0,
        GOVERNAMENTAL = 1,
        EMPRESA = 2
    }

    public class ProjetoBO
    {
        public Projeto SalvarProjeto(
            int registroCap, int anoCap, String titulo, DateTime inicio, DateTime fim, 
            bool hasConvenio, String nomeConvenio,
            bool hasFinanciamento, String financiador, float valorFinanciamento, int areaConhecimentoUID,
            bool produtoTecnologico,
            bool hasInteracao, int tipoInteracaoInstituicao,
            bool hasArquivo, String nomeArquivo, String descricaoArquivo
            )
        {
            Projeto projeto = new Projeto();
            projeto.registroCap = registroCap;
            projeto.anoCap = anoCap;
            projeto.titulo = titulo;
            projeto.inicio = inicio;
            projeto.fim = fim;
            projeto.areaConhecimentoUID = areaConhecimentoUID;
            projeto.produtoTecnologico = produtoTecnologico;
            
            ProjetoDAL projetoDAL = new ProjetoDAL();
            projetoDAL.SaveProjeto(projeto, 1);

            if (hasConvenio)
            {
                Convenio convenio = new Convenio();
                convenio.convenio1 = nomeConvenio;
                convenio.Projeto = projeto;
                
                projetoDAL.SaveConvenio(convenio, 1);
            }

            if (hasFinanciamento)
            {
                Financiamento financiamento = new Financiamento();
                financiamento.financiador = financiador;

                //financiamento.valor = valorFinanciamento;
                financiamento.Projeto = projeto;

                projetoDAL.SaveFinanciamento(financiamento, 1);
            }

            if (hasInteracao)
            {
                InteracaoProjetoInstituicao interacao = new InteracaoProjetoInstituicao();
                interacao.Projeto = projeto;
                interacao.tipoInstituicao = tipoInteracaoInstituicao;
                projetoDAL.SaveInteracaoProjetoInstituicao(interacao, 1);
            }

            if (hasArquivo)
            {
                this.SalvarArquivoProjeto(projeto, nomeArquivo, descricaoArquivo);
                
            }


            return projeto;
        }

        public IList<vwProjetosCoordenador> GetProjetosCoordenador(String titulo, String coordenador)
        {
            if (String.IsNullOrEmpty(titulo))
                titulo = "";

            if (String.IsNullOrEmpty(coordenador))
                coordenador = "";

            ProjetoDAL projetoDAL = new ProjetoDAL();
            IList<vwProjetosCoordenador> projetos = projetoDAL.GetProjetosCoordenador(titulo, coordenador);

            return projetos;
        }

        public Projeto GetProjeto(long projetoUID)
        {
            ProjetoDAL projetoDAL = new ProjetoDAL();
            Projeto projeto = projetoDAL.GetProjeto(projetoUID);

            return projeto;
        }

        public Projeto AtualizarProjeto(
            int projetoUID,
            int registroCap, int anoCap, String titulo, DateTime inicio, DateTime fim,
            bool hasConvenio, int? convenioUID, String nomeConvenio, bool hasFinanciamento, 
            int? financiamentoUID, String financiador, Decimal valorFinanciamento, int areaConhecimentoUID,
            bool produtoTecnologico,
            bool hasInteracao, int? interacaoUID, int tipoInteracao
            )
        {
            Projeto projeto = new Projeto();
            projeto.projetoUID = projetoUID;
            projeto.registroCap = registroCap;
            projeto.anoCap = anoCap;
            projeto.titulo = titulo;
            projeto.inicio = inicio;
            projeto.fim = fim;
            projeto.areaConhecimentoUID = areaConhecimentoUID;
            projeto.produtoTecnologico = produtoTecnologico;

            if (hasConvenio)
            {
                Convenio convenio = new Convenio();

                if (convenioUID.HasValue)
                    convenio.convenioUID = convenioUID.Value;
                convenio.convenio1 = nomeConvenio;
                convenio.projetoUID = projeto.projetoUID;                

                projeto.Convenio.Add(convenio);
            }
            else
            {
                VerificaExcluiConvenio(projeto);
            }

            
            if (hasFinanciamento)
            {
                Financiamento financiamento = new Financiamento();
                if (financiamentoUID.HasValue)
                    financiamento.financiamentoUID = financiamentoUID.Value;

                financiamento.projetoUID = projeto.projetoUID;
                financiamento.financiador = financiador;

                financiamento.valor = valorFinanciamento;

                projeto.Financiamento.Add(financiamento);
            }
            else
            {
                VerificaExcluiFinanciamento(projeto);
            }

            if (hasInteracao)
            {
                InteracaoProjetoInstituicao interacao = new InteracaoProjetoInstituicao();
                if (interacaoUID.HasValue)
                    interacao.interacaoProjetoInstituicaoUID = interacaoUID.Value;

                interacao.projetoUID = projeto.projetoUID;
                interacao.tipoInstituicao = tipoInteracao;

                projeto.InteracaoProjetoInstituicao.Add(interacao);
            }
            else
            {
                VerificaExcluiInteracao(projeto);
            }

            ProjetoDAL projetoDAL = new ProjetoDAL();
            projetoDAL.SaveProjeto(projeto, 2);

            //if (hasConvenio)
            //{
            //    Convenio convenio = new Convenio();

            //    if (convenioUID.HasValue)
            //        convenio.convenioUID = convenioUID.Value;
            //    convenio.convenio1 = nomeConvenio;
            //    convenio.projeto = projeto;

            //    projetoDAL.SaveConvenio(convenio, 2);
            //}

            //Financiamento financiamento = new Financiamento();

            //if (hasFinanciamento)
            //{
            //    if (financiamentoUID.HasValue)
            //        financiamento.financiamentoUID = financiamentoUID.Value;
            //    financiamento.financiador = financiador;
            //    financiamento.valor = valorFinanciamento;
            //    financiamento.projeto = projeto;

            //    projetoDAL.SaveFinanciamento(financiamento, 2);
            //}
            //else
            //{
            //    VerificaExcluiFinanciamento(projeto);
            //}


            return projeto;
        }

        public void VerificaExcluiFinanciamento(Projeto objProjeto)
        {
            Projeto projeto = GetProjeto(objProjeto.projetoUID);

            if (projeto.Financiamento.Count != 0)
            {
                ProjetoDAL projetoDAL = new ProjetoDAL();
                projetoDAL.DeleteFinanciamento(projeto.Financiamento.FirstOrDefault<Financiamento>());
            }

        }

        public void VerificaExcluiInteracao(Projeto objProjeto)
        {
            Projeto projeto = GetProjeto(objProjeto.projetoUID);

            if (projeto.InteracaoProjetoInstituicao.Count != 0)
            {
                ProjetoDAL projetoDAL = new ProjetoDAL();
                projetoDAL.DeleteInteracao(projeto.InteracaoProjetoInstituicao.FirstOrDefault<InteracaoProjetoInstituicao>());
            }

        }

        public void VerificaExcluiConvenio(Projeto objProjeto)
        {
            Projeto projeto = GetProjeto(objProjeto.projetoUID);

            if (projeto.Convenio.Count != 0)
            {
                ProjetoDAL projetoDAL = new ProjetoDAL();
                projetoDAL.DeleteConvenio(projeto.Convenio.FirstOrDefault<Convenio>());
            }
        }

        public void ExcluirProjeto(int projetoUID)
        {
            Projeto projeto = GetProjeto(projetoUID);

            VerificaExcluiConvenio(projeto);
            VerificaExcluiFinanciamento(projeto);
            VerificaExcluiInteracao(projeto);

            AlunoProjetoDAL alunoProjetoDAL = new AlunoProjetoDAL();
            List<AlunoProjeto> alunosProjeto = alunoProjetoDAL.GetAlunosPorProjeto(projetoUID);

            foreach (AlunoProjeto aluno in alunosProjeto)
            {
                alunoProjetoDAL.DeleteAlunoProjeto(aluno);
            }

            MembroBO membroBO = new MembroBO();
            PessoaMembro pessoaMembro = membroBO.GetCoordenadorProjeto(projeto.projetoUID);
            
            membroBO.ExcluirMembro(pessoaMembro.pessoaMembroUID, projeto.projetoUID);

            List<PessoaMembro> pessoasMembroProjeto = membroBO.GetMembrosProjeto(projeto.projetoUID, 2);

            foreach (PessoaMembro pm in pessoasMembroProjeto)
            {
                membroBO.ExcluirMembro(pm.pessoaMembroUID, projeto.projetoUID);
            }

            

            ProjetoDAL projetoDAL = new ProjetoDAL();

            List<ArquivoProjeto> arquivosProjeto = projetoDAL.GetArquivosProjeto(projetoUID);

            foreach (ArquivoProjeto arquivo in arquivosProjeto)
            {
                projetoDAL.DeleteArquivoProjeto(arquivo);
            }            

            projetoDAL.DeleteProjeto(projeto);            
        }

        public Projeto UltimoProjeto()
        {
            ProjetoDAL projetoDAL = new ProjetoDAL();
            return projetoDAL.GetUltmoProjeto();
        }

        public List<AreaConhecimento> AreasConhecimento()
        {
            ProjetoDAL projetoDAL = new ProjetoDAL();
            return projetoDAL.GetAreasConhecimento();
        }

        public List<vwProjetosMembro> GetProjetosMembro(String cpf)
        {
            ProjetoDAL projetoDAL = new ProjetoDAL();
            return projetoDAL.GetProjetosMembro(cpf);
        }

        public void SalvarArquivoProjeto(Projeto projeto, String nomeArquivo, String descricao)
        {
            ArquivoProjeto arquivo = new ArquivoProjeto();
            arquivo.nomeArquivo = nomeArquivo;
            arquivo.Projeto = projeto;
            arquivo.descricao = descricao;
            ProjetoDAL projetoDAL = new ProjetoDAL();
            projetoDAL.SaveArquivoProjeto(arquivo);
        }

        public List<ArquivoProjeto> GetArquivosProjeto(int projetoUID)
        {
            ProjetoDAL projetoDAL = new ProjetoDAL();
            return projetoDAL.GetArquivosProjeto(projetoUID);
        }

        public void ExcluirArquivoProjeto(int arquivoProjetoUID)
        {
            ProjetoDAL projetoDAL = new ProjetoDAL();
            
            ArquivoProjeto arquivoProjeto = projetoDAL.GetArquivoProjeto(arquivoProjetoUID);

            projetoDAL.DeleteArquivoProjeto(arquivoProjeto);
        }

        public ArquivoProjeto GetArquivoProjeto(int arquivoProjetoUID)
        {
            ProjetoDAL projetoDAL = new ProjetoDAL();
            return projetoDAL.GetArquivoProjeto(arquivoProjetoUID);
        }

    }
}
