﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.Business
{
    public class MonitoriaController
    {
        public BolsistaFolhaPagamento GetBolsistaFolhaPagamentoPorBolsista(int bolsistaUID)
        {
            try
            {
                BolsistaFolhaPagamentoBO bfpBO = new BolsistaFolhaPagamentoBO();
                return bfpBO.GetBolsistaUltimaFolhaPagamento(bolsistaUID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public FolhaPagamento GetUltimaFolhaPagamento(int processoSeletivoUID)
        {
            try
            {
                FolhaPagamentoBO fpBO = new FolhaPagamentoBO();
                return fpBO.GetUltimaFolhaPagamento(processoSeletivoUID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            } 
        }

        public List<Monitor> GetMonitoresPorBolsista(int bolsistaUID)
        {
            try
            {
                MonitorBO monitorBO = new MonitorBO();
                return monitorBO.GetMonitoresPorBolsista(bolsistaUID);
            }
            catch 
            {
                throw;
            }  
            
        }

        public Monitor GetMonitor(int monitorUID)
        {
            try
            {
                return new MonitorBO().GetMonitor(monitorUID);
            }
            catch
            {
                throw;
            }
        }
    }
}
