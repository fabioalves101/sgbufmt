﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ufmt.ssbic.DataAccess;
using System.Collections;

namespace ufmt.ssbic.Business
{
    
    public class FinanciadorBO
    {

        public List<vwFinanciador> GetRegistros(int? instrumentoSelecaoUID)
        {
            try
            {
                FinanciadorDAL objDAL = new FinanciadorDAL();
                return objDAL.GetRegistros(instrumentoSelecaoUID);

            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public List<Financiador> GetTipoFinanciador()
        {
            try
            {
                FinanciadorDAL objDAL = new FinanciadorDAL();
                return objDAL.GetTipoFinanciador();

            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
            
        }


        public vwFinanciador GetFinanciador(int financiadorInstrumentoSelecaoUID)
        {

            try
            {
                FinanciadorDAL objDAL = new FinanciadorDAL();
                return objDAL.GetFinanciador(financiadorInstrumentoSelecaoUID);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        
        
        }

        public List<vwFinanciador> FindRegistros(int? instrumentoSelecaoUID, string criterio)
        {

            try
            {
                FinanciadorDAL objDAL = new FinanciadorDAL();
                return objDAL.FindRegistros(instrumentoSelecaoUID, criterio);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public bool Salvar(FinanciadorInstrumentoSelecao fis, int operacao)
        {

            try
            {
                FinanciadorDAL objDAL = new FinanciadorDAL();
                return objDAL.Salvar(fis, operacao);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível Salvar os registros.", exception);

            }
        }

        public bool Salvar(Financiador financiador, int operacao)
        {
            try
            {
                FinanciadorDAL objDAL = new FinanciadorDAL();
                return objDAL.Salvar(financiador, operacao);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível Salvar os registros.", exception);

            }
        }

        public bool Excluir(int financiadorInstrumentoSelecaoUID)
        {

            try
            {
                FinanciadorDAL objDAL = new FinanciadorDAL();
                return objDAL.Excluir(financiadorInstrumentoSelecaoUID);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível excluir os registros.", exception);

            }
        }





        public FinanciadorInstrumentoSelecao GetFinanciadorInstrumentoSelecao(int p)
        {
            FinanciadorDAL objDAL = new FinanciadorDAL();
            return objDAL.GetFinanciadorInstrumentoSelecao(p);
        }
    }
}
