﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ufmt.sig.entity;
using ufmt.sig.business;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.Business
{
    public class CursoBO
    {
        public List<Curso> GetCursos(int campusUID, String nomeCurso)
        {
            using (ICursoBO cursoBO = BusinessFactory.GetInstance<ICursoBO>())
            {
                IList<Curso> cursos = cursoBO.GetCursos(campusUID, 1, nomeCurso, null, null);

                return cursos.ToList<Curso>();              
            }
        }

        public Curso GetCurso(long cursoUID)
        {
            using (ICursoBO cursoBO = BusinessFactory.GetInstance<ICursoBO>())
            {
                Curso curso = cursoBO.GetCurso(cursoUID);

                return curso;
            }
        }

        public Curso GetCursoPorCodCurso(int codCurso, int campusUID)
        {
            List<Curso> cursos = this.GetCursos(campusUID, null);
            return this.FiltrarCursosPorCodCurso(cursos, codCurso);
        }

        public Curso FiltrarCursosPorCodCurso(List<Curso> cursos, int codCurso)
        {
            Curso cursoProcurado = new Curso();
            foreach (Curso curso in cursos)
            {
                if (curso.CodigoExterno == codCurso)
                {
                    cursoProcurado = curso;
                    break;
                }
            }

            if (String.IsNullOrEmpty(cursoProcurado.Nome))
                return null;
            else
                return cursoProcurado;
        }
        

        public List<MatrizCurricular> GetMatrizes(int campusUID, long cursoUID)
        {
            using (ICursoBO cursoBO = BusinessFactory.GetInstance<ICursoBO>())
            {                
                IList<MatrizCurricular> matrizes = cursoBO.GetMatrizesCurriculares(campusUID, null, cursoUID, null, null);
                return matrizes.ToList<MatrizCurricular>();
            }
        }

        public IList<Campus> GetCampi()
        {
            using (ILocalizacaoBO localizacaoBO = BusinessFactory.GetInstance<ILocalizacaoBO>())
            {
                return localizacaoBO.GetCampi();
            }
        }

        public Campus GetCampusPorNome(String nomeCampus)
        {
            IList<Campus> campi = this.GetCampi();
            return this.FiltrarCampus(campi, nomeCampus);
        }

        public Campus FiltrarCampus(IList<Campus> campi, String nomeCampus)
        {
            Campus campusProcurado = new Campus();
            foreach (Campus campus in campi)
            {
                if (campus.Nome.Contains(nomeCampus))
                {
                    campusProcurado = campus;
                    break;
                }
            }

            return campusProcurado;
        }

        public List<DisciplinasAprovadasEntity> GetDisciplinasAprovadas(String rga, double media)
        {
            DisciplinaDAL disciplinaDAL = new DisciplinaDAL();
            return disciplinaDAL.GetDisciplinasAprovadas(rga, media);
        }

        public ufmt.ssbic.DataAccess.MatrizDisciplinar GetDisciplina(int codigoDisciplina)
        {
            idbufmtEntities contexto = new idbufmtEntities();

            ufmt.ssbic.DataAccess.MatrizDisciplinar matrizes = 
                (from c in contexto.MatrizDisciplinar
                 where c.codigoExterno == codigoDisciplina
                 select c).FirstOrDefault<ufmt.ssbic.DataAccess.MatrizDisciplinar>();

            return matrizes;
        }

        public ufmt.sig.entity.MatrizDisciplinar GetMatrizDisciplinar(long matrizUID)
        {
            using (ICursoBO cursoBO = BusinessFactory.GetInstance<ICursoBO>())
            {
                ufmt.sig.entity.MatrizDisciplinar matriz = cursoBO.GetMatrizDisciplinar(matrizUID);
                return matriz;
            }
        }


    }
}
