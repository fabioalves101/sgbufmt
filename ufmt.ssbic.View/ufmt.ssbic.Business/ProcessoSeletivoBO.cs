﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ufmt.ssbic.DataAccess;
using System.Collections;

namespace ufmt.ssbic.Business
{
    
    public class ProcessoSeletivoBO
    {

        public List<vwProcessoSeletivo> GetRegistros(int? instrumentoSelecaoUID)
        {
            try
            {
                ProcessoSeletivoDAL objDAL = new ProcessoSeletivoDAL();
                return objDAL.GetRegistros(instrumentoSelecaoUID);

            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public vwProcessoSeletivo GetProcessoSeletivo(int processoSeletivoUID)
        {

            try
            {
                ProcessoSeletivoDAL objDAL = new ProcessoSeletivoDAL();
                return objDAL.GetProcessoSeletivo(processoSeletivoUID);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        
        
        }

        public ProcessoSeletivo GetProcesso(int instrumentoSelecaoUID)
        {

            try
            {
                ProcessoSeletivoDAL objDAL = new ProcessoSeletivoDAL();
                return objDAL.GetProcesso(instrumentoSelecaoUID);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public ProcessoSeletivo GetProcessoSeletivoPorId(int processoSeletivoUID)
        {
            try
            {
                return new ProcessoSeletivoDAL().GetProcessoSeletivoPorId(processoSeletivoUID);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public List<vwProcessoSeletivo> FindRegistros(int? instrumentoSelecaoUID, string criterio)
        {

            try
            {
                ProcessoSeletivoDAL objDAL = new ProcessoSeletivoDAL();
                return objDAL.FindRegistros(instrumentoSelecaoUID, criterio);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public int Salvar(ProcessoSeletivo ps, int operacao)
        {

            try
            {
                ProcessoSeletivoDAL objDAL = new ProcessoSeletivoDAL();
                return objDAL.Salvar(ps, operacao);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível Salvar os registros.", exception);

            }
        }

        public bool Excluir(int processoSeletivoUID)
        {
            try
            {
                ProcessoSeletivoDAL objDAL = new ProcessoSeletivoDAL();
                return objDAL.Excluir(processoSeletivoUID);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível excluir os registros.", exception);
            }
        }

        internal ProcessoSeletivo GetProcessoSeletivoPorInstrumentoSelecao(int instrumentoSelecaoUID)
        {
            try
            {
                ProcessoSeletivoDAL objDAL = new ProcessoSeletivoDAL();
                return objDAL.GetProcesso(instrumentoSelecaoUID);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível selecionar o registro.", exception);
            }
        }
    }
}
