﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.Business
{

    public enum ProReitoria
    {

        PROEG = 1,
        PROCEV = 2,
        PROPEQ = 3,
        PROPG = 4,
        Financeiro = 5,
        Coordenador = 6,
        Monitoria = 7,
        CARE = 8

    }

    public class BolsaBO
    {
        public Proreitoria GetPrograma(string programa)
        {
            BolsaDAL bolsaDAL = new BolsaDAL();
            return bolsaDAL.GetPrograma(programa);
        }

        public string GetPrograma(int programaUID)
        {
            string programa = string.Empty;
            switch (programaUID)
            {
                case (int)ProReitoria.PROEG:
                    #region subitens PROEG
                    programa = ProReitoria.PROEG.ToString();
                    #endregion
                    break;

                case (int)ProReitoria.PROCEV:
                    #region subitens PROCEV
                    programa = ProReitoria.PROCEV.ToString();
                    #endregion
                    break;

                case (int)ProReitoria.PROPEQ:
                    #region subitens PROPEQ
                    programa = ProReitoria.PROPEQ.ToString();
                    #endregion

                    break;

                case (int)ProReitoria.PROPG:
                    #region subitens PROPG
                    programa = ProReitoria.PROPG.ToString();
                    #endregion

                    break;

                case (int)ProReitoria.Financeiro:
                    #region subitens Financeiro
                    programa = ProReitoria.Financeiro.ToString();
                    #endregion

                    break;

                case (int)ProReitoria.Coordenador:
                    #region subitens Coordenador
                    programa = ProReitoria.Coordenador.ToString();
                    #endregion

                    break;

                case (int)ProReitoria.Monitoria:
                    #region subitens Monitoria
                    programa = ProReitoria.Monitoria.ToString();
                    #endregion

                    break;

                case (int)ProReitoria.CARE:
                    #region subitens CARE
                    programa = ProReitoria.CARE.ToString();
                    #endregion

                    break;

            }


            return programa;

        
        }

        public FinanciadorInstrumentoSelecao GetTotalBolsaRestante(int financiadorInstrumentoSelecaoUID)
        {

            try
            {
                BolsaDAL objDAL = new BolsaDAL();
                return objDAL.GetTotalBolsaRestante(financiadorInstrumentoSelecaoUID);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }



        public List<Bolsa> GetRegistros(int? proreitoriaUID)
        {
            try
            {
                BolsaDAL objDAL = new BolsaDAL();
                return objDAL.GetRegistros(proreitoriaUID);

            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public Bolsa GetBolsa(int bolsaUID) {

            try
            {
                BolsaDAL objDAL = new BolsaDAL();
                return objDAL.GetBolsa(bolsaUID);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        
        
        }

        public List<vwBolsaProcessoSeletivo> GetBolsaProcessoSeletivo(int? processoSeletivoUID)
        {
            try
            {
                BolsaDAL objDAL = new BolsaDAL();
                return objDAL.GetBolsaProcessoSeletivo(processoSeletivoUID);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }

        }

        //public List<FinanciadorInstrumentoSelecao> GetBolsaProcessoSeletivo(int? instrumentoSelecaoUID)
        //{
        //    try
        //    {
        //        BolsaDAL objDAL = new BolsaDAL();
        //        return objDAL.GetBolsaProcessoSeletivo(instrumentoSelecaoUID);
        //    }
        //    catch (Exception exception)
        //    {
        //        throw new Exception("Não foi possível recuperar os registros.", exception);
        //    }

        //}


        public List<Bolsa> FindRegistros(int? proreitoriaUID, string criterio)
        {

            try
            {
                BolsaDAL objDAL = new BolsaDAL();
                return objDAL.FindRegistros(proreitoriaUID, criterio);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public bool Salvar(Bolsa bolsa, int operacao)
        {

            try
            {
                BolsaDAL objDAL = new BolsaDAL();
                return objDAL.Salvar(bolsa, operacao);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível Salvar os registros.", exception);

            }
        }

        public bool Excluir(int bolsaUID)
        {

            try
            {
                BolsaDAL objDAL = new BolsaDAL();
                return objDAL.Excluir(bolsaUID);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível excluir os registros.", exception);

            }
        }
    }
}
