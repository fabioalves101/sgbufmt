﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.Business
{

    public class BolsistaProcessoSeletivoBO
    {

        public bool Salvar(BolsistaProcessoSeletivo bolsistaps, Bolsista bolsista, Responsavel res, int financiadorInstrumentoSelecaoUID, int folhaPagamentoUID)
        {

            try
            {
                BolsistaProcessoSeletivoDAL objDAL = new BolsistaProcessoSeletivoDAL();
                return objDAL.Salvar(bolsistaps, bolsista, res, financiadorInstrumentoSelecaoUID, folhaPagamentoUID);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível Salvar os registros.", exception);

            }
        }



        public bool Excluir(int id)
        {

            try
            {
                BolsistaProcessoSeletivoDAL objDAL = new BolsistaProcessoSeletivoDAL();
                return objDAL.Excluir(id);
            }

            
            catch (Exception exception)
            {
                throw new Exception("Não foi possível excluir os registros.", exception);

            }
        }

        public BolsistaProcessoSeletivo GetBolsistaProcessoSeletivo(int bolsistaUID, int processoSeletivoUID)
        {
            try
            {
                BolsistaProcessoSeletivoDAL objDAL = new BolsistaProcessoSeletivoDAL();
                return objDAL.GetBolsistaProcessoSeletivo(bolsistaUID, processoSeletivoUID);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível selecionar o registro.", exception);
            }
        }

        //public void AtualizarSituacaoBolsista(int bolsistaUID, int processoSeletivoUID, int folhaPagamentoUID,  
        //    int situacao)
        //{
        //    try
        //    {
        //        BolsistaProcessoSeletivoDAL objDAL = new BolsistaProcessoSeletivoDAL();
        //        BolsistaProcessoSeletivo objBPS = objDAL.GetBolsistaProcessoSeletivo(bolsistaUID, processoSeletivoUID);

        //        objBPS.situacao = situacao;
                
        //        objDAL.AtualizarSituacaoBolsita(objBPS);
                
        //        bool ativo = false;
        //        if (situacao == 1)
        //            ativo = true;

        //        //new BolsistaFolhaPagamentoDAL().AtualizarSituacaoBolsistaFolhaPagamento(
        //          //  bolsistaUID, folhaPagamentoUID, ativo);

        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("Não foi possível atualizar o registro.", ex);
        //    }
        //}

        public bool IsBolsistaCadastrado(int bolsistaUID, int processoSeletivoUID)
        {
            try
            {
                BolsistaProcessoSeletivoDAL bpsDAL = new BolsistaProcessoSeletivoDAL();
                BolsistaProcessoSeletivo bps = bpsDAL.GetBolsistaProcessoSeletivo(bolsistaUID, processoSeletivoUID);

                if (bps != null)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }     
        }






        public bool VerificaSituacao(int bolsistaUID, int processoSeletivoUID)
        {
            try
            {
                List<BolsistaProcessoSeletivo> list = new BolsistaProcessoSeletivoDAL()
                    .GetListBolsistaProcessoSeletivo(bolsistaUID, processoSeletivoUID);

                bool ativo = false;
                foreach (BolsistaProcessoSeletivo bps in list)
                {
                    if (bps.situacao.HasValue)
                    {
                        if (bps.situacao.Value == 1)
                            ativo = true;
                    }
                }

                return ativo;
            }
            catch
            {
                throw;
            }
        }
    }
}
