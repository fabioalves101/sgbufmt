﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.Business
{
    public class BolsistaBO
    {
        public List<vwBolsista> GetRegistros(int processoSeletivoUID)
        {
            try
            {
                BolsistaDAL objDAL = new BolsistaDAL();
                return objDAL.GetRegistros(processoSeletivoUID);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public vwBolsista GetBolsista(int processoSeletivoUID)
        {
            try
            {
                BolsistaDAL objDAL = new BolsistaDAL();
                return objDAL.GetBolsista(processoSeletivoUID);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }        
        
        }

        public List<vwBolsista> FindRegistros(int processoSeletivoUID, string registroAluno)
        {

            try
            {
                BolsistaDAL objDAL = new BolsistaDAL();
                return objDAL.FindRegistros(processoSeletivoUID, registroAluno);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }



        public int Salvar(Bolsista bolsista, BolsistaProcessoSeletivo bps)
        {

            try
            {
                BolsistaDAL objDAL = new BolsistaDAL();
                return objDAL.Salvar(bolsista, bps);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível Salvar os registros.", exception);

            }
        }

        public bool AtualizarBolsista(int bolsistaUID, string email, string numeroConta,
            int? bancoUID, string agencia)
        {
            try
            {
                Bolsista bolsista = this.GetBolsistaPorId(bolsistaUID);
                bolsista.email = email;
                bolsista.numeroConta = numeroConta;
                bolsista.bancoUID = bancoUID;
                bolsista.agencia = agencia;

                BolsistaDAL objDAL = new BolsistaDAL();
                return objDAL.AtualizarBolsista(bolsista);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível Salvar os registros.", exception);
            }
        }

        public bool Excluir(int bolsistaUID, int processoSeletivoUID)
        {
            try
            {
                BolsistaDAL objDAL = new BolsistaDAL();
                return objDAL.Excluir(bolsistaUID, processoSeletivoUID);
            }
            
            catch (Exception exception)
            {
                throw new Exception("Não foi possível excluir os registros.", exception);
            }
        }

        public bool VerificaBolsistaAtivo(String rga)
        {
            try
            {
                BolsistaDAL objDAL = new BolsistaDAL();
                return objDAL.VerificaBolsistaAtivo(rga);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int? VerificaRGA(string rga, int situacao)
        {
            try
            {
                BolsistaDAL objDAL = new BolsistaDAL();

                return objDAL.VerificaRGA(rga, situacao);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar o registro.", exception);
            }

        }

        public List<vwBolsistasFolhaPagamento> GetBolsistasFolhaPagamento(int folhaPagamentoUID, String nome)
        {
            try
            {
                BolsistaDAL bolsistaDAL = new BolsistaDAL();

                if(String.IsNullOrEmpty(nome))
                    return bolsistaDAL.GetBolsistasFolhaPagamento(folhaPagamentoUID);
                else
                    return bolsistaDAL.GetBolsistasFolhaPagamentoPorNome(folhaPagamentoUID, nome);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar o registros.", exception);
            }
        }

        public List<BolsistaFolhaPagamento> GetBolsistasFolhaPagamentoPorId(int folhaPagamentoUID)
        {
            try
            {
                BolsistaDAL bolsistaDAL = new BolsistaDAL();
                return bolsistaDAL.GetBolsistasFolhaPagamentoPorId(folhaPagamentoUID);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar o registros.", exception);
            }
        }

        public IList<Bolsista> GetBolsistasPorFolhaPagamento(int folhaPagamentoUID, bool suplementar)
        {
            try
            {
                BolsistaDAL bolsistaDAL = new BolsistaDAL();

                IList<Bolsista> listBolsista = new List<Bolsista>();
                if (suplementar)
                {
                    listBolsista = bolsistaDAL.GetBolsistasPorFolhaPagamentoSuplementar(folhaPagamentoUID);
                }
                else
                {
                    listBolsista = bolsistaDAL.GetBolsistasPorFolhaPagamento(folhaPagamentoUID);
                }

                return listBolsista;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar o registros.", exception);
            }
        }

        public List<vwBolsistasFolhaPagamento> GetBolsistasFolhaPagamentoSuplementar(int folhaPagamentoUID, String nome)
        {
            try
            {
                BolsistaDAL bolsistaDAL = new BolsistaDAL();

                if(String.IsNullOrEmpty(nome))
                    return bolsistaDAL.GetBolsistasFolhaPagamentoSuplementar(folhaPagamentoUID);
                else
                    return bolsistaDAL.GetBolsistasFolhaPagamentoSuplementarPorNome(folhaPagamentoUID, nome);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar o registros.", exception);
            }
        }

        public BolsistaFolhaPagamento GetBolsistaFolhaPagamento(int bolsistaUID, int folhaPagamentoUID)
        {
            try
            {
                BolsistaDAL bolsistaDAL = new BolsistaDAL();
                return bolsistaDAL.GetBolsistaFolhaPagamento(bolsistaUID, folhaPagamentoUID);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar o registros.", exception);
            }
        }

        public void AtualizarPagamento(int bolsistaUID, int folhaPagamentoUID)
        {
            try
            {
                BolsistaFolhaPagamento bolsistaFP = this.GetBolsistaFolhaPagamento(bolsistaUID, folhaPagamentoUID);

                bolsistaFP.pagamentoAutorizado = true;
                bolsistaFP.pagamentoRecusado = false;

                BolsistaDAL bolsistaDAL = new BolsistaDAL();
                bolsistaDAL.SalvarBolsistaFolhaPagamento(bolsistaFP, 2);
          
                //VERIFICAR FECHAMENTO FOLHA PAGAMENTO
                FolhaPagamentoBO folhaPagamentoBO = new FolhaPagamentoBO();

                if (!folhaPagamentoBO.VerificarFechamentoFolhaPagamento(folhaPagamentoUID))
                {
                    folhaPagamentoBO.FecharFolhaPagamento(folhaPagamentoUID);
                }
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível atualizar o registro.", exception);
            }
        }

        public void RecusarPagamento(int bolsistaUID, int folhaPagamentoUID, String parecerRecusaPagamento)
        {
            try
            {
                BolsistaFolhaPagamento bolsistaFP = this.GetBolsistaFolhaPagamento(bolsistaUID, folhaPagamentoUID);

                bolsistaFP.pagamentoRecusado = true;
                bolsistaFP.pagamentoAutorizado = false;
                bolsistaFP.parecer = parecerRecusaPagamento;

                BolsistaDAL bolsistaDAL = new BolsistaDAL();
                bolsistaDAL.SalvarBolsistaFolhaPagamento(bolsistaFP, 2);

                //VERIFICAR FECHAMENTO FOLHA PAGAMENTO
                FolhaPagamentoBO folhaPagamentoBO = new FolhaPagamentoBO();

                if (!folhaPagamentoBO.VerificarFechamentoFolhaPagamento(folhaPagamentoUID))
                {
                    folhaPagamentoBO.FecharFolhaPagamento(folhaPagamentoUID);
                }
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível atualizar o registro.", exception);
            }
        }

        public Bolsista GetBolsistaPorId(int bolsistaUID)
        {
            try
            {
                BolsistaDAL bolsistaDAL = new BolsistaDAL();
                return bolsistaDAL.GetBolsistaPorId(bolsistaUID);
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível selecionar o registro.", ex);
            }
        }

        public void HomologarBolsistas(int? bolsistaUID, int processoSeletivoUID, int folhaPagamentoUID, String semestre)
        {
            try
            {
                if (semestre.Equals("1"))
                    new BolsistaDAL().HomologarBolsistas1Semestre(bolsistaUID, processoSeletivoUID, folhaPagamentoUID);
                else {
                    if (bolsistaUID != null)
                    {
                        //if(semestre.Equals("2"))
                            new BolsistaDAL().HomologarBolsistaIndividual(bolsistaUID.Value, processoSeletivoUID, folhaPagamentoUID, int.Parse(semestre));
                        //else if(semestre.Equals("3"))
                          //  new BolsistaDAL().HomologarBolsistaIndividual(bolsistaUID.Value, processoSeletivoUID, folhaPagamentoUID);


                    }
                }
                    //new BolsistaDAL().HomologarBolsistas2Semestre(bolsistaUID, processoSeletivoUID, folhaPagamentoUID);
            }
            catch (Exception ex) 
            {
                throw new Exception(ex.Message);
            }
        }

        public void AlterarVinculoMonitoria(int bolsistaUID)
        {
            try
            {
                new BolsistaDAL().AlterarVinculoBolsista(bolsistaUID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }





        public bool VerificaBolsistaAtivoMonitoria(string rga, int folhaPagamentoUID)
        {
            FolhaPagamento fp = new FolhaPagamentoDAL().GetFolhaPagamento(folhaPagamentoUID);
            BolsistaFolhaPagamento bfp = new BolsistaDAL().GetFolhaPagamentoBolsistaAtivo(rga);

            bool retorno = false;
            if (bfp != null)
            {
                if (bfp.FolhaPagamento.ProcessoSeletivo.InstrumentoSelecao.Proreitoria.proreitoriaUID == 7)
                {
                    if (fp.dataInicio.Value.Month != bfp.FolhaPagamento.dataInicio.Value.Month)
                    {
                        retorno = true;
                    }
                    else
                    {
                        retorno = false;
                    }
                }
                else
                {

                    int bolsaPermanenciaUID = int.Parse(System.Configuration.ConfigurationManager.AppSettings["bolsaPermanencia"].ToString());
                    if (bfp.FolhaPagamento.ProcessoSeletivo.InstrumentoSelecao.bolsaUID == bolsaPermanenciaUID)
                    {
                        if (bfp.FolhaPagamento.processoSeletivoUID == fp.processoSeletivoUID)
                        {
                            retorno = false;
                        }
                        else
                        {
                            retorno = true;
                        }
                    }
                    else
                    {
                        retorno = true;
                    }
                }
            }
            else
            {
                retorno = true;

            }

            return retorno;
        }

        internal Bolsista GetBolsistaPorRGA(string rga)
        {
            try
            {
                BolsistaDAL bolsistaDAL = new BolsistaDAL();
                return bolsistaDAL.GetBolsista(rga);
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível selecionar o registro.", ex);
            }
        }

        internal void AtualizarBolsista(Bolsista bolsista)
        {
            try
            {
                BolsistaDAL bolsistaDAL = new BolsistaDAL();
                bolsistaDAL.AtualizarBolsista(bolsista);
            }
            catch
            {
                throw;
            }
        }

        internal void Salvar(Bolsista bolsista)
        {
            try
            {
                BolsistaDAL bolsistaDAL = new BolsistaDAL();
                bolsistaDAL.SalvarBolsista(bolsista);
            }
            catch
            {
                throw;
            }
        }
    }
}
