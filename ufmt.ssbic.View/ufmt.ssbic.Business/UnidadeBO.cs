﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ufmt.sig.entity;
using ufmt.sig.business;

namespace ufmt.ssbic.Business
{
    public class UnidadeBO
    {
        public IList<Campus> GetCampi()
        {
            using (ILocalizacaoBO localizacaoBO = BusinessFactory.GetInstance<ILocalizacaoBO>())
            {
                IList<Campus> campi;
                campi = localizacaoBO.GetCampi();
                return campi;
            }
        }

        public IList<Unidade> getUnidadesbyCampus(String unidade, int campus)
        {
            using (ILocalizacaoBO localizacaoBO = BusinessFactory.GetInstance<ILocalizacaoBO>())
            {
                IList<Unidade> unidades = null;
                if (String.IsNullOrEmpty(unidade))
                {
                    unidades = localizacaoBO.GetUnidades(campus, null, null, null, null, null, null);
                }
                else
                {
                    unidades = localizacaoBO.GetUnidades(campus, null, unidade, null, null, null, null);
                }
                return unidades;
            }
        }

        public Campus GetCampus(String campus)
        {
            using (ILocalizacaoBO localizacaoBO = BusinessFactory.GetInstance<ILocalizacaoBO>())
            {
                IList<Campus> campi;
                campi = localizacaoBO.GetCampi();

                foreach (Campus c in campi)
                {
                    if (c.Nome.ToLower().Contains(campus.ToLower()))
                    {
                        return c;
                    }
                }
                return null;
            }
        }

        public Campus GetCampus(int campusUID)
        {
            using (ILocalizacaoBO localizacaoBO = BusinessFactory.GetInstance<ILocalizacaoBO>())
            {
                IList<Campus> campi;
                campi = localizacaoBO.GetCampi();

                foreach (Campus c in campi)
                {
                    if (c.CampusUID == campusUID)
                    {
                        return c;
                    }
                }
                return null;
            }
        }
    }
}
