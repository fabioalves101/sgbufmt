﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.Business
{
    public class BolsistaFolhaSuplementarBO
    {

        internal void CadastrarAluno(Bolsista bolsista, int folhaSuplementarUID, decimal valor)
        {
            BolsistaBO bolsistaBO = new BolsistaBO();
            Bolsista bolsistaExistente = bolsistaBO.GetBolsistaPorRGA(bolsista.registroAluno);

            if (bolsistaExistente != null)
            {
                bolsista.bolsistaUID = bolsistaExistente.bolsistaUID;
                bolsistaBO.AtualizarBolsista(bolsista);                
            }
            else
            {
                bolsistaBO.Salvar(bolsista);
            }

            BolsistaFolhaSuplementar bfs = new BolsistaFolhaSuplementar()
            {
                folhaSuplementarUID = folhaSuplementarUID,
                ativo = true,
                Bolsista = bolsista,
                cpf = bolsista.cpf,
                pagamentoAutorizado = false,
                pagamentoRecusado = false,
                registroAluno = bolsista.registroAluno,
                valor = valor,
                justificativaSituacao = string.Empty,
                parecer = string.Empty
            
            };            

            BolsistaFolhaSuplementarDAL bfsDAL = new BolsistaFolhaSuplementarDAL();
            bfsDAL.Salvar(bfs);
        }

        public List<BolsistaFolhaSuplementar> GetListBolsistaFolhaSuplementar(int folhaSuplementarUID)
        {
            BolsistaFolhaSuplementarDAL bfsDAL = new BolsistaFolhaSuplementarDAL();
            return bfsDAL.GetListBolsistaFolhaSuplementar(folhaSuplementarUID);
        }



        internal void Excluir(int bolsistaFolhaSuplementarUID)
        {
            BolsistaFolhaSuplementarDAL bfsDAL = new BolsistaFolhaSuplementarDAL();
            bfsDAL.Excluir(bolsistaFolhaSuplementarUID);
        }
    }
}
