﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.Business
{
    public class RelatorioController
    {
        public List<InstrumentoSelecao> GetListInstrumentoSelecaoPorPrograma(int programaUID)
        {
            try
            {
                InstrumentoSelecaoBO isBO = new InstrumentoSelecaoBO();
                List<InstrumentoSelecao> list = isBO.GetListInstrumentoSelecaoPorPrograma(programaUID);
                return list;
            }
            catch
            {
                throw;
            }

        }

        public Proreitoria GetPrograma(int programaUID)
        {
            try
            {
                ProgramaBO programaBO = new ProgramaBO();
                return programaBO.GetPrograma(programaUID);
            }
            catch
            {
                throw;
            }
        }
    }
}
