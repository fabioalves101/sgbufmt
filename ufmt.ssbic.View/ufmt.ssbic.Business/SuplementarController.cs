﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.Business
{
    public class SuplementarController
    {

        public BolsistaFolhaSuplementar GetAlunoEmFolha(string cpf, int folhaSuplementarUID)
        {
            FolhaPagamentoBO folhaBO = new FolhaPagamentoBO();
            BolsistaFolhaSuplementar bfs = folhaBO.GetAlunoEmFolhaSuplementar(cpf, folhaSuplementarUID);
            return bfs;
        }

        public void CadastrarAluno(Bolsista bolsista, int folhaSuplementarUID, decimal valor)
        {
            BolsistaFolhaSuplementarBO bfsBO = new BolsistaFolhaSuplementarBO();
            bfsBO.CadastrarAluno(bolsista, folhaSuplementarUID, valor);
        }

        public List<BolsistaFolhaSuplementar> GetListBolsistaFolhaSuplementar(int folhaSuplementarUID)
        {
            BolsistaFolhaSuplementarBO bfsBO = new BolsistaFolhaSuplementarBO();
            return bfsBO.GetListBolsistaFolhaSuplementar(folhaSuplementarUID);
        }

        public void ExcluirBolsistaFolhaSuplementar(int bolsistaFolhaSuplementarUID)
        {
            BolsistaFolhaSuplementarBO bfsBO = new BolsistaFolhaSuplementarBO();
            bfsBO.Excluir(bolsistaFolhaSuplementarUID);
        }
    }
}
