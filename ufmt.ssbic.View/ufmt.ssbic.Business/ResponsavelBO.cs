﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.Business
{

    public class ResponsavelBO
    {


        public List<Responsavel> FindRegistros(string matricula, string criterio)
        {
            try
            {                
                ResponsavelDAL objDAL = new ResponsavelDAL();
                return objDAL.FindRegistros(matricula, criterio);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public bool Salvar(Responsavel responsavel)
        {

            try
            {
                ResponsavelDAL objDAL = new ResponsavelDAL();
                return objDAL.Salvar(responsavel);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível Salvar os registros.", exception);

            }
        }

        public bool Atualizar(Responsavel responsavel)
        {
            try
            {
                ResponsavelDAL objDAL = new ResponsavelDAL();
                return objDAL.Atualizar(responsavel);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível Salvar os registros.", exception);

            }
        }

        public bool Excluir(int responsavelUID)
        {

            try
            {
                ResponsavelDAL objDAL = new ResponsavelDAL();
                return objDAL.Excluir(responsavelUID);
            }

            
            catch (Exception exception)
            {
                throw new Exception("Não foi possível excluir os registros.", exception);

            }
        }

        public Responsavel GetResponsavel(int responsavelUID)
        {
            try
            {
                ResponsavelDAL objDAL = new ResponsavelDAL();
                return objDAL.GetResponsavel(responsavelUID);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }


    }
}
