﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.Business
{
    public class AuxiliadoBO
    {
        private AuxiliadoDAL auxiliadoDAL = new AuxiliadoDAL();
        
        internal void Salvar(DataAccess.Auxiliado auxiliado)
        {
            auxiliadoDAL.Salvar(auxiliado);
        }

        internal List<Auxiliado> GetAuxiliados(int folhaPagamentoUID)
        {
            return auxiliadoDAL.GetAuxiliados(folhaPagamentoUID);
        }

        internal vwAluno GetAlunoAuxiliado(int auxiliadoUID)
        {
            return auxiliadoDAL.GetAlunoAuxiliado(auxiliadoUID);
        }

        internal Auxiliado GetAuxiliado(int auxiliadoUID)
        {
            return auxiliadoDAL.GetAuxiliado(auxiliadoUID);


        }

        internal void Atualizar(Auxiliado auxiliado)
        {
            auxiliadoDAL.Atualizar(auxiliado);
        }

        internal List<AuxilioFolhaPagamento> GetAuxiliadosFolhaPagamento(int folhaPagamentoUID, bool somenteAtivos)
        {
            return auxiliadoDAL.GetAuxiliadosFolhaPagamento(folhaPagamentoUID, somenteAtivos);
        }

        internal void SalvarBolsistaAuxilioFolhaPagamento(List<BolsistaAuxilioFolhaPagamento> listBolsistasAuxilios)
        {
            BolsistaAuxilioFolhaPagamentoDAL bafDAL = new BolsistaAuxilioFolhaPagamentoDAL();
            foreach (BolsistaAuxilioFolhaPagamento baf in listBolsistasAuxilios)
            {
                bafDAL.Salvar(baf);
            }
        }

        internal void LimparBolsistaAuxilioFolhaPagamento()
        {
            new BolsistaAuxilioFolhaPagamentoDAL().Limpar();
        }

        internal void Excluir(int auxiliadoUID, int folhaPagamentoUID)
        {
            BolsistaAuxilioFolhaPagamentoDAL bafDAL = new BolsistaAuxilioFolhaPagamentoDAL();
            bafDAL.Limpar();

            AuxilioFolhaPagamentoDAL afDAL = new AuxilioFolhaPagamentoDAL();
            afDAL.Excluir(auxiliadoUID, folhaPagamentoUID);

            AuxilioAlunoDAL aaDAL = new AuxilioAlunoDAL();
            aaDAL.Excluir(auxiliadoUID);

            this.auxiliadoDAL.Excluir(auxiliadoUID);
        }

        internal List<BolsistaAuxilioFolhaPagamento> GetBolsistasAuxilioFolhaPagamento(int folhaPagamentoUID)
        {
            BolsistaAuxilioFolhaPagamentoDAL bafDAL = new BolsistaAuxilioFolhaPagamentoDAL();
            return bafDAL.GetBolsistasAuxilioFolhaPagamento(folhaPagamentoUID);
        }

        internal List<Auxiliado> GetAuxiliados(int folhaPagamentoUID, string nome)
        {
            return auxiliadoDAL.GetAuxiliados(folhaPagamentoUID, nome);
        }

        internal AuxilioFolhaPagamento GetAuxilioFolhaPagamento(string cpf, int folhaPagamentoUID)
        {
            return auxiliadoDAL.GetAuxilioFolhaPagamento(cpf, folhaPagamentoUID);
        }

        internal void AtualizarVinculoInstitucional(int folhaPagamentoUID)
        {
            new AuxilioFolhaPagamentoDAL().AtualizarVinculoInstitucional(folhaPagamentoUID);
        }
    }
}
