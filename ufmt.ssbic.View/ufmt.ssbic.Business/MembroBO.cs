﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ufmt.sig.entity;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.Business
{
    public class MembroBO
    {
        public void SalvarCoordenador(long pessoaUID, Projeto projeto, int cargaHoraria)
        {
            PessoaBO pessoaBO = new PessoaBO();
            Servidor servidor = pessoaBO.GetServidor(pessoaUID);

            PessoaMembro pessoaMembro = new PessoaMembro();
            pessoaMembro.registro = servidor.Registro;
            pessoaMembro.nome = servidor.Pessoa.Nome;

            //TITULAÇÃO 1 É IGUAL A 'NÃO INFORMADO'
            int titulacaoUID = 1;
            if(servidor.Pessoa.TipoTitulacao != null)
                titulacaoUID = servidor.Pessoa.TipoTitulacao.TipoTitulacaoUID;

            pessoaMembro.tipoTitulacaoUID = titulacaoUID;
            pessoaMembro.instituicao = "UFMT";
            pessoaMembro.pessoaUID = servidor.Pessoa.PessoaUID;
            pessoaMembro.cpf = servidor.Pessoa.Cpf;

            MembroDAL membroDAL = new MembroDAL();
            PessoaMembro pmembro = membroDAL.GetPessoaMembro(pessoaUID);

            PessoaMembro coordenador;

            if (pmembro == null)
            {
                coordenador = membroDAL.SaveCoordenador(pessoaMembro, 1);
            }
            else
            {
                coordenador = pmembro;
            }            

            Membro membro = new Membro();
            membro.pessoaMembroUID = coordenador.pessoaMembroUID;
            membro.projetoUID = projeto.projetoUID;
            membro.funcaoProjetoUID = 1;
            membro.cargaHoraria = cargaHoraria;

            membroDAL.SaveMembro(membro, 1);                
        }

        public void AtualizarCoordenador(long pessoaUID, Projeto projeto, int cargaHoraria)
        {
            PessoaBO pessoaBO = new PessoaBO();
            Servidor servidor = pessoaBO.GetServidor(pessoaUID);

            PessoaMembro pessoaMembro = new PessoaMembro();
            pessoaMembro.registro = servidor.Registro;
            pessoaMembro.nome = servidor.Pessoa.Nome;

            //TITULAÇÃO 1 É IGUAL A 'NÃO INFORMADO'
            int titulacaoUID = 1;
            if (servidor.Pessoa.TipoTitulacao != null)
                titulacaoUID = servidor.Pessoa.TipoTitulacao.TipoTitulacaoUID;

            pessoaMembro.tipoTitulacaoUID = titulacaoUID;
            pessoaMembro.instituicao = "UFMT";            
            pessoaMembro.pessoaUID = servidor.Pessoa.PessoaUID;
            pessoaMembro.cpf = servidor.Pessoa.Cpf;

            MembroDAL membroDAL = new MembroDAL();
            PessoaMembro pmembro = membroDAL.GetPessoaMembro(pessoaUID);

            PessoaMembro coordenador;

            if (pmembro == null)
            {
                coordenador = membroDAL.SaveCoordenador(pessoaMembro, 1);
            }
            else
            {
                coordenador = pmembro;
            }

            Membro membroCoordenador = membroDAL.GetMembroCoordenador(projeto.projetoUID);

            Membro membro = new Membro();

            membro.membroUID = membroCoordenador.membroUID;
            membro.pessoaMembroUID = coordenador.pessoaMembroUID;
            membro.projetoUID = projeto.projetoUID;
            membro.funcaoProjetoUID = 1;
            membro.cargaHoraria = cargaHoraria;

            membroDAL.SaveMembro(membro, 2);
        }

        public PessoaMembro GetCoordenadorProjeto(long projetoUID)
        {
            MembroDAL membroDAL = new MembroDAL();
            return membroDAL.GetCoordenadorProjeto(projetoUID);
        }

        public void SalvarMembroProjeto(long pessoaUID, int projetoUID, int cargaHoraria)
        {
            PessoaBO pessoaBO = new PessoaBO();
            Servidor servidor = pessoaBO.GetServidor(pessoaUID);
            
            MembroDAL membroDAL = new MembroDAL();
            PessoaMembro pmembro = membroDAL.GetPessoaMembro(pessoaUID);

            PessoaMembro membroProjeto;

            if (pmembro == null)
            {
                PessoaMembro pessoaMembro = new PessoaMembro();
                pessoaMembro.registro = servidor.Registro;
                pessoaMembro.nome = servidor.Pessoa.Nome;

                //TITULAÇÃO 1 É IGUAL A 'NÃO INFORMADO'
                int titulacaoUID = 1;
                if (servidor.Pessoa.TipoTitulacao != null)
                    titulacaoUID = servidor.Pessoa.TipoTitulacao.TipoTitulacaoUID;

                pessoaMembro.tipoTitulacaoUID = titulacaoUID;
                pessoaMembro.instituicao = "UFMT";
                pessoaMembro.pessoaUID = servidor.Pessoa.PessoaUID;
                pessoaMembro.cpf = servidor.Pessoa.Cpf;

                membroProjeto = membroDAL.SavePessoaMembro(pessoaMembro, 1);
            }
            else
            {
                membroProjeto = pmembro;
            }
            
            Membro membro = new Membro();
            membro.pessoaMembroUID = membroProjeto.pessoaMembroUID;
            membro.projetoUID = projetoUID;
            membro.funcaoProjetoUID = 2;
            membro.cargaHoraria = cargaHoraria;

            membroDAL.SaveMembro(membro, 1);    
        }

        public void SalvarMembroProjeto(String nome, String instituicao, int tipoTitulacaoUID, int cargaHoraria, int projetoUID, String cpf)
        {
            MembroDAL membroDAL = new MembroDAL();
            PessoaMembro pmembro = membroDAL.GetPessoaMembro(cpf);

            PessoaMembro membroProjeto;

            if (pmembro == null)
            {
                PessoaMembro pessoaMembro = new PessoaMembro();
                pessoaMembro.registro = String.Empty;
                pessoaMembro.nome = nome;
                pessoaMembro.tipoTitulacaoUID = tipoTitulacaoUID;
                pessoaMembro.instituicao = instituicao;
                pessoaMembro.pessoaUID = null;
                pessoaMembro.cpf = cpf;

                membroProjeto = membroDAL.SavePessoaMembro(pessoaMembro, 1);
            }
            else
            {
                membroProjeto = pmembro;
            }

            Membro membro = new Membro();
            membro.pessoaMembroUID = membroProjeto.pessoaMembroUID;
            membro.projetoUID = projetoUID;
            membro.funcaoProjetoUID = 2;
            membro.cargaHoraria = cargaHoraria;

            membroDAL.SaveMembro(membro, 1);
        }

        public List<PessoaMembro> GetMembrosProjeto(int projetoUID, int funcaoProjetoUID)
        {
            MembroDAL membroDAL = new MembroDAL();
            List<PessoaMembro> pessoasMembroProjeto = membroDAL.GetMembrosProjeto(projetoUID, funcaoProjetoUID);
            return pessoasMembroProjeto;
        }

        public List<PessoaMembro> GetMembrosProjeto(int projetoUID)
        {
            MembroDAL membroDAL = new MembroDAL();
            List<PessoaMembro> pessoasMembroProjeto = membroDAL.GetMembrosProjeto(projetoUID);
            return pessoasMembroProjeto;
        }

        public PessoaMembro GetPessoaMembro(String cpf)
        {
            MembroDAL membroDAL = new MembroDAL();
            PessoaMembro pessoaMembro = new PessoaMembro();
            pessoaMembro = membroDAL.GetPessoaMembro(cpf);
            return pessoaMembro;
        }

        public List<PessoaMembro> BuscaPessoaMembro(String param, int tipoParam)
        {
            MembroDAL membroDAL = new MembroDAL();
            List<PessoaMembro> pessoasMembro = new List<PessoaMembro>();
            if (tipoParam == 1)
            {
                pessoasMembro = membroDAL.GetPessoaMembroPorNome(param);
            }
            else
            {
                PessoaMembro pessoaMembro = membroDAL.GetPessoaMembro(param);                
                if(pessoaMembro != null)
                    pessoasMembro.Add(pessoaMembro);
            }

            return pessoasMembro;
        }
        

        public Membro GetMembro(int pessoaMembroUID, int projetoUID)
        {
            MembroDAL membroDAL = new MembroDAL();
            Membro membro = membroDAL.GetMembro(pessoaMembroUID, projetoUID);

            return membro;
        }

        public bool ExcluirMembro(int pessoaMembroUID, int projetoUID)
        {
            try
            {
                Membro membro = GetMembro(pessoaMembroUID, projetoUID);

                MembroDAL membroDAL = new MembroDAL();
                return membroDAL.DeleteMembro(membro);
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        public List<PessoaMembro> GetMembros(int funcaoProjetoUID)
        {
            MembroDAL membroDAL = new MembroDAL();
            return membroDAL.GetMembros(funcaoProjetoUID);
        }
        
    }
}
