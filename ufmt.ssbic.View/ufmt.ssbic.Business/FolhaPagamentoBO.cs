﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.Business
{
    public class FolhaPagamentoBO
    {
        public List<FolhaPagamento> GetFolhasPagamento(int? processoSeletivoUID)
        {
            try
            {
                if (processoSeletivoUID != null)
                {
                    FolhaPagamentoDAL folhaDAL = new FolhaPagamentoDAL();
                    return folhaDAL.GetRegistros(processoSeletivoUID);
                }
                else
                {
                    throw new Exception("Não foi possível recuperar os registros.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível recuperar os registros.", ex);
            }

        }

        public List<FolhaPagamento> GetFolhasPagamento(int? processoSeletivoUID, DateTime dataInicio)
        {
            try
            {
                if (processoSeletivoUID != null)
                {
                    FolhaPagamentoDAL folhaDAL = new FolhaPagamentoDAL();
                    return folhaDAL.FindRegistros(processoSeletivoUID, dataInicio);
                }
                else
                {
                    throw new Exception("Não foi possível recuperar os registros.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível recuperar os registros.", ex);
            }

        }

        public FolhaPagamento GetFolhaPagamento(int folhaPagamentoUID)
        {            
            try
            {
                FolhaPagamentoDAL folhaDAL = new FolhaPagamentoDAL();
                return folhaDAL.GetFolhaPagamento(folhaPagamentoUID);
             
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível recuperar os registros.", ex);
            }
        
        }

        public IList<FolhaPagamento> GetFolhaPagamentoProcessoSeletivo(int processoSeletivoUID)
        {
            try
            {
                FolhaPagamentoDAL folhaDAL = new FolhaPagamentoDAL();
                return folhaDAL.GetFolhaPagamentoProcessoSeletivo(processoSeletivoUID);

            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível recuperar os registros.", ex);
            }

        }

        public void AtualizarFolhaPagamento(int folhaPagamentoUID, int? etapa1, int? etapa2, int? etapa3, 
            string parecer2, string parecer3, bool suplementar)
        {
            FolhaPagamentoDAL folhaDAL = new FolhaPagamentoDAL();
            FolhaPagamento folha = folhaDAL.GetFolhaPagamento(folhaPagamentoUID);

            if (etapa1.HasValue)
                folha.etapa1 = etapa1.Value;
            
            if (etapa2.HasValue)
                folha.etapa2 = etapa2.Value;

            if (etapa3.HasValue)
                folha.etapa3 = etapa3.Value;

            if (!String.IsNullOrEmpty(parecer2))
                folha.parecerE2 = parecer2;
            
            if (!String.IsNullOrEmpty(parecer3))
                folha.parecerE2 = parecer3;

            if (suplementar)
                folha.suplementar = suplementar;

            folhaDAL.SalvarFolhaPagamento(folha, 2);
        }

        public void SalvarFolhaPagamento(int processoSeletivoUID, DateTime dataInicio, DateTime dataFim)
        {
            FolhaPagamentoDAL folhaDAL = new FolhaPagamentoDAL();
            folhaDAL.GerarNovaFolhaPagamento(processoSeletivoUID, dataInicio, dataFim);

            /*
            BolsistaFolhaPagamentoBO bfpBO = new BolsistaFolhaPagamentoBO();
            List<BolsistaFolhaPagamento> listBFP = bfpBO.GetBolsistasUltimaFolhaPagamento(processoSeletivoUID);
                       
            FolhaPagamento folha = new FolhaPagamento();
            folha.processoSeletivoUID = processoSeletivoUID;
            folha.dataInicio = dataInicio;
            folha.dataFim = dataFim;
            folha.etapa1 = 1;                      
            FolhaPagamento folhaPagamento = folhaDAL.SalvarFolhaPagamento(folha, 1);

            if (listBFP != null)
            {                
                foreach (BolsistaFolhaPagamento bfp in listBFP)
                {
                    BolsistaFolhaPagamento newBfp = new BolsistaFolhaPagamento();
                    newBfp.folhaPagamentoUID = folhaPagamento.folhaPagamentoUID;
                    newBfp.pagamentoAutorizado = false;
                    newBfp.pagamentoRecusado = false;
                    newBfp.parecer = "";
                    newBfp.bolsistaUID = bfp.bolsistaUID;
                    newBfp.ativo = true;
                    
                    bfpBO.Salvar(newBfp);
                }
            }
            */
        }       

        public bool VerificarFechamentoFolhaPagamento(int folhaPagamentoUID)
        {
            BolsistaDAL bolsistaDAL = new BolsistaDAL();
            List<vwBolsistasFolhaPagamento> bolsistasFolhaPagamento = bolsistaDAL.GetBolsistasFolhaPagamento(folhaPagamentoUID);

            bool fechada = false;
            foreach (vwBolsistasFolhaPagamento bolsistaFpagamento in bolsistasFolhaPagamento)
            {
                if (bolsistaFpagamento.pagamentoAutorizado == false)
                {
                    fechada = true;
                    break;
                }
            }

            return fechada;
        }

        public void FecharFolhaPagamento(int folhaPagamentoUID)
        {
            FolhaPagamentoDAL folhaPagamentoDAL = new FolhaPagamentoDAL();
            FolhaPagamento folhaPagamento = folhaPagamentoDAL.GetFolhaPagamento(folhaPagamentoUID);

            folhaPagamento.fechada = true;
            folhaPagamento.etapa3 = 3;

            folhaPagamentoDAL.SalvarFolhaPagamento(folhaPagamento, 2);
        }

        public void Atualizar(FolhaPagamento folhaPagamento)
        {
            try
            {
                FolhaPagamentoDAL folhaDAL = new FolhaPagamentoDAL();
                FolhaPagamento folha = folhaDAL.GetFolhaPagamento(folhaPagamento.folhaPagamentoUID);

                folha.etapa1 = folhaPagamento.etapa1;
                folha.etapa2 = folhaPagamento.etapa2;
                folha.etapa3 = folhaPagamento.etapa3;
                folha.parecerE2 = folhaPagamento.parecerE2;
                folha.parecerE2 = folhaPagamento.parecerE3;
                folha.suplementar = folhaPagamento.suplementar;
                folha.fechada = folhaPagamento.fechada;

                folhaDAL.SalvarFolhaPagamento(folha, 2);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        internal void AtualizarEtapaFinanceiro(int folhaPagamentoUID, int situacao)
        {
            try
            {
                FolhaPagamentoDAL fpDAL = new FolhaPagamentoDAL();
                FolhaPagamento fp = fpDAL.GetFolhaPagamento(folhaPagamentoUID);

                fp.etapa3 = situacao;

                fpDAL.SalvarFolhaPagamento(fp, 2);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        internal FolhaPagamento GetUltimaFolhaPagamento(int processoSeletivoUID)
        {
            try
            {
                FolhaPagamentoDAL fpDAL = new FolhaPagamentoDAL();
                return fpDAL.GetUltimaFolhaPagamento(processoSeletivoUID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        public void SalvarFolhaPagamentoSuplementar(int processoSeletivoUID, DateTime dataInicio, DateTime dataFim)
        {
            FolhaPagamentoSuplementarDAL folhaDAL = new FolhaPagamentoSuplementarDAL();
            folhaDAL.SalvarFolhaPagamentoSuplementar(processoSeletivoUID, dataInicio, dataFim);
        }

        public List<FolhaPagamentoSuplementar> GetFolhasPagamentoSuplementarPorProcessoSeletivo(int processoSeletivoUID)
        {
            FolhaPagamentoSuplementarDAL folhaDAL = new FolhaPagamentoSuplementarDAL();
            return folhaDAL.GetFolhaPagamentoSuplementarProcessoSeletivo(processoSeletivoUID);
        }

        internal BolsistaFolhaSuplementar GetAlunoEmFolhaSuplementar(string cpf, int folhaSuplementarUID)
        {
            FolhaPagamentoSuplementarDAL folhaDAL = new FolhaPagamentoSuplementarDAL();
            return folhaDAL.GetAlunoEmFolhaSuplementar(cpf, folhaSuplementarUID);
        }
    }
}
