﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.Business
{
    public class BolsistaFolhaPagamentoBO
    {
        private BolsistaFolhaPagamentoDAL bfpDAL;

        public BolsistaFolhaPagamentoBO()
        {
            this.bfpDAL = new BolsistaFolhaPagamentoDAL();
        }

        public void Salvar(BolsistaFolhaPagamento bolsistaFolhaPagamento)
        {
            try
            {
                this.bfpDAL.Save(bolsistaFolhaPagamento);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<BolsistaFolhaPagamento> GetBolsistasUltimaFolhaPagamento(int processoSeletivoUID)
        {
            FolhaPagamentoBO folhaBO = new FolhaPagamentoBO();
            FolhaPagamento folha = folhaBO.GetFolhaPagamentoProcessoSeletivo(processoSeletivoUID).LastOrDefault();

            if (folha != null)
            {
                BolsistaBO bolsistaBO = new BolsistaBO();
                return bolsistaBO.GetBolsistasFolhaPagamentoPorId(folha.folhaPagamentoUID);
            }
            else
            {
                return null;
            }
        }

        internal BolsistaFolhaPagamento GetBolsistaFolhaPagamentoPorBolsista(int bolsistaUID)
        {
            try
            {
                return this.bfpDAL.GetBolsistaFolhaPagamentoPorBolsista(bolsistaUID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }




        internal bool VerificarSituacao(int bolsistaUID, int folhaPagamentoUID)
        {
            try
            {
                BolsistaFolhaPagamento bfp = 
                    this.bfpDAL.GetBolsistaFolhaPagamento(bolsistaUID, folhaPagamentoUID);

                return bfp.ativo;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        internal void AtualizarSituacaoBolsistaFolhaPagamento(int bolsistaUID, int folhaPagamentoUID, bool ativo, String justificativa)
        {
            try 
            {
                bfpDAL.AtualizarSituacaoBolsistaFolhaPagamento(bolsistaUID, folhaPagamentoUID, ativo, justificativa);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        internal BolsistaFolhaPagamento GetBolsistaFolhaPagamentoPorBolsista(int bolsistaUID, int folhaPagamentoUID)
        {
            try
            {
                return bfpDAL.GetBolsistaFolhaPagamento(bolsistaUID, folhaPagamentoUID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        internal List<BolsistaFolhaPagamento> GetBolsistasPorFolhaPagamento(int folhaPagamentoUID, bool somenteAtivos)
        {
            try
            {
                return bfpDAL.GetBolsistasPorFolhaPagamento(folhaPagamentoUID, somenteAtivos);
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        internal BolsistaFolhaPagamento GetBolsistaUltimaFolhaPagamento(int bolsistaUID)
        {
            try
            {
                return bfpDAL.GetBolsistaUltimaFolhaPagamento(bolsistaUID);
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        internal BolsistaFolhaPagamento GetBolsistaFolhaPagamento(int bolsistaUID, int folhaPagamentoUID)
        {
            return bfpDAL.GetBolsistaFolhaPagamento(bolsistaUID, folhaPagamentoUID);
        }

        internal void AtualizarVinculoInstitucionalBolsistas(int folhaPagamentoUID)
        {
            bfpDAL.AtualizarVinculoInstitucionalBolsistas(folhaPagamentoUID);
        }
    }
}
