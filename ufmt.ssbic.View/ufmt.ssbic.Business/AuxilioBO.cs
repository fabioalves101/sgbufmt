﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.Business
{
    public class AuxilioBO
    {
        internal List<Auxilio> GetAuxilios()
        {
            return new AuxilioDAL().GetAuxilios();
        }


        internal Auxilio GetAuxilio(int auxilioUID)
        {
            return new AuxilioDAL().GetAuxilio(auxilioUID);
        }



        internal List<AuxilioAluno> GetAuxiliosAuxiliado(int auxiliadoUID)
        {
            return new AuxilioDAL().GetAuxiliosAuxiliado(auxiliadoUID);
        }

        internal Auxiliado GetAuxiliado(int auxiliadoUID)
        {
            return new AuxilioDAL().GetAuxiliado(auxiliadoUID);
        }
    }
}
