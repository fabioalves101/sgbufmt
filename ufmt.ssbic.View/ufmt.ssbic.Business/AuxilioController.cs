﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.Business
{
    public class AuxilioController
    {
        public List<Auxilio> GetAuxilios()
        {
            return new AuxilioBO().GetAuxilios();
        }

        public Auxilio GetAuxilio(int auxilioUID)
        {
            return new AuxilioBO().GetAuxilio(auxilioUID);
        }

        public void CadastrarAuxiliado(Auxiliado auxiliado, List<Auxilio> auxilios, int folhaPagamentoUID)
        {
            AuxiliadoBO auxiliadoBO = new AuxiliadoBO();

            foreach (Auxilio auxilio in auxilios)
            {
                AuxilioAluno auxilioAluno = new AuxilioAluno()
                {
                    auxilioUID = auxilio.auxilioUID,
                    Auxiliado = auxiliado
                };

                auxiliado.AuxilioAluno.Add(auxilioAluno);                
            }

            auxiliado.AuxilioFolhaPagamento.Add(
                new AuxilioFolhaPagamento() {
                    ativo = true,
                    Auxiliado = auxiliado,
                    folhaPagamentoUID = folhaPagamentoUID                    
                }
            );

            auxiliadoBO.Salvar(auxiliado);
        }

        public List<AuxiliadoAlunoSiga> GetAuxiliados(int folhaPagamentoUID)
        {
            List<Auxiliado> listAuxiliado = new AuxiliadoBO().GetAuxiliados(folhaPagamentoUID);

            List<AuxiliadoAlunoSiga> listAlunoAuxiliado = new List<AuxiliadoAlunoSiga>();
            AlunoBO alunoBO = new AlunoBO();
            foreach (Auxiliado auxiliado in listAuxiliado)
            {                
                AuxiliadoAlunoSiga alunoAuxiliado = new AuxiliadoAlunoSiga();
                alunoAuxiliado.Auxiliado = auxiliado;
                alunoAuxiliado.AuxiliadoUID = auxiliado.auxiliadoUID;

                alunoAuxiliado.AuxilioFolhaPagamento = auxiliado.AuxilioFolhaPagamento.FirstOrDefault();
                alunoAuxiliado.ListAuxilioAluno = auxiliado.AuxilioAluno.ToList();
                
                alunoAuxiliado.Aluno = alunoBO.GetAluno(decimal.Parse(auxiliado.registroAluno));
                listAlunoAuxiliado.Add(alunoAuxiliado);
            }

            return listAlunoAuxiliado;
        }

        public List<AuxiliadoAlunoSiga> GetAuxiliados(int folhaPagamentoUID, string nome)
        {
            List<Auxiliado> listAuxiliado = new AuxiliadoBO().GetAuxiliados(folhaPagamentoUID, nome);

            List<AuxiliadoAlunoSiga> listAlunoAuxiliado = new List<AuxiliadoAlunoSiga>();
            AlunoBO alunoBO = new AlunoBO();
            foreach (Auxiliado auxiliado in listAuxiliado)
            {
                AuxiliadoAlunoSiga alunoAuxiliado = new AuxiliadoAlunoSiga();
                alunoAuxiliado.Auxiliado = auxiliado;
                alunoAuxiliado.AuxiliadoUID = auxiliado.auxiliadoUID;

                alunoAuxiliado.AuxilioFolhaPagamento = auxiliado.AuxilioFolhaPagamento.FirstOrDefault();
                alunoAuxiliado.ListAuxilioAluno = auxiliado.AuxilioAluno.ToList();

                alunoAuxiliado.Aluno = alunoBO.GetAluno(decimal.Parse(auxiliado.registroAluno));
                listAlunoAuxiliado.Add(alunoAuxiliado);
            }

            return listAlunoAuxiliado;
        }

        public vwAluno GetAlunoAuxiliado(int auxiliadoUID)
        {
            AuxiliadoBO auxBO = new AuxiliadoBO();
            return auxBO.GetAlunoAuxiliado(auxiliadoUID);
        }

        public Auxiliado GetAuxiliado(int auxiliadoUID)
        {
            AuxiliadoBO auxBO = new AuxiliadoBO();
            return auxBO.GetAuxiliado(auxiliadoUID);
        }

        public List<AuxilioAluno> GetAuxiliosAuxiliado(int auxiliadoUID)
        {
            AuxilioBO auxBO = new AuxilioBO();
            return auxBO.GetAuxiliosAuxiliado(auxiliadoUID);
        }

        public void AtualizarAuxiliado(Auxiliado auxiliado, List<Auxilio> auxiliosSelecionados, int folhaPagamentoUID)
        {
            AuxiliadoBO auxBO = new AuxiliadoBO();

            foreach (Auxilio auxilio in auxiliosSelecionados)
            {
                AuxilioAluno auxilioAluno = new AuxilioAluno()
                {
                    auxilioUID = auxilio.auxilioUID,
                    Auxiliado = auxiliado
                };

                auxiliado.AuxilioAluno.Add(auxilioAluno);
            }

            auxBO.Atualizar(auxiliado);
        }

        public List<AuxilioFolhaPagamento> GetAuxiliadosFolhaPagamento(int folhaPagamentoUID, bool somenteAtivos)
        {
            return new AuxiliadoBO().GetAuxiliadosFolhaPagamento(folhaPagamentoUID, somenteAtivos);
        }

        public void SalvarBolsistaAuxilioFolhaPagamento(List<BolsistaAuxilioFolhaPagamento> listBolsistasAuxilios)
        {
            new AuxiliadoBO().SalvarBolsistaAuxilioFolhaPagamento(listBolsistasAuxilios);
        }

        public void LimparBolsistaAuxilioFolhaPagamento()
        {
            new AuxiliadoBO().LimparBolsistaAuxilioFolhaPagamento();
        }

        public void Excluir(int auxiliadoUID, int folhaPagamentoUID)
        {
            new AuxiliadoBO().Excluir(auxiliadoUID, folhaPagamentoUID);
        }

        public List<BolsistaAuxilioFolhaPagamento> GetBolsistasAuxilioFolhaPagamento(int folhaPagamentoUID)
        {
            return new AuxiliadoBO().GetBolsistasAuxilioFolhaPagamento(folhaPagamentoUID);
        }

        public AuxilioFolhaPagamento GetAuxilioFolhaPagamento(string cpf, int folhaPagamentoUID)
        {
            return new AuxiliadoBO().GetAuxilioFolhaPagamento(cpf, folhaPagamentoUID);
        }

        public void AtualizarVinculoInstitucional(int folhaPagamentoUID)
        {
            new AuxiliadoBO().AtualizarVinculoInstitucional(folhaPagamentoUID);
        }
    }
}
