﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.Business
{
    public class AlunoProjetoBO
    {
        public List<vwAlunosProjetoResponsavel> GetAlunosProjeto(int projetoUID)
        {
            AlunoProjetoDAL alunoProjetoDAL = new AlunoProjetoDAL();
            List<vwAlunosProjetoResponsavel> alunosProjeto = alunoProjetoDAL.GetAlunosProjeto(projetoUID);

            return alunosProjeto;
        }

        public vwAluno GetAluno(Int64 matricula)
        {
            AlunoProjetoDAL alunoProjetoDAL = new AlunoProjetoDAL();
            return alunoProjetoDAL.GetAluno(matricula);
        }

        public void SalvarAluno(int projetoUID, int membroUID, Int64 registroAluno)
        {
            MembroDAL membroDAL = new MembroDAL();
            PessoaMembro pessoaMembro = membroDAL.GetPessoaMembroByMembro(membroUID);

            AlunoProjeto alunoProjeto = new AlunoProjeto();
            alunoProjeto.projetoUID = projetoUID;
            alunoProjeto.membroUID = membroUID;
            alunoProjeto.registroAluno = registroAluno.ToString();
            alunoProjeto.pessoaMembroUID = pessoaMembro.pessoaMembroUID;

            AlunoProjetoDAL alunoProjetoDAL = new AlunoProjetoDAL();
            alunoProjetoDAL.SaveAlunoProjeto(alunoProjeto, 1);            
        }

        public void ExcluirAlunoProjeto(int alunoProjetoUID)
        {
            AlunoProjetoDAL alunoProjetoDAL = new AlunoProjetoDAL();
            AlunoProjeto alunoProjeto = alunoProjetoDAL.GetAlunoProjeto(alunoProjetoUID);

            alunoProjetoDAL.DeleteAlunoProjeto(alunoProjeto);
        }

        public void ValidarRegras(Int64 matricula, int pessoaMembroUID, int projetoUID)
        {
            try
            {
                AlunoProjeto alunoProjeto = GetAlunoProjeto(matricula);

                if (alunoProjeto != null)
                    throw new Exception("O Aluno já está participando de um projeto.");

                this.VerificaMembroTitulacao(pessoaMembroUID);

                List<vwAlunosProjetoResponsavel> alunosProjeto = GetAlunosProjeto(projetoUID);
                if (alunosProjeto.Count >= 6)
                    throw new Exception("O limite de alunos por projeto foi alcançado.");
            }
            catch (Exception ex)
            {
                throw new Exception("Houve um erro ao verificar os dados selecionados. "+ex.Message);
            }

        }

        public AlunoProjeto GetAlunoProjeto(Int64 matricula)
        {
            AlunoProjetoDAL alunoProjetoDAL = new AlunoProjetoDAL();
            AlunoProjeto alunoProjeto = alunoProjetoDAL.GetAlunoProjeto(matricula);

            return alunoProjeto;
        }

        public void VerificaMembroTitulacao(int pessoaMembroUID)
        {
            try
            {
                MembroDAL membroDAL = new MembroDAL();
                PessoaMembro pessoaMembro = membroDAL.GetPessoaMembro(pessoaMembroUID);


                AlunoProjetoDAL alunoProjetoDAL = new AlunoProjetoDAL();

                List<vwAlunosProjetoResponsavel> alunosProjetoResponsavel =
                    alunoProjetoDAL.GetAlunosProjetoMembro(pessoaMembro.pessoaMembroUID);

                if (pessoaMembro.tipoTitulacaoUID == 8)
                {
                    if (alunosProjetoResponsavel.Count >= 2)
                        throw new Exception("O membro do projeto ultrapassou o número máximo de alunos que pode solicitar");
                }
                else if (pessoaMembro.tipoTitulacaoUID == 9)
                {
                    if (alunosProjetoResponsavel.Count >= 3)
                        throw new Exception("O membro do projeto ultrapassou o número máximo de alunos que pode solicitar");
                }
                else
                {
                    throw new Exception("O Membro do projeto não possui titulação suficiente para solicitar aluno");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
            

    }
}
