﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.Business
{
    public class CursoProcessoSeletivoBO
    {
        CursoProcessoSeletivoDAL cursoProcDAL;

        public CursoProcessoSeletivoBO()
        {
            this.cursoProcDAL = new CursoProcessoSeletivoDAL();
        }
        public void SalvarVagasPorCurso(List<CursoProcessoSeletivo> lista)
        {
            try
            {
                foreach (CursoProcessoSeletivo cursoProc in lista)
                {
                    this.cursoProcDAL.Save(cursoProc);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível salvar o registro. "+ex.Message);
            }
        }

        public List<vwCursoProcessoSeletivo> GetCursosProcessoSeletivo(int processoSeletivoUID)
        {
            try
            {
                return this.cursoProcDAL.GetRegistros(processoSeletivoUID);
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível selecionar os registros. " + ex.Message);
            }

        }

        public void Excluir(int cursoProcUID)
        {
            try
            {
                this.cursoProcDAL.Delete(cursoProcUID);
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível selecionar os registros. " + ex.Message);
            }
        }
    }
}
