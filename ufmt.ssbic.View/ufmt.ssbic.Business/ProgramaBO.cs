﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.Business
{
    public class ProgramaBO
    {
        internal Proreitoria GetPrograma(int programaUID)
        {
            try
            {
                return new ProgramaDAL().GetPrograma(programaUID);
            }
            catch
            {
                throw;
            }

        }

        internal List<Proreitoria> GetListPrograma()
        {
            try
            {
                return new ProgramaDAL().GetListPrograma();
            }
            catch
            {
                throw;
            }

        }
    }
}
