﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.Business
{
    public class BolsistaController
    {
        public BolsistaFolhaPagamento GetBolsistaFolhaPagamentoPorBolsista(int bolsistaUID)
        {
            try
            {
                BolsistaFolhaPagamentoBO bfpBO = new BolsistaFolhaPagamentoBO();
                return bfpBO.GetBolsistaFolhaPagamentoPorBolsista(bolsistaUID);
            }
            catch
            {
                throw;
            }
        }

        public bool VerificarSituacaoBolsistaFolhaPagamento(int bolsistaUID, int folhaPagamentoUID)
        {
            try
            {
                BolsistaFolhaPagamentoBO bfpBO = new BolsistaFolhaPagamentoBO();
                return bfpBO.VerificarSituacao(bolsistaUID, folhaPagamentoUID);
            }
            catch
            {
                throw;
            }
        }

        public void AtualizarSituacaoBolsista(int bolsistaUID, int folhaPagamentoUID, int situacao, String justificativa)
        {
            try
            {
                BolsistaFolhaPagamentoBO bfpBO = new BolsistaFolhaPagamentoBO();

                bool ativo = false;
                if (situacao == 1)
                    ativo = true;

                bfpBO.AtualizarSituacaoBolsistaFolhaPagamento(bolsistaUID, folhaPagamentoUID, ativo, justificativa);
            }
            catch
            {
                throw;
            }
        }

        public BolsistaFolhaPagamento GetBolsistaPorFolhaPagamento(int bolsistaUID, int folhaPagamentoUID)
        {
            try
            {
                BolsistaFolhaPagamentoBO bfpBO = new BolsistaFolhaPagamentoBO();
                return bfpBO.GetBolsistaFolhaPagamentoPorBolsista(bolsistaUID, folhaPagamentoUID);
            }
            catch
            {
                throw;
            }
        }

        public List<BolsistaFolhaPagamento> GetBolsistasPorFolhaPagamento(int folhaPagamentoUID, bool somenteAtivos)
        {
            try
            {
                BolsistaFolhaPagamentoBO bfpBO = new BolsistaFolhaPagamentoBO();
                return bfpBO.GetBolsistasPorFolhaPagamento(folhaPagamentoUID, somenteAtivos);
            }
            catch
            {
                throw;
            }
        }

        public Bolsista GetBolsista(int bolsistaUID)
        {
            try
            {
                return new BolsistaBO().GetBolsistaPorId(bolsistaUID);
            }
            catch
            {
                throw;
            }
        }

        public BolsistaFolhaPagamento GetBolsistaFolhaPagamento(int bolsistaUID, int folhaPagamentoUID)
        {
            BolsistaFolhaPagamentoBO bfpBO = new BolsistaFolhaPagamentoBO();
            return bfpBO.GetBolsistaFolhaPagamento(bolsistaUID, folhaPagamentoUID);
        }


        public void AtualizarVinculoInstitucionalBolsistas(int folhaPagamentoUID)
        {
            BolsistaFolhaPagamentoBO bfpBO = new BolsistaFolhaPagamentoBO();
            bfpBO.AtualizarVinculoInstitucionalBolsistas(folhaPagamentoUID);
        }
    }
}
