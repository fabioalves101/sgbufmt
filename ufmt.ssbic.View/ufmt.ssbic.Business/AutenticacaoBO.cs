﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ufmt.sig.entity;
using ufmt.sig.business;

namespace ufmt.ssbic.Business
{
    public class AutenticacaoBO
    {
        public Usuario Login(String cpf, String senha)
        {
            using(IAutenticacaoBO autenticacaoBO = BusinessFactory.GetInstance<IAutenticacaoBO>())
            {
                Usuario usuario = autenticacaoBO.GetUsuario(cpf, senha);

                PermissaoUsuario permissao = autenticacaoBO.GetPermissaoUsuario(14, 36, usuario.UsuarioUID);

                return usuario;
            }            
        }

        public bool VerificaPermissaoEspecifica(long usuarioUID, long permissaoUID)
        {
            using (IAutenticacaoBO autenticacaoBO = BusinessFactory.GetInstance<IAutenticacaoBO>())
            {
                PermissaoUsuario permissaoUsuario;

                permissaoUsuario = autenticacaoBO.GetPermissaoUsuario(14, permissaoUID, usuarioUID);
                if (permissaoUsuario != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public List<PermissaoUsuario> VerificaPermissoes(long usuarioUID)
        {
            using (IAutenticacaoBO autenticacaoBO = BusinessFactory.GetInstance<IAutenticacaoBO>())
            {
                //SOLICITA AO ufmt.sig TODAS AS PERMISSÕES DO USUÁRIO NO SISTEMA
                List<PermissaoUsuario> permissoes = autenticacaoBO.GetPermissoesUsuario(14, usuarioUID).ToList<PermissaoUsuario>();
                return permissoes;
            }            
        }
        
        public List<Permissao> GetPermissoes(long usuarioUID)
        {

            AutenticacaoBO autenticacaoBO = new AutenticacaoBO();
            List<PermissaoUsuario> permissoesUsuario = autenticacaoBO.VerificaPermissoes(usuarioUID);

            List<Permissao> permissoes = new List<Permissao>();
            foreach (PermissaoUsuario pUsuario in permissoesUsuario)
            {
                Permissao permissao = new Permissao();
                permissao.PermissaoUID = pUsuario.Permissao.PermissaoUID;
                permissao.Nome = pUsuario.Permissao.Nome;
                permissao.Descricao = pUsuario.Permissao.Descricao;
                permissao.Aplicacao = pUsuario.Permissao.Aplicacao;
                permissoes.Add(permissao);
            }

            return permissoes;
        }

        public Usuario GetUsuario(long usuarioUID)
        {
            using (IAutenticacaoBO autenticacaoBO = BusinessFactory.GetInstance<IAutenticacaoBO>())
            {
                return autenticacaoBO.GetUsuario(usuarioUID);
            }

        }

        public bool criaUsuario(String cpf, String registro, DateTime dataNascimento, String nomeMae, String email, int permissaoUID)
        {
            using (IAutenticacaoBO autenticacaobo = BusinessFactory.GetInstance<IAutenticacaoBO>())
            {
                try
                {
                    //Busca pessoa por CPF e e-mail
                    Pessoa pessoa = this.GetPessoa(cpf, email);
                    autenticacaobo.CrieUsuario(pessoa, cpf, null);

                    this.DefinePermissao(pessoa.PessoaUID, permissaoUID);
                    
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
                return true;
            }
        }

        public Pessoa GetPessoa(String cpf, String email)
        {
            using (IPessoaBO pessoabo = BusinessFactory.GetInstance<IPessoaBO>())
            {
                Pessoa pessoa = pessoabo.GetPessoa(cpf);
                if (pessoa != null)
                {
                    if (!String.IsNullOrEmpty(email))
                    {
                        pessoa.Mail = email;
                        pessoabo.SalvePessoa(pessoa);
                        pessoa = pessoabo.GetPessoa(cpf);
                    }
                    else
                        throw new Exception("O e-mail é nulo.");
                }
                else
                    throw new Exception("Pessoa não encontrada");

                return pessoa;

            }
        }

        public Servidor GetServidor(long servidorUID)
        {
            using (IPessoaBO pessoabo = BusinessFactory.GetInstance<IPessoaBO>())
            {
                Servidor servidor = pessoabo.GetServidor(servidorUID);
                
                if(servidor == null)
                    throw new Exception("Pessoa não encontrada");

                return servidor;
            }
        }

        public Servidor GetServidor(string registro)
        {
            using (IPessoaBO pessoabo = BusinessFactory.GetInstance<IPessoaBO>())
            {
                Servidor servidor = pessoabo.GetServidor(registro, 'S');

                if (servidor == null)
                    throw new Exception("Pessoa não encontrada");

                return servidor;
            }
        }

        public bool TrocaDeSenha(Usuario usuario, String senhaAtual, String novaSenha)
        {
            using (IAutenticacaoBO autenticacaobo = BusinessFactory.GetInstance<IAutenticacaoBO>())
            {
                try
                {
                    autenticacaobo.TroqueSenha(usuario, senhaAtual, novaSenha);
                  
                }
                catch (Exception e)
                {
                    return false;
                    throw new Exception(e.Message);
                }
                return true;
            }
        }

        public void DefinePermissao(long pessoaUID, int permissaoUID)
        {
            using (IAutenticacaoBO autenticacaobo = BusinessFactory.GetInstance<IAutenticacaoBO>())
            {
                Usuario usuario = autenticacaobo.GetUsuarioPorPessoa(pessoaUID);
                if (usuario == null)
                {
                    throw new Exception();
                }
                else
                {
                    Permissao permissao = new Permissao();
                    permissao.PermissaoUID = permissaoUID;

                    Aplicacao aplicacao = new Aplicacao();
                    aplicacao.AplicacaoUID = 14;

                    permissao.Aplicacao = aplicacao;

                    autenticacaobo.SalvePermissaoUsuario(permissao, usuario, null);
                }
            }
        }

        public Usuario GetUsuarioPorPessoa(int pessoaUID)
        {
            using (IAutenticacaoBO autenticacaobo = BusinessFactory.GetInstance<IAutenticacaoBO>())
            {
                return autenticacaobo.GetUsuarioPorPessoa(pessoaUID);
            }
        }

        public PermissaoUsuario GetPermissaoUsuario(long permissaoUsuarioUID)
        {
            using (IAutenticacaoBO autenticacaoBO = BusinessFactory.GetInstance<IAutenticacaoBO>())
            {
                return autenticacaoBO.GetPermissaoUsuario(permissaoUsuarioUID);
            }
        }

        public Permissao GetPermissao(long permissaoUID)
        {
            using (IAutenticacaoBO autenticacaoBO = BusinessFactory.GetInstance<IAutenticacaoBO>())
            {
                return autenticacaoBO.GetPermissao(permissaoUID);
            }
        }

    }
}
