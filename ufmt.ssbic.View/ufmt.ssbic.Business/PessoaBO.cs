﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ufmt.sig.entity;
using ufmt.sig.business;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.Business
{
    public class PessoaBO
    {
        public IList<Servidor> GetPessoas(String parametro, String tipoParametro)
        {
            using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
            {
                IList<Servidor> servidores = new List<Servidor>();
                if (tipoParametro.Equals("Nome"))
                {
                    servidores = pessoaBO.GetServidores( parametro, null, false, null, null);
                }
                else
                    if(tipoParametro.Equals("siape"))
                    {
                        servidores = pessoaBO.GetServidores(null, parametro, false, null, null);
                    }
                    else if (tipoParametro.Equals("CPF"))
                    {
                        Pessoa pessoa = pessoaBO.GetPessoa(parametro);

                        servidores = pessoaBO.GetVinculosPorPessoa(pessoa.PessoaUID, true);                         
                    }

                return servidores;
            }
        }

        public Pessoa GetPessoa(Int64 servidorUID)
        {
            using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
            {
                Servidor servidor = pessoaBO.GetServidor(servidorUID);

                return servidor.Pessoa;
            }
        }


        public Servidor GetServidor(Int64 pessoaUID)
        {
            using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
            {
                Servidor servidor = pessoaBO.GetVinculosPorPessoa(pessoaUID, true).Last<Servidor>();

                return servidor;
            }
        }

        public Servidor GetServidorUnico(Int64 servidorUID)
        {
            using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
            {
                Servidor servidor = pessoaBO.GetServidor(servidorUID);

                return servidor;
            }
        }

        

        

        


    }
}
