﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ufmt.ssbic.DataAccess;
using System.Collections;
using ufmt.sig.entity;

namespace ufmt.ssbic.Business
{
    
    public class InstrumentoSelecaoBO
    {

        public List<vwInstrumentoSelecao> GetRegistros(int? proreitoriaUID)
        {
            try
            {
                InstrumentoSelecaoDAL objDAL = new InstrumentoSelecaoDAL();
                return objDAL.GetRegistros(proreitoriaUID);

            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public List<TipoInstrumentoSelecao> GetTipoInstrumentoSelecao()
        {

            try
            {
                InstrumentoSelecaoDAL objDAL = new InstrumentoSelecaoDAL();
                return objDAL.GetTipoInstrumentoSelecao();

            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }


        public vwInstrumentoSelecao GetInstrumentoSelecao(int instrumentoSelecaoUID)
        {

            try
            {
                InstrumentoSelecaoDAL objDAL = new InstrumentoSelecaoDAL();
                return objDAL.GetInstrumentoSelecao(instrumentoSelecaoUID);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        
        
        }

        public List<vwInstrumentoSelecao> FindRegistros(int? proreitoriaUID, string criterio)
        {

            try
            {
                InstrumentoSelecaoDAL objDAL = new InstrumentoSelecaoDAL();
                return objDAL.FindRegistros(proreitoriaUID, criterio);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public int Salvar(InstrumentoSelecao instrumentoSelecao, bool hasArquivo, String nomeArquivo, int operacao)
        {
            try
            {
                InstrumentoSelecaoDAL objDAL = new InstrumentoSelecaoDAL();
                int instrumentoSelecaoUID = objDAL.Salvar(instrumentoSelecao, operacao);


                if (instrumentoSelecaoUID != 0)
                {
                    if (hasArquivo)
                    {
                        ArquivoInstrumentoSelecao arquivoIS = new ArquivoInstrumentoSelecao();
                        arquivoIS.instrumentoSelecaoUID = instrumentoSelecaoUID;
                        arquivoIS.nomeArquivo = nomeArquivo;

                        objDAL.SaveArquivoInstrumentoSelecao(arquivoIS);
                    }
                   
                }

                return instrumentoSelecaoUID;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível Salvar os registros.", exception);

            }
        }

        public bool Excluir(int instrumentoSelecaoUID)
        {

            try
            {
                InstrumentoSelecaoDAL objDAL = new InstrumentoSelecaoDAL();
                this.ExcluirArquivoIS(instrumentoSelecaoUID);

                return objDAL.Excluir(instrumentoSelecaoUID);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível excluir os registros.", exception);

            }
        }

        public List<vwInstrumentoSelecaoBolsa> FindRegistrosISB(int? proreitoriaUID, string criterio, long? cursoUID)
        {
            try
            {
                InstrumentoSelecaoDAL objDAL = new InstrumentoSelecaoDAL();

                List<vwInstrumentoSelecaoBolsa> registros = null;
                if(!String.IsNullOrEmpty(criterio))
                    registros = objDAL.FindRegistroISB(proreitoriaUID, criterio, cursoUID);
                else
                    registros = objDAL.GetInstrumentosProcessosBolsas(proreitoriaUID, cursoUID);

                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public List<vwInstrumentoSelecaoBolsa> GetInstrumentosPorResponsavel(int? proreitoriaUID, string criterio, long? cursoUID, InstrumentoSelecaoResponsavel responsavelIS)
        {
            try
            {
                InstrumentoSelecaoDAL objDAL = new InstrumentoSelecaoDAL();

                List<vwInstrumentoSelecaoBolsa> registros = null;
                if(String.IsNullOrEmpty(criterio))
                    registros = objDAL.GetInstrumentosPorResponsavel(proreitoriaUID, cursoUID, responsavelIS);
                else
                    registros = objDAL.GetInstrumentosPorResponsavelPorCriterio(proreitoriaUID, criterio, cursoUID, responsavelIS);
                
                return registros;
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public void FecharInstrumentoSelecao(int instrumentoSelecaoUID)
        {
            InstrumentoSelecao instrumentoSelecao = this.GetObjInstrumentoSelecao(instrumentoSelecaoUID);
            instrumentoSelecao.situacao = false;

            InstrumentoSelecaoDAL isDAL = new InstrumentoSelecaoDAL();
            isDAL.Salvar(instrumentoSelecao, 2);

        }

        public InstrumentoSelecao GetObjInstrumentoSelecao(int instrumentoSelecaoUID)
        {
            InstrumentoSelecaoDAL isDAL = new InstrumentoSelecaoDAL();
            return isDAL.GetObjInstrumentoSelecao(instrumentoSelecaoUID);
        }

        public void SalvarInstrumentoSelecaoResponsavel(int instrumentoSelecaoUID, long usuarioUID, long? cursoUID)
        {
            InstrumentoSelecaoResponsavel ISResponsavel = new InstrumentoSelecaoResponsavel();
            ISResponsavel.instrumentoSelecaoUID = instrumentoSelecaoUID;
            ISResponsavel.usuarioUID = usuarioUID;
            ISResponsavel.cursoUID = cursoUID;

            InstrumentoSelecaoDAL ISDal = new InstrumentoSelecaoDAL();
            ISDal.SaveInstrumentoSelecaoResponsavel(ISResponsavel);

            AutenticacaoBO authBO = new AutenticacaoBO();


            if (!authBO.VerificaPermissaoEspecifica(usuarioUID, 79))
            {
                Usuario usuario = authBO.GetUsuario(usuarioUID);
                authBO.DefinePermissao(usuario.Pessoa.PessoaUID, 79);
            }
        }

        public List<vwInstrumentoSelecaoResponsavel> GetResponsaveisPorInstrumentoSelecao(int instrumentoSelecaoUID)
        {
            InstrumentoSelecaoDAL ISDAL = new InstrumentoSelecaoDAL();
            return ISDAL.GetResponsaveisPorInstrumentoSelecao(instrumentoSelecaoUID);
        }

        public InstrumentoSelecaoResponsavel GetResponsavelPorInstrumentoSelecao(long usuarioUID)
        {
            InstrumentoSelecaoDAL ISDAL = new InstrumentoSelecaoDAL();
            return ISDAL.GetResponsavelPorInstrumentoSelecao(usuarioUID);
        }

        public void ExcluirInstrumentoSelecaoResponsavel(int id)
        {
            InstrumentoSelecaoDAL ISDAL = new InstrumentoSelecaoDAL();
            ISDAL.ExcluirReponsavel(id);
        }

        public void SalvarArquivoProjeto(InstrumentoSelecao instrumentoSelecao, String nomeArquivo, String descricao)
        {
            ArquivoInstrumentoSelecao arquivo = new ArquivoInstrumentoSelecao();
            arquivo.nomeArquivo = nomeArquivo;
            arquivo.InstrumentoSelecao = instrumentoSelecao;
            arquivo.descricao = descricao;
            InstrumentoSelecaoDAL IsDAL = new InstrumentoSelecaoDAL();
            IsDAL.SaveArquivoInstrumentoSelecao(arquivo);
        }

        public void ExcluirArquivoIS(int instrumentoSelecaoUID)
        {
            InstrumentoSelecaoDAL IsDAL = new InstrumentoSelecaoDAL ();

            ArquivoInstrumentoSelecao arquivoIs = IsDAL.GetArquivoInstrumentoSelecao(instrumentoSelecaoUID);

            if (arquivoIs != null)
            {
                IsDAL.DeleteArquivoIs(arquivoIs);
            }
        }

        public ArquivoInstrumentoSelecao GetArquivoIs(int instrumentoSelecaoUID)
        {
            InstrumentoSelecaoDAL IsDAL = new InstrumentoSelecaoDAL();
            return IsDAL.GetArquivoInstrumentoSelecao(instrumentoSelecaoUID);
        }

        public IList<InstrumentoSelecao> GetInstrumentoSelecaoFolhaPagamentoAtiva(int? programaUID)
        {
            try
            {
                FolhaPagamentoDAL folhaDAL = new FolhaPagamentoDAL();
                List<FolhaPagamento> listFolha = folhaDAL.GetFolhasPagamentoAtivas();

                IList<InstrumentoSelecao> listIS = new List<InstrumentoSelecao>();

                foreach (FolhaPagamento folha in listFolha)
                {
                    bool add = true;
                    foreach (InstrumentoSelecao isel in listIS)
                    {
                        if (folha.ProcessoSeletivo.instrumentoSelecaoUID == isel.instrumentoSelecaoUID)
                            add = false;                        
                    }

                    if (add)
                    {
                        if (folha.ProcessoSeletivo.InstrumentoSelecao.situacao.Value)
                        {
                            if (programaUID.HasValue)
                            {
                                if (folha.ProcessoSeletivo.InstrumentoSelecao.proreitoriaUID.Value ==
                                    programaUID.Value)
                                {
                                    listIS.Add(folha.ProcessoSeletivo.InstrumentoSelecao);
                                }
                            }
                            else
                            {
                                listIS.Add(folha.ProcessoSeletivo.InstrumentoSelecao);
                            }
                        }
                    }
                    
                }

                return listIS;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public InstrumentoSelecao GetInstrumento(int instrumentoSelecaoUID)
        {
            try
            {
                InstrumentoSelecaoDAL isDAL = new InstrumentoSelecaoDAL();
                return isDAL.GetInstrumento(instrumentoSelecaoUID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        internal List<InstrumentoSelecao> GetListInstrumentoSelecaoPorPrograma(int programaUID)
        {
            try
            {
                InstrumentoSelecaoDAL isDAL = new InstrumentoSelecaoDAL();
                return isDAL.GetListInstrumentoSelecaoPorPrograma(programaUID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
