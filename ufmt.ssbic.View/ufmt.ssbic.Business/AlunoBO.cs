﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.Business
{

    public class AlunoBO
    {
        public List<vwAluno> SearchAlunos(String tipo, String str)
        {
            List<vwAluno> registros = null;

            if (tipo.Equals("0"))
                registros = this.FindRegistros(str, 0);
            else
                registros = this.FindRegistros(null, decimal.Parse(str));

            return registros;
        }

        public List<vwAluno> FindRegistros(string nome, decimal matricula)
        {
            try
            {
                AlunoDAL objDAL = new AlunoDAL();
                return objDAL.FindRegistros(nome, matricula);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public vwAluno GetAlunoMatriculado(decimal matricula)
        {
            try
            {
                AlunoDAL objDAL = new AlunoDAL();
                return objDAL.GetAlunoMatriculado(matricula);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }

        public vwAlunoSiga GetAluno(decimal matricula)
        {
            try
            {
                AlunoDAL objDAL = new AlunoDAL();
                return objDAL.GetAluno(matricula);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }




        public vwAlunoSiga GetAlunoPorCPF(string cpf)
        {
            try
            {
                AlunoDAL objDAL = new AlunoDAL();
                return objDAL.GetAlunoPorCPF(cpf);
            }
            catch (Exception exception)
            {
                throw new Exception("Não foi possível recuperar os registros.", exception);
            }
        }
    }
}
