﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.View.session
{
    public class VerificaSession : System.Web.UI.Page
    {
        public VerificaSession() {

            


            base.Load += new EventHandler(VerificaSessionPage_Load);
        }

        private void VerificaSessionPage_Load(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Request.QueryString["trocar"]))
            {
                Session["permissao"] = null;
            }

            VerificaAcesso();
        }

        private void VerificaAcesso()
        {
            try
            {

                if (Session["programaUID"] != null)
                {
                    //Se estiver na área administrativa tratar montagem de menu
                    if (Session["administracao"] != null)
                    {
                        if (Session["permissao"] != null)
                        {
                            if (Session["permissao"].ToString().Contains("ADMINISTRADOR"))
                            {
                                Page.Master.FindControl("MenuInstrumentoSelecao").Visible = true;
                                //Page.Master.FindControl("MenuProcessoSeletivo").Visible = true;
                                Page.Master.FindControl("MenuBolsas").Visible = true;
                                Page.Master.FindControl("MenuRelatorios").Visible = true;


                                if (Session["permissao"].ToString().Contains("PROPEQ"))
                                {
                                    Page.Master.FindControl("MenuPropeq").Visible = true;
                                }

                                if (Session["permissao"].ToString().Contains("CARE"))
                                {
                                    Page.Master.FindControl("MenuFormCare").Visible = true;
                                }

                            }


                            else if (Session["permissao"].ToString().Contains("COORDENADOR"))
                            {

                            }
                            else
                            { //Financeiro
                                Page.Master.FindControl("MenuInstrumentoSelecao").Visible = false;
                                Page.Master.FindControl("MenuFluxo").Visible = false;
                            }



                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("Logout.aspx");
            }

        }

        public int GetProgramaUID()
        {
            BolsaBO bolsaBo = new BolsaBO();
            Proreitoria programa = null;
            if (!Session["permissao"].ToString().Contains("FINANCEIRO"))
            {
                int subsvalue = 21;

                if (Session["permissao"].ToString().Contains("COORDENADOR"))
                    subsvalue = 19;

                string permissao = Session["permissao"].ToString();
                int length = permissao.Length - subsvalue;

                string strprograma = permissao.Substring(subsvalue, length);

                programa = bolsaBo.GetPrograma(strprograma);                
            }            
            else
            {                
                programa = bolsaBo.GetPrograma("FINANCEIRO");
            }

            if (Session["permissao"].ToString().Contains("PROJETOS"))
            {
                programa = bolsaBo.GetPrograma("PROPEQ");                
            }

            return programa.proreitoriaUID;
            /*
            int programaUID = 0;

            
            if (Session["permissao"].ToString().Contains("ADMINISTRADOR"))
            {
                string permissao = Session["permissao"].ToString();
                int length = permissao.Length - 21;

                string programa = permissao.Substring(21, length);

                


                if (Session["permissao"].ToString().Contains("CARE"))
                {
                    programaUID = 8;
                }

                if (Session["permissao"].ToString().Contains("PROEG"))
                {

                }
            }

            //TODO: buscar id programa.
             * */
        }

    }
}