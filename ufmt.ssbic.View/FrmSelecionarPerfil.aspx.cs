﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.sig.entity;
using ufmt.ssbic.View.session;

namespace ufmt.ssbic.View
{
    public partial class FrmSelecionarPerfil : VerificaSession
    {
        protected AutenticacaoBO aBO;
        protected void Page_Load(object sender, EventArgs e)
        {          

                aBO = new AutenticacaoBO();
                rpPerfilAcesso.DataSource = aBO.GetPermissoes(long.Parse(Session["usuarioUID"].ToString()));
                rpPerfilAcesso.DataBind();

                BolsaBO objBO = new BolsaBO();
                lblProReitoria.Text = "Perfil de Acesso";
                //lblProReitoria.Text = objBO.GetPrograma(int.Parse(Session["programaUID"].ToString()));
                Image ImageTopo = (Image)Page.Master.FindControl("ImageTopo");
                ImageTopo.ImageUrl = "~/images/bolsa_topo.png";
            
        }

        protected void rpPerfilAcesso_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName == "SetPermissao") {
                AutenticacaoBO autenticacaoBO = new AutenticacaoBO();
                Permissao permissao = autenticacaoBO.GetPermissao(long.Parse(e.CommandArgument.ToString()));
                Session["permissao"] = permissao.Nome;
                Session["administracao"] = "true";

                Session["programaUID"] = GetProgramaUID();

                if (permissao.Nome.Contains("ADMINISTRADOR")) {
                    //Response.Redirect("~/webapp/instrumentoSelecao/FrmListar.aspx");
                    Response.Redirect("~/webapp/editais/FrmListar.aspx");
                }
                else if (permissao.Nome.Contains("COORDENADOR"))
                {
                    Response.Redirect("~/webapp/editais/FrmListar.aspx");
                    //Response.Redirect("~/webapp/processoSeletivo/FrmProcessoSeletivo.aspx");
                }
                else
                { //FINANCEIRO
                    //Response.Redirect("~/webapp/financeiro/FrmFinanceiro.aspx");
                    Response.Redirect("~/webapp/financeiro/FrmInstrumentosSelecao.aspx");
                }

            }

        }

       


        protected void rpPerfilAcesso_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                /*
                MenuAdminstracao mADM = new MenuAdminstracao();
                Label lblNomePermissao = (Label)e.Item.FindControl("lblNomePermissao");
                lblNomePermissao.Text = mADM.FormataNomePermissão(lblNomePermissao.Text);
                 */ 
            }
        }


        public String FormatarDatabind(String dado)
        {
            string dadoFormatado = "";
            int length = 0;

            if (dado.Contains("ADMINISTRADOR"))
            {
                if (!dado.Contains("PROJETOS"))
                {
                    length = dado.Length - "ADMINISTRADOR_BOLSAS_".Length;

                    dadoFormatado = "Administrador " + dado.Substring("ADMINISTRADOR_BOLSAS_".Length, length);
                }
                else
                {
                    dadoFormatado = "Administrador de Projetos";
                }
            }

            if(dado.Contains("COORDENADOR"))
            {
                length = dado.Length - "COORDENADOR_BOLSAS_".Length;

                dadoFormatado = "Coordenador " + dado.Substring("COORDENADOR_BOLSAS_".Length, length);
            }

            if(dado.Contains("FINANCEIRO"))
            {

                dadoFormatado = "Financeiro";
            }

            return dadoFormatado;
        }

    }
}