﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.sig.entity;
using ufmt.sig.business;

namespace ufmt.ssbic.View
{
    public partial class controlReiniciarSenha : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtCpf.Focus();
            }
        }


        protected void btnReiniciarSenha_Click(object sender, EventArgs e)
        {
            lblErro.Visible = false;
            String cpfUsuario = txtCpf.Text;

            if (String.IsNullOrEmpty(cpfUsuario))
            {
                erro("Insira o seu CPF");
            }
            else
            {
                Usuario usuario = null;

                try
                {
                    usuario = getUsuario(cpfUsuario);
                }
                catch
                {
                }

                if (usuario == null)
                {
                    erro("Não existe um usuário para este CPF");
                    return;
                }

                if (reiniciaSenha(usuario))
                {
                    PanelConcluido.Visible = true;
                    PanelReiniciarSenha.Visible = false;

                    lblEmailUsuario.Text = usuario.Pessoa.Mail;
                }
                else
                {
                    erro("Problemas em reiniciar a senha. Entre em contato com a STI");
                }
            }
        }

        private void erro(String msg)
        {
            lblErro.Visible = true;
            lblErro.Text = msg + "<br/>";
        }

        public Usuario getUsuario(String cpf)
        {
            using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
            {
                Pessoa pessoa = pessoaBO.GetPessoa(cpf);

                if (pessoa == null)
                {
                    return null;
                }
                else
                {
                    using (IAutenticacaoBO autenticacaoBO = BusinessFactory.GetInstance<IAutenticacaoBO>())
                    {
                        return autenticacaoBO.GetUsuarioPorPessoa(pessoa.PessoaUID);
                    }
                }
            }
        }

        public bool reiniciaSenha(Usuario usuario)
        {
            using (IAutenticacaoBO autenticacaoBO = BusinessFactory.GetInstance<IAutenticacaoBO>())
            {
                try
                {
                    autenticacaoBO.ReiniciarSenha(usuario);
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
        }
    }
}