﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.sig.entity;
using ufmt.ssbic.Business;

namespace ufmt.ssbic.View
{
    public partial class ArvoreUnidades : System.Web.UI.UserControl
    {
        private List<Unidade> unidadesPermitidas;
        private Unidade unidadeBusca;
        private List<Campus> campi = new List<Campus>();

        public TreeNodeEventHandler OnNodeChanged;

        protected void Page_Load(object sender, EventArgs e)
        {
            unidadesPermitidas = new List<Unidade>();
            if (!IsPostBack)
            {
                divArvore.Style.Add("display", "none");
                lblUnidadeSelecionada.Text = "Nenhuma unidade selecionada";

                if (campi.Count > 0)
                {
                    this.carregaCampus(campi);
                }
                else
                {
                    this.carregaCampus();
                }
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            if (dropCampus.SelectedIndex <= 0)
            {
                lblaviso.Visible = true;
            }
            else
            {
                lblaviso.Visible = false;
                divArvore.Style.Remove("display");
                divArvore.Style.Add("display", "block");
                carregarArvoreUnidade(txtBusca.Text, int.Parse(dropCampus.SelectedValue), "sem limite");
            }
        }

        protected void treeUnidades_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                hfUnidadeUID.Value = treeUnidades.SelectedValue;
                lblUnidadeSelecionada.Text = treeUnidades.SelectedNode.Text;
                divArvore.Style.Add("display", "none");
                OnNodeChanged.Invoke(sender, new TreeNodeEventArgs(treeUnidades.SelectedNode));
            }
            catch (Exception ex)
            {
            }
        }

        public Int64? GetUnidadeUID
        {
            get
            {
                if (String.IsNullOrEmpty(hfUnidadeUID.Value))
                {
                    return null;
                }
                else
                {
                    Int64 retorno = 0;
                    Int64.TryParse(hfUnidadeUID.Value, out retorno);
                    return retorno;
                }
            }
        }

        /// <summary>
        /// Função que popula o dropCampus  com os campus
        /// </summary>
        private void carregaCampus()
        {
            UnidadeBO unidadeBO = new UnidadeBO();
            dropCampus.DataSource = unidadeBO.GetCampi();
            dropCampus.DataValueField = "campusUID";
            dropCampus.DataTextField = "nome";
            dropCampus.DataBind();
            dropCampus.Items.Insert(0, "Escolha um campus");
        }

        private void carregaCampus(List<Campus> campi)
        {
            dropCampus.DataSource = campi;
            dropCampus.DataValueField = "campusUID";
            dropCampus.DataTextField = "nome";
            dropCampus.DataBind();
            dropCampus.Items.Insert(0, "Escolha um campus");
        }

        /// <summary>
        /// Função que retorna uma lista hierarquica dos pais da unidade informada
        /// </summary>
        /// <param name="unidade">unidade para montar a lista</param>
        /// <returns></returns>
        private List<Unidade> getNoSuperior(Unidade unidade)
        {
            List<Unidade> formaLista = new List<Unidade>();
            while (unidade != null)
            {
                formaLista.Add(unidade);
                unidade = unidade.UnidadeSuperior;
            }

            return formaLista;
        }

        /// <summary>
        /// Função que retorna uma lista hierarquica dos pais da unidade informada
        /// </summary>
        /// <param name="unidade">unidade para montar a lista</param>
        /// <returns></returns>
        private List<Unidade> getNoSuperiorcomLimite(Unidade unidade, List<Unidade> limites)
        {
            List<Unidade> formaLista = new List<Unidade>();
            while (unidade != null)
            {
                unidadeBusca = unidade;
                formaLista.Add(unidade);
                if (limites.Exists(comparaUnidades))
                    return formaLista;

                unidade = unidade.UnidadeSuperior;
            }

            return null;
        }
        /// <summary>
        /// Função que compara a unidade passada por parametro com a unidade predefinida 'unidadeBusca'
        /// </summary>
        /// <param name="unidade">Unidade que será comparada com a 'unidadeBusca'</param>
        /// <returns></returns>
        private bool comparaUnidades(Unidade unidade)
        {
            if (unidade != null)
            {
                if (unidade.UnidadeUID.Equals(unidadeBusca.UnidadeUID))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Retorna o nó pai da unidade informada
        /// </summary>
        /// <param name="unidade">Unidade que irá ter seu pai retornado</param>
        /// <returns></returns>
        private Unidade retornaPai(Unidade unidade)
        {
            if (unidade.UnidadeSuperior != null)
            {
                return unidade.UnidadeSuperior;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Função que retorna o caminho relativo de uma unidade da arvore de unidades
        /// </summary>
        /// <param name="unidade">Unidade para formar o caminho</param>
        /// <returns></returns>
        private string getFormaCaminho(Unidade unidade)
        {
            string formaLista = "";
            while (unidade != null)
            {
                formaLista = unidade.UnidadeUID.ToString() + "/" + formaLista;
                if (unidade.UnidadeSuperior != null)
                {
                    unidade = unidade.UnidadeSuperior;
                }
                else
                {
                    unidade = null;
                }
            }
            int tamanho = formaLista.Length - 1;
            String lista = formaLista.Substring(0, tamanho);
            return lista;
        }

        protected void btnRecolherArvore_Click(object sender, EventArgs e)
        {
            treeUnidades.CollapseAll();
        }

        protected void btnExpandirArvore_Click(object sender, EventArgs e)
        {
            treeUnidades.ExpandAll();
        }

        public List<Campus> Campi
        {
            get { return campi; }
            set { campi = value; }
        }

        public void LimparArvore()
        {
            treeUnidades.Nodes.Clear();
        }

        public void setUnidadesPermitidas(List<Unidade> unidades)
        {
            unidadesPermitidas = unidades;
        }

        public bool OpcaoBusca
        {
            get { return divBusca.Visible; }
            set { divBusca.Visible = value; }
        }

        /// <summary>
        /// Função que carrega os nodos da unidade baseado no nome da unidade, campus e tipo passados
        /// </summary>
        /// <param name="unidade">Nome da unidade para servir como filtro</param>
        /// <param name="campus">Id do campus para servir como filtro</param>
        /// <param name="tipo">Tipo do modo que será carregado, pode ser "com limite" ou "sem limite".
        ///                     Caso seja escolhido o tipo com limite especificar antes as unidades permitidas (setUnidadesPermitidas).</param>
        public void carregarArvoreUnidade(String unidade, int campus, String tipo)
        {
            treeUnidades.Nodes.Clear();
            treeUnidades.AutoGenerateDataBindings = true;
            UnidadeBO unidadeBO = new UnidadeBO();
            IList<Unidade> listaUnidades = unidadeBO.getUnidadesbyCampus(unidade, campus);
            foreach (Unidade dadoUnidade in listaUnidades)
            {
                List<Unidade> hierarquiaUnidade = null;
                if (tipo.ToLower().Equals("com limite"))
                {
                    hierarquiaUnidade = getNoSuperiorcomLimite(dadoUnidade, unidadesPermitidas);
                }
                else
                {
                    hierarquiaUnidade = getNoSuperior(dadoUnidade);
                }

                if (hierarquiaUnidade != null)
                {
                    int i = hierarquiaUnidade.Count - 1;

                    while (i > -1)
                    {
                        Unidade valor = hierarquiaUnidade[i];
                        TreeNode tnnovo = new TreeNode();
                        if (valor.UnidadeSuperior == null)
                        {
                            TreeNode nomaster = treeUnidades.FindNode(getFormaCaminho(valor));
                            if (nomaster == null)
                            {
                                if (valor.TipoUnidade.Match(TipoUnidadeEnum.INFORMAL))
                                    tnnovo.Text = "<span style='color:red;'>[" + valor.Sigla + "]" + valor.Nome + " (adjunta)</span>";
                                else
                                    tnnovo.Text = "[" + valor.Sigla + "]" + valor.Nome;
                                tnnovo.Value = valor.UnidadeUID.ToString();
                                treeUnidades.Nodes.Add(tnnovo);
                            }
                        }
                        else
                        {
                            TreeNode no = new TreeNode(valor.UnidadeSuperior.Nome, valor.UnidadeSuperior.UnidadeUID.ToString());
                            TreeNode verificaNodo = treeUnidades.FindNode(getFormaCaminho(valor));

                            if (verificaNodo == null)
                            {

                                TreeNode verificaPai = treeUnidades.FindNode(getFormaCaminho(valor.UnidadeSuperior));


                                if (verificaPai != null)
                                {
                                    no = verificaPai;
                                }

                                TreeNode tnauxiliar = new TreeNode();
                                if (valor.TipoUnidade.Match(TipoUnidadeEnum.INFORMAL))
                                    tnauxiliar.Text = "<span style='color:red;'>" + valor.Nome + " (adjunta)</span>";
                                else
                                    tnauxiliar.Text = valor.Nome;

                                tnauxiliar.Value = valor.UnidadeUID.ToString();
                                if (!no.ChildNodes.Contains(tnauxiliar))
                                    no.ChildNodes.Add(tnauxiliar);
                            }
                        }
                        i--;
                    }
                }
            }
            treeUnidades.ExpandAll();
        }

        public String CampoBusca
        {
            get { return txtBusca.Text; }
            set { txtBusca.Text = value; }
        }

        public String CampoCampus
        {
            get { return dropCampus.SelectedItem.Text; }
            set
            {
                try
                {
                    UnidadeBO unidadeBO = new UnidadeBO();
                    dropCampus.SelectedValue = unidadeBO.GetCampus(value).CampusUID.ToString();
                }
                catch (Exception ex)
                {
                    dropCampus.SelectedIndex = -1;
                }
            }
        }

        public bool HabilitaArvoreUnidades
        {
            get { return treeUnidades.Enabled; }
            set { treeUnidades.Enabled = value; }
        }

        public void Buscar()
        {
            this.btnBuscar_Click(this, new EventArgs());
        }

        public void DadosUnidades(long unidadeUID, string unidadeNome)
        {
            lblUnidadeSelecionada.Text = unidadeNome;
            hfUnidadeUID.Value = unidadeUID.ToString();
            divArvore.Visible = false;

        }

    }
}