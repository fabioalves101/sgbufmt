﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="FrmRelatorios.aspx.cs" Inherits="ufmt.ssbic.View.FrmRelatorios" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<h1>Relatórios</h1>
<p>
    <asp:HyperLink ID="hpProjetosPeriodo" runat="server" 
        NavigateUrl="~/relatorios/ProjetosPeriodo.aspx">Projetos por Período</asp:HyperLink>
</p>

<p>
    <asp:HyperLink ID="HyperLink1" runat="server" 
        NavigateUrl="~/relatorios/ProjetosFinanciados.aspx">Projetos Financiados</asp:HyperLink>
</p>

<p>
    <asp:HyperLink ID="HyperLink2" runat="server" 
        NavigateUrl="~/relatorios/ProjetosProfessor.aspx">Projetos Por Professor (Coordenador / Membro)</asp:HyperLink>
</p>

<p>
    <asp:HyperLink ID="HyperLink3" runat="server" 
        NavigateUrl="~/relatorios/ProjetosUnidadeEspecifica.aspx">Projetos Por Unidade Específica</asp:HyperLink>
</p>
</asp:Content>
