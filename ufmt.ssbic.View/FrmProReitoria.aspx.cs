﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ufmt.ssbic.View
{
    public partial class FrmProReitoria : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Redirect("~/Login.aspx");

            if (!IsPostBack) {

                Session.Abandon();
                Image ImageTopo = (Image)Page.Master.FindControl("ImageTopo");
                ImageTopo.ImageUrl = "~/images/bolsa_topo.png";
            }
        }

        protected void btnContinuar_Click(object sender, EventArgs e)
        {
            Session["proreitoriaUID"] = DropDownList1.SelectedValue;
            Session["programaUID"] = DropDownList1.SelectedValue;

            if (Session["programaUID"].ToString().Equals("3"))
            {
                Response.Redirect("~/webapp/propeq/FrmLogin.aspx");
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }
        }


    }
}