﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="controlReiniciarSenha.ascx.cs" Inherits="ufmt.ssbic.View.controlReiniciarSenha" %>
<asp:Panel ID="PanelReiniciarSenha" runat="server" DefaultButton="btnReiniciarSenha">
    <div style="" align="center">
        <asp:Label ID="Label2" runat="server" Text="Informe o seu CPF no campo abaixo para reiniciar a sua senha."></asp:Label>
        <br />
        <asp:Label ID="Label3" runat="server" Text="Uma senha aleatória será gerada e enviada para o seu e-mail cadastrado."></asp:Label>
        <br />
        <asp:Label ID="Label4" runat="server" Text="Se o seu e-mail estiver errado, entre em contato com a STI."></asp:Label>
    </div>

    <br />
    <br />

    <div align="center">
        <asp:Label ID="lblErro" runat="server" ForeColor="#FF3300"></asp:Label>
    </div>
    
    <div align="center">
        <table>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" style="font-weight: 700" Text="CPF:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtCpf" runat="server" Width="130px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <br />
                    <asp:Button ID="btnReiniciarSenha" runat="server" Text="Reiniciar Senha" 
                        onclick="btnReiniciarSenha_Click" />
                </td>
            </tr>
        </table>
    </div>
</asp:Panel>


<asp:Panel ID="PanelConcluido" runat="server" Visible="false">
    <div align="center">
        Senha reiniciada com sucesso!
        <br />
        Ela foi enviada para o e-mail 
        <asp:Label ID="lblEmailUsuario" runat="server" Font-Bold="True"></asp:Label>
    </div>
</asp:Panel>