﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCBuscarAluno.ascx.cs" Inherits="ufmt.ssbic.View.controles.UCBuscarAluno" %>
<style>
.mGrid { 
    width: 100%; 
    background-color: #fff; 
    margin: 5px 0 10px 0; 
    border: solid 1px #525252; 
    border-collapse:collapse; 
}
.mGrid td { 
    padding: 2px; 
    border: solid 1px #c1c1c1; 
    color: #717171; 
}
.mGrid th { 
    padding: 4px 2px; 
    color: #fff; 
    background: #424242 url(../../media/images/grd_head.png) repeat-x top; 
    border-left: solid 1px #525252; 
    font-size: 0.9em; 
}
.mGrid .alt { background: #fcfcfc url(../../media/images/grd_alt.png) repeat-x top; }
.mGrid .pgr { background: #424242 url(../../media/images/grd_pgr.png) repeat-x top; }
.mGrid .pgr table { margin: 5px 0; }
.mGrid .pgr td { 
    border-width: 0; 
    padding: 0 6px; 
    border-left: solid 1px #666; 
    font-weight: bold; 
    color: #fff; 
    line-height: 12px; 
 }   
.mGrid .pgr a { color: #666; text-decoration: none; }
.mGrid .pgr a:hover { color: #000; text-decoration: none; }
.minibutton {
    background: url("../../media/images/minibutton_matrix.png") no-repeat scroll 0 0 transparent;
    border: medium none;
    color: #333333 !important;
    cursor: pointer;
    display: inline-block;
    font-size: 11px;
    font-weight: bold;
    height: 23px;
    margin-bottom: 5px;
    overflow: visible;
    padding: 0 0 0 3px;
    text-decoration: none !important;
    text-shadow: 1px 1px 0 #FFFFFF;
    white-space: nowrap;
}
.containerFormulario { margin-bottom:10px; margin-top:10px; padding-left:10px;} 
</style>
<div class="containerFormulario">

<asp:Panel ID="PanelEtapa1" runat="server" Visible="true">
                <strong>Localizar Aluno por
                <asp:RadioButtonList ID="rblTipoConsultaAluno" runat="server" 
                    AutoPostBack="True" 
                    onselectedindexchanged="rblTipoConsultaAluno_SelectedIndexChanged" 
                    RepeatDirection="Horizontal" RepeatLayout="Flow">
                    <asp:ListItem Selected="True" Value="0">Nome</asp:ListItem>
                    <asp:ListItem Value="1">Matrícula</asp:ListItem>
                </asp:RadioButtonList>
                &nbsp;<asp:RegularExpressionValidator ID="REVAluno" runat="server" 
                    ControlToValidate="txtAluno" Display="Dynamic" Enabled="false" 
                    ErrorMessage="* Digite somente números" ForeColor="Red" 
                    ValidationExpression="^[0-9]+$">*Digite somente números</asp:RegularExpressionValidator>
               
                </strong>
                <br />

                <asp:TextBox ID="txtAluno" runat="server" Width="497px"></asp:TextBox>
                <asp:Button ID="btBuscar" runat="server" Text="Buscar" 
                    onclick="Button1_Click" />
                &nbsp;<asp:HiddenField ID="hddProcessoSeletivoUID" runat="server" />
                <asp:Panel ID="PanelResultadoConsulta" runat="server" Visible="false">
                    <fieldset>
                        <legend>Alunos Localizados (<asp:Label ID="lblTotalAlunos" runat="server" 
                                style="font-weight: 700"></asp:Label>
                            )</legend>
                        <asp:GridView ID="gdrAlunos" runat="server" AllowPaging="True" 
                            AlternatingRowStyle-CssClass="alt" AutoGenerateColumns="False" CssClass="mGrid" 
                            EmptyDataText="Vazio" onpageindexchanging="gdrAlunos_PageIndexChanging" 
                            onrowcommand="gdrAlunos_RowCommand" PagerStyle-CssClass="pgr" PageSize="5">
                            <AlternatingRowStyle CssClass="alt" />
                            <Columns>
                                <asp:TemplateField HeaderText="Selecionar" ItemStyle-HorizontalAlign="Center" 
                                    ItemStyle-Width="60px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" 
                                            CommandArgument='<%# Eval("Matricula") + "," + Eval("Nome")+ "," + Eval("Cpf") + "," + Eval("Email")+ "," + Eval("CodCurso")+ "," + Eval("Curso") + "," + Eval("CodCampus") + "," + Eval("Campus")%>' 
                                            CommandName="adicionar" CssClass="minibutton" onclick="LinkButton1_Click">
                            <span>
                                <img src="../../media/images/select.png" alt="" />
                                Selecionar
                            </span>
                            </asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="60px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Matricula" SortExpression="Matricula">
                                    <ItemTemplate>
                                        <asp:Label ID="Label36" runat="server" Text='<%# Eval("Matricula") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Nome" SortExpression="Nome">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNome" runat="server" Text='<%# Eval("Nome") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="CPF" SortExpression="CPF">
                                    <ItemTemplate>
                                        <asp:Label ID="Label3" runat="server" Text='<%# Eval("CPF") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Tipo" SortExpression="Tipo">
                                    <ItemTemplate>
                                        <asp:Label ID="Label4" runat="server" Text='<%# Eval("Tipo") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                <div style="padding:20px;text-align:center">
                                    Nenhum aluno localizado no critério da consulta.&nbsp; Verifique os dígitos e tente 
                                    novamente.
                                </div>
                            </EmptyDataTemplate>
                            <PagerStyle CssClass="pgr" />
                        </asp:GridView>
                    </fieldset>
                </asp:Panel>
            </div>
            
      </asp:Panel>
      <asp:Panel ID="pnlDadosAluno" Visible="false" runat="server">
        <p>
            Nome: 
            <asp:Label ID="lblNome" runat="server"></asp:Label>
        </p>
        <p>
            CPF:<asp:Label ID="lblCpf" runat="server"></asp:Label>
        </p>
        <p>
            <asp:HiddenField ID="hdmatricula" runat="server" />
            <asp:LinkButton ID="lnkNovaBusca" runat="server" onclick="lnkNovaBusca_Click">Realizar nova busca</asp:LinkButton>
        </p>
      </asp:Panel>

      </div>
      