﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCVagasCurso.ascx.cs" Inherits="ufmt.ssbic.View.controles.UCVagasCurso" %>
<%@ Register src="UCBuscarAluno.ascx" tagname="UCBuscarAluno" tagprefix="uc1" %>
<%@ Register src="UCBuscarCursos.ascx" tagname="UCBuscarCursos" tagprefix="uc2" %>

<uc2:UCBuscarCursos ID="UCBuscarCursos1" runat="server" />
<div id="containerMensagens">
                <asp:Panel ID="PanelMsgResultado" runat="server" Visible="false">
                    <asp:Label ID="lblMsgResultado" runat="server" Font-Bold="True"></asp:Label>
                </asp:Panel>
        </div>
<p>
Quantidade de Vagas: <asp:TextBox ID="txtVagas" runat="server" 
        Width="30px"></asp:TextBox>
<asp:Button CausesValidation="false" ID="btAdicionar" runat="server" Text="Adicionar" 
        onclick="btAdicionar_Click" />
</p>
<asp:GridView ID="grdVagas" runat="server" AutoGenerateColumns="False" 
    onrowcommand="grdVagas_RowCommand" onrowdeleted="grdVagas_RowDeleted">
    <Columns>
        <asp:TemplateField HeaderText="Nome do Curso">
            <ItemTemplate>
                <asp:Label ID="LabelNome" runat="server" Text='<%# Eval("curso.Nome") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Campus">
            <ItemTemplate>
                <asp:Label ID="LabelCampus" runat="server" Text='<%# Eval("curso.Campus.Nome") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Vagas">
            <ItemTemplate>
                <asp:Label ID="LabelqtdeVagas" runat="server" Text='<%# Eval("qtdeVagas") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Remover">
            <ItemTemplate>
                <asp:LinkButton CausesValidation="false" CommandArgument='<%# Eval("curso.CursoUID") %>' CommandName="remover" ID="lnkRemover" runat="server">Remover</asp:LinkButton>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>

</asp:GridView>
