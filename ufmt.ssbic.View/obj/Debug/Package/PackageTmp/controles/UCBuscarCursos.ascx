﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCBuscarCursos.ascx.cs" Inherits="ufmt.ssbic.View.controles.UCBuscarCursos" %>
<asp:Panel ID="pnlBusca" runat="server">
<p>
Campus: 
<asp:DropDownList ID="ddlCampus" runat="server" 
    DataSourceID="ObjectDataSource1" DataTextField="Nome" 
    DataValueField="CampusUID" AutoPostBack="True" 
    onselectedindexchanged="ddlCampus_SelectedIndexChanged">
</asp:DropDownList>
</p>
<asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
    SelectMethod="GetCampi" TypeName="ufmt.ssbic.Business.CursoBO">
</asp:ObjectDataSource>
<p>
    
Buscar:
    <asp:TextBox ID="txtBuscaCurso" runat="server" Width="500px"></asp:TextBox>
    <asp:DropDownList ID="opcaoDropdown" runat="server">
        <asp:ListItem>Nome</asp:ListItem>
        <asp:ListItem Value="Código"></asp:ListItem>
    </asp:DropDownList>
    <asp:Button ID="btBuscar" runat="server" onclick="btBuscar_Click" 
        Text="Buscar" CausesValidation="false" />
    <asp:Button ID="btMostrarTodos" runat="server" onclick="btMostrarTodos_Click" 
        Text="Mostrar Todos" CausesValidation="false" />
</p>
<p>
Selecione o curso:
<asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
    AutoGenerateColumns="False" DataSourceID="ObjectDataSource2" Width="800px" 
        DataKeyNames="CursoUID" onrowcommand="GridView1_RowCommand">
    <Columns>
        <asp:TemplateField HeaderText="Selecionar">
            <ItemTemplate>
                <asp:LinkButton CausesValidation="false" CommandArgument='<%# Eval("cursoUID") %>' CommandName="selecionar" ID="lnkSelecionar" 
                    runat="server" onclick="lnkSelecionar_Click">Selecionar</asp:LinkButton>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="Nome" HeaderText="Nome do Curso" 
            SortExpression="Nome" />
    </Columns>
</asp:GridView>
</p>
<asp:ObjectDataSource ID="ObjectDataSource2" runat="server" 
    SelectMethod="GetCursos" TypeName="ufmt.ssbic.Business.CursoBO">
    <SelectParameters>
        <asp:ControlParameter ControlID="ddlCampus" Name="campusUID" 
            PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="txtBuscaCurso" Name="nomeCurso" 
            PropertyName="Text" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>

    <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" 
        SelectMethod="GetCursoPorCodCurso" TypeName="ufmt.ssbic.Business.CursoBO">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlCampus" Name="campusUID" 
                PropertyName="SelectedValue" Type="Int32" />
            <asp:ControlParameter ControlID="txtBuscaCurso" Name="codCurso" 
                PropertyName="Text" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>

</asp:Panel>
<asp:Panel ID="pnlDados" runat="server" Visible="false">
<p>
<strong>
Campus: 
</strong>
    <asp:Label ID="lblCampus" runat="server"></asp:Label>
</p>
<p>
<strong>
Curso:
</strong>
    <asp:Label ID="lblCurso" runat="server"></asp:Label>
</p>
<p>
    <asp:LinkButton CausesValidation="false" ID="lnkNovaBusca" runat="server" onclick="lnkNovaBusca_Click">Realizar nova busca</asp:LinkButton>
</p>
</asp:Panel>

<asp:HiddenField ID="hdCursoUID" runat="server" />


