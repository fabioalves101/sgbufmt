﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCDadosMonitorVoluntario.ascx.cs" Inherits="ufmt.ssbic.View.controles.UCDadosMonitorVoluntario" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register src="UCBuscarServidor.ascx" tagname="UCBuscarServidor" tagprefix="uc1" %>

<%@ Register src="UCDadosBancariosAluno.ascx" tagname="UCDadosBancariosAluno" tagprefix="uc2" %>

<%@ Register src="UCBuscarDisciplinas.ascx" tagname="UCBuscarDisciplinas" tagprefix="uc3" %>

<div id="containerMensagens">
    <asp:Panel ID="PanelMsgResultado" runat="server" Visible="false">
        <asp:Label ID="lblMsgResultado" runat="server"></asp:Label>
    </asp:Panel>
        <div>
            <fieldset>
                <legend>Professor Orientador:</legend>
                <asp:Panel ID="pnlDadosOrientador" runat="server">
                    <p>
                    <asp:Label ID="lblOrientador" runat="server"></asp:Label>
                    </p>
                    <p>
                    <asp:LinkButton ID="lnkOrientador" CausesValidation="false" runat="server" onclick="lnkOrientador_Click">Alterar</asp:LinkButton>
                    </p>
                </asp:Panel>
                <asp:Panel ID="pnlOrientador" runat="server" Visible="false">
                    <uc1:UCBuscarServidor ID="UCBuscarOrientador" runat="server" />
                    
                    
                </asp:Panel>
            </fieldset>
        </div>
        <div>
            <fieldset>
                <legend>Coordenador:</legend>
                <asp:Panel ID="pnlDadosCoordenador" runat="server">
                    <p>
                    <asp:Label ID="lblCoordenador" runat="server"></asp:Label>
                    </p>
                    <p>
                    <asp:LinkButton ID="lnkCoordenador" CausesValidation="false" runat="server" onclick="lnkCoordenador_Click">Alterar</asp:LinkButton>
                    </p>
                </asp:Panel>
                <asp:Panel ID="pnlCoordenador" runat="server" Visible="false">
                    <uc1:UCBuscarServidor ID="UCBuscarCoordenador" runat="server" />
                    
                </asp:Panel>

            </fieldset>
        </div>
        <div>
        <fieldset>
                <legend>Modalidade:</legend>
                <asp:RadioButtonList ID="rdoModalidade" runat="server" 
                    RepeatDirection="Horizontal" Width="500px">
                    <asp:ListItem Value="1">Voluntária</asp:ListItem>
                    <asp:ListItem Value="2">Remunerada</asp:ListItem>
                </asp:RadioButtonList>
            </fieldset>
        </div>
        <div>
            <fieldset>
                <legend>Período:</legend>
                <asp:RadioButtonList ID="rdoPeriodo" runat="server" 
                    RepeatDirection="Horizontal" Width="300px">
                    <asp:ListItem Value="1">1° Semestre</asp:ListItem>
                    <asp:ListItem Value="2">2° Semestre</asp:ListItem>
                    <asp:ListItem Value="3">Anual</asp:ListItem>
                </asp:RadioButtonList>
            </fieldset>
        </div>
        <div>
            <fieldset>
                <legend>Atuação:</legend>
                <p>
                Início: 
                    <asp:TextBox ID="txtInicio" runat="server"></asp:TextBox>
                </p>
                <p>
                Final: 
                    <asp:TextBox ID="txtFinal" runat="server"></asp:TextBox>
                </p>
                
            </fieldset>
        </div>

        <div>

            <uc2:UCDadosBancariosAluno ID="UCDadosBancariosAluno1" runat="server" />

        </div>
        <div>

            <asp:Panel ID="pnlDisciplinas" runat="server" Visible="true">
                <uc3:UCBuscarDisciplinas ID="UCBuscarDisciplinas1" runat="server" />
            

            
            </asp:Panel>
            
        </div>
        <div>
            <fieldset>
                <legend>Situação:</legend>
                <p>
                <asp:RadioButtonList ID="rdoStatus" runat="server" 
                    RepeatDirection="Horizontal" Width="300px">
                    <asp:ListItem Value="1">Ativo</asp:ListItem>
                    <asp:ListItem Value="0">Inativo</asp:ListItem>
                </asp:RadioButtonList>
                </p>
                <p>
                Justificativa de Situação:<br />
                    <asp:TextBox ID="TextJustificativaSituacao" runat="server" TextMode="MultiLine" 
                    Height="69px" Width="424px"></asp:TextBox>
                </p>
            </fieldset>
        </div>
        <div style="text-align:center">
            <asp:Button ID="btAtualizar" runat="server" Text="Atualizar" Height="30px" 
                onclick="btAtualizar_Click" Width="193px" />
        
        </div>

</div>
<asp:HiddenField ID="hdUrlRedirect" runat="server" />

    
     <asp:HiddenField ID="hdBolsistaUID" runat="server" />
</div>
 </asp:Panel>

