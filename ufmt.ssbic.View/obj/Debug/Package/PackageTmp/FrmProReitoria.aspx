﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="FrmProReitoria.aspx.cs" Inherits="ufmt.ssbic.View.FrmProReitoria" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div>
        
        <h1><asp:Label ID="Label1" runat="server" Text="Escolha o Programa"></asp:Label></h1>
        <br />
        <div style="padding:20px;text-align:left">
        <asp:DropDownList ID="DropDownList1" runat="server" CssClass="login">
                    <asp:ListItem Value="">:::::::SELECIONE:::::::</asp:ListItem>
                    <asp:ListItem Value="7">PROEG - Monitoria</asp:ListItem>
                    <asp:ListItem Value="8">PROCEV - CARE</asp:ListItem>
                    <asp:ListItem Value="1">PROEG</asp:ListItem>
                    <asp:ListItem Value="2">PROCEV</asp:ListItem>
                    <asp:ListItem Value="3">PROPEQ</asp:ListItem>
                    <asp:ListItem Value="4">PROPG</asp:ListItem>
                    <asp:ListItem Value="5">Financeiro</asp:ListItem>
                    <asp:ListItem Value="6">Coordenação</asp:ListItem>
        </asp:DropDownList>
        &nbsp;<asp:Button ID="btnContinuar" runat="server" Text="Continuar" 
                onclick="btnContinuar_Click" CssClass="buttons" />
&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                ControlToValidate="DropDownList1" 
                ErrorMessage="Selecione a Pró-Reitoria para continuar" ForeColor="#FF3300">* Selecione a Pró-Reitoria para continuar</asp:RequiredFieldValidator>
        </div>
    </div>

</asp:Content>
