﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true" CodeBehind="FrmCadastrar.aspx.cs" Inherits="ufmt.ssbic.View.webapp.diretorio.FrmCadastrar" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>&nbsp;Novo Registro &raquo;
        <asp:Label ID="lblTitulo" runat="server"></asp:Label>
    </h1>

        <div id="containerMensagens">
                <asp:Panel ID="PanelMsgResultado" runat="server" Visible="false">
                    <asp:Label ID="lblMsgResultado" runat="server"></asp:Label>
                </asp:Panel>
        </div>

<asp:Panel ID="PanelFormulario" runat="server">


<div class="containerFormulario">
<div id="containerValidacao">
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" 
        HeaderText="Para continuar, preencha corretamente os campos abaixo:" 
        CssClass="ResumoErro" />
</div>
    

         <table class="Largura100Porcento">
        <tr>
            <td class="Largura150">
                &nbsp;
                
<asp:Label ID="Label1" runat="server" 
            style="font-weight: 700" Text="Financiador:"></asp:Label> 
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                    ControlToValidate="ddlFinanciador" ErrorMessage="Financiador" 
                    ForeColor="Red">*</asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:DropDownList ID="ddlFinanciador" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="Largura150">
            <asp:Label ID="Label3" runat="server" 
            style="font-weight: 700" Text="Quantidade de Bolsas:"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="txtQuantidadeBolsa" ErrorMessage="Quantidade de Bolsa" 
                    ForeColor="Red">*</asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:TextBox ID="txtQuantidadeBolsa" runat="server" Width="75px"></asp:TextBox>
            </td>
        </tr>
             <tr>
                 <td class="Largura150">
                     <asp:Label ID="Label4" runat="server" style="font-weight: 700" Text="Valor R$:"></asp:Label>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                         ControlToValidate="txtValor" ErrorMessage="Valor" ForeColor="Red">*</asp:RequiredFieldValidator>
                 </td>
                 <td>
                     <asp:TextBox ID="txtValor" runat="server" Width="75px"></asp:TextBox>
                 </td>
             </tr>
        </table>

</div>

</asp:Panel>

<div style="padding:10px;text-align:center">

        <asp:ImageButton onmouseover="~/images/nnewrecordbutton.png" 
    onmouseout="~/images/newrecordbutton.png" ID="btnNovo" runat="server" 
    ImageUrl="~/images/newrecordbutton.png" onclick="btnNovo_Click" 
        CausesValidation="false" Visible="False" />

    <asp:ImageButton onmouseover="~/images/savebutton_hover.png" 
    onmouseout="~/images/savebutton.png" ID="btnSalvar" runat="server" 
    ImageUrl="~/images/savebutton.png" onclick="btnSalvar_Click" 
        style="height: 28px" />

        <asp:ImageButton onmouseover="~/images/cancelbutton_hover.png" 
    onmouseout="~/images/cancelbutton.png" ID="btnVoltar" runat="server" 
    ImageUrl="~/images/cancelbutton.png" onclick="btnVoltar_Click" 
            CausesValidation="false" />

</div>
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="head">

</asp:Content>

