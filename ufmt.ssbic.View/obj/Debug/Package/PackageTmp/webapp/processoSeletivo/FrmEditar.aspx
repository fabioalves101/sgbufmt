﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true" CodeBehind="FrmEditar.aspx.cs" Inherits="ufmt.ssbic.View.webapp.processoSeletivo.FrmEditar" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>&nbsp;Editar Registro &raquo;
        <asp:Label ID="lblTitulo" runat="server"></asp:Label>
        <asp:HiddenField ID="hddCodigoRegistro" runat="server" />
    </h1>

        <div id="containerMensagens">
                <asp:Panel ID="PanelMsgResultado" runat="server" Visible="false">
                    <asp:Label ID="lblMsgResultado" runat="server"></asp:Label>
                </asp:Panel>
        </div>


<asp:Panel ID="PanelFormulario" runat="server">

<div class="containerFormulario">
<div id="containerValidacao">
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" 
        HeaderText="Para continuar, preencha corretamente os campos abaixo:" 
        CssClass="ResumoErro" />
    <table class="Largura100Porcento">
        <tr>
            <td class="Largura150">
                <asp:Label ID="Label7" runat="server" style="font-weight: 700" 
                    Text="Título:"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" 
                    ControlToValidate="txtTitulo" ErrorMessage="Título" ForeColor="Red">*</asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:TextBox ID="txtTitulo" runat="server" Width="487px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="Largura150">
                <asp:Label ID="Label8" runat="server" style="font-weight: 700" 
                    Text="Descrição:"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" 
                    ControlToValidate="txtDescricao" ErrorMessage="Descrição" 
                    ForeColor="Red">*</asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:TextBox ID="txtDescricao" runat="server" Width="487px" Height="91px" 
                    TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="Largura150">
                <asp:Label ID="Label5" runat="server" style="font-weight: 700" 
                    Text="Data de Início:"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                    ControlToValidate="txtDataAplicacao" ErrorMessage="Data de Aplicação" 
                    ForeColor="Red">*</asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:TextBox ID="txtDataAplicacao" runat="server" Width="92px"></asp:TextBox>
                <asp:CalendarExtender ID="txtDataAplicacao_CalendarExtender" runat="server" 
                    Enabled="True" Format="dd/MM/yyyy" PopupButtonID="imgCalendarioData1" 
                    TargetControlID="txtDataAplicacao">
                                </asp:CalendarExtender>
                <asp:MaskedEditExtender ID="txtDataAplicacao_MaskedEditExtender" runat="server" 
                    CultureDateFormat="pt-br" Enabled="True" Mask="99/99/9999" MaskType="Date" 
                    TargetControlID="txtDataAplicacao">
                                 </asp:MaskedEditExtender>
                <asp:Image ID="imgCalendarioData1" runat="server" 
                    ImageUrl="~/images/Calendar_scheduleHS.png" />
                <asp:MaskedEditValidator ID="MEVData1" runat="server" 
                    ControlExtender="txtDataAplicacao_MaskedEditExtender" 
                    ControlToValidate="txtDataAplicacao" CssClass="failureNotification" 
                    Display="Dynamic" ForeColor="Red" 
                    InvalidValueMessage="Data no formato inválido" IsValidEmpty="True"></asp:MaskedEditValidator>
            </td>
        </tr>
        <tr>
            <td class="Largura150">
                <asp:Label ID="Label6" runat="server" style="font-weight: 700" 
                    Text="Data Final:"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                    ControlToValidate="txtDataResultado" ErrorMessage="Data do Resultado" 
                    ForeColor="Red">*</asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:TextBox ID="txtDataResultado" runat="server" Width="92px"></asp:TextBox>
                <asp:MaskedEditExtender ID="txtDataResultado_MaskedEditExtender" runat="server" 
                    CultureDateFormat="pt-br" Enabled="True" Mask="99/99/9999" MaskType="Date" 
                    TargetControlID="txtDataResultado">
                    </asp:MaskedEditExtender>
                <asp:CalendarExtender ID="CalendarExtender_txtDataResultado" runat="server" 
                    Enabled="True" Format="dd/MM/yyyy" PopupButtonID="imgCalendarioData2" 
                    TargetControlID="txtDataResultado">
                    </asp:CalendarExtender>
                <asp:Image ID="imgCalendarioData2" runat="server" 
                    ImageUrl="~/images/Calendar_scheduleHS.png" />
                <asp:MaskedEditValidator ID="MEVData2" runat="server" 
                    ControlExtender="txtDataResultado_MaskedEditExtender" 
                    ControlToValidate="txtDataResultado" CssClass="failureNotification" 
                    Display="Dynamic" ForeColor="Red" 
                    InvalidValueMessage="Data no formato inválido" IsValidEmpty="True">
                      </asp:MaskedEditValidator>
            </td>
        </tr>
    </table>
</div>
    
    
</div>
    </asp:Panel>
<div style="padding:10px;text-align:center">
    <asp:ImageButton onmouseover="~/images/savebutton.png" 
    onmouseout="~/images/savebutton.png" ID="btnSalvar" runat="server" 
    ImageUrl="~/images/savebutton.png" onclick="btnSalvar_Click" />

        <asp:ImageButton onmouseover="~/images/cancelbutton_hover.png" 
    onmouseout="~/images/cancelbutton.png" ID="btnVoltar" runat="server" 
    ImageUrl="~/images/cancelbutton.png" onclick="btnVoltar_Click" 
        CausesValidation="false" />

</div>
</asp:Content>
