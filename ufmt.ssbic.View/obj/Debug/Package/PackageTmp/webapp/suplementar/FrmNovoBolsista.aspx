﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true" CodeBehind="FrmNovoBolsista.aspx.cs" Inherits="ufmt.ssbic.View.webapp.auxilio.FrmNovoBolsista" %>
<%@ Register src="../../controles/UCBuscarAluno.ascx" tagname="UCBuscarAluno" tagprefix="uc1" %>
<%@ Register src="../../controles/UCDadosBancariosAluno.ascx" tagname="UCDadosBancariosAluno" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<style>
.mGrid td {color:#000;}
.mGrid td a{color:#000;}
</style>

<!-- BREADCRUMB -->
        <asp:Panel ID="breadcrumb" CssClass="breadcrumb" runat="server" Visible="true">
        Você está em: <strong>Fluxo de Bolsistas</strong> &raquo;
                <asp:HyperLink NavigateUrl="~/webapp/editais/FrmListar.aspx" ID="HyperLink2" runat="server">Instrumentos de Seleção em aberto</asp:HyperLink>
                <strong>&raquo;</strong>
                <asp:HyperLink NavigateUrl="~/webapp/editais/FrmListar.aspx" ID="hpetapas" runat="server">Etapas Mensais</asp:HyperLink>
                <strong>&raquo;</strong>
                <asp:Label ID="Label4" Text="Gerenciamento dos bolsistas" runat="server" Font-Bold="True"></asp:Label>
            </asp:Panel>
<!-- FIM BREADCRUMB -->

<div style="padding: 5px; text-align: left;">
                <h1>
                    Adicionar aluno<span class="allcaps">
                        » Folha Suplementar
                    </span>
                </h1>
                

                <div id="containerMensagens">
                    <asp:Panel ID="PanelMsgResultado" runat="server" Visible="false">
                        <asp:Label ID="lblMsgResultado" runat="server"></asp:Label>
                    </asp:Panel>
                </div>
<h2>Adicionar aluno</h2>
    <asp:Panel ID="PanelFormulario" runat="server">
    <uc1:UCBuscarAluno ID="UCBuscarAluno1" runat="server" />
    
    <div class="containerFormulario">
    Dados Bancários
        <asp:RadioButtonList ID="rdoImportaDados" runat="server" AutoPostBack="True" 
            RepeatDirection="Horizontal" 
            onselectedindexchanged="rdoImportaDados_SelectedIndexChanged">
            <asp:ListItem Value="1">Importar do SIGA</asp:ListItem>
            <asp:ListItem Value="2">Cadastrar Dados Bancários</asp:ListItem>
        </asp:RadioButtonList>
        <asp:Panel ID="pnlDadosBancarios" runat="server" Visible="False">
        <p>
            Banco: 
            <asp:DropDownList ID="ddlBanco" runat="server" DataSourceID="odsBancos" 
                DataTextField="banco" DataValueField="bancoUID">
            </asp:DropDownList>
            <asp:ObjectDataSource ID="odsBancos" runat="server" SelectMethod="GetBancos" 
                TypeName="ufmt.ssbic.Business.BancoBO"></asp:ObjectDataSource>
        </p>
            
        <p>
            Agência:<asp:TextBox ID="txtAgencia" runat="server"></asp:TextBox>
        </p>
        <p>
            Número da Conta:<asp:TextBox ID="txtConta" runat="server"></asp:TextBox>
        </p>
        </asp:Panel>
        <hr />
        <asp:Panel ID="pnlValor" runat="server" Visible="true">
        Valor a ser pago para o aluno:
            <asp:TextBox ID="txtValor" runat="server"></asp:TextBox>
        </asp:Panel>

        <asp:Button ID="btnSalvar" runat="server" Text="Salvar" 
            onclick="btnSalvar_Click" />    
    </div>

    </asp:Panel>
    </div>
</asp:Content>
