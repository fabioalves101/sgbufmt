﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="FrmListar.aspx.cs" Inherits="ufmt.ssbic.View.webapp.folha.FrmListar" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
    

    <!-- BREADCRUMB -->
        <asp:Panel ID="breadcrumb" CssClass="breadcrumb" runat="server" Visible="true">
        Você está em: <strong>Fluxo de Bolsistas</strong> &raquo;
                <asp:HyperLink NavigateUrl="~/webapp/editais/FrmListar.aspx" ID="HyperLink2" runat="server">Instrumentos de Seleção em aberto</asp:HyperLink>
                &raquo;
                <asp:Label ID="Label4" Text="Etapas Mensais" runat="server" Font-Bold="True"></asp:Label>
            </asp:Panel>
    <!-- FIM BREADCRUMB -->

            <div style="padding: 5px; text-align: left;">
                <h1>
                    <span class="allcaps">
                        <asp:Label ID="Label1" runat="server"></asp:Label>
                    </span>
                </h1>
                

                <div id="containerMensagens">
                    <asp:Panel ID="PanelMsgResultado" runat="server" Visible="false">
                        <asp:Label ID="lblMsgResultado" runat="server"></asp:Label>
                    </asp:Panel>
                </div>
                <fieldset style="padding: 10px">
                    <legend>
                        <img alt="Filtrar Registros" class="style2" src="../../images/ico_filters.png" />
                        Filtar Registros</legend>
                    <asp:Panel ID="PanelFiltro" runat="server">
                        Mês: 
                        <asp:DropDownList ID="ddlMes" runat="server">

                            <asp:ListItem Selected="True" Value="Selecione"></asp:ListItem>
                            <asp:ListItem Value="1">Janeiro</asp:ListItem>
                            <asp:ListItem Value="2">Fevereiro</asp:ListItem>
                            <asp:ListItem Value="3">Março</asp:ListItem>
                            <asp:ListItem Value="4">Abril</asp:ListItem>
                            <asp:ListItem Value="5">Maio</asp:ListItem>
                            <asp:ListItem Value="6">Junho</asp:ListItem>
                            <asp:ListItem Value="7">Julho</asp:ListItem>
                            <asp:ListItem Value="8">Agosto</asp:ListItem>
                            <asp:ListItem Value="9">Setembro</asp:ListItem>
                            <asp:ListItem Value="10">Outubro</asp:ListItem>
                            <asp:ListItem Value="11">Novembro</asp:ListItem>
                            <asp:ListItem Value="12">Dezembro</asp:ListItem>

                        </asp:DropDownList>
                        &nbsp; Ano: <asp:TextBox ID="txtAno" runat="server" Width="40px" EnableViewState="False"
                            ViewStateMode="Disabled"></asp:TextBox>
                        <asp:LinkButton ID="lnkConsultar" runat="server" CssClass="minibutton" 
                            onclick="lnkConsultar_Click">
                                       
    <span>Consultar</span>                   
                        </asp:LinkButton>
                        <asp:LinkButton ID="lnkMostrarTodos" runat="server" CssClass="minibutton" 
                            onclick="lnkMostrarTodos_Click">
  
    <span>
        Mostrar Todos
    </span>
                                             
                        </asp:LinkButton>


                    </asp:Panel>
                </fieldset>
                <div style="float:left">
                <p style="float:left">
                <asp:LinkButton ID="lnkGerarFolha" runat="server" CssClass="minibutton" 
                        onclick="lnkGerarFolha_Click">  
                    <span>
                     Gerar Nova Folha de Pagamento                 
                     </span>
                </asp:LinkButton>
                </p>
                <p style="float:right; padding-left:20px">
                <asp:LinkButton ID="lnkFolhaSuplementar" runat="server" CssClass="minibutton" 
                        onclick="lnkFolhaSuplementar_Click">  
                    <span>
                     Folhas de Pagamento Suplementar
                     </span>
                </asp:LinkButton>
                </p>
                </div>
                <br style="clear:both" />
                <p>

                <asp:LinkButton ID="lnkFecharIS" runat="server" CssClass="minibutton" Visible="false">  
                    <span>
                     Fechar Instrumento de Seleção           
                     </span>
                </asp:LinkButton>
                </p>
                <!-- PANEL NOVA FOLHA -->
                <asp:Panel ID="pnlNovaFolha" runat="server" Visible="false">
                <fieldset>
                <legend>Selecione o período da folha de pagamento</legend>

                Mês: 
                <asp:DropDownList runat="server" ID="ddlMesFolha">
                               <asp:ListItem Value="1">Janeiro</asp:ListItem>
                               <asp:ListItem Value="2">Fevereiro</asp:ListItem>
                               <asp:ListItem Value="3">Março</asp:ListItem>
                               <asp:ListItem Value="4">Abril</asp:ListItem>
                               <asp:ListItem Value="5">Maio</asp:ListItem>
                               <asp:ListItem Value="6">Junho</asp:ListItem>
                               <asp:ListItem Value="7">Julho</asp:ListItem>
                               <asp:ListItem Value="8">Agosto</asp:ListItem>
                               <asp:ListItem Value="9">Setembro</asp:ListItem>
                               <asp:ListItem Value="10">Outubro</asp:ListItem>
                               <asp:ListItem Value="11">Novembro</asp:ListItem>
                               <asp:ListItem Value="12">Dezembro</asp:ListItem>
                           </asp:DropDownList>  
                Ano: 
                           <asp:DropDownList ID="ddlAnoFolha" runat="server">
                               <asp:ListItem>2012</asp:ListItem>
                               <asp:ListItem>2013</asp:ListItem>
                               <asp:ListItem>2014</asp:ListItem>
                               <asp:ListItem>2015</asp:ListItem>
                               <asp:ListItem>2016</asp:ListItem>
                               <asp:ListItem>2017</asp:ListItem>
                               <asp:ListItem>2018</asp:ListItem>
                               <asp:ListItem>2019</asp:ListItem>
                               <asp:ListItem>2020</asp:ListItem>
                           </asp:DropDownList>

              <%-- <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                   CssClass="ResumoErro" ForeColor="Red" 
                   HeaderText="Para continuar, preencha corretamente os campos abaixo:" />--%>
            


               <%--<table class="Largura100Porcento">
                  
                   <tr>
                       <td class="Largura150">
                           <asp:Label ID="Label5" runat="server" style="font-weight: 700" 
                               Text="Data de Início:"></asp:Label>
                           <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                               ControlToValidate="txtDataInicio" ErrorMessage="Data de Aplicação" 
                               ForeColor="Red">*</asp:RequiredFieldValidator>
                       </td>
                       <td>
                           <asp:TextBox ID="txtDataInicio" runat="server" Width="92px"></asp:TextBox>
                           <asp:CalendarExtender ID="txtDataAplicacao_CalendarExtender" runat="server" 
                               Enabled="True" Format="dd/MM/yyyy" PopupButtonID="imgCalendarioData1" 
                               TargetControlID="txtDataInicio">
                           </asp:CalendarExtender>
                           <asp:MaskedEditExtender ID="txtDataAplicacao_MaskedEditExtender" runat="server" 
                               CultureDateFormat="pt-br" Enabled="True" Mask="99/99/9999" MaskType="Date" 
                               TargetControlID="txtDataInicio">
                           </asp:MaskedEditExtender>
                           <asp:Image ID="imgCalendarioData1" runat="server" 
                               ImageUrl="~/images/Calendar_scheduleHS.png" />
                           <asp:MaskedEditValidator ID="MEVData1" runat="server" 
                               ControlExtender="txtDataAplicacao_MaskedEditExtender" 
                               ControlToValidate="txtDataInicio" CssClass="failureNotification" 
                               Display="Dynamic" ForeColor="Red" 
                               InvalidValueMessage="Data no formato inválido" IsValidEmpty="True"></asp:MaskedEditValidator>

                       </td>
                   </tr>
                   <tr>
                       <td class="Largura150">
                           <asp:Label ID="Label10" runat="server" style="font-weight: 700" 
                               Text="Data Final:"></asp:Label>
                           <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                               ControlToValidate="txtDataFinal" ErrorMessage="Data do Resultado" 
                               ForeColor="Red">*</asp:RequiredFieldValidator>
                       </td>
                       <td>
                           <asp:TextBox ID="txtDataFinal" runat="server" Width="92px"></asp:TextBox>
                           <asp:MaskedEditExtender ID="txtDataResultado_MaskedEditExtender" runat="server" 
                               CultureDateFormat="pt-br" Enabled="True" Mask="99/99/9999" MaskType="Date" 
                               TargetControlID="txtDataFinal">
                           </asp:MaskedEditExtender>
                           <asp:CalendarExtender ID="CalendarExtender_txtDataResultado" runat="server" 
                               Enabled="True" Format="dd/MM/yyyy" PopupButtonID="imgCalendarioData2" 
                               TargetControlID="txtDataFinal">
                           </asp:CalendarExtender>
                           <asp:Image ID="imgCalendarioData2" runat="server" 
                               ImageUrl="~/images/Calendar_scheduleHS.png" />
                           <asp:MaskedEditValidator ID="MEVData2" runat="server" 
                               ControlExtender="txtDataResultado_MaskedEditExtender" 
                               ControlToValidate="txtDataFinal" CssClass="failureNotification" 
                               Display="Dynamic" ForeColor="Red" 
                               InvalidValueMessage="Data no formato inválido" IsValidEmpty="True">
                      </asp:MaskedEditValidator>
                       </td>
                   </tr>
                
               </table>--%>
               <p>
    <asp:ImageButton onmouseover="~/images/savebutton_hover.png" 
    onmouseout="~/images/savebutton.png" ID="btnSalvar" runat="server" 
    ImageUrl="~/images/savebutton.png" onclick="btnSalvar_Click" 
        style="height: 28px" />

        </p>

               <asp:HiddenField ID="hdProcessoSeletivo" runat="server" />
               </fieldset>
                </asp:Panel>
                
                <!--GRIDVIEW -->
                <asp:Panel ID="PanelAcoes" runat="server" Style="padding: 10px">
                    <div style="clear: both">
                        <asp:GridView ID="GdrFolha" runat="server" AllowPaging="True" 
                        AutoGenerateColumns="False" 
                        OnPageIndexChanging="GridView1_PageIndexChanging" Width="100%" 
                            CssClass="mGrid" onrowcommand="GdrFolha_RowCommand" 
                        >
                        <Columns>
                            <asp:TemplateField HeaderText="Período" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("dataInicio", "{0:MM/yyyy}") %>'></asp:Label>                                   
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Width="5%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Etapa 1 - Processo Seletivo" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>                
                                    <asp:Label ID="lbletapa1" runat="server" Text='<%# verificarSituacao((int)Eval("etapa1")) %>'></asp:Label>                                   
                                </ItemTemplate>                                
                                <ItemStyle HorizontalAlign="Center" Width="15%" />
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="Etapa 2 - Homologação" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>                
                                    <asp:Label ID="lbletapa2" runat="server" Text='<%# verificarSituacao((int)Eval("etapa2")) %>'></asp:Label>
                                    
                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="parecer2" Text='<%# VerificarParecer((String)Eval("parecerE2")) %>' CommandArgument='<%# Eval("folhaPagamentoUID") %>' />
                                </ItemTemplate>                                
                                <ItemStyle HorizontalAlign="Center" Width="15%" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Etapa 3 - Financeiro" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>                
                                    <asp:Label ID="lbletapa3" runat="server" Text='<%# verificarSituacao((int)Eval("etapa3")) %>'></asp:Label>                                   
                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="parecer3" Text='<%# VerificarParecer((String)Eval("parecerE3")) %>' CommandArgument='<%# Eval("folhaPagamentoUID") %>' />
                                </ItemTemplate>                                
                                <ItemStyle HorizontalAlign="Center" Width="15%" />
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="Gerenciar Bolsistas" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                   <asp:HyperLink ID="HyperLink1" NavigateUrl='<%# Eval("folhaPagamentoUID", "../bolsista/FrmListar.aspx?folhaPagamentoUID={0}") %>' runat="server">Gerenciar Bolsistas</asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="10%" />
                            </asp:TemplateField>

                             <asp:TemplateField HeaderText="Gerenciar Auxílios" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                   <asp:HyperLink ID="hpAuxilios" NavigateUrl='<%# Eval("folhaPagamentoUID", "../auxilio/FrmListar.aspx?folhaPagamentoUID={0}") %>' runat="server">Gerenciar Auxílios</asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="10%" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Gerenciar Voluntários" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <a href='../bolsista/FrmListarVoluntarios.aspx?processoSeletivoUID=<%=Request.QueryString["processoSeletivoUID"] %>&folhaPagamentoUID=<%# Eval("folhaPagamentoUID") %>'>Gerenciar Voluntários</a>                                    
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="10%" />
                            </asp:TemplateField>

                            <asp:TemplateField Visible="false" HeaderText="Imprimir Folha" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                   <asp:HyperLink ID="HyperLink1" ImageUrl="~/images/pdf.gif" NavigateUrl='<%# Eval("folhaPagamentoUID", "../relatorio/FrmRelatorio.aspx?folhaPagamentoUID={0}") %>' runat="server">Imprimir Folha</asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="10%" />
                            </asp:TemplateField>

                            <asp:TemplateField Visible="false" HeaderText="Imprimir Folha" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                   <asp:HyperLink ID="HyperLink1" ImageUrl="~/images/pdf.gif" NavigateUrl='<%# Eval("folhaPagamentoUID", "../relatorio/FrmRelatorioPrae.aspx?folhaPagamentoUID={0}") %>' runat="server">Imprimir Folha</asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="10%" />
                            </asp:TemplateField>

                        </Columns>
                        </asp:GridView>
                    </div>
                    <div class="addbutton" style="float: left; margin-right: 5px">
                        <asp:Label ID="lblResultadoConsulta" runat="server"></asp:Label>
                        &nbsp;<asp:Label ID="lblTotalRegistros" runat="server" CssClass="ArialBlack16"></asp:Label>
                    </div>
                    <div style="clear: both" />
                    </asp:Panel>
            <!-- FIM GRIDVIEW -->    
            <!-- PAINEL MODAL POPUP -->        
                
               <asp:modalpopupextender 
                ID="ModalPopupExtender1"
                BehaviorID="behavior"
                runat="server"
                TargetControlID="lnkModal"
                PopupControlID="PanelDetalhes"
                BackgroundCssClass="modalBackground"
                DropShadow="True"
                DynamicServicePath="" 
                Enabled="True"
        
        />

        <asp:HyperLink ID="lnkModal" runat="server" Visible="true" Text=""  />

 <asp:Panel ID="PanelDetalhes" runat="server" CssClass="modalPopup" style="display:none">
          <div id="ContainerDetalhes" style="width:800px">
          <div id="MenuDetalhes" style="background-color:White;padding-left:5px;padding-bottom:5px;height:40px">
            <div style="width:650px; float:left; height: 32px;">
                <h2 style="height: 35px"><asp:Label ID="lblTituloParecer" runat="server" Text="Label"></asp:Label></h2>
            </div>
            <div style="float:left;width:130px;text-align:right; padding-top:10px;">
                <asp:ImageButton ID="ImageButton1" runat="server" 
                    ImageUrl="../../images/close24.png" OnclientClick="$find('behavior').hide(); return false;" ToolTip="Fechar Janela" ImageAlign="AbsMiddle" />
                &nbsp;<asp:LinkButton ID="LinkButtonFechar" runat="server" Visible="true" Text="Fechar" OnclientClick="$find('behavior').hide(); return false;"   />
            </div>
          </div>
           <div id="corpoDetalhes" style="border:1px solid #cccccc;height:450px;padding-top:10px;background-color:#f7f7f7">
            
               <table class="Largura100Porcento">
                   <tr>
                       <td class="Largura150">
                           <asp:Label ID="Label9" runat="server" style="font-weight: 700" 
                               Text="Parecer:"></asp:Label>
                       </td>
                       <td>
                           <asp:Label ID="lblParecer" runat="server"></asp:Label>
                       </td>
                   </tr>
                   
               </table>
            
           </div> 


          </div>      
</asp:Panel>
                <!-- FIM MODAL POPUP -->
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
