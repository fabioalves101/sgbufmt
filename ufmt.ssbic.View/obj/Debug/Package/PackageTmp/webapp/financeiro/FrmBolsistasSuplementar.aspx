﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true" CodeBehind="FrmBolsistasSuplementar.aspx.cs" Inherits="ufmt.ssbic.View.webapp.financeiro.FrmBolsistasSuplementar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <!-- BREADCRUMB -->
        <asp:Panel ID="breadcrumb" CssClass="breadcrumb" runat="server" Visible="true">
        Você está em: <strong>Fluxo de Bolsistas</strong> &raquo;
                <asp:HyperLink NavigateUrl="~/webapp/editais/FrmListar.aspx" ID="HyperLink2" runat="server">Instrumentos de Seleção em aberto</asp:HyperLink>
                &raquo;
                <asp:Label ID="Label4" Text="Etapas Mensais" runat="server" Font-Bold="True"></asp:Label>
            </asp:Panel>
    <!-- FIM BREADCRUMB -->

            <div style="padding: 5px; text-align: left;">
                <h1>
                    <span class="allcaps">
                        <asp:Label ID="Label1" runat="server"></asp:Label>
                    </span>
                </h1>
                

                <div id="containerMensagens">
                    <asp:Panel ID="PanelMsgResultado" runat="server" Visible="false">
                        <asp:Label ID="lblMsgResultado" runat="server"></asp:Label>
                    </asp:Panel>
                </div>
                <span style="margin-left:20px">
                    <asp:LinkButton ID="lnkImprimir" runat="server" CssClass="minibutton" 
                    onclick="lnkImprimir_Click">
                        <span>Imprimir Folha Suplementar</span>
                        </asp:LinkButton>
                </span>

                <asp:Panel ID="pnlGridview" runat="server">
                    <asp:GridView ID="grdAlunosFolha" runat="server" AutoGenerateColumns="False" 
                        DataSourceID="odsAlunosFolha" Width="100%" 
                        onrowcommand="grdAlunosFolha_RowCommand" CssClass="mGrid">
                        <Columns>
                            <asp:TemplateField HeaderText="CPF" SortExpression="cpf">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# GetNomeAluno((string)Eval("cpf")) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="valor" HeaderText="Valor" SortExpression="valor" />
                         </Columns>
                    </asp:GridView>
                    <asp:ObjectDataSource ID="odsAlunosFolha" runat="server" 
                        SelectMethod="GetListBolsistaFolhaSuplementar" 
                        TypeName="ufmt.ssbic.Business.BolsistaFolhaSuplementarBO">
                        <SelectParameters>
                            <asp:QueryStringParameter Name="folhaSuplementarUID" 
                                QueryStringField="folhaSuplementarUID" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </asp:Panel>

            </div>

</asp:Content>
