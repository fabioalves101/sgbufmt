﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true" CodeBehind="FrmInstrumentosSelecao.aspx.cs" Inherits="ufmt.ssbic.View.webapp.financeiro.FrmInstrumentosSelecao" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<style>
.mGrid td 
{
    height:50px;
}
</style>
<div>
<h1>Instrumentos de Seleção com Folha de Pagamento Ativa</h1>
</div>
<div>
Filtrar por: 
    <asp:DropDownList ID="ddlPrograma" runat="server" AppendDataBoundItems="true" DataSourceID="odsProgramas" 
        DataTextField="descricao" DataValueField="proreitoriaUID">
        <asp:ListItem Value="">Todos</asp:ListItem>
    </asp:DropDownList>
    <asp:Button ID="btBuscar" runat="server" Text="Buscar" 
        onclick="btBuscar_Click" />
    <asp:ObjectDataSource ID="odsProgramas" runat="server" 
        SelectMethod="GetListPrograma" 
        TypeName="ufmt.ssbic.Business.FinanceiroController"></asp:ObjectDataSource>
    </div>

    <asp:GridView ID="grdInstrumentosSelecao" runat="server" CssClass="mGrid" 
        AutoGenerateColumns="False" DataSourceID="odsInstrumentosSelecao" Width="100%">
        <Columns>
            <asp:TemplateField HeaderText="Pró-Reitoria / Programa">
                <ItemStyle Width="80px" HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Proreitoria.descricao") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="descricao" HeaderText="descricao" 
                SortExpression="descricao" />
            <asp:TemplateField HeaderText="Bolsa">
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("Bolsa.descricao") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Opções">
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLink1" NavigateUrl='<%# Eval("instrumentoSelecaoUID", "FrmFolhasPagamento.aspx?is={0}") %>' runat="server">
                        <img src="../../images/detail.png" alt="Visualizar Folhas de Pagamento" title="Visualizar Folhas de Pagamento" width="30px" /> 
                    </asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:ObjectDataSource ID="odsInstrumentosSelecao" runat="server" 
        SelectMethod="GetInstrumentoSelecaoFolhaPagamentoAtiva" 
        TypeName="ufmt.ssbic.Business.FinanceiroController">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlPrograma" DefaultValue="" 
                Name="programaUID" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
