﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="FrmSuplementar.aspx.cs" Inherits="ufmt.ssbic.View.webapp.financeiro.FrmSuplementar" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
    

    <!-- BREADCRUMB -->
        <asp:Panel ID="breadcrumb" CssClass="breadcrumb" runat="server" Visible="true">
        Você está em: <strong>Fluxo de Bolsistas</strong> &raquo;
                <asp:HyperLink NavigateUrl="~/webapp/editais/FrmListar.aspx" ID="HyperLink2" runat="server">Instrumentos de Seleção em aberto</asp:HyperLink>
                &raquo;
                <asp:Label ID="Label4" Text="Etapas Mensais" runat="server" Font-Bold="True"></asp:Label>
            </asp:Panel>
    <!-- FIM BREADCRUMB -->
            <div style="padding: 5px; text-align: left;">
                <h1>
                    <span class="allcaps">
                        <asp:Label ID="Label1" runat="server"></asp:Label>
                    </span>
                </h1>
                

                <div id="containerMensagens">
                    <asp:Panel ID="PanelMsgResultado" runat="server" Visible="false">
                        <asp:Label ID="lblMsgResultado" runat="server"></asp:Label>
                    </asp:Panel>
                </div>
             
                <!-- PANEL NOVA FOLHA -->
                <asp:Panel ID="pnlNovaFolha" runat="server" Visible="false">
                <fieldset>
                <legend>Selecione o período da folha de pagamento</legend>

                Mês: 
                <asp:DropDownList runat="server" ID="ddlMesFolha">
                               <asp:ListItem Value="1">Janeiro</asp:ListItem>
                               <asp:ListItem Value="2">Fevereiro</asp:ListItem>
                               <asp:ListItem Value="3">Março</asp:ListItem>
                               <asp:ListItem Value="4">Abril</asp:ListItem>
                               <asp:ListItem Value="5">Maio</asp:ListItem>
                               <asp:ListItem Value="6">Junho</asp:ListItem>
                               <asp:ListItem Value="7">Julho</asp:ListItem>
                               <asp:ListItem Value="8">Agosto</asp:ListItem>
                               <asp:ListItem Value="9">Setembro</asp:ListItem>
                               <asp:ListItem Value="10">Outubro</asp:ListItem>
                               <asp:ListItem Value="11">Novembro</asp:ListItem>
                               <asp:ListItem Value="12">Dezembro</asp:ListItem>
                           </asp:DropDownList>  
                Ano: 
                           <asp:DropDownList ID="ddlAnoFolha" runat="server">
                               <asp:ListItem>2012</asp:ListItem>
                               <asp:ListItem>2013</asp:ListItem>
                               <asp:ListItem>2014</asp:ListItem>
                               <asp:ListItem>2015</asp:ListItem>
                               <asp:ListItem>2016</asp:ListItem>
                               <asp:ListItem>2017</asp:ListItem>
                               <asp:ListItem>2018</asp:ListItem>
                               <asp:ListItem>2019</asp:ListItem>
                               <asp:ListItem>2020</asp:ListItem>
                           </asp:DropDownList>

             
               <p>
    <asp:ImageButton onmouseover="~/images/savebutton_hover.png" 
    onmouseout="~/images/savebutton.png" ID="btnSalvar" runat="server" 
    ImageUrl="~/images/savebutton.png" onclick="btnSalvar_Click" 
        style="height: 28px" />

        </p>

               <asp:HiddenField ID="hdProcessoSeletivo" runat="server" />
               </fieldset>
                </asp:Panel>

                <asp:Panel ID="panelGridview" runat="server">
                <asp:GridView ID="grdFolhaSuplementar" runat="server" AutoGenerateColumns="False" 
                        DataSourceID="odsFolhasSuplementares" Width="100%" CssClass="mGrid">
                    <Columns>
                        <asp:BoundField DataField="dataInicio" HeaderText="Início" 
                            SortExpression="dataInicio" />
                        <asp:BoundField DataField="dataFim" HeaderText="Fim" SortExpression="dataFim" />
                        <asp:TemplateField HeaderText="Gerenciar Alunos da Folha">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:HyperLink ID="hpGerenciarBolsistas" NavigateUrl='<%# Eval("folhaSuplementarUID", "FrmBolsistasSuplementar.aspx?folhaSuplementarUID={0}") %>' runat="server">
                                    <img src="../../images/folha.png" alt="Gerenciar Alunos da Folha" title="Gerenciar Alunos da Folha" />                    
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="true" HeaderText="Imprimir Folha" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                   <asp:HyperLink ID="HyperLink1" ImageUrl="../../images/print.png" NavigateUrl='<%# Eval("folhaSuplementarUID", "../relatorio/FrmRelatorioSuplementar.aspx?folhaSuplementarUID={0}") %>' runat="server">
                                    Imprimir Folha
                                   </asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="10%" />
                            </asp:TemplateField>
                    </Columns>
                    </asp:GridView>

                    <asp:ObjectDataSource ID="odsFolhasSuplementares" runat="server" 
                        SelectMethod="GetFolhasPagamentoSuplementarPorProcessoSeletivo" 
                        TypeName="ufmt.ssbic.Business.FolhaPagamentoBO">
                        <SelectParameters>
                            <asp:QueryStringParameter Name="processoSeletivoUID" 
                                QueryStringField="processoSeletivoUID" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>

                </asp:Panel>

            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
