﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="FrmListar.aspx.cs" Inherits="ufmt.ssbic.View.webapp.responsaveis.FrmListar" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



            <div style="padding: 5px; text-align: left;">
                <h1>
                    <span class="allcaps">
                        <asp:Label ID="Label1" runat="server"></asp:Label>
                    </span>
                </h1>               
                

                <div id="containerMensagens">
                    <asp:Panel ID="PanelMsgResultado" runat="server" Visible="false">
                        <asp:Label ID="lblMsgResultado" runat="server"></asp:Label>
                    </asp:Panel>
                </div>
                <fieldset style="padding: 10px">
                    <legend>
                        <img alt="Filtrar Registros" class="style2" src="../../images/ico_filters.png" />
                        Filtar Registros</legend>
                    <asp:Panel ID="PanelFiltro" runat="server">
                        &nbsp;<asp:TextBox ID="txtCriterio" runat="server" Width="503px" EnableViewState="False"
                            ViewStateMode="Disabled"></asp:TextBox>
                        <asp:LinkButton ID="lnkConsultar" runat="server" CssClass="minibutton" 
                            onclick="lnkConsultar_Click">
                                       
    <span>Consultar</span>                   
                        </asp:LinkButton>
                        <asp:LinkButton ID="lnkMostrarTodos" runat="server" CssClass="minibutton" 
                            onclick="lnkMostrarTodos_Click">
  
    <span>
        Mostrar Todos
    </span>
                                             
                        </asp:LinkButton>
                    </asp:Panel>
                </fieldset>

                <asp:HyperLink ID="lnkNovoRegistro" runat="server" CssClass="minibutton">
                    <span>
                        <img alt="" src="../../images/add.png" border="0" />&nbsp;&nbsp; Adicionar Registro
                    </span>
                </asp:HyperLink>

                <!--GRIDVIEW -->
                <asp:Panel ID="PanelAcoes" runat="server" Style="padding: 10px" Visible="True">
                    <div style="clear: both">
                        <asp:GridView ID="GdrEditais" runat="server" AllowPaging="True" 
                        AutoGenerateColumns="False" 
                        OnPageIndexChanging="GridView1_PageIndexChanging" Width="100%" CssClass="mGrid" 
                        onrowcommand="GridView1_RowCommand" onrowdeleting="GdrEditais_RowDeleting"  
                        
                        >
                        <Columns>
                            <asp:TemplateField HeaderText="Nome" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("Nome") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Width="60%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Remover" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkBtExcluir" runat="server" CausesValidation="False" 
                                        CommandName="Delete" CommandArgument='<%# Eval("id") %>'
                                    onclientclick="javascript:return window.confirm('Tem certeza que deseja excluir o projeto?');">
                                    <span>
                                        <img src="../../../media/images/delete.png" alt="Excluir Projeto" title="Excluir Projeto" />                                
                                    </span>
                                </asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="10%" />
                            </asp:TemplateField>

                            

                        </Columns>
                        </asp:GridView>
                    </div>
                    <div class="addbutton" style="float: left; margin-right: 5px">
                        <asp:Label ID="lblResultadoConsulta" runat="server"></asp:Label>
                        &nbsp;<asp:Label ID="lblTotalRegistros" runat="server" CssClass="ArialBlack16"></asp:Label>
                    </div>
                    <div style="clear: both" />
                    </asp:Panel>
            <!-- FIM GRIDVIEW -->    
            <!-- PAINEL MODAL POPUP -->        
                
               <asp:modalpopupextender 
                ID="ModalPopupExtender1"
                BehaviorID="behavior"
                runat="server"
                TargetControlID="lnkModal"
                PopupControlID="PanelDetalhes"
                BackgroundCssClass="modalBackground"
                DropShadow="True"
                DynamicServicePath="" 
                Enabled="True"
        
        />

        <asp:HyperLink ID="lnkModal" runat="server" Visible="true" Text=""  />

 <asp:Panel ID="PanelDetalhes" runat="server" CssClass="modalPopup" style="display:none">
          <div id="ContainerDetalhes" style="width:800px">
          <div id="MenuDetalhes" style="background-color:White;padding-left:5px;padding-bottom:5px;height:40px">
            <div style="width:650px; float:left; height: 32px;">
                <h2 style="height: 35px"><asp:Label ID="Label2" runat="server" Text="Label">Detalhes do Registro</asp:Label></h2>
            </div>
            <div style="float:left;width:130px;text-align:right; padding-top:10px;">
                <asp:ImageButton ID="ImageButton1" runat="server" 
                    ImageUrl="../../images/close24.png" OnclientClick="$find('behavior').hide(); return false;" ToolTip="Fechar Janela" ImageAlign="AbsMiddle" />
                &nbsp;<asp:LinkButton ID="LinkButtonFechar" runat="server" Visible="true" Text="Fechar" OnclientClick="$find('behavior').hide(); return false;"   />
            </div>
          </div>
           <div id="corpoDetalhes" style="border:1px solid #cccccc;height:450px;padding-top:10px;background-color:#f7f7f7">
            
               <table class="Largura100Porcento">
                   <tr>
                       <td class="Largura150">
                           <asp:Label ID="Label9" runat="server" style="font-weight: 700" 
                               Text="Pró-Reitoria:"></asp:Label>
                       </td>
                       <td>
                           <asp:Label ID="LabelDetalhe4" runat="server"></asp:Label>
                       </td>
                   </tr>
                   <tr>
                       <td class="Largura150">
                           <asp:Label ID="Label6" runat="server" style="font-weight: 700" Text="Bolsa:"></asp:Label>
                       </td>
                       <td>
                           <asp:Label ID="LabelDetalhe1" runat="server"></asp:Label>
                       </td>
                   </tr>
                   <tr>
                       <td class="Largura150">
                           <asp:Label ID="Label7" runat="server" style="font-weight: 700" 
                               Text="Descrição:"></asp:Label>
                       </td>
                       <td>
                           <asp:Label ID="LabelDetalhe2" runat="server"></asp:Label>
                       </td>
                   </tr>
                   <tr>
                       <td class="Largura150">
                           <asp:Label ID="Label8" runat="server" style="font-weight: 700" Text="Tipo:"></asp:Label>
                       </td>
                       <td>
                           <asp:Label ID="LabelDetalhe3" runat="server"></asp:Label>
                       </td>
                   </tr>
               </table>
            
           </div> 


          </div>      
</asp:Panel>
                <!-- FIM MODAL POPUP -->
                <!-- PAINÉL DE HOMOLOGAÇÃO -->
                <asp:Panel ID="PnlHomologacao" runat="server" Visible="false">
                    <div style="text-align:center">
                    </div>
                </asp:Panel>
                <!-- FIM PANEL HOMOLOGAÇÃO -->
            </div>
</asp:Content>
