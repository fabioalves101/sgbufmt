﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true" CodeBehind="FrmEditar.aspx.cs" Inherits="ufmt.ssbic.View.webapp.bolsa.bolsa.FrmEditar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Editar Registro »
        <asp:Label ID="lblTitulo" runat="server"></asp:Label>
    &nbsp;</h1>

        <div id="containerMensagens">
                <asp:Panel ID="PanelMsgResultado" runat="server" Visible="false">
                    <asp:Label ID="lblMsgResultado" runat="server"></asp:Label>
                </asp:Panel>
        </div>


<asp:Panel ID="PanelFormulario" runat="server">

<div class="containerFormulario">
<div id="containerValidacao">
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" 
        HeaderText="Para continuar, preencha corretamente os campos abaixo:" 
        CssClass="ResumoErro" />
</div>
    
    
    <table class="Largura100Porcento">
        <tr>
            <td class="Largura150">
                &nbsp;
            <asp:Label ID="Label1" runat="server" 
            style="font-weight: 700" Text="Descrição:"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="txtDescricao" ErrorMessage="Bolsa" ForeColor="Red">*</asp:RequiredFieldValidator>
            </td>
            <td>
            <asp:TextBox ID="txtDescricao" runat="server" Width="486px"></asp:TextBox>
                <asp:HiddenField ID="hddCodigoRegistro" runat="server" />
            </td>
        </tr>

        <tr>
            <td class="Largura150">
            <asp:Label ID="Label2" runat="server" 
            style="font-weight: 700" Text="Detalhes:"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="txtDetalhes" ErrorMessage="Detalhes" ForeColor="Red">*</asp:RequiredFieldValidator>
            </td>
            <td>
            <asp:TextBox ID="txtDetalhes" runat="server" Width="754px" Height="200px" 
                    TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>

        </table>

</div>
    </asp:Panel>
<div style="padding:10px;text-align:center">
    <asp:ImageButton onmouseover="~/images/savebutton.png" 
    onmouseout="~/images/savebutton.png" ID="btnSalvar" runat="server" 
    ImageUrl="~/images/savebutton.png" onclick="btnSalvar_Click" />

        <asp:ImageButton onmouseover="~/images/cancelbutton_hover.png" 
    onmouseout="~/images/cancelbutton.png" ID="btnVoltar" runat="server" 
    ImageUrl="~/images/cancelbutton.png" onclick="btnVoltar_Click" 
        CausesValidation="false" />

</div>
</asp:Content>
