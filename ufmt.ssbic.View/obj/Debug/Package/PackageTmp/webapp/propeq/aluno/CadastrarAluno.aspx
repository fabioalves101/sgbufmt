﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="CadastrarAluno.aspx.cs" Inherits="ufmt.ssbic.View.CadastrarAluno" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:Panel ID="panelAviso" runat="server" Visible="false">    
        <div class="pnlAviso">
                    <img alt="" src="/media/images/alerta.png" border="0" />&nbsp;&nbsp;
                    <asp:Label ID="lblAlerta" runat="server" Text="" Visible="true"></asp:Label>
        </div>
</asp:Panel>
    <h1>Cadastrar Aluno</h1>
<fieldset>
<p>
    <asp:HyperLink ID="hpVoltar" runat="server">&gt;&gt;Voltar para lista de alunos</asp:HyperLink>
</p>
<p>
    Buscar:<asp:TextBox ID="txtAluno" runat="server" Width="300px"></asp:TextBox>
    <asp:DropDownList ID="ddlAluno" runat="server">
        <asp:ListItem Value="0">Nome</asp:ListItem>
        <asp:ListItem Value="1">Matrícula</asp:ListItem>
    </asp:DropDownList>
    <asp:ImageButton ID="btBuscar" runat="server" 
            ImageUrl="~/media/images/busca.png" CssClass="imgBtBuscar" 
        onclick="btBuscar_Click"/>
    </p>
    <asp:HiddenField ID="hdNome" runat="server" />
    <asp:HiddenField ID="hdMatricula" runat="server" />
    
</fieldset>

    <asp:Panel ID="pnlMembros" runat="server" Visible="false">    
<fieldset>
<legend>Aluno Selecionado</legend>
<p>
        Nome do Aluno:
        <asp:Label ID="lblNome" runat="server" Font-Bold="True"></asp:Label>
</p>
<p>
        Matrícula:
        <asp:Label ID="lblMatricula" runat="server" Font-Bold="True"></asp:Label>
</p>
<p>
        Tipo:
        <asp:Label ID="lblTipo" runat="server" Font-Bold="True"></asp:Label>
</p>
<p>
        Selecione o membro do projeto:
    <asp:DropDownList ID="ddlMembros" runat="server" DataSourceID="odsMembros" 
        DataTextField="nome" DataValueField="pessoaMembroUID">
    </asp:DropDownList>
</p>
    <asp:ObjectDataSource ID="odsMembros" runat="server" 
        SelectMethod="GetMembrosProjeto" TypeName="ufmt.ssbic.Business.MembroBO">
        <SelectParameters>
            <asp:QueryStringParameter Name="projetoUID" QueryStringField="projetoUID" 
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
</fieldset>
<fieldset>
    <asp:ImageButton onmouseover="~/media/images/savebutton_hover.png" 
    onmouseout="~/media/images/savebutton.png" ID="btSalvar" runat="server" 
    ImageUrl="~/media/images/savebutton.png" onclick="btSalvar_Click" />
</fieldset>
    </asp:Panel>

<asp:Panel ID="pnlAlunos" runat="server" Visible="false">    
<fieldset>
    <legend>Alunos</legend>
    <asp:GridView ID="gdrAlunos" runat="server" CssClass="mGrid"
         PagerStyle-CssClass="pgr"
         AlternatingRowStyle-CssClass="alt" AllowPaging="True" 
        AutoGenerateColumns="False" DataKeyNames="Matricula" 
        onselectedindexchanged="gdrAlunos_SelectedIndexChanged"
        EmptyDataText="Vazio"
         >
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>

        <Columns>
            
            <asp:TemplateField HeaderText="Selecionar" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="60px">
                <ItemTemplate>
                    <asp:LinkButton CssClass="minibutton" ID="LinkButton1" runat="server" CausesValidation="False" 
                                CommandName="Select">
                            <span>
                                <img src="../../../media/images/select.png" alt="" />
                                Selecionar
                            </span>
                            </asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:BoundField DataField="Matricula" HeaderText="Matricula" ReadOnly="True" 
                SortExpression="Matricula" />
            <asp:BoundField DataField="Nome" HeaderText="Nome" SortExpression="Nome" />
            <asp:BoundField DataField="CPF" HeaderText="CPF" SortExpression="CPF" />
            <asp:BoundField DataField="Tipo" HeaderText="Tipo" ReadOnly="True" 
                SortExpression="Tipo" />
        </Columns>

<PagerStyle CssClass="pgr"></PagerStyle>
    </asp:GridView>
    <asp:SqlDataSource ID="sqlAlunos" runat="server" 
        ConnectionString="<%$ ConnectionStrings:idbufmt %>" 
        SelectCommand="GetVinculo" SelectCommandType="StoredProcedure" 
        CancelSelectOnNullParameter="False">
        <SelectParameters>
            <asp:ControlParameter ControlID="hdNome" DefaultValue="" Name="Nome" 
                PropertyName="Value" Type="String" />
            <asp:ControlParameter ControlID="hdMatricula" DefaultValue="" Name="Matricula" 
                PropertyName="Value" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />
</fieldset>
</asp:Panel>
</asp:Content>
