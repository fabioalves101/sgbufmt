﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="EditarProjeto.aspx.cs" Inherits="ufmt.ssbic.View.EditarProjeto" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:Panel ID="panelAviso" runat="server" Visible="false">    
        <div class="pnlAviso">
                    <img alt="" src="/media/images/alert.png" border="0" />&nbsp;&nbsp;
                    <asp:Label ID="lblAlerta" runat="server" Text="" Visible="true"></asp:Label>
        </div>
</asp:Panel>
<h1>Editar projeto</h1>
<fieldset>
    <legend>Projeto</legend>
    <asp:Panel ID="pnlProjeto" runat="server">
        <p>
        <asp:Label ID="lblNumeroRegistro" runat="server" Text="Número de Registro CAP: "></asp:Label>
        <asp:TextBox ID="txtNumeroRegistro" runat="server" Width="50px"></asp:TextBox>

            <asp:MaskedEditExtender ID="txtNumeroRegistro_MaskedEditExtender" 
                runat="server" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" 
                CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" 
                InputDirection="RightToLeft" Mask="99999" MaskType="Number" 
                TargetControlID="txtNumeroRegistro">
            </asp:MaskedEditExtender>

        <asp:Label ID="lblAnoRegistro" runat="server" Text="Ano do Registro CAP: "></asp:Label>
        <asp:TextBox ID="txtAnoRegistro" runat="server" Width="50px"></asp:TextBox>
            <asp:MaskedEditExtender ID="txtAnoRegistro_MaskedEditExtender" runat="server" 
                CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" 
                CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" 
                InputDirection="RightToLeft" Mask="9999" MaskType="Number" 
                TargetControlID="txtAnoRegistro">
            </asp:MaskedEditExtender>
        </p>
        <p>
        <asp:Label ID="lblTitulo" runat="server" Text="Título do projeto: "></asp:Label> <br />
        <asp:TextBox ID="txtTitulo" runat="server" Width="700px" Rows="2" 
            TextMode="MultiLine"></asp:TextBox>
        </p>
        <p>
            Área de Conhecimento:
            <asp:DropDownList ID="ddlAreaConhecimento" runat="server" DataSourceID="odsAreaConhecimento" 
                DataTextField="descricao" DataValueField="areaConhecimentoUID">
            </asp:DropDownList>
            <asp:ObjectDataSource ID="odsAreaConhecimento" runat="server" 
                SelectMethod="AreasConhecimento" TypeName="ufmt.ssbic.Business.ProjetoBO">
            </asp:ObjectDataSource>
        </p>
        <p>
            <asp:Label ID="lblInicio" runat="server" Text="Início: "></asp:Label>
            <asp:TextBox ID="txtInicio" runat="server" Width="70px"></asp:TextBox>
            <asp:CalendarExtender ID="txtInicio_CalendarExtender" runat="server" 
                Enabled="True" TargetControlID="txtInicio" Format="dd/MM/yyyy">
            </asp:CalendarExtender>
            <asp:Label ID="lblFim" runat="server" Text="Fim: "></asp:Label>
            <asp:TextBox ID="txtFim" runat="server" Width="70px"></asp:TextBox>
            <asp:CalendarExtender ID="txtFim_CalendarExtender" runat="server" 
                Enabled="True" TargetControlID="txtFim" Format="dd/MM/yyyy">
            </asp:CalendarExtender>
        </p>
        <p>
            <asp:CheckBox ID="chkProdutoTecnologico" runat="server" AutoPostBack="false" Text="Vai gerar produto tecnológico? " 
                TextAlign="Left" />
        </p>
        <asp:UpdatePanel ID="updInteracaoInstituicao" runat="server">
            <ContentTemplate>
                <p>
                    <asp:CheckBox ID="chkInteracao" runat="server" AutoPostBack="true" Text="Tem interação com outra instituição? " 
                        TextAlign="Left" oncheckedchanged="chkInteracao_CheckedChanged" />
                </p>

                <asp:Panel ID="pnlInteracao" runat="server" Visible="false">
                <fieldset>
                <p>
                    Interação com instituição: 
                        <asp:RadioButtonList ID="rdoInteracaoInstituicao" runat="server">

                            <asp:ListItem Value="0">Privada</asp:ListItem>
                            <asp:ListItem Value="1">Governamental</asp:ListItem>
                            <asp:ListItem Value="2">Empresa</asp:ListItem>

                        </asp:RadioButtonList>
                </p>
                </fieldset>
                </asp:Panel>

            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:HiddenField ID="hdInteracao" runat="server" />
        <asp:UpdatePanel ID="UpdPnlConvenio" runat="server">
        <ContentTemplate>
        <p>
            <asp:CheckBox ID="chkConvenio" runat="server" AutoPostBack="True" Text="Possui convênio? " 
                TextAlign="Left" oncheckedchanged="chkConvenio_CheckedChanged" />
                <asp:HiddenField ID="hdConvenio" runat="server" />
        </p>

        <asp:Panel ID="pnlConvenio" runat="server" 
            Visible="False">
            <asp:Label ID="lblConvenio" runat="server" Text="Convênio: "></asp:Label>
            <asp:TextBox ID="txtConvenio" runat="server" Width="400px"></asp:TextBox>
        </asp:Panel>
        </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="UpdPnlFinanciamento" runat="server">
        <ContentTemplate>
        <p>
            <asp:CheckBox ID="chkFinanciamento" runat="server" AutoPostBack="True" Text="Possui financiamento? " 
                TextAlign="Left" oncheckedchanged="chkFinanciamento_CheckedChanged" />
            <asp:HiddenField ID="hdFinanciamento" runat="server" />
        </p>
        <asp:Panel ID="pnlFinanciamento" runat="server" Visible="False">
        <p>
            <asp:Label ID="lblFinanciador" runat="server" Text="Financiador: "></asp:Label>
            <asp:TextBox ID="txtFinanciador" runat="server" Width="400px"></asp:TextBox>
        </p>
        <p>
            <asp:Label ID="lblValorFinanciamento" runat="server" 
                Text="Valor do Financiamento: "></asp:Label>
            <asp:TextBox ID="txtValorFinanciamento" runat="server" Width="85px"></asp:TextBox>
            
        </p>
        </asp:Panel>
        </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</fieldset>
<fieldset>
    <legend>Coordenador</legend>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updPnlListaServidores">
        <ProgressTemplate>
        <div style="text-align:center">
            <img alt="Carregando" src="media/images/ajax-loader.gif" />
        </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updPnlListaServidores" runat="server">
        
    <ContentTemplate>
    <asp:Panel ID="pnlCoordenador" runat="server">
        <asp:Panel ID="pnlCoordenadorSelecionado" runat="server" Visible="False">        
            <p style="font-weight:bold"> 
                <asp:Label ID="lblCoordenadorSelecionado" runat="server" Text="" Visible="false"></asp:Label>
                <asp:HiddenField ID="hdCoordenadorSelecionado" runat="server" />
            </p>
            <p>
                <asp:Label ID="lblCargaHoraria" runat="server" Text="Carga Horária: "></asp:Label>
                <asp:TextBox ID="txtCargaHoraria" runat="server" Width="40px"></asp:TextBox>
                
                <asp:NumericUpDownExtender ID="txtCargaHoraria_NumericUpDownExtender" 
                    runat="server" Enabled="True" Maximum="10" Minimum="0" RefValues="" 
                    ServiceDownMethod="" ServiceDownPath="" ServiceUpMethod="" Tag="" 
                    TargetButtonDownID="" TargetButtonUpID="" TargetControlID="txtCargaHoraria" 
                    Width="40">
                </asp:NumericUpDownExtender>
                
            </p>
        </asp:Panel>
    <p>
        <asp:Label ID="lblBuscaCoordenador" runat="server" Text="Buscar: "></asp:Label>       
        <asp:TextBox ID="txtBuscaCoordenador" runat="server" Width="400px"></asp:TextBox>
        <asp:DropDownList ID="ddlBuscaCoordenador" runat="server">
            <asp:ListItem>Nome</asp:ListItem>
            <asp:ListItem>CPF</asp:ListItem>
            <asp:ListItem Value="siape">Registro SIAPE</asp:ListItem>
        </asp:DropDownList>
        <asp:ImageButton ID="btBuscar" runat="server" 
            ImageUrl="~/media/images/busca.png" CssClass="imgBtBuscar" 
            onclick="btBuscar_Click" />
    </p>
        
        <asp:Panel ID="pnlListaServidores" runat="server" Visible="False">
        Selecione o coordenador.
            <asp:GridView ID="gdrListaServidores" runat="server" 
                AutoGenerateColumns="False" DataSourceID="ObjectDataSource1" 
                AllowPaging="True" Width="100%"
                CssClass="mGrid"
                PagerStyle-CssClass="pgr"
                AlternatingRowStyle-CssClass="alt" DataKeyNames="ServidorUID" onselectedindexchanged="gdrListaServidores_SelectedIndexChanged"
                >            
                <AlternatingRowStyle CssClass="alt" />
                <Columns>                    
                    <asp:TemplateField HeaderText="Selecionar" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="120px">
                        <ItemTemplate>
                            <asp:LinkButton CssClass="minibutton" ID="LinkButton1" runat="server" CausesValidation="False" 
                                CommandName="Select">
                            <span>
                                <img src="media/images/select.png" alt="" />
                                Selecionar
                            </span>
                            </asp:LinkButton>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="120px" />
                    </asp:TemplateField>
                    
                    <asp:BoundField DataField="Registro" HeaderText="Registro SIAPE" ItemStyle-HorizontalAlign="Center" 
                        SortExpression="Registro" ItemStyle-Width="120px">
                    <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                        Nome do Servidor
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='  <%# Eval("Pessoa.Nome")%>'></asp:Label>
                        
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle CssClass="pgr" />
            </asp:GridView>
            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
                SelectMethod="GetPessoas" TypeName="ufmt.ssbic.Business.PessoaBO">
                <SelectParameters>
                    <asp:ControlParameter ControlID="txtBuscaCoordenador" DefaultValue="null" 
                        Name="parametro" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="ddlBuscaCoordenador" DefaultValue="null" 
                        Name="tipoParametro" PropertyName="SelectedValue" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
            
        </asp:Panel>       
        
    </asp:Panel>
    </ContentTemplate>
    </asp:UpdatePanel>
   
</fieldset>
<fieldset>
    <asp:ImageButton onmouseover="~/media/images/savebutton_hover.png" 
    onmouseout="~/media/images/savebutton.png" ID="btSalvar" runat="server" 
    ImageUrl="~/media/images/savebutton.png" onclick="btSalvar_Click" />
</fieldset>
</asp:Content>
