﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true" CodeBehind="FrmCadastrar.aspx.cs" Inherits="ufmt.ssbic.View.webapp.financiador.FrmCadastrar" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>&nbsp;Novo Registro &raquo;
        <asp:Label ID="lblTitulo" runat="server"></asp:Label>
    </h1>

        <div id="containerMensagens">
                <asp:Panel ID="PanelMsgResultado" runat="server" Visible="false">
                    <asp:Label ID="lblMsgResultado" runat="server"></asp:Label>
                </asp:Panel>
        </div>

<asp:Panel ID="PanelFormulario" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>

<div class="containerFormulario">
<div id="containerValidacao">
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" 
        HeaderText="Para continuar, preencha corretamente os campos abaixo:" 
        CssClass="ResumoErro" />
</div>
    

         <table class="Largura100Porcento">
        <tr>
            <td class="Largura150">
                &nbsp;
                
<asp:Label ID="Label1" runat="server" 
            style="font-weight: 700" Text="Financiador:"></asp:Label> 
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                    ControlToValidate="ddlFinanciador" ErrorMessage="Financiador" 
                    ForeColor="Red">*</asp:RequiredFieldValidator>
            </td>
            <td>
                 <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>

                <asp:DropDownList ID="ddlFinanciador" runat="server">
                </asp:DropDownList>
                <asp:ImageButton ID="imgBtnFinanciador" runat="server" ImageUrl="~/images/add_20.png" 
                        ToolTip="Novo registro" onclick="imgBtnFinanciador_Click" 
                            CausesValidation="False" />
&nbsp;<asp:ImageButton ID="imgBtnEdit" runat="server" ImageUrl="~/images/edit_20.png" 
                        ToolTip="Atualizar Registro" onclick="imgBtnEdit_Click" 
                            CausesValidation="False" />
        
        <!-- panel collapse -->

        <asp:Panel ID="Panel3" runat="server">
                <asp:Label ID="lblNovaInstituicao" runat="server"></asp:Label>
         &nbsp;<asp:TextBox ID="txtNovaInstituicao" runat="server" 
             Width="190px"></asp:TextBox> <br />
             Banco: 
            <asp:DropDownList ID="ddBanco" runat="server" DataSourceID="odsBanco" 
                    DataTextField="banco" DataValueField="bancoUID">
            </asp:DropDownList>
            <asp:ObjectDataSource ID="odsBanco" runat="server" SelectMethod="GetBancos" 
                    TypeName="ufmt.ssbic.Business.BancoBO"></asp:ObjectDataSource>
         &nbsp;<asp:Button ID="btnNovaInstituicao" runat="server" Text="OK" 
             onclick="btnNovaInstituicao_Click" CausesValidation="False" />
         &nbsp;<asp:Button ID="btnCancelarNovoFinanciador" runat="server" 
             onclientclick="javascript:return false;" Text="Cancelar" 
             CausesValidation="False" />
             </asp:Panel>
             <asp:HiddenField ID="hdd_op_i" runat="server" />
                
                <asp:Label ID="Label6" runat="server" 
            style="font-weight: 700" Text="Financiador:"></asp:Label> 
                <asp:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" 
                        TargetControlID="Panel3"
                        CollapsedSize="0"
                        Collapsed="True"
                        ExpandControlID="imgBtnFinanciador"
                        CollapseControlID="btnCancelarNovoFinanciador"
                        AutoCollapse="False"
                        AutoExpand="False"
                        ScrollContents="false"
                        TextLabelID="Label6"
                        CollapsedText="Mostrar detalhes..."
                        ExpandedText="Ocultar detalhes" 
                        ExpandDirection="Vertical"
                />

                </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="Largura150">
            <asp:Label ID="Label5" runat="server" 
            style="font-weight: 700" Text="Tipo de Bolsa:"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                    ControlToValidate="rblTipoBolsa" ErrorMessage="Tipo de Bolsa" ForeColor="Red">*</asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:RadioButtonList ID="rblTipoBolsa" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="rblTipoBolsa_SelectedIndexChanged" 
                    RepeatDirection="Horizontal">
                    <asp:ListItem Value="1">Remunerado</asp:ListItem>
                    <asp:ListItem Value="2">Voluntário</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
             <tr>
                 <td class="Largura150">
                     <asp:Label ID="Label3" runat="server" style="font-weight: 700" 
                         Text="Quantidade de Bolsas:"></asp:Label>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                         ControlToValidate="txtQuantidadeBolsa" ErrorMessage="Quantidade de Bolsa" 
                         ForeColor="Red">*</asp:RequiredFieldValidator>
                 </td>
                 <td>
                     <asp:TextBox ID="txtQuantidadeBolsa" runat="server" Width="75px"></asp:TextBox>
                 </td>
             </tr>
             <tr>
                 <td class="Largura150">
                     <asp:Label ID="Label4" runat="server" style="font-weight: 700" Text="Valor R$:"></asp:Label>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                         ControlToValidate="txtValor" ErrorMessage="Valor" ForeColor="Red">*</asp:RequiredFieldValidator>
                 </td>
                 <td>
                     <asp:TextBox ID="txtValor" runat="server" Width="75px"></asp:TextBox>
                 </td>
             </tr>
             <tr>
                 <td class="Largura150">
                     &nbsp;</td>
                 <td>
                     
                 </td>
             </tr>
             <tr runat="server" id="trtable" style="font-weight: 700" visible="false">
                 <td class="Largura150">Curso: 
                     &nbsp;</td>
                 <td>
                     <asp:Label ID="lblnomecurso" runat="server"></asp:Label>
                 </td>
             </tr>
        </table>
    <asp:Panel ID="Panel2" runat="server">
        <fieldset>
            <legend>Vincular bolsas à curso</legend>
            <asp:Button ID="btnNaoAplica" runat="server" Text="Não se aplica" 
                CausesValidation="false" onclick="btnNaoAplica_Click" />

            <asp:Button ID="btVincularCurso" runat="server" Text="Selecionar Curso" 
                         onclick="btVincularCurso_Click" CausesValidation="false" />
        </fieldset>
    </asp:Panel>

    <asp:Panel ID="pnlCurso" runat="server" Visible="false">
        <fieldset>
            <legend>Buscar curso</legend>
            Nome: 
            <asp:TextBox ID="txtNomeCurso" runat="server" Width="300px"></asp:TextBox>
            <asp:Button ID="btBuscar" runat="server" Text="Buscar Curso" 
                onclick="btBuscar_Click" CausesValidation="false" />
            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
            AutoGenerateColumns="False" CssClass="mGrid" 
                onpageindexchanging="GridView1_PageIndexChanging1" OnRowCommand="GridView1_RowCommand">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton CssClass="minibutton" ID="LinkButton1" runat="server" CausesValidation="False" 
                                CommandName="Select" CommandArgument='<%# Eval("cursoUID") %>'>
                            <span>
                                <img src="../../media/images/select.png" alt="" />
                                Selecionar
                            </span>
                            </asp:LinkButton>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="10%" />
                </asp:TemplateField>  
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Eval("nome") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="90%" />
                </asp:TemplateField>               
            </Columns>
            <PagerTemplate>
                <asp:Panel ID="Panel1" runat="server" DefaultButton="cmdIr">
                    <div style="text-align: left">
                        <asp:ImageButton ID="cmdFirst" runat="server" CausesValidation="False" 
                            CommandArgument="First" CommandName="Page" 
                            ImageUrl="../../images/i_primeira_pagina.gif" ToolTip="Ir para a primeira página" />
                        &nbsp;<asp:ImageButton ID="cmdPrev" runat="server" CausesValidation="False" 
                            CommandArgument="Prev" CommandName="Page" 
                            ImageUrl="../../images/i_pagina_anterior.gif" ToolTip="Ir para a página anterior" />
                        &nbsp;&nbsp;&nbsp;&nbsp;<asp:ImageButton ID="cmdNext" runat="server" 
                            CausesValidation="False" CommandArgument="Next" CommandName="Page" 
                            ImageUrl="../../images/i_proxima_pagina.gif" ToolTip="Ir para a próxima página" />
                        &nbsp;<asp:ImageButton ID="cmdLast" runat="server" CausesValidation="False" 
                            CommandArgument="Last" CommandName="Page" 
                            ImageUrl="../../images/i_ultima_pagina.gif" ToolTip="Ir para a última página" />
                        &nbsp;&nbsp; Ir para a página
                        Ir para a página
                        <asp:TextBox ID="txtPagina" runat="server" 
                            Text="<% # GridView1.PageIndex + 1 %>" ValidationGroup="grid" Width="29px"></asp:TextBox>
                        &nbsp;<asp:ImageButton ID="cmdIr" runat="server" ImageUrl="../../images/btnIr.gif" 
                            ValidationGroup="grid" />
                        <b>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                            ControlToValidate="txtPagina" Display="Dynamic" 
                            ErrorMessage="* Entre com um valor para continuar" Font-Bold="False" 
                            ValidationGroup="grid"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                            ControlToValidate="txtPagina" Display="Dynamic" 
                            ErrorMessage="* Digite somente números para continuar" Font-Bold="False" 
                            ValidationExpression="^[0-9]+$" ValidationGroup="grid">* Digite somente números* Digite somente números</asp:RegularExpressionValidator>
                        </b>
                    </div>
                    <div style="text-align: left">
                        Página&nbsp;Página&nbsp;<asp:Label ID="lblTotalPaginaAtual" runat="server" 
                            Font-Bold="True" Text="<%# GridView1.PageIndex + 1 %>"></asp:Label>
                        &nbsp;de&nbsp;&nbsp;de&nbsp;<asp:Label ID="lblTotalPaginas" runat="server" 
                            Text="<% # GridView1.PageCount %>"></asp:Label>
                        &nbsp;
                    </div>
                </asp:Panel>
            </PagerTemplate>
            <EmptyDataTemplate>
                <div 
                    style="text-align: center; padding-top: 20px; padding-bottom: 20px; font-size: 18px;
                                                    font-family: Arial Narrow; font-weight: bold">
                    <asp:Label ID="lblMsgNenhumRegistro" runat="server" 
                        Text="Nenhum registro localizado no critério da consulta."></asp:Label>
                </div>
            </EmptyDataTemplate>
        </asp:GridView>


        </fieldset>
    </asp:Panel>
    <asp:HiddenField ID="hdCursoUID" runat="server" />
</div>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Panel>

<div style="padding:10px;text-align:center">

        <asp:ImageButton onmouseover="~/images/nnewrecordbutton.png" 
    onmouseout="~/images/newrecordbutton.png" ID="btnNovo" runat="server" 
    ImageUrl="~/images/newrecordbutton.png" onclick="btnNovo_Click" 
        CausesValidation="false" Visible="False" />

    <asp:ImageButton onmouseover="~/images/savebutton_hover.png" 
    onmouseout="~/images/savebutton.png" ID="btnSalvar" runat="server" 
    ImageUrl="~/images/savebutton.png" onclick="btnSalvar_Click" 
        style="height: 28px" />

        <asp:ImageButton onmouseover="~/images/cancelbutton_hover.png" 
    onmouseout="~/images/cancelbutton.png" ID="btnVoltar" runat="server" 
    ImageUrl="~/images/cancelbutton.png" onclick="btnVoltar_Click" 
            CausesValidation="false" />

</div>
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="head">

</asp:Content>

