﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true" CodeBehind="FrmEditar.aspx.cs" Inherits="ufmt.ssbic.View.webapp.auxilio.FrmEditar" %>

<%@ Register src="../../controles/UCDadosBancariosAluno.ascx" tagname="UCDadosBancariosAluno" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<h2>Editar auxiliado</h2>
    <asp:Panel ID="PanelFormulario" runat="server">
        <p>
        Nome: 
        <asp:Label ID="lblNomeAluno" runat="server" Text=""></asp:Label>
        </p>
        <p>
        CPF: 
        <asp:Label ID="lblCPF" runat="server" Text=""></asp:Label>
        </p>
        <p>
        RGA: 
        <asp:Label ID="lblRGA" runat="server" Text=""></asp:Label>
        </p>
        <p>
        Curso: 
        <asp:Label ID="lblCurso" runat="server" Text=""></asp:Label>
        </p>
        <p>
        Campus: 
        <asp:Label ID="lblCampus" runat="server" Text=""></asp:Label>
        </p>
    
    <div class="containerFormulario">
    
    <asp:CheckBoxList ID="chkAuxilios" runat="server" DataSourceID="odsAuxilios" 
            DataTextField="nome" DataValueField="auxilioUID" 
            ondatabound="chkAuxilios_DataBound">
    </asp:CheckBoxList>
        <asp:ObjectDataSource ID="odsAuxilios" runat="server" 
            SelectMethod="GetAuxilios" TypeName="ufmt.ssbic.Business.AuxilioController">
        </asp:ObjectDataSource>
        <br />
        <hr />
        <asp:RadioButtonList ID="rdoImportaDados" runat="server" AutoPostBack="True" 
            RepeatDirection="Horizontal" 
            onselectedindexchanged="rdoImportaDados_SelectedIndexChanged">
            <asp:ListItem Value="1">Importar do SIGA</asp:ListItem>
            <asp:ListItem Value="2" Selected="True">Cadastrar Dados Bancários</asp:ListItem>
        </asp:RadioButtonList>
        <asp:Panel ID="pnlDadosBancarios" runat="server">
        <p>
            Banco: 
            <asp:DropDownList ID="ddlBanco" runat="server" DataSourceID="odsBancos" 
                DataTextField="banco" DataValueField="bancoUID" AppendDataBoundItems="true">
                <asp:ListItem>-- Selecione --</asp:ListItem>
            </asp:DropDownList>
            <asp:ObjectDataSource ID="odsBancos" runat="server" SelectMethod="GetBancos" 
                TypeName="ufmt.ssbic.Business.BancoBO"></asp:ObjectDataSource>
        </p>
            
        <p>
            Agência:<asp:TextBox ID="txtAgencia" runat="server"></asp:TextBox>
        </p>
        <p>
            Número da Conta:<asp:TextBox ID="txtConta" runat="server"></asp:TextBox>
        </p>
        </asp:Panel>
        <hr />
        <asp:Button ID="btnSalvar" runat="server" Text="Atualizar" 
            onclick="btnSalvar_Click" />    
    </div>

    </asp:Panel>
</asp:Content>
