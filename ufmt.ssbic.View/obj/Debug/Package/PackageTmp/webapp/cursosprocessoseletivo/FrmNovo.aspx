﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true" CodeBehind="FrmNovo.aspx.cs" Inherits="ufmt.ssbic.View.webapp.cursosprocessoseletivo.FrmNovo" %>
<%@ Register src="../../controles/UCBuscarCursos.ascx" tagname="UCBuscarCursos" tagprefix="uc1" %>
<%@ Register src="../../controles/UCVagasCurso.ascx" tagname="UCVagasCurso" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1><span class="allcaps">Vagas por curso</span></h1>
<div>
    <uc2:UCVagasCurso ID="UCVagasCurso1" runat="server" />
</div>
<div>
    <asp:Button ID="btVagasCurso" runat="server" Text="Salvar vagas por curso" 
        onclick="btVagasCurso_Click" />
</div>
</asp:Content>
