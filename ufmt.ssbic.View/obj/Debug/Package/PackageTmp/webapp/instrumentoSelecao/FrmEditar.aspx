﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true" CodeBehind="FrmEditar.aspx.cs" Inherits="ufmt.ssbic.View.webapp.instrumentoSelecao.FrmEditar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>&nbsp;Editar Registro &raquo;
        <asp:Label ID="lblTitulo" runat="server"></asp:Label>
        <asp:HiddenField ID="hddCodigoRegistro" runat="server" />
    </h1>

        <div id="containerMensagens">
                <asp:Panel ID="PanelMsgResultado" runat="server" Visible="false">
                    <asp:Label ID="lblMsgResultado" runat="server"></asp:Label>
                </asp:Panel>
        </div>


<asp:Panel ID="PanelFormulario" runat="server">

<div class="containerFormulario">
<div id="containerValidacao">
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" 
        HeaderText="Para continuar, preencha corretamente os campos abaixo:" 
        CssClass="ResumoErro" />
    <table class="Largura100Porcento">
        <tr>
            <td class="Largura150">
                &nbsp;
                <asp:Label ID="Label1" runat="server" style="font-weight: 700" Text="Tipo:"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                    ControlToValidate="ddlTipo" ErrorMessage="Tipo" ForeColor="Red">*</asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:DropDownList ID="ddlTipo" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="Largura150">
                <asp:Label ID="Label3" runat="server" style="font-weight: 700" 
                    Text="Descrição:"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="txtDescricao" ErrorMessage="Descrição" ForeColor="Red">*</asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:TextBox ID="txtDescricao" runat="server" Width="486px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="Largura150">
                <asp:Label ID="Label4" runat="server" style="font-weight: 700" Text="Bolsa:"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                    ControlToValidate="ddlBolsa" ErrorMessage="Bolsa" ForeColor="Red">*</asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:DropDownList ID="ddlBolsa" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
                 <td class="Largura150">
                    <asp:Label ID="Label5" runat="server" style="font-weight: 700" Text="Arquivo Atual:"></asp:Label>
                 </td>
                 <td>
                     <asp:HyperLink ID="hpArquivoAtual" runat="server">
                        <img src="" alt="Download" />
                     </asp:HyperLink>
                 </td>
             </tr>

        <tr>
            <td class="Largura150">
                    <asp:Label ID="Label2" runat="server" style="font-weight: 700" Text="Substituir Arquivo:"></asp:Label>
                 </td>
                 <td>
                     <asp:FileUpload ID="fUpload" runat="server" />
                 </td>
             </tr>

    </table>
</div>
    
    
</div>
    </asp:Panel>
<div style="padding:10px;text-align:center">
    <asp:ImageButton onmouseover="~/images/savebutton.png" 
    onmouseout="~/images/savebutton.png" ID="btnSalvar" runat="server" 
    ImageUrl="~/images/savebutton.png" onclick="btnSalvar_Click" />

        <asp:ImageButton onmouseover="~/images/cancelbutton_hover.png" 
    onmouseout="~/images/cancelbutton.png" ID="btnVoltar" runat="server" 
    ImageUrl="~/images/cancelbutton.png" onclick="btnVoltar_Click" 
        CausesValidation="false" />

</div>
</asp:Content>
