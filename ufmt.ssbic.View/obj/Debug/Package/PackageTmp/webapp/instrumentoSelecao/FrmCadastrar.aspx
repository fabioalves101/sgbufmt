﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true" CodeBehind="FrmCadastrar.aspx.cs" Inherits="ufmt.ssbic.View.webapp.instrumentoSelecao.FrmCadastrar" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="FrmCadastrarInstrumentoSelecao.ascx" tagname="FrmCadastrarInstrumentoSelecao" tagprefix="uc1" %>
<%@ Register src="../financiador/UCCadastrarFinanciador.ascx" tagname="UCCadastrarFinanciador" tagprefix="uc2" %>
<%@ Register src="../processoSeletivo/UCCadastrarProcessoSeletivo.ascx" tagname="UCCadastrarProcessoSeletivo" tagprefix="uc3" %>
<%@ Register src="../responsaveis/UCCadastrarResponsaveis.ascx" tagname="UCCadastrarResponsaveis" tagprefix="uc4" %>
<%@ Register src="../../controles/UCVagasCurso.ascx" tagname="UCVagasCurso" tagprefix="uc5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>&nbsp;Novo Registro &raquo;
        <asp:Label ID="lblTitulo" runat="server"></asp:Label>
    </h1>
    <div id="containerMensagens">
                <asp:Panel ID="PanelMsgResultado" runat="server" Visible="false">
                    <asp:Label ID="lblMsgResultado" runat="server"></asp:Label>
                </asp:Panel>
        </div>

    <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0"
        Width="100%">
        <asp:TabPanel ID="TabPanel1" runat="server" HeaderText="Instrumento de Seleção">
            <ContentTemplate>
<uc1:FrmCadastrarInstrumentoSelecao ID="FrmCadastrarInstrumentoSelecao1" runat="server" />


</ContentTemplate>
        



</asp:TabPanel>
        <asp:TabPanel ID="TabPanel2" runat="server" HeaderText="Financiador">
            <ContentTemplate>
<uc2:UCCadastrarFinanciador ID="UCCadastrarFinanciador1" runat="server" />
</ContentTemplate>
        



</asp:TabPanel>
        <asp:TabPanel ID="TabPanel3" runat="server" HeaderText="Processo Seletivo">
            <ContentTemplate>                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                <uc3:UCCadastrarProcessoSeletivo ID="UCCadastrarProcessoSeletivo1" 
                    runat="server" />
</ContentTemplate>
        



</asp:TabPanel>
        <asp:TabPanel ID="TabPanel4" runat="server" HeaderText="Responsável" Visible="false">
            <ContentTemplate>                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                <uc4:UCCadastrarResponsaveis ID="UCCadastrarResponsaveis1" runat="server" />
</ContentTemplate>
        



</asp:TabPanel>
        <asp:TabPanel ID="TabCursos" runat="server" HeaderText="Cursos">
            <ContentTemplate>
                
                <uc5:UCVagasCurso ID="UCVagasCurso1" runat="server" />
                
            
</ContentTemplate>
        


</asp:TabPanel>
    </asp:TabContainer>
    

<div style="padding:10px;text-align:center">

        <asp:ImageButton onmouseover="~/images/nnewrecordbutton.png" 
    onmouseout="~/images/newrecordbutton.png" ID="btnNovo" runat="server" 
    ImageUrl="~/images/newrecordbutton.png" onclick="btnNovo_Click" 
        CausesValidation="false" Visible="False" />

    <asp:ImageButton onmouseover="~/images/savebutton_hover.png" 
    onmouseout="~/images/savebutton.png" ID="btnSalvar" runat="server" 
    ImageUrl="~/images/savebutton.png" onclick="btnSalvar_Click" 
        style="height: 28px" />

        <asp:ImageButton onmouseover="~/images/cancelbutton_hover.png" 
    onmouseout="~/images/cancelbutton.png" ID="btnVoltar" runat="server" 
    ImageUrl="~/images/cancelbutton.png" onclick="btnVoltar_Click" 
            CausesValidation="false" />

</div>
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="head">

</asp:Content>

