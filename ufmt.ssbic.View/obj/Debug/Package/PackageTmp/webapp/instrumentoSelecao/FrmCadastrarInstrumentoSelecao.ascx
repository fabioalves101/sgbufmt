﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FrmCadastrarInstrumentoSelecao.ascx.cs" Inherits="ufmt.ssbic.View.webapp.instrumentoSelecao.FrmCadastrarInstrumentoSelecao" %>
 <div id="containerMensagens">
                <asp:Panel ID="PanelMsgResultado" runat="server" Visible="false">
                    <asp:Label ID="lblMsgResultado" runat="server"></asp:Label>
                </asp:Panel>
        </div>
<asp:Panel ID="PanelFormulario" runat="server">


<div class="containerFormulario">
<div id="containerValidacao">
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" 
        HeaderText="Para continuar, preencha corretamente os campos abaixo:" 
        CssClass="ResumoErro" />
</div>
    

         <table class="Largura100Porcento">
        <tr>
            <td class="Largura150">
                &nbsp;
                
<asp:Label ID="Label1" runat="server" 
            style="font-weight: 700" Text="Tipo:"></asp:Label> 
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                    ControlToValidate="ddlTipo" ErrorMessage="Tipo" ForeColor="Red">*</asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:DropDownList ID="ddlTipo" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="Largura150">
            <asp:Label ID="Label3" runat="server" 
            style="font-weight: 700" Text="Descrição:"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="txtDescricao" ErrorMessage="Descrição" ForeColor="Red">*</asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:TextBox ID="txtDescricao" runat="server" Width="486px"></asp:TextBox>
            </td>
        </tr>
             <tr>
                 <td class="Largura150">
                     <asp:Label ID="Label4" runat="server" style="font-weight: 700" Text="Bolsa:"></asp:Label>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                         ControlToValidate="ddlBolsa" ErrorMessage="Bolsa" ForeColor="Red">*</asp:RequiredFieldValidator>
                 </td>
                 <td>
                     <asp:DropDownList ID="ddlBolsa" runat="server">
                     </asp:DropDownList>
                 </td>
             </tr>

             <tr>
                 <td class="Largura150">
                    <asp:Label ID="Label2" runat="server" style="font-weight: 700" Text="Arquivo:"></asp:Label>
                 </td>
                 <td>
                     <asp:FileUpload ID="fUpload" runat="server" />
                 </td>
             </tr>
        </table>

</div>

</asp:Panel>