﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="FrmListar.aspx.cs" Inherits="ufmt.ssbic.View.webapp.bolsista.FrmListar" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<style>
.mGrid td {color:#000;}
.mGrid td a{color:#000;}
</style>
<!-- BREADCRUMB -->
        <asp:Panel ID="breadcrumb" CssClass="breadcrumb" runat="server" Visible="true">
        Você está em: <strong>Fluxo de Bolsistas</strong> &raquo;
                <asp:HyperLink NavigateUrl="~/webapp/editais/FrmListar.aspx" ID="HyperLink2" runat="server">Instrumentos de Seleção em aberto</asp:HyperLink>
                <strong>&raquo;</strong>
                <asp:HyperLink NavigateUrl="~/webapp/folha/FrmListar.aspx" ID="hpetapas" runat="server">Etapas Mensais</asp:HyperLink>
                <strong>&raquo;</strong>
                <asp:Label ID="Label4" Text="Gerenciamento dos bolsistas" runat="server" Font-Bold="True"></asp:Label>
            </asp:Panel>
<!-- FIM BREADCRUMB -->

            <div style="padding: 5px; text-align: left;">
                <h1>
                    <span class="allcaps">
                        <asp:Label ID="Label1" runat="server"></asp:Label>
                    </span>
                </h1>               
                

                <div id="containerMensagens">
                    <asp:Panel ID="PanelMsgResultado" runat="server" Visible="false">
                        <asp:Label ID="lblMsgResultado" runat="server"></asp:Label>
                    </asp:Panel>
                </div>
                <fieldset style="padding: 10px">
                    <legend>
                        <img alt="Filtrar Registros" class="style2" src="../../images/ico_filters.png" />
                        Filtar Registros</legend>
                    <asp:Panel ID="PanelFiltro" runat="server">
                        <a href="FrmListarBolsistas.aspx?folhaPagamentoUID=<%=Request.QueryString["folhaPagamentoUID"] %>">
                            Visualizar todos os alunos da folha de pagamento
                        </a>
                        <br />
                        <br />
                        &nbsp;<asp:TextBox ID="txtCriterio" runat="server" Width="503px" EnableViewState="False"
                            ViewStateMode="Disabled"></asp:TextBox>
                        <asp:LinkButton ID="lnkConsultar" runat="server" CssClass="minibutton" 
                            onclick="lnkConsultar_Click">
                                       
    <span>Consultar</span>                   
                        </asp:LinkButton>
                        <asp:LinkButton ID="lnkMostrarTodos" runat="server" CssClass="minibutton" 
                            onclick="lnkMostrarTodos_Click">
  
    <span>
        Mostrar Todos
    </span>
                                             
                        </asp:LinkButton>

                        <p>
                            Modo de exibição: 
                        <asp:DropDownList ID="ddlSituacao" runat="server" AutoPostBack="True" 
                                onselectedindexchanged="ddlSituacao_SelectedIndexChanged">
                            
                            <asp:ListItem Value="0">Exibir todos</asp:ListItem>
                            <asp:ListItem Value="1">Exibir apenas ativos</asp:ListItem>
                            <asp:ListItem Value="2">Exibir apenas inativos</asp:ListItem>
                            
                        </asp:DropDownList>
                        </p>
                    </asp:Panel>
                </fieldset>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
                <div style="text-align:right; margin-top:10px; margin-right:20px; font-size:14px; font-weight:bold">
                <p>
                 Período da Folha de Pagamento: <asp:Label runat="server" ID="lblPeriodoFolha"></asp:Label>
                 </p>
                <p>
                 Quantidade de bolsistas ativos: <asp:Label runat="server" ID="lblBolsistasAtivos"></asp:Label>
                 </p>
                 <p>
                 Quantidade de bolsistas inativos: <asp:Label runat="server" ID="lblBolsistasInativos"></asp:Label>
                 </p>
                 <p>
                 Quantidade de bolsistas com pagamento recusado pelo financeiro: 
                 <asp:Label runat="server" ID="lblBolsistasPgtoRecusado"></asp:Label>
                 </p>
                </div>
                <!--GRIDVIEW -->
                <asp:Panel ID="PanelAcoes" runat="server" Style="padding: 10px" Visible="True">
                    <asp:Panel ID="pnlAcoesCoordenador" runat="server" Visible="false">
                    <asp:LinkButton ID="lnkNovoBolsista" runat="server" CssClass="minibutton" 
                        onclick="lnkNovoBolsista_Click">
                    <span>Novo Bolsista</span>
                    </asp:LinkButton>
                    
                    <asp:LinkButton ID="lnkFecharEtapa" runat="server" CssClass="minibutton" 
                            onclick="lnkFecharEtapa_Click">
                    <span>Finalizar Seleção</span>

                </asp:LinkButton>
                    </asp:Panel>
                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="minibutton" 
                        onclick="LinkButton1_Click">
                    <span>Imprimir Lista de Bolsistas</span>

                </asp:LinkButton>
                
                    <div style="clear: both">
                        <asp:GridView ID="GdrEditais" runat="server" AllowPaging="True" 
                        AutoGenerateColumns="False" Width="100%" CssClass="mGrid" 
                        onrowcommand="GridView1_RowCommand" onrowcreated="GridView1_RowCreated" DataSourceID="ObjectDataSource1"
                        
                        >
                        <Columns>
                            <asp:TemplateField HeaderText="Nome" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("Nome") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Width="30%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Gerenciar" ItemStyle-HorizontalAlign="Center" Visible="true">
                                <ItemTemplate>
                                    <asp:HyperLink ID="HyperLink1" NavigateUrl='<%# LinkEditarBolsista((int)Eval("bolsistaUID")) %>' runat="server">Editar</asp:HyperLink>                                    
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Width="10%" />
                            </asp:TemplateField>

                             <asp:TemplateField HeaderText="Data Ingresso" ItemStyle-HorizontalAlign="Center" Visible="false">
                                <ItemTemplate>
                                    
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Width="5%" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Situação" ItemStyle-HorizontalAlign="Center" ItemStyle-Height="50px">
                                <ItemTemplate>
                                    <asp:Label ID="LblAtivo" runat="server" Text='<%# VerificarSituacao((int)Eval("bolsistaUID")) %>'></asp:Label>
                                    <asp:LinkButton ID="lnkPagamento" runat="server" CommandName="pagamento" Text='<%# VerificarPermissaoAutorizacaoPagamento((bool)Eval("PagamentoAutorizado")) %>' CommandArgument='<%# Eval("BolsistaUID") %>' />                                     
                                    <asp:LinkButton ID="lnkRecusarPagamento" runat="server" CommandName="recusarpagamento" Text='<%# VerificarPermissaoRecusarPagamento((bool)Eval("PagamentoRecusado")) %>' CommandArgument='<%# Eval("BolsistaUID") %>' />                                     
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Width="25%" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:Label ID="lblJustificativaSituacao" runat="server" Text='<%# Eval("justificativaSituacao") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Parecer">
                                <ItemTemplate>
                                    <asp:Label ID="lblParecer" runat="server" Text='<%# GetParecer((int)Eval("bolsistaUID")) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                           


                        </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
                            SelectMethod="PreecherGridView" 
                            TypeName="ufmt.ssbic.View.webapp.bolsista.FrmListar">
                            <SelectParameters>
                                <asp:SessionParameter Name="proreitoriaUID" SessionField="programaUID" 
                                    Type="String" />
                                <asp:QueryStringParameter Name="folhaPagamentoUID" 
                                    QueryStringField="folhaPagamentoUID" Type="Int32" />
                                <asp:ControlParameter ControlID="txtCriterio" Name="nomeBusca" 
                                    PropertyName="Text" Type="String" />
                                <asp:ControlParameter ControlID="ddlSituacao" Name="situacao" 
                                    PropertyName="SelectedValue" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </div>
                    <div style="clear: both" />
                    </asp:Panel>
            <!-- FIM GRIDVIEW -->    
            <!-- PAINEL MODAL POPUP -->        
                
               <asp:modalpopupextender 
                ID="ModalPopupExtender1"
                BehaviorID="behavior"
                runat="server"
                TargetControlID="lnkModal"
                PopupControlID="PanelDetalhes"
                BackgroundCssClass="modalBackground"
                DropShadow="True"
                DynamicServicePath="" 
                Enabled="True"
        
        />

        <asp:HyperLink ID="lnkModal" runat="server" Visible="true" Text=""  />

 <asp:Panel ID="PanelDetalhes" runat="server" CssClass="modalPopup" style="display:none">
          <div id="ContainerDetalhes" style="width:800px">
          <div id="MenuDetalhes" style="background-color:White;padding-left:5px;padding-bottom:5px;height:40px">
            <div style="width:650px; float:left; height: 32px;">
                <h2 style="height: 35px"><asp:Label ID="Label2" runat="server" Text="Label">Detalhes do Registro</asp:Label></h2>
            </div>
            <div style="float:left;width:130px;text-align:right; padding-top:10px;">
                <asp:ImageButton ID="ImageButton1" runat="server" 
                    ImageUrl="../../images/close24.png" OnclientClick="$find('behavior').hide(); return false;" ToolTip="Fechar Janela" ImageAlign="AbsMiddle" />
                &nbsp;<asp:LinkButton ID="LinkButtonFechar" runat="server" Visible="true" Text="Fechar" OnclientClick="$find('behavior').hide(); return false;"   />
            </div>
          </div>
           <div id="corpoDetalhes" style="border:1px solid #cccccc;height:450px;padding-top:10px;background-color:#f7f7f7">
            
               <table class="Largura100Porcento">
                   <tr>
                       <td class="Largura150">
                           <asp:Label ID="Label9" runat="server" style="font-weight: 700" 
                               Text="Pró-Reitoria:"></asp:Label>
                       </td>
                       <td>
                           <asp:Label ID="LabelDetalhe4" runat="server"></asp:Label>
                       </td>
                   </tr>
                   <tr>
                       <td class="Largura150">
                           <asp:Label ID="Label6" runat="server" style="font-weight: 700" Text="Bolsa:"></asp:Label>
                       </td>
                       <td>
                           <asp:Label ID="LabelDetalhe1" runat="server"></asp:Label>
                       </td>
                   </tr>
                   <tr>
                       <td class="Largura150">
                           <asp:Label ID="Label7" runat="server" style="font-weight: 700" 
                               Text="Descrição:"></asp:Label>
                       </td>
                       <td>
                           <asp:Label ID="LabelDetalhe2" runat="server"></asp:Label>
                       </td>
                   </tr>
                   <tr>
                       <td class="Largura150">
                           <asp:Label ID="Label8" runat="server" style="font-weight: 700" Text="Tipo:"></asp:Label>
                       </td>
                       <td>
                           <asp:Label ID="LabelDetalhe3" runat="server"></asp:Label>
                       </td>
                   </tr>
               </table>
            
           </div> 


          </div>      
</asp:Panel>
                <!-- FIM MODAL POPUP -->
                <!-- PAINÉL DE HOMOLOGAÇÃO -->
                <asp:Panel ID="PnlHomologacao" runat="server" Visible="false">
                    <div style="text-align:center">
                        <asp:Button ID="btAceitar" runat="server" Text="Aceitar" 
                            onclick="btAceitar_Click" />
                        <asp:Button ID="btRecusar" runat="server" Text="Recusar" 
                            onclick="btRecusar_Click" />
                        <asp:Panel ID="PnlParecer" runat="server" Visible="false">
                            <asp:TextBox ID="txtParecer" runat="server" TextMode="MultiLine" Rows="3" 
                                Width="320px"></asp:TextBox>
                            <br />
                            <asp:Button ID="btSalvar" runat="server" Text="Salvar Parecer de Recusa" 
                                onclick="btSalvar_Click" />
                        </asp:Panel>
                    </div>
                </asp:Panel>
                <!-- FIM PANEL HOMOLOGAÇÃO -->
            </div>
    <asp:HiddenField ID="hdProcessoSeletivoUID" runat="server" />
    

     <asp:modalpopupextender 
                ID="mdlPanelRecusa"
                runat="server"
                TargetControlID="lnkModalRecusa"
                PopupControlID="pnlParecerRecusaPagamento"
                BackgroundCssClass="modalBackground"
                DynamicServicePath="" 
                Enabled="True"
                CancelControlID="btCancelarParecer"
        />

        <asp:HyperLink ID="lnkModalRecusa" runat="server" Visible="true" Text=""  />

    <asp:Panel ID="pnlParecerRecusaPagamento" runat="server" Style="">
        <asp:HiddenField ID="hdbolsistaUID" runat="server" />
        <div style="width:500px; height:300px; background:#FFF">
            <h1>Parece de Pagamento Não Autorizado</h1>
            <p style="padding:30px;">
            Digite aqui o parecer:
            <br />           
                <asp:TextBox ID="txtParecerRecusa" runat="server" Height="100px" 
                    TextMode="MultiLine" Width="380px"></asp:TextBox>
            </p>
            <p style="text-align:center">
                <asp:Button ID="btSalvarParecer" runat="server" Text="Salvar" 
                    onclick="salvarParecer_Click" />
                <asp:Button ID="btCancelarParecer" runat="server" Text="Cancelar" />
            </p>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlFinalizarPagamento" runat="server" Visible="false" HorizontalAlign="Center">
        <asp:Button ID="btGerarFolhaSuplementar" runat="server" 
            Text="Gerar Folhas Suplementares" onclick="btGerarFolhaSuplementar_Click" CausesValidation="false" />
    </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
