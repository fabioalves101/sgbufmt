﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true" CodeBehind="FrmListarVoluntarios.aspx.cs" Inherits="ufmt.ssbic.View.webapp.bolsista.FrmListarVoluntarios" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<h1>Gerenciar Voluntários</h1>

    <asp:Panel ID="pnlFiltro" runat="server">
    <p>
    RGA: <asp:TextBox ID="txtRga" runat="server"></asp:TextBox>

        &nbsp;Curso: 
        <asp:DropDownList ID="ddlCursos" runat="server" DataSourceID="odsCursos" 
            DataTextField="nome" DataValueField="cursoUID" AppendDataBoundItems="true">

            <asp:ListItem Value="0">Mostrar Todos</asp:ListItem>

        </asp:DropDownList>
    </p>
    <p>
        Situação: <asp:DropDownList ID="ddlSituacao" runat="server">
            <asp:ListItem Value="true">Ativos</asp:ListItem>
            <asp:ListItem Value="false">Inativos</asp:ListItem>
        </asp:DropDownList>
         
        Período de Atuação:
        <asp:DropDownList ID="ddlPeriodoAtuacao" runat="server">
            <asp:ListItem Value="0">Todos</asp:ListItem>
            <asp:ListItem Value="1">1º Semestre</asp:ListItem>
            <asp:ListItem Value="2">2º Semestre</asp:ListItem>
            <asp:ListItem Value="3">Anual</asp:ListItem>
        </asp:DropDownList>

         <asp:Button ID="btFiltrar" runat="server" Text="Buscar" 
            onclick="btFiltrar_Click" />
        <asp:ObjectDataSource ID="odsCursos" runat="server" 
            SelectMethod="GetCursosProcessoSeletivo" 
            TypeName="ufmt.ssbic.Business.CursoProcessoSeletivoBO">
            <SelectParameters>
                <asp:QueryStringParameter Name="processoSeletivoUID" 
                    QueryStringField="processoSeletivoUID" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </p>
    </asp:Panel>
    <asp:Panel ID="pnlGridview" runat="server">
    
    <asp:GridView ID="grdVoluntarios" runat="server" AutoGenerateColumns="False" 
        DataSourceID="odsVoluntarios" Width="100%">
        <Columns>
            <asp:BoundField DataField="nomeAluno" HeaderText="Nome" 
                SortExpression="nomeAluno" />
            <asp:BoundField DataField="Matricula" HeaderText="Matrícula" 
                SortExpression="Matricula" />
            <asp:BoundField DataField="Curso" HeaderText="Curso" SortExpression="Curso" />
            <asp:BoundField DataField="nomeDisciplina" HeaderText="Disciplina" 
                SortExpression="nomeDisciplina" />
            
            <asp:TemplateField HeaderText="Período" SortExpression="periodoAtuacao">               
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# GetPeriodoAtuacao((string)Eval("periodoAtuacao")) %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Editar">
                <ItemTemplate>
                    <asp:HyperLink NavigateUrl='<%# LinkEditarVoluntario((int)Eval("monitorUID")) %>' ID="hpEditar" runat="server">Editar</asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:ObjectDataSource ID="odsVoluntarios" runat="server" 
        SelectMethod="GetVwMonitoresPorRemuneracao" 
        TypeName="ufmt.ssbic.Business.MonitorBO">
        <SelectParameters>
            <asp:Parameter DefaultValue="false" Name="remunerado" Type="Boolean" />
            <asp:ControlParameter ControlID="txtRga" DefaultValue="" Name="rga" 
                PropertyName="Text" Type="String" />
            <asp:QueryStringParameter Name="processoSeletivoUID" 
                QueryStringField="processoSeletivoUID" Type="Int32" />
            <asp:ControlParameter ControlID="ddlCursos" Name="cursoUID" 
                PropertyName="SelectedValue" Type="Int64" />
            <asp:ControlParameter ControlID="ddlSituacao" DefaultValue="true" Name="ativo" 
                PropertyName="SelectedValue" Type="Boolean" />
            <asp:ControlParameter ControlID="ddlPeriodoAtuacao" DefaultValue="0" 
                Name="periodoAtuacao" PropertyName="SelectedValue" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>

    </asp:Panel>
</asp:Content>
