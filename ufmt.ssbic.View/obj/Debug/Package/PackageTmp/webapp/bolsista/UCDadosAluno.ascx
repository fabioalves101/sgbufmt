﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCDadosAluno.ascx.cs" Inherits="ufmt.ssbic.View.webapp.bolsista.UCDadosAluno" %>

<div id="divDetalheAluno" runat="server" visible="true">
    <div style="background-color:#cccccc;font-weight:bold;padding:10px;width:98%">
        DADOS DO ALUNO&nbsp;        
    </div>



       <table class="Largura100Porcento">
        <tr>
            <td class="Largura150">
                <asp:Label ID="Label9" runat="server" style="font-weight: 700" 
                    Text="Nome do Aluno:"></asp:Label>
            </td>
            <td>
                &nbsp;
                <asp:Label ID="lblNomeAluno" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="Largura150">
                <asp:Label ID="Label6" runat="server" style="font-weight: 700" 
                    Text="Matrícula:"></asp:Label>
            </td>
            <td>
                &nbsp;
                <asp:Label ID="lblMatricula" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="Largura150">
                <asp:Label ID="Label7" runat="server" style="font-weight: 700" Text="Curso:"></asp:Label>
            </td>
            <td>
                &nbsp;
                <asp:Label ID="lblCurso" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="Largura150">
                <asp:Label ID="Label8" runat="server" style="font-weight: 700" Text="Campus:"></asp:Label>
            </td>
            <td>
                &nbsp;
                <asp:Label ID="lblCampus" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="Largura150">
                <asp:Label ID="Label10" runat="server" style="font-weight: 700" Text="CPF:"></asp:Label>
            </td>
            <td>
                &nbsp;
                <asp:Label ID="lblCpf" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="Largura150">
                <asp:Label ID="Label11" runat="server" style="font-weight: 700" Text="E-mail:"></asp:Label>
                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                    ControlToValidate="txtEmailAluno" ErrorMessage="E-mail do aluno" 
                    ForeColor="Red">*</asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:TextBox ID="txtEmailAluno" runat="server" Width="400px"></asp:TextBox>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="Largura150">
                <asp:Label ID="Label15" runat="server" style="font-weight: 700" Text="Banco:"></asp:Label>
                
            </td>
            <td>
                &nbsp;<asp:DropDownList ID="ddlBanco" runat="server" DataSourceID="odsBancos" 
                    DataTextField="banco" DataValueField="bancoUID" AppendDataBoundItems="true">
                    <asp:ListItem Selected="True" Value="0">Selecione</asp:ListItem>
                </asp:DropDownList>

                <asp:ObjectDataSource ID="odsBancos" runat="server" SelectMethod="GetBancos" 
                    TypeName="ufmt.ssbic.Business.BancoBO"></asp:ObjectDataSource>

                &nbsp;</td>
        </tr>
        <tr>
            <td class="Largura150">
                <asp:Label ID="Label18" runat="server" style="font-weight: 700" Text="Agência:"></asp:Label>
                
            </td>
            <td>
                <asp:TextBox ID="txtAgencia" runat="server" Width="95px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="Largura150">
                <asp:Label ID="Label17" runat="server" style="font-weight: 700" 
                    Text="Número da Conta:"></asp:Label>
                
            </td>
            <td>
                <asp:TextBox ID="txtContaCorrente" runat="server" Width="297px"></asp:TextBox>
            </td>
        </tr>
    </table>

    <div id="divAcoesEtapa1" style="text-align:center;padding:10px" runat="server" visible="true">
       <asp:Button ID="btnSalvarDadosAluno" runat="server" Text="Salvar Dados do Aluno" 
            onclick="btnSalvarDadosAluno_Click" />
   </div>
</div>