﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true" CodeBehind="FrmEditarBolsista.aspx.cs" Inherits="ufmt.ssbic.View.webapp.bolsista.FrmEditarBolsita" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>&nbsp;Editar Bolsista &raquo;
        <asp:Label ID="lblTitulo" runat="server"></asp:Label>
        <asp:HiddenField ID="hddCodigoRegistro" runat="server" />
    </h1>

        <div id="containerMensagens">
                <asp:Panel ID="PanelMsgResultado" runat="server" Visible="false">
                    <asp:Label ID="lblMsgResultado" runat="server"></asp:Label>
                </asp:Panel>
        </div>

<asp:Panel ID="PanelFormulario" runat="server">

<div class="containerFormulario" style="text-align:center">
    <asp:Label ID="Label1" runat="server" Text="Cancelar Bolsa"></asp:Label>
    &nbsp;|
    <asp:Label ID="Label2" runat="server" Text="Editar dados financeiros"></asp:Label>
    &nbsp;|
    <asp:Label ID="Label3" runat="server" Text="Alterar Coordenador"></asp:Label>
    &nbsp;</div>
    </asp:Panel>
</asp:Content>
