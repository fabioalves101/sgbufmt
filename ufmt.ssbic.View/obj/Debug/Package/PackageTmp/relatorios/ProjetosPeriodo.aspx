﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="ProjetosPeriodo.aspx.cs" Inherits="ufmt.ssbic.View.relatorios.ProjetosPeriodo" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<h1>Projetos por período</h1>
    <p>
        Selecione o período:</p>
    <p>
        Início: 
        <asp:TextBox ID="txtInicio" runat="server" Width="80px"></asp:TextBox>
        
        <asp:CalendarExtender
            ID="CalendarExtender1" runat="server" TodaysDateFormat="dd/MM/yyyy" 
            TargetControlID="txtInicio" DefaultView="Years" Format="dd/MM/yyyy">
        </asp:CalendarExtender>
        &nbsp;Fim: 
        <asp:TextBox ID="txtFim" runat="server" Width="80px"></asp:TextBox><asp:CalendarExtender
            ID="CalendarExtender2" runat="server" TargetControlID="txtFim" 
            DefaultView="Years" Format="dd/MM/yyyy">
        </asp:CalendarExtender>
        <asp:Button ID="btFiltrar" runat="server" Text="Filtrar" 
            onclick="btFiltrar_Click" />
    </p>

</asp:Content>
