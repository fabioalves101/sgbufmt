﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ArvoreUnidades.ascx.cs" Inherits="ufmt.ssbic.View.ArvoreUnidades" %>
<!-- link href="css/responsaveis.css" rel="stylesheet" type="text/css" /-->
<div ID="divBusca"  runat="server">
    <span style="padding-right:10px; font-weight:bolder">Busca:</span>
    <asp:TextBox ID="txtBusca" runat="server" CssClass="Text Field300"></asp:TextBox>
    <span style="font-weight:bolder; margin-top:5px;">Campus:<asp:DropDownList ID="dropCampus" runat="server"></asp:DropDownList></span>
    <span style="padding-left:5px"><asp:Button ID="btnBuscar" runat="server" Text="Buscar" BorderStyle="None" 
        onclick="btnBuscar_Click" CausesValidation="False"/></span>
    <br />
    <span>
    <asp:Label ID="lblaviso" runat="server" Text="Por favor escolha um Campus!" 
        Font-Bold="True" Font-Size="Smaller" ForeColor="#CC0000" Visible="False"></asp:Label></span> 
</div>
<asp:Panel ID="divArvore" ClientIDMode="Static" style="margin-top:20px" runat="server">
<span style="font-weight:bolder; margin-right:20px">Unidades:</span>
    <asp:Button ID="btnExpandirArvore" runat="server" BackColor="White" 
        BorderStyle="None" CausesValidation="False" CssClass="Field" 
        EnableTheming="True" onclick="btnExpandirArvore_Click" 
        Text="[+] Expandir Árvore" UseSubmitBehavior="False" />
    <asp:Button ID="btnRecolherArvore" runat="server" BackColor="White" 
        BorderStyle="None" CausesValidation="False" CssClass="Field" 
        EnableTheming="True" onclick="btnRecolherArvore_Click" 
        Text="[-] Recolher Árvore" UseSubmitBehavior="False" />
    <asp:TreeView ID="treeUnidades" runat="server" ImageSet="Arrows" 
        onselectednodechanged="treeUnidades_SelectedNodeChanged" NodeWrap="True" 
        NoExpandImageUrl="~/images/details_open.png" ViewStateMode="Enabled">
        <HoverNodeStyle Font-Underline="True" ForeColor="#5555DD" Font-Bold="True" />
        <NodeStyle Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" 
            HorizontalPadding="5px" NodeSpacing="0px" VerticalPadding="0px" />
        <ParentNodeStyle Font-Bold="true" />
        <SelectedNodeStyle Font-Underline="True" ForeColor="#5555DD" 
            HorizontalPadding="0px" VerticalPadding="0px" Font-Bold="True" 
            ImageUrl="~/images/next.gif" />
    </asp:TreeView>
</asp:Panel>
<div style="margin:10px">
    <asp:HiddenField ID="hfUnidadeUID" runat="server" />
    Unidade Selecionada:
    <asp:Label ID="lblUnidadeSelecionada" runat="server" style="font-weight:bolder"></asp:Label>
</div>
