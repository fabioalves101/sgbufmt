﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.sig.entity;

namespace ufmt.ssbic.View
{
    public partial class Default : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                long usuarioUID = long.Parse(Session["usuarioUID"].ToString());
                AutenticacaoBO autenticacaoBO = new AutenticacaoBO();

                long permissaoProjetos = long.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["PermissaoProjetos"].ToString());

                if (!autenticacaoBO.VerificaPermissaoEspecifica(long.Parse(Session["usuarioUID"].ToString()), permissaoProjetos))
                {
                    Response.Redirect("~/Login.aspx");
                }
                else
                {
                    Usuario usuario = autenticacaoBO.GetUsuario(usuarioUID);

                    usuarioLogado.Text = usuario.Pessoa.Nome;

                }
                
            } catch (Exception ex) {
                Response.Redirect("~/Login.aspx");
            }
        }
    }
}