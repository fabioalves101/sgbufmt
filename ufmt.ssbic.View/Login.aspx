﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="ufmt.ssbic.View.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:Panel ID="panelAviso" runat="server" Visible="false">    
        <div class="pnlAviso">
                    <img alt="" src="/media/images/alert.png" border="0" />&nbsp;&nbsp;
                    <asp:Label ID="lblAlerta" runat="server" Text="" Visible="true"></asp:Label>
        </div>
</asp:Panel>

<h1>
<asp:Label ID="lblProReitoria" runat="server" Text="Label"></asp:Label>
</h1>
    

<div class="tela_login">
    

<div class="intro">

<h3>Autenticação de Usuário</h3>
<p class="textintro">
    <asp:Label ID="lblIntro" runat="server"></asp:Label>
&nbsp;Esta autenticação de usuário utiliza o Login unificado dos Sistemas da UFMT.
Caso você ainda não tenha utilizado nenhum sistema que faça parte do login unificado e ainda não tem nenhum usuário criado,
<a href="FrmNovoUsuario.aspx">clique aqui para se cadastrar</a>.
</p></div>

<div class="login_box">
    <asp:Panel ID="pnlLogin" runat="server">
    
<p>
<label>CPF<br />
    <asp:TextBox ID="login" CssClass="login" runat="server"></asp:TextBox>
</label>  	
</p>
<p>
<label>Senha<br />
<asp:TextBox ID="senha" CssClass="senha" runat="server" TextMode="Password"></asp:TextBox>
</label>
</p>
<div>
    <div style="float:left">
        <a href="FrmReiniciarSenha.aspx">Recuperar Senha!</a>
    </div>  
    <div style="float:right">
        <asp:ImageButton ID="btEntrar" runat="server" 
            ImageUrl="~/media/images/bt_acessar.png" onclick="btEntrar_Click" />        
    </div>
</div>
</asp:Panel>
<asp:panel runat="server" ID="pnlNovaSenha" Visible="False">
<p>
<label>
Senha Atual:<br />
<asp:TextBox runat="server" ID="senhaAtual" CssClass="senha" TextMode="Password"></asp:TextBox>
</label>
</p>
<p>
<label>
Nova Senha:<br />
<asp:TextBox runat="server" ID="novaSenha" CssClass="senha" TextMode="Password"></asp:TextBox>
</label>
</p>    
<div style="float:right">
            <asp:Button runat="server" Text="Alterar" 
                ID="btAlterar" onclick="btAlterar_Click" Width="56px"></asp:Button>
            <asp:HiddenField ID="hdUser" runat="server" />
</div>        
</asp:panel>


</div>

</div>


</asp:Content>
