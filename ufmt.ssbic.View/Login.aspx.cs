﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.sig.entity;
using ufmt.ssbic.View.session;

namespace ufmt.ssbic.View
{
    public partial class Login : VerificaSession
    {

        string TextoIntro = "Bem vindo ao SGB - Sistema de Gerenciamento de Bolsas da UFMT.";
        string[] permissoesMonitoria = {"ADMINISTRADOR_BOLSAS_MONITORIA", "COORDENADOR_BOLSAS_MONITORIA" };
        string[] permissoesCare = { "ADMINISTRADOR_BOLSAS_CARE", "COORDENADOR_BOLSAS_CARE" };
        string[] permissoesMobilidade = { "ADMINISTRADOR_BOLSAS_MOBILIDADE", "COORDENADOR_BOLSAS_MOBILIDADE" };
        //string[] permissoesCapacitacao = { "ADMINISTRADOR_BOLSAS_CAPACITACAO", "COORDENADOR_BOLSAS_CAPACITACAO" };

        string[] permissoesProeg = { "ADMINISTRADOR_BOLSAS_PROEG", "COORDENADOR_BOLSAS_PROEG" };  
        string[] permissoesProcev = { "ADMINISTRADOR_BOLSAS_PROCEV", "COORDENADOR_BOLSAS_PROCEV" };
        string[] permissoesPropeq = { "ADMINISTRADOR_BOLSAS_PROPEQ", "COORDENADOR_BOLSAS_PROPEQ" };
        string[] permissoesFinanceiro = { "FINANCEIRO_BOLSAS" };
        string[] permissoesCoordenacao = { "COORDENADOR_BOLSAS" };

        string[] permissoesProgramas = { "ADMINISTRADOR_BOLSAS_MONITORIA", "COORDENADOR_BOLSAS_MONITORIA", 
                                        "ADMINISTRADOR_BOLSAS_CARE", "COORDENADOR_BOLSAS_CARE",
                                         "ADMINISTRADOR_BOLSAS_PROEG", "COORDENADOR_BOLSAS_PROEG",
                                        "ADMINISTRADOR_BOLSAS_PROCEV", "COORDENADOR_BOLSAS_PROCEV",
                                        "ADMINISTRADOR_BOLSAS_PROPEQ", "COORDENADOR_BOLSAS_PROPEQ",
                                        "ADMINISTRADOR_BOLSAS_CAPACITACAO", "COORDENADOR_BOLSAS_CAPACITACAO",
                                        "ADMINISTRADOR_BOLSAS_MOBILIDADE", "COORDENADOR_BOLSAS_MOBILIDADE",
                                        "ADMINISTRADOR_BOLSAS_TUTORIA", "COORDENADOR_BOLSAS_TUTORIA",
                                         "FINANCEIRO_BOLSAS", "COORDENADOR_BOLSAS"};
        /*PROEG: 85396265191
            PROCEV: 26740936846
            PROPEQ: 00484544195 --12345
            Financeiro: 81416795898
            Coordenador: 02000791158  
         * **/

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                Image ImageTopo = (Image)Page.Master.FindControl("ImageTopo");
                BolsaBO objBO = new BolsaBO();
                lblProReitoria.Text = "Login";
                //lblProReitoria.Text = objBO.GetPrograma(int.Parse(Session["programaUID"].ToString()));
                lblIntro.Text = TextoIntro;
                ImageTopo.ImageUrl = "~/images/bolsa_topo.png";
            }
        }

        protected void btEntrar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                String cpf = login.Text;
                String passwd = senha.Text;

                //REALIZA O LOGIN DO USUÁRIO
                AutenticacaoBO autenticacaoBO = new AutenticacaoBO();
                Usuario usuario = autenticacaoBO.Login(cpf, passwd);

                //GRAVA ID DO USUÁRIO EM SESSÃO
                Session["usuarioUID"] = usuario.UsuarioUID;

                //SOLICITA PERMISSÕES DO USUÁRIO LOGADO
                List<PermissaoUsuario> permissoesUsuario = autenticacaoBO.VerificaPermissoes(usuario.UsuarioUID);
                PermissaoUsuario permissaoUsuario = permissoesUsuario.FirstOrDefault<PermissaoUsuario>();
                
                if (permissaoUsuario != null)
                {
                    if (usuario.DataProximaTrocaSenha <= DateTime.Now)
                    {
                        pnlLogin.Visible = false;
                        pnlNovaSenha.Visible = true;
                        btEntrar.Enabled = false;
                    }
                    else // Verifica permissão por Pró Reitoria
                    {

                        VerificaPermissaoAcesso(permissoesUsuario, permissoesProgramas);

                        /*
                         if (lblProReitoria.Text == "PROEG")
                             VerificaPermissaoAcesso(permissoesUsuario, permissoesProeg, 1);

                        if (lblProReitoria.Text == "PROCEV")
                            VerificaPermissaoAcesso(permissoesUsuario, permissoesProcev, 2);

                        if (lblProReitoria.Text == "PROPEQ")
                            VerificaPermissaoAcesso(permissoesUsuario, permissoesPropeq, 3);

                        if(lblProReitoria.Text == "Financeiro")
                            VerificaPermissaoAcesso(permissoesUsuario, permissoesFinanceiro, 5);

                        if (lblProReitoria.Text == "Coordenador")
                            VerificaPermissaoAcesso(permissoesUsuario, permissoesCoordenacao, 6);
                        
                        if (lblProReitoria.Text == "Monitoria")
                            VerificaPermissaoAcesso(permissoesUsuario, permissoesMonitoria, 7);

                        if (lblProReitoria.Text == "CARE")
                            VerificaPermissaoAcesso(permissoesUsuario, permissoesCare, 8);
                         * 
                         */
                    }
                }// Não tem permissão para esse aplicação 14
                else
                {
                    throw new Exception("O usuário não possui permissão para essa aplicação.");
                }
                
            }
            catch (Exception ex)
            {
                panelAviso.Visible = true;
                lblAlerta.Visible = true;
                lblAlerta.Text = ex.Message;
            }

        }

        private void VerificaPermissaoAcesso(List<PermissaoUsuario> permissoesUsuario, string[] permissaoPrograma)
        {
            foreach (string permissao in permissaoPrograma)
            {
                foreach (PermissaoUsuario permissaoUsuario in permissoesUsuario)
                {
                    if (permissaoUsuario.Permissao.Nome == permissao)
                    {
                        //Session["programaUID"] = programaUID.ToString();
                        Response.Redirect("FrmSelecionarPerfil.aspx");
                    }
                    else if (permissaoUsuario.Permissao.Nome.Contains("PROJETOS"))
                    {
                        Response.Redirect("FrmSelecionarPerfil.aspx");
                    }
                }
            }
            throw new Exception("O usuário não possui permissão para essa aplicação.");
        }

        protected void btAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                AutenticacaoBO autenticacaoBO = new AutenticacaoBO();
                long usuarioUID = long.Parse(Session["usuarioUID"].ToString());
                Usuario usuario = autenticacaoBO.GetUsuario(usuarioUID);
                if (autenticacaoBO.TrocaDeSenha(usuario, senhaAtual.Text, novaSenha.Text))
                {
                    Response.Redirect("Login.aspx");
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                panelAviso.Visible = true;
                lblAlerta.Text = "Verifique se a senha atual foi digitada corretamente";
            }
        }
    }
}