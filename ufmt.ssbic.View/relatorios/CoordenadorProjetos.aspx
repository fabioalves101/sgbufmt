﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="CoordenadorProjetos.aspx.cs" Inherits="ufmt.ssbic.View.relatorios.CoordenadorProjetos" %>
<h1>Projetos por Coordenador</h1>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        DataSourceID="ObjectDataSource1"
        AllowPaging="True" Width="100%"
        CssClass="mGrid"
        PagerStyle-CssClass="pgr"
        AlternatingRowStyle-CssClass="alt" DataKeyNames="pessoaMembroUID" onselectedindexchanged="gdrListaServidores_SelectedIndexChanged"
        >
        <Columns>
            <asp:TemplateField HeaderText="Selecionar">
                <ItemTemplate>
                   <asp:LinkButton CssClass="minibutton" ID="LinkButton1" runat="server" CausesValidation="False" 
                                CommandName="Select">
                            <span>
                                <img src="../media/images/select.png" alt="" />
                                Selecionar
                            </span>
                            </asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="registro" HeaderText="SIAPE" 
                SortExpression="registro" />
            <asp:BoundField DataField="nome" HeaderText="Nome" SortExpression="nome" />
            <asp:BoundField DataField="instituicao" HeaderText="Instituição" 
                SortExpression="instituicao" />
            <asp:BoundField DataField="cpf" HeaderText="CPF" SortExpression="cpf" />
        </Columns>
    </asp:GridView>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
        SelectMethod="GetMembros" TypeName="ufmt.ssbic.Business.MembroBO">
        <SelectParameters>
            <asp:Parameter DefaultValue="1" Name="funcaoProjetoUID" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
