﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Stimulsoft.Report;
using Stimulsoft.Report.Web;

namespace ufmt.ssbic.View.relatorios
{
    public partial class ProjetosUnidadeEspecifica : System.Web.UI.Page
    {
        public long unidadeUID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(ArvoreUnidades1.GetUnidadeUID != null)
                unidadeUID = ArvoreUnidades1.GetUnidadeUID.Value;
        }
                
        protected void btGerar_Click(object sender, EventArgs e)
        {
            if (unidadeUID != 0)
            {
                string descricao = String.Empty;
                GerarRelatorio(string.Empty, "ProjetosPorUnidadeEspecifica", unidadeUID);
            }
        }

        public void GerarRelatorio(string basedados, string nomeRelatorio, long unidadeUID)
        {
            StiReport report = new StiReport();
            report.Load(AppDomain.CurrentDomain.BaseDirectory + "reports\\" + nomeRelatorio + ".mrt");
            report.Compile();

            report.CompiledReport.DataSources["Unidade"].Parameters["@UnidadeUID"].ParameterValue = unidadeUID;
            report.CompiledReport.DataSources["ProjetosUnidadeEspecifica"].Parameters["@UnidadeLotacaoUID"].ParameterValue = unidadeUID;

            StiReportResponse.ResponseAsPdf(this, report, true);
        }



    }
}