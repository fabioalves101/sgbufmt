﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;
using Stimulsoft.Report;
using Stimulsoft.Report.Web;

namespace ufmt.ssbic.View.relatorios
{
    public partial class ProjetosProfessor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btBuscar_Click(object sender, EventArgs e)
        {
            grdProfessor.DataBind();
        }

        protected void grdProfessor_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridView gridview = (GridView)sender;
            int pessoaMembroUID = int.Parse(gridview.SelectedValue.ToString());

            string descricao = String.Empty;
            GerarRelatorio(string.Empty, "ProjetosProfessor", pessoaMembroUID);
        }

        public void GerarRelatorio(string basedados, string nomeRelatorio, int pessoaMembroUID)
        {
            StiReport report = new StiReport();
            report.Load(AppDomain.CurrentDomain.BaseDirectory + "reports\\" + nomeRelatorio + ".mrt");
            report.Compile();

            report.CompiledReport.DataSources["PessoaMembro"].Parameters["@PessoaMembroUID"].ParameterValue = pessoaMembroUID;
            report.CompiledReport.DataSources["ProjetosProfessor"].Parameters["@PessoaMembroUID"].ParameterValue = pessoaMembroUID;

            StiReportResponse.ResponseAsPdf(this, report, true);
        }
    }
}