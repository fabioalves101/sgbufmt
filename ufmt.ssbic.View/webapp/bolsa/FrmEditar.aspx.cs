﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;
using ufmt.ssbic.View.session;

namespace ufmt.ssbic.View.webapp.bolsa.bolsa
{
    public partial class FrmEditar : VerificaSession
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                BolsaBO objBO = new BolsaBO();

                string programaUID = Session["programaUID"].ToString();
                string area = Request.QueryString["area"].ToString();
                lblTitulo.Text = area;
                string proreitoria = objBO.GetPrograma(int.Parse(programaUID));
                string registroUID = Request.QueryString["registroUID"];
                Bolsa bolsa = objBO.GetBolsa(int.Parse(registroUID));
                txtDescricao.Text = bolsa.descricao;
                txtDetalhes.Text = bolsa.detalhe;
                hddCodigoRegistro.Value = bolsa.bolsaUID.ToString();

            }

        }


        protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                BolsaBO objBO = new BolsaBO();
                Bolsa bolsa = new Bolsa();
                bolsa.descricao = txtDescricao.Text;
                bolsa.detalhe = txtDetalhes.Text;
                bolsa.bolsaUID = int.Parse(hddCodigoRegistro.Value);
                bolsa.proReitoriaUID = int.Parse(Session["programaUID"].ToString());
                objBO.Salvar(bolsa, 0);
                lblMsgResultado.Text = "Registro atualizado com sucesso.";
                PanelMsgResultado.CssClass = "success";
                PanelFormulario.Visible = false;
                btnSalvar.Visible = false;

            }
            catch (Exception err)
            {

                lblMsgResultado.Text = "Não foi possível efetuar a operação.  Por favor, entre em contato com o administrador do sistema.<br />" + err.Message.ToString();
                PanelMsgResultado.CssClass = "error";
            }

            PanelMsgResultado.Visible = true;
        }

        protected void btnVoltar_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("FrmListar.aspx");
        }
    }
}