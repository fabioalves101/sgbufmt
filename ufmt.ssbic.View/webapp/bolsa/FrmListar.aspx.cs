﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;
using ufmt.ssbic.View.session;

namespace ufmt.ssbic.View.webapp.bolsa
{
    public partial class FrmListar : VerificaSession
    {
        public int? ProReitoriaUID { get; set; }
        public String Criterio { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            //
            if (!IsPostBack)
            {
               
                BolsaBO objBO = new BolsaBO();

                string programaUID = Session["programaUID"].ToString();
                string proreitoria = objBO.GetPrograma(int.Parse(programaUID));
                string area = "Bolsa";
                lblResultadoConsulta.Text = "<b>Mostrando todos</b>";
                PreecherGridView(Session["programaUID"].ToString(), txtCriterio.Text, false);
                lnkNovoRegistro.NavigateUrl = "FrmCadastrar.aspx?area=" + area;
                Label1.Text = "Bolsas &raquo;" + proreitoria;
            }
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {


            GridView1.PageIndex = e.NewPageIndex;
            PreecherGridView(Session["programaUID"].ToString(), txtCriterio.Text, false);

        }

        protected void PreecherGridView(String proreitoriaUID, String criterio, bool visible)
        {

            PanelMsgResultado.Visible = visible;
            if (!String.IsNullOrEmpty(proreitoriaUID))
                this.ProReitoriaUID = int.Parse(proreitoriaUID);
            else
                this.ProReitoriaUID = null;

            BolsaBO objBO = new BolsaBO();
            List<Bolsa> registros = null;

            //Consulta
            if (!String.IsNullOrEmpty(criterio))
            {
                registros = objBO.FindRegistros(this.ProReitoriaUID, criterio);
            }
             //Listagem padrão
            else {

                registros = objBO.GetRegistros(this.ProReitoriaUID);
            }
            

            GridView1.DataSource = registros;
            GridView1.DataBind();
            lblTotalRegistros.Text = registros.Count.ToString() + " registro(s) disponível(is)";

            if (registros.Count > 0)
            {

                ImageButton btnPrev = (ImageButton)GridView1.BottomPagerRow.FindControl("cmdPrev");
                if (GridView1.PageIndex == 0)
                    btnPrev.Enabled = false;
                else
                    btnPrev.Enabled = true;

                divExcluir.Visible = true;
                
            }
            else
            {

                divExcluir.Visible = false;

            }

            

        }

        protected void cmdIr2_Click(object sender, ImageClickEventArgs e)
        {
            TextBox txt = (TextBox)GridView1.BottomPagerRow.FindControl("txtPagina");
            GridView1.PageIndex = (Convert.ToInt32(txt.Text) - 1);
            PreecherGridView(Session["programaUID"].ToString(), txtCriterio.Text, false);

        }

        protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                /*
                HyperLink lnkTituloResolucao = (HyperLink)e.Row.FindControl("lnkTituloResolucao");
                HyperLink lnkAtualizarRegistro = (HyperLink)e.Row.FindControl("lnkAtualizarRegistro");
                Label lblAssunto = (Label)e.Row.FindControl("lblAssunto");
               
                lnkAtualizarRegistro.NavigateUrl = "FrmEditar.aspx?registroUID=" + (int?)DataBinder.Eval(e.Row.DataItem, "bolsaUID") + "&proreitoriaUID=" + (int?)DataBinder.Eval(e.Row.DataItem, "proreitoriaUID");
                */

            }



        }

        protected void lnkExcluirSelecionados_Click(object sender, EventArgs e)
        {
            bool resultado = false;
            BolsaBO objBO = new BolsaBO();

            try
            {

                for (int i = 0; i <= GridView1.Rows.Count - 1; i++)
                {
                    CheckBox chk = new CheckBox();
                    chk = (CheckBox)GridView1.Rows[i].FindControl("chkRegistro");
                    Label lblID = (Label)GridView1.Rows[i].FindControl("lblID");

                    if (chk.Checked)
                    {
                        resultado = objBO.Excluir(int.Parse(lblID.Text));
                    }

                }

                lblMsgResultado.Text = "Registro(s) excluído(s) com sucesso.";
                PanelMsgResultado.CssClass = "success";

            }
            catch (Exception err)
            {

                lblMsgResultado.Text = "Não foi possível excluir o(s) registro(s) selecionado(s).  Por favor, entre em contato com o administrador do sistema.<br />" + err.Message.ToString();
                PanelMsgResultado.CssClass = "error";

            } 

            PanelMsgResultado.Visible = true;
            PreecherGridView(Session["programaUID"].ToString(), txtCriterio.Text, true);
        }


        protected void lnkConsultar_Click(object sender, EventArgs e)
        {

            PreecherGridView(Session["programaUID"].ToString(), txtCriterio.Text, false);

        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "detalhes") {

                BolsaBO objBO = new BolsaBO();
                Bolsa bolsa = objBO.GetBolsa(int.Parse(e.CommandArgument.ToString()));
                LabelDetalhe1.Text = bolsa.descricao;
                LabelDetalhe2.Text = bolsa.detalhe;
                ModalPopupExtender1.Show();
                PanelMsgResultado.Visible = false;
            }

            

        }

        protected void lnkMostrarTodos_Click(object sender, EventArgs e)
        {
            PreecherGridView(Session["programaUID"].ToString(), null, false);
            txtCriterio.Text = String.Empty;
        }

        //protected void ToolkitScriptManager1_AsyncPostBackError(object sender, AsyncPostBackErrorEventArgs e)
        //{
        //    if (e.Exception.Data["ExtraInfo"] != null)
        //    {
        //        ToolkitScriptManager1.AsyncPostBackErrorMessage =
        //            e.Exception.Message +
        //            e.Exception.Data["ExtraInfo"].ToString();
        //    }
        //    else
        //    {
        //        ToolkitScriptManager1.AsyncPostBackErrorMessage =
        //            "An unspecified error occurred.";
        //    }
        //}
        //err.Data["ExtraInfo"] = "Não foi possível efetuar a operação.  Por favor, entre em contato com o administrador do sistema.<br />" + err.Message.ToString();
        //throw err;
    }



}