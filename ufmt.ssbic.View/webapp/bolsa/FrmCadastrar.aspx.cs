﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.sig.entity;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;
using ufmt.ssbic.View.session;


namespace ufmt.ssbic.View.webapp.bolsa
{
    public partial class FrmCadastrar : VerificaSession
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack) {

                BolsaBO objBO = new BolsaBO();
                string area = Request.QueryString["area"].ToString();
                lblTitulo.Text = area;
                string programaUID = Session["programaUID"].ToString();
                string proreitoria = objBO.GetPrograma(int.Parse(programaUID));
            }
            
        }

        protected void btnVoltar_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("FrmListar.aspx");
        }

        protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                BolsaBO objBO = new BolsaBO();
                Bolsa bolsa = new Bolsa();
                bolsa.descricao = txtDescricao.Text;
                bolsa.detalhe = txtDetalhes.Text;
                bolsa.proReitoriaUID = int.Parse(Session["programaUID"].ToString());
                objBO.Salvar(bolsa, 1);
                lblMsgResultado.Text = "Registro adicionado com sucesso.";
                PanelMsgResultado.CssClass = "success";
                PanelFormulario.Visible = false;
                btnSalvar.Visible = false;
                btnNovo.Visible = true;
            }
            catch (Exception err)
            {

                lblMsgResultado.Text = "Não foi possível efetuar a operação.  Por favor, entre em contato com o administrador do sistema.<br />" + err.Message.ToString();
                PanelMsgResultado.CssClass = "error";
                //err.Data["ExtraInfo"] = "Não foi possível efetuar a operação.  Por favor, entre em contato com o administrador do sistema.<br />" + err.Message.ToString();
            }

            PanelMsgResultado.Visible = true;
        }

        protected void btnNovo_Click(object sender, ImageClickEventArgs e)
        {
            ReiniciarCadastro();
        }

        private void ReiniciarCadastro()
        {
            PanelMsgResultado.Visible = false;
            PanelFormulario.Visible = true;
            btnSalvar.Visible = true;
            btnNovo.Visible = false;
            txtDescricao.Text = String.Empty;
            txtDetalhes.Text = String.Empty;
        }



    }
}