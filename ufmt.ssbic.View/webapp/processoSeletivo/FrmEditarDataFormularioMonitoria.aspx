﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true" CodeBehind="FrmEditarDataFormularioMonitoria.aspx.cs" Inherits="ufmt.ssbic.View.webapp.processoSeletivo.FrmEditarDataFormularioMonitoria" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </asp:Panel>
    <asp:Panel ID="pnlForm" runat="server">
    
    <p>
    Data de Abertura: <asp:TextBox ID="txtAbertura" runat="server"></asp:TextBox>
        <asp:CalendarExtender ID="txtAbertura_CalendarExtender" runat="server" 
            Enabled="True" Format="dd/MM/yyyy" TargetControlID="txtAbertura">
        </asp:CalendarExtender>
    </p>
    <p>
    Data de Fechamento: <asp:TextBox ID="txtFechamento" runat="server"></asp:TextBox>
        <asp:CalendarExtender ID="txtFechamento_CalendarExtender" runat="server" 
            Enabled="True" Format="dd/MM/yyyy" TargetControlID="txtFechamento">
        </asp:CalendarExtender>
    </p>
    <p>
        <asp:Button ID="btSalvar" runat="server" Text="Salvar" 
            onclick="btSalvar_Click" />
    </p>

    </asp:Panel>
</asp:Content>
