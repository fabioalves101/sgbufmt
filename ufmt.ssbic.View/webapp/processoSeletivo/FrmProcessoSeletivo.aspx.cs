﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;
using ufmt.sig.entity;
using ufmt.ssbic.View.session;

namespace ufmt.ssbic.View.webapp.processoSeletivo
{
    public partial class FrmProcessoSeletivo : VerificaSession
    {

        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {

                int processoSeletivoUID = int.Parse(Request.QueryString["processoSeletivoUID"]);
                int folhaPagamentoUID = int.Parse(Request.QueryString["folhaPagamentoUID"]);

                hddFolhaPagamentoUID.Value = folhaPagamentoUID.ToString();

                BolsaBO objBO = new BolsaBO();
                BancoDAL bDAL = new BancoDAL();

                ddlBanco.DataSource = bDAL.GetBanco();
                ddlBanco.DataTextField = "bancoNumero";
                ddlBanco.DataValueField = "bancoUID";
                ddlBanco.DataBound += new EventHandler(IndiceZero);
                ddlBanco.DataBind();

                List<vwBolsaProcessoSeletivo> bolsasProcessoSeletivo = objBO.GetBolsaProcessoSeletivo(processoSeletivoUID);
                RepeaterBolsa.DataSource = bolsasProcessoSeletivo;
                RepeaterBolsa.DataBind();

                lblInstrumentoSelecao.Text = ViewState["tituloProcessoSeletivo"].ToString();
                //ViewState["instrumentoSelecaoUID"] = instrumentoSelecaoUID.ToString();
                ViewState["processoSeletivoUID"] = processoSeletivoUID.ToString();
                string programaUID = Session["programaUID"].ToString();
                string proreitoria = objBO.GetPrograma(int.Parse(programaUID));
                hddProReitoria.Value = proreitoria;
                Label1.Text = "TERMO DE COMPROMISSO DE BOLSA &raquo;" + proreitoria;
                lblEtapa.Text = "Etapa 01: Selecione a Bolsa";
            }
            
        }



        protected void btBuscar_Click(object sender, ImageClickEventArgs e)
        {
            PanelResultadoConsulta.Visible = true;
            gdrAlunos.DataBind();
        }

        protected void montaLinkButtonBolsa(int instrumentoSelecaoUID, BolsaBO objBO)
        {

            int i = 0;
            List<vwBolsaProcessoSeletivo> bolsasProcessoSeletivo = objBO.GetBolsaProcessoSeletivo(instrumentoSelecaoUID);
            foreach (var bolsaProcessoSeletivo in bolsasProcessoSeletivo)
            {
                FinanciadorInstrumentoSelecao fisel = objBO.GetTotalBolsaRestante(bolsaProcessoSeletivo.financiadorInstrumentoSelecaoUID);
                lblInstrumentoSelecao.Text = bolsaProcessoSeletivo.processoSeletivo + " - " + bolsaProcessoSeletivo.instrumentoSelecao;
                //ddlTipoBolsa.Items.Add(new ListItem(bolsaProcessoSeletivo.descricaoBolsa + " ### Financiador: " + bolsaProcessoSeletivo.financiador + " #### " + fisel.quantidadeBolsas + " Bolsas de " + fisel.quantidadeRestantes + " Restante(s) #### Valor R$: " + fisel.valorBolsa, bolsaProcessoSeletivo.financiadorInstrumentoSelecaoUID.ToString()));
                //if (fisel.quantidadeRestantes == 0)
                //{
                //    ddlTipoBolsa.Items[i].Attributes.Add("style", "color:Red");
                // }

                //LinkButton link = new LinkButton();
                //link.Text = bolsaProcessoSeletivo.descricaoBolsa + " ### Financiador: " + bolsaProcessoSeletivo.financiador + " #### " + fisel.quantidadeBolsas + " Bolsas de " + fisel.quantidadeRestantes + " Restante(s) #### Valor R$: " + fisel.valorBolsa;
                //link.ID = "lnkBolsa" + i.ToString();
                //PlaceHolder1.Controls.Add(link);
                

                //Literal lit = new Literal();
                //lit.Text = "<br>";
                //PlaceHolder1.Controls.Add(lit);

                i++;
            }
        
        
        }


        protected void PreencherGridAluno()
        {

            AlunoBO objBO = new AlunoBO();
            int totalRegistros = 0;
            List<vwAluno> registros = null;
            if (rblTipoConsultaAluno.SelectedValue.Equals("0"))
                registros = objBO.FindRegistros(txtAluno.Text, 0);
            else
                registros = objBO.FindRegistros(null, decimal.Parse(txtAluno.Text));

            gdrAlunos.DataSource = registros;
            gdrAlunos.DataBind();
            totalRegistros = registros.Count;
            PanelResultadoConsulta.Visible = true;
            PanelResultadoConsulta.Visible = true;
            lblTotalAlunos.Text = totalRegistros.ToString();


        }

        protected void gdrAlunos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gdrAlunos.PageIndex = e.NewPageIndex;
            //PreencherGridAluno();
        }

        protected void gdrAlunos_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "adicionar")
            {

                string[] dados = e.CommandArgument.ToString().Split(',');
                BolsistaBO objBO = new BolsistaBO();
                Bolsista bolsista = new Bolsista();
                BolsistaProcessoSeletivo bps = new BolsistaProcessoSeletivo();

                int folhaPagamentoUID = int.Parse(Request.QueryString["folhaPagamentoUID"]);
               
                
                    if (objBO.VerificaBolsistaAtivo(dados[0]))
                    {//Possui bolsa

                        if (objBO.VerificaBolsistaAtivoMonitoria(dados[0], folhaPagamentoUID))
                        {
                            lblMatricula.Text = dados[0];
                            lblNomeAluno.Text = dados[1];
                            lblCpf.Text = dados[2];
                            txtEmailAluno.Text = dados[3];
                            lblCurso.Text = dados[4] + " - " + dados[5];
                            lblCampus.Text = dados[6] + " - " + dados[7];

                            divDetalheAluno.Visible = true;
                            divAcoesEtapa1.Visible = true;
                            lnkAlterarDadosAluno.Visible = false;
                            lnkAlterarDadosCoordenador.Visible = false;
                        }
                        else
                        {
                            //lblMsgResultado.Text = "";
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(),
                                Guid.NewGuid().ToString(),
                                "alert('Este aluno já possui um registro de bolsa no sistema. Por favor, selecione outro aluno para continuar.');", true);
                        }
                    }
                    else
                    {//Pode prosseguir com o cadastro de bolsista

                        lblMatricula.Text = dados[0];
                        lblNomeAluno.Text = dados[1];
                        lblCpf.Text = dados[2];
                        txtEmailAluno.Text = dados[3];
                        lblCurso.Text = dados[4] + " - " + dados[5];
                        lblCampus.Text = dados[6] + " - " + dados[7];

                        divDetalheAluno.Visible = true;
                        divAcoesEtapa1.Visible = true;
                        lnkAlterarDadosAluno.Visible = false;
                        lnkAlterarDadosCoordenador.Visible = false;
                    }
            }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            pnlListaServidores.Visible = true;
            gdrListaServidores.DataBind();
        }
        
        protected void gdrListaServidores_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridView gridview = (GridView)sender;
            Int64 servidorUID = int.Parse(gridview.SelectedValue.ToString());

            PessoaBO pessoaB = new PessoaBO();
            Pessoa pessoa = pessoaB.GetPessoa(servidorUID);
            Servidor servidor = pessoaB.GetServidor(pessoa.PessoaUID);
            pnlCoordenadorSelecionado.Visible = true;
            lblNomeCoordenador.Text = pessoa.Nome;
            lblMatriculaSiape.Text = servidor.Registro;
            lblCpfCoordenador.Text = pessoa.Cpf;
            txtEmailResponsavel.Text = pessoa.Mail;
            txtTelefoneResponsavel.Text = pessoa.TelefonePrimario;
            hdCoordenadorSelecionado.Value = pessoa.PessoaUID.ToString();
            divConsultaServidor.Visible = true;
            pnlListaServidores.Visible = true;
            divAcoesEtapa2.Visible = true;
            lnkAlterarDadosAluno.Visible = false;
            lnkAlterarDadosCoordenador.Visible = false;

            if (hddProReitoria.Value == "PROEG")
            {
                PanelProeg.Visible = true;
            }
            else if (hddProReitoria.Value == "PROCEV")
            {
                int processoSeletivoUID = int.Parse(ViewState["processoSeletivoUID"].ToString());
                ProcessoSeletivoBO psBo = new ProcessoSeletivoBO();
                vwProcessoSeletivo processoSeletivo = psBo.GetProcessoSeletivo(processoSeletivoUID);

                //BOLSA PERMANENCIA NÃO POSSUI NUMERO DO SIGPROJ
                if (processoSeletivo.bolsaUID == 44)
                {
                    tblsigproj.Visible = false;
                }

                PanelProcev.Visible = true;
            }
            else if (hddProReitoria.Value == "PROPEQ")
            {
                try
                {
                    MembroBO membroBO = new MembroBO();
                    PessoaMembro pessoaMembro = membroBO.GetPessoaMembro(pessoa.Cpf);

                    if (pessoaMembro != null)
                    {
                        AlunoProjetoBO alunoProjetoBO = new AlunoProjetoBO();
                        alunoProjetoBO.VerificaMembroTitulacao(pessoaMembro.pessoaMembroUID);

                        PanelPropeq.Visible = true;
                        lblAlertaPropeq.Visible = false;

                        hdprojetopropeq.Value = pessoa.Cpf;

                        PanelPropeq.Visible = true;
                        btnProximoE3.Visible = true;
                    }
                    else
                    {
                        btnProximoE3.Visible = true;
                    }

                    

                }
                catch (Exception exception)
                {
                    lblAlertaPropeq.Text = exception.Message;
                    lblAlertaPropeq.Visible = true;
                    PanelPropeq.Visible = false;
                    btnProximoE3.Visible = false;
                }
            }
            else
            {
                PanelProeg.Visible = true;
            }            
        }
        
        protected void btnProximoE1_Click(object sender, EventArgs e)
        {
            ReiniciarEtapa3();
        }

        protected void btnProximoE3_Click(object sender, EventArgs e)
        {
            PanelEtapa2.Visible = false;
            lblEtapa.Text = "Etapa 04: Confirmação de Dados";

            PanelEtapa1.Visible = true;
            PanelEtapa2.Visible = true;
            
            divConsultaAluno.Visible = false;
            divConsultaServidor.Visible = false;
            
            divAcoesEtapa1.Visible = false;
            divAcoesEtapa2.Visible = false;

            lnkAlterarDadosAluno.Visible = true;
            lnkAlterarDadosCoordenador.Visible = true;
            divConfirmarInscricao.Visible = true;

            DesativarControles(false);

        }

        private void DesativarControles(bool estado)
        {
            txtEmailAluno.Enabled = estado;
            ddlBanco.Enabled = estado;
            txtAgencia.Enabled = estado;
            txtContaCorrente.Enabled = estado;
            txtEmailResponsavel.Enabled = estado;
            txtTelefoneResponsavel.Enabled = estado;
        }


        protected void btnVoltarE1_Click(object sender, EventArgs e)
        {
            int processoSeletivoUID = int.Parse(ViewState["processoSeletivoUID"].ToString());

            ProcessoSeletivo proc =  new ProcessoSeletivoBO().GetProcessoSeletivoPorId(processoSeletivoUID);

            Response.Redirect("../bolsista/FrmListar.aspx?folhaPagamentoUID=" + proc.FolhaPagamento.FirstOrDefault().folhaPagamentoUID);
        }

        protected void lnkAlterarDadosAluno_Click(object sender, EventArgs e)
        {
            ReiniciarEtapa2();
        }

        protected void lnkAlterarDadosCoordenador_Click(object sender, EventArgs e)
        {
            ReiniciarEtapa3();
            DesativarControles(true);
        }


        private void ReiniciarEtapa1()
        {
            lblEtapa.Text = "Etapa 01: Selecione a Bolsa";
            PanelEtapa0.Visible = true;
            PanelEtapa1.Visible = false;
            PanelEtapa2.Visible = false;
            DesativarControles(true);
            divConfirmarInscricao.Visible = false;

        }



        private void ReiniciarEtapa2()
        {
            lblEtapa.Text = "Etapa 03: Selecione o Supervisor/Responsável";
            PanelEtapa1.Visible = true;
            PanelEtapa2.Visible = false;
            divConsultaAluno.Visible = true;
            divAcoesEtapa1.Visible = false;
            PanelResultadoConsulta.Visible = false;
            divDetalheAluno.Visible = false;
            lnkAlterarDadosAluno.Visible = false;
            lnkAlterarDadosCoordenador.Visible = false;
            DesativarControles(true);
            divConfirmarInscricao.Visible = false;
        }

        private void ReiniciarEtapa3()
        {
            lblEtapa.Text = "Etapa 03: Selecione o Supervisor/Responsável";
            PanelEtapa1.Visible = false;
            PanelEtapa2.Visible = true;
            divConsultaServidor.Visible = true;
            divAcoesEtapa2.Visible = false;
            pnlListaServidores.Visible = false;
            pnlCoordenadorSelecionado.Visible = false;

            lnkAlterarDadosAluno.Visible = false;
            lnkAlterarDadosCoordenador.Visible = false;
            divConfirmarInscricao.Visible = false;
            DesativarControles(true);
        }

        protected void IndiceZero(object sender, EventArgs e)
        {
            DropDownList objDropDownList = (DropDownList)sender;
            objDropDownList.Items.Insert(0, new ListItem("Selecione", ""));
        }

        protected void rblTipoConsultaAluno_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblTipoConsultaAluno.Items[1].Selected)
                REVAluno.Enabled = true;
            else
                REVAluno.Enabled = false;
        }

        protected void RepeaterBolsa_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                BolsaBO objBO = new BolsaBO();
                LinkButton lnkItemBolsa = e.Item.FindControl("lnkItemBolsa") as LinkButton;
                Label lblDetalhes = (Label)e.Item.FindControl("lblId");
                Label lblConteudoBolsa = (Label)e.Item.FindControl("lblConteudo");
                string tipoBolsa = String.Empty;
                string[] dados = lblDetalhes.Text.ToString().Split(',');
                FinanciadorInstrumentoSelecao fisel = objBO.GetTotalBolsaRestante(int.Parse(dados[0]));

                BolsistaController controller = new BolsistaController();
                List<BolsistaFolhaPagamento> bfpList =
                    controller.GetBolsistasPorFolhaPagamento(
                        int.Parse(Request.QueryString["folhaPagamentoUID"]),
                        true
                        );


                int restantes = fisel.quantidadeBolsas.Value - bfpList.Count;

                if (restantes == 0)
                {
                    lnkItemBolsa.Enabled = false;
                    lnkItemBolsa.Attributes.Add("Style", "color:red");
                    lnkItemBolsa.Text = dados[1] + " [Quantidade Indisponível]";
                }
                else
                {
                    lnkItemBolsa.Enabled = true;
                    lnkItemBolsa.Text = dados[1];
                }


                if (int.Parse(dados[dados.Length - 1]) == 1)
                {
                    tipoBolsa = "<b>Tipo de Bolsa:</b> Remunerada<br /> <b>Valor R$: </b>" + fisel.valorBolsa;
                }
                else {
                    tipoBolsa = "<b>Tipo de Bolsa:</b> Voluntário <br /> <b>Valor R$: </b> Não recebe bolsa";
                }

                lblConteudoBolsa.Text = "<b>Financiador: </b>" + dados[2] + "<br /><b>Quantidade de Bolsa(s) Ofertada(s): </b>" + fisel.quantidadeBolsas + " <br /><b>Bolsa(s) Restantes(s): </b>" + restantes + "<br/>" + tipoBolsa;
                ViewState["tituloProcessoSeletivo"] = dados[3] + " - " + dados[4];
                //ViewState["processoSeletivoUID"] = dados[5];
                

            }

        }


        protected void RepeaterBolsa_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "selecionarBolsa")
            {
                ViewState["FinanciadorInstrumentoSelecaoUID"] = e.CommandArgument.ToString();
                PanelEtapa1.Visible = true;
                PanelEtapa0.Visible = false;
                lblEtapa.Text = "Etapa 02: Selecione o Aluno";
                
            }
        }

        protected void btnConfirmarInscricao_Click(object sender, EventArgs e)
        {
            try
            {

                Bolsista bolsista = new Bolsista();
                Responsavel responsavel = new Responsavel();
                BolsistaProcessoSeletivo bolsistaps = new BolsistaProcessoSeletivo();
                BolsistaProcessoSeletivoBO bpsBO = new BolsistaProcessoSeletivoBO();

                if(!String.IsNullOrEmpty(ddlBanco.SelectedValue))
                    bolsista.bancoUID = int.Parse(ddlBanco.SelectedValue.ToString());

                bolsista.agencia = txtAgencia.Text;
                bolsista.numeroConta = txtContaCorrente.Text;
                bolsista.email = txtEmailAluno.Text;
                bolsista.registroAluno = lblMatricula.Text;

                responsavel.matricula = lblMatriculaSiape.Text;
                responsavel.telefone = txtTelefoneResponsavel.Text;
                responsavel.email = txtEmailResponsavel.Text;

                bolsistaps.processoSeletivoUID = int.Parse(ViewState["processoSeletivoUID"].ToString());
                bolsistaps.situacao = 1;

                //Procev
                bolsistaps.horaAtvSegMat = txtAtvSegMat.Text;
                bolsistaps.horaAtvTerMat = txtAtvTerMat.Text;
                bolsistaps.horaAtvQuaMat = txtAtvQuaMat.Text;
                bolsistaps.horaAtvQuiMat = txtAtvQuiMat.Text;
                bolsistaps.horaAtvSexMat = txtAtvSextMat.Text;
                bolsistaps.horaAtvSabMat = txtAtvSabMat.Text;

                bolsistaps.horaAtvSegVesp = txtAtvSegVesp.Text;
                bolsistaps.horaAtvTerVesp = txtAtvTerVesp.Text;
                bolsistaps.horaAtvQuaVesp = txtAtvQuaVesp.Text;
                bolsistaps.horaAtvQuiVesp = txtAtvQuiVesp.Text;
                bolsistaps.horaAtvSexVesp = txtAtvSextVesp.Text;
                bolsistaps.horaAtvSabVesp = txtAtvSabVesp.Text;

                bolsistaps.horaAtvSegNot = txtAtvSegNot.Text;
                bolsistaps.horaAtvTerNot = txtAtvTerNot.Text;
                bolsistaps.horaAtvQuaNot = txtAtvQuaNot.Text;
                bolsistaps.horaAtvQuiNot = txtAtvQuiNot.Text;
                bolsistaps.horaAtvSexNot = txtAtvSexNot.Text;
                bolsistaps.horaAtvSabNot = txtAtvSabNot.Text;
                bolsistaps.numSigProj = txtNumeroSigProj.Text;
                bolsistaps.tituloSigProj = txtTituloProjetoSigproj.Text;

                //Número do Projeto Propeq
                if (!String.IsNullOrEmpty(rdoProjetos.SelectedValue))
                {
                    int projetoUID = int.Parse(rdoProjetos.SelectedValue);
                    ProjetoBO projetoBO = new ProjetoBO();
                    Projeto projeto = projetoBO.GetProjeto(projetoUID);

                    bolsistaps.numProjetoPropeq = int.Parse(projeto.registroCap.ToString() + projeto.anoCap.ToString());

                }
                else
                {
                    bolsistaps.numProjetoPropeq = null;
                }


                if (bpsBO.Salvar(bolsistaps, bolsista, responsavel, int.Parse(ViewState["FinanciadorInstrumentoSelecaoUID"].ToString()), int.Parse(hddFolhaPagamentoUID.Value)))
                {

                    PanelMsgResultado.Visible = true;
                    PanelMsgResultado.CssClass = "pnlAviso";
                    lblMsgResultado.Text = "Bolsista adicionado com sucesso.";
                    conteudoGeral.Visible = false;
                    btnNovoBolsista.Visible = true;
                }
                else { 
                
                    //Msg erro
                    PanelMsgResultado.Visible = true;
                    lblMsgResultado.Text = "Ocorreu um erro na inserção de dados, por favor, entre em contato com o administrador do sistema.";
                }
                
            }
            catch (Exception exception) {

                throw new Exception("Não foi possível Salvar os registros.", exception);
            }


        }

        protected void btnNovoBolsista_Click(object sender, EventArgs e)
        {
            Response.Redirect("FrmProcessoSeletivo.aspx?processoSeletivoUID=" + ViewState["processoSeletivoUID"].ToString() + "&folhaPagamentoUID="+ hddFolhaPagamentoUID.Value);
        }

        protected void rdoProjetos_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnProximoE3.Visible = true;
        }

       



    }
}