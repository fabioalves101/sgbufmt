﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true" CodeBehind="FrmProcessoSeletivo.aspx.cs" Inherits="ufmt.ssbic.View.webapp.processoSeletivo.FrmProcessoSeletivo" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
  <ContentTemplate>

    <div style="padding:5px;text-align:left;">
        <h1><span class="allcaps"><asp:Label ID="Label1" runat="server"></asp:Label>

            </span></h1>
        <div id="divInstrumentoSelecao" style="padding-top:10px;padding-bottom:10px;margin-bottom:10px;border:1px solid #cccccc">
            <asp:Image ID="Image7" runat="server" ImageAlign="AbsBottom" 
                ImageUrl="~/images/form_blue_large.gif" />
            &nbsp;<asp:Label ID="lblInstrumentoSelecao" runat="server" 
                style="font-weight: 700; font-size: large"></asp:Label>
            <asp:HiddenField ID="hddProReitoria" runat="server" />
            <asp:HiddenField ID="hddFolhaPagamentoUID" runat="server" />
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                CssClass="ResumoErro" ForeColor="Red" 
                HeaderText="Para continuar, preencha corretamente os campos abaixo:" />
        </div>



        <div id="containerMensagens">
                <asp:Panel ID="PanelMsgResultado" runat="server" Visible="false" 
                    CssClass="pnlAviso">
                    <asp:Label ID="lblMsgResultado" runat="server"></asp:Label>
                    
                    
                    &nbsp;<br /> <br />
                    
                    
                    <asp:Button ID="btnNovoBolsista" runat="server" onclick="btnNovoBolsista_Click" 
                        Text="Cadastrar Novo" Visible="False" />
                </asp:Panel>
        </div>
    

    <fieldset ID="conteudoGeral" runat="server">
    <legend>
        <asp:Label ID="lblEtapa" runat="server" style="font-size: x-large"></asp:Label>
    </legend>

    <asp:Panel ID="PanelEtapa0" runat="server">

               <asp:Repeater ID="RepeaterBolsa" runat="server" 
                    onitemdatabound="RepeaterBolsa_ItemDataBound" 
                   onitemcommand="RepeaterBolsa_ItemCommand">
                    <ItemTemplate>
                         <div style="width:100%;padding-top:10px;padding-bottom:10px">
                                <asp:Label ID="lblId" runat="server" Text='<%# Eval("financiadorInstrumentoSelecaoUID") + "," + Eval("descricaoBolsa") + "," + Eval("financiador") + "," + Eval("processoSeletivo") + "," + Eval("instrumentoSelecao") + "," + Eval("processoSeletivoUID")  + "," + Eval("tipoOferta") %>' Visible="false" />
                                <fieldset>
                                <legend><asp:LinkButton ID="lnkItemBolsa" runat="server" CommandArgument='<%# Eval("financiadorInstrumentoSelecaoUID") %>' CommandName="selecionarBolsa" /></legend>
                                    <asp:Label ID="lblConteudo" runat="server"></asp:Label>
                                </fieldset>
                        </div>
                    </ItemTemplate>
                    </asp:Repeater>

    </asp:Panel>

    <asp:Panel ID="PanelEtapa1" runat="server" Visible="false">

            <div ID="divConsultaAluno" style="padding-top:20px;padding-bottom:20px" runat="server">


                <strong>Localizar Aluno por
                <asp:RadioButtonList ID="rblTipoConsultaAluno" runat="server" 
                    AutoPostBack="True" 
                    onselectedindexchanged="rblTipoConsultaAluno_SelectedIndexChanged" 
                    RepeatDirection="Horizontal" RepeatLayout="Flow">
                    <asp:ListItem Selected="True" Value="0">Nome</asp:ListItem>
                    <asp:ListItem Value="1">Matrícula</asp:ListItem>
                </asp:RadioButtonList>
                &nbsp;<asp:RegularExpressionValidator ID="REVAluno" runat="server" 
                    ControlToValidate="txtAluno" Display="Dynamic" Enabled="false" 
                    ErrorMessage="* Digite somente números" ForeColor="Red" 
                    ValidationExpression="^[0-9]+$">*Digite somente números</asp:RegularExpressionValidator>
               
                </strong>
                <br />

                 <asp:UpdateProgress ID="UpdateProgress1" runat="server" 
                    AssociatedUpdatePanelID="UpdatePanel1">
                    <ProgressTemplate>
                        <div>
                            &nbsp;<asp:Image ID="Image6" runat="server" 
                                ImageUrl="~/images/loading_circle.gif" />
                            &nbsp;Processando...
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>

                <asp:TextBox ID="txtAluno" runat="server" Width="497px"></asp:TextBox>
                &nbsp;<asp:ImageButton ID="btBuscar" runat="server" CssClass="imgBtBuscar" 
                    ImageUrl="~/media/images/busca.png" onclick="btBuscar_Click" />
                <asp:HiddenField ID="hddProcessoSeletivoUID" runat="server" />
                <asp:Panel ID="PanelResultadoConsulta" runat="server" Visible="false">
                    <fieldset>
                        <legend>Alunos Localizados (<asp:Label ID="lblTotalAlunos" runat="server" 
                                style="font-weight: 700"></asp:Label>
                            )</legend>
                        <asp:GridView ID="gdrAlunos" runat="server" AllowPaging="True" 
                            AlternatingRowStyle-CssClass="alt" AutoGenerateColumns="False" CssClass="mGrid" 
                            EmptyDataText="Vazio" onpageindexchanging="gdrAlunos_PageIndexChanging" 
                            onrowcommand="gdrAlunos_RowCommand" PagerStyle-CssClass="pgr" PageSize="5" 
                            DataSourceID="ObjectDataSource3">
                            <AlternatingRowStyle CssClass="alt" />
                            <Columns>
                                <asp:TemplateField HeaderText="Selecionar" ItemStyle-HorizontalAlign="Center" 
                                    ItemStyle-Width="60px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" 
                                            CommandArgument='<%# Eval("Matricula") + "," + Eval("Nome")+ "," + Eval("Cpf") + "," + Eval("Email")+ "," + Eval("CodCurso")+ "," + Eval("Curso") + "," + Eval("CodCampus") + "," + Eval("Campus")%>' 
                                            CommandName="adicionar" CssClass="minibutton">
                            <span>
                                <img src="../../images/select.png" alt="" />
                                Selecionar
                            </span>
                            </asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="60px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Matricula" SortExpression="Matricula">
                                    <ItemTemplate>
                                        <asp:Label ID="Label36" runat="server" Text='<%# Eval("Matricula") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Nome" SortExpression="Nome">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNome" runat="server" Text='<%# Eval("Nome") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="CPF" SortExpression="CPF">
                                    <ItemTemplate>
                                        <asp:Label ID="Label3" runat="server" Text='<%# Eval("CPF") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Tipo" SortExpression="Tipo">
                                    <ItemTemplate>
                                        <asp:Label ID="Label4" runat="server" Text='<%# Eval("Tipo") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                <div style="padding:20px;text-align:center">
                                    Nenhum aluno localizado no critério da consulta.&nbsp; Verifique os dígitos e tente 
                                    novamente.
                                </div>
                            </EmptyDataTemplate>
                            <PagerStyle CssClass="pgr" />
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" 
                            SelectMethod="SearchAlunos" TypeName="ufmt.ssbic.Business.AlunoBO">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="rblTipoConsultaAluno" DefaultValue="0" 
                                    Name="tipo" PropertyName="SelectedValue" Type="String" />
                                <asp:ControlParameter ControlID="txtAluno" DefaultValue="" Name="str" 
                                    PropertyName="Text" Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </fieldset>
                </asp:Panel>
            </div>

<div id="divDetalheAluno" runat="server" visible="false">
    <div style="background-color:#cccccc;font-weight:bold;padding:10px;width:98%">
        DADOS DO ALUNO&nbsp;
        <asp:LinkButton ID="lnkAlterarDadosAluno" runat="server" 
            onclick="lnkAlterarDadosAluno_Click" Visible="False">[Alterar Dados]</asp:LinkButton>
    </div>



       <table class="Largura100Porcento">
        <tr>
            <td class="Largura150">
                <asp:Label ID="Label9" runat="server" style="font-weight: 700" 
                    Text="Nome do Aluno:"></asp:Label>
            </td>
            <td>
                &nbsp;
                <asp:Label ID="lblNomeAluno" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="Largura150">
                <asp:Label ID="Label6" runat="server" style="font-weight: 700" 
                    Text="Matrícula:"></asp:Label>
            </td>
            <td>
                &nbsp;
                <asp:Label ID="lblMatricula" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="Largura150">
                <asp:Label ID="Label7" runat="server" style="font-weight: 700" Text="Curso:"></asp:Label>
            </td>
            <td>
                &nbsp;
                <asp:Label ID="lblCurso" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="Largura150">
                <asp:Label ID="Label8" runat="server" style="font-weight: 700" Text="Campus:"></asp:Label>
            </td>
            <td>
                &nbsp;
                <asp:Label ID="lblCampus" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="Largura150">
                <asp:Label ID="Label10" runat="server" style="font-weight: 700" Text="CPF:"></asp:Label>
            </td>
            <td>
                &nbsp;
                <asp:Label ID="lblCpf" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="Largura150">
                <asp:Label ID="Label11" runat="server" style="font-weight: 700" Text="E-mail:"></asp:Label>
                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                    ControlToValidate="txtEmailAluno" ErrorMessage="E-mail do aluno" 
                    ForeColor="Red">*</asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:TextBox ID="txtEmailAluno" runat="server" Width="400px"></asp:TextBox>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="Largura150">
                <asp:Label ID="Label15" runat="server" style="font-weight: 700" Text="Banco:"></asp:Label>
               
            </td>
            <td>
                &nbsp;<asp:DropDownList ID="ddlBanco" runat="server">
                </asp:DropDownList>

                &nbsp;</td>
        </tr>
        <tr>
            <td class="Largura150">
                <asp:Label ID="Label18" runat="server" style="font-weight: 700" Text="Agência:"></asp:Label>
                
            </td>
            <td>
                <asp:TextBox ID="txtAgencia" runat="server" Width="95px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="Largura150">
                <asp:Label ID="Label17" runat="server" style="font-weight: 700" 
                    Text="Número da Conta:"></asp:Label>
               
            </td>
            <td>
                <asp:TextBox ID="txtContaCorrente" runat="server" Width="297px"></asp:TextBox>
            </td>
        </tr>
    </table>

    <div id="divAcoesEtapa1" style="text-align:center;padding:10px" runat="server" visible="false">
       <asp:Button ID="btnProximoE2" runat="server" Text="Próxima Etapa" 
           onclick="btnProximoE1_Click" />
   </div>
</div>
   </asp:Panel>

    <asp:Panel ID="PanelEtapa2" runat="server" Visible="false">
    

   
    <asp:UpdatePanel ID="updPnlListaServidores" runat="server">
        
    <ContentTemplate>
    <asp:Panel ID="pnlCoordenador" runat="server">
    <div id="divConsultaServidor" runat="server">
         <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="updPnlListaServidores">
        <ProgressTemplate>
        <div style="text-align:left">
            &nbsp;<asp:Image ID="Image8" runat="server" 
                ImageUrl="~/images/loading_circle.gif" />
            &nbsp;Processando...
        </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
        
        <asp:Label ID="lblBuscaCoordenador" runat="server" 
            Text="Localizar Responsável/Coordenador" style="font-weight: 700"></asp:Label>       
        <br />
        <asp:TextBox ID="txtBuscaCoordenador" runat="server" Width="400px"></asp:TextBox>
        &nbsp;<asp:DropDownList ID="ddlBuscaCoordenador" runat="server">
            <asp:ListItem>Nome</asp:ListItem>
            <asp:ListItem>CPF</asp:ListItem>
            <asp:ListItem Value="siape">Registro SIAPE</asp:ListItem>
        </asp:DropDownList>
        &nbsp;<asp:ImageButton ID="ImageButton1" runat="server" 
            ImageUrl="~/media/images/busca.png" CssClass="imgBtBuscar" 
            onclick="ImageButton1_Click" />
<asp:Panel ID="pnlListaServidores" runat="server" Visible="False">
        Selecione o coordenador.
            <asp:GridView ID="gdrListaServidores" runat="server" 
                AutoGenerateColumns="False" DataSourceID="ObjectDataSource1" 
                AllowPaging="True" Width="100%"
                CssClass="mGrid"
                PagerStyle-CssClass="pgr"
                AlternatingRowStyle-CssClass="alt" DataKeyNames="ServidorUID" 
                onselectedindexchanged="gdrListaServidores_SelectedIndexChanged"
                >            
                <AlternatingRowStyle CssClass="alt" />
                <Columns>                    
                    <asp:TemplateField HeaderText="Selecionar" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="120px">
                        <ItemTemplate>
                            <asp:LinkButton CssClass="minibutton" ID="LinkButton1" runat="server" CausesValidation="False" 
                                            CommandName="select">
                            <span>
                                <img src="../../media/images/select.png" alt="" />
                                Selecionar
                            </span>
                            </asp:LinkButton>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="120px" />
                    </asp:TemplateField>
                    
                    <asp:BoundField DataField="Registro" HeaderText="Registro SIAPE" ItemStyle-HorizontalAlign="Center" 
                        SortExpression="Registro" ItemStyle-Width="120px">
                    <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                        Nome do Servidor
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='  <%# Eval("Pessoa.Nome")%>'></asp:Label>
                        
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle CssClass="pgr" />
            </asp:GridView>
            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
                SelectMethod="GetPessoas" TypeName="ufmt.ssbic.Business.PessoaBO">
                <SelectParameters>
                    <asp:ControlParameter ControlID="txtBuscaCoordenador" DefaultValue="null" 
                        Name="parametro" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="ddlBuscaCoordenador" DefaultValue="null" 
                        Name="tipoParametro" PropertyName="SelectedValue" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
            
           
            
        </asp:Panel>


    </div>
        
               
        <asp:Panel ID="pnlCoordenadorSelecionado" runat="server" Visible="False">
                <p style="font-weight:bold">
                    <asp:HiddenField ID="hdCoordenadorSelecionado" runat="server" />
                </p>
                <div style="background-color:#cccccc;font-weight:bold;padding:10px;width:98%">
                    DADOS DO SUPERVISOR / COORDENADOR&nbsp;
                    <asp:LinkButton ID="lnkAlterarDadosCoordenador" runat="server" 
                        onclick="lnkAlterarDadosCoordenador_Click" Visible="False">[Alterar Dados]</asp:LinkButton>
                </div>
                <p style="font-weight:bold">
                    <table class="Largura100Porcento">
                        <tr>
                            <td class="Largura150">
                                <asp:Label ID="Label37" runat="server" style="font-weight: 700" 
                                    Text="Nome do Coordenador:"></asp:Label>
                            </td>
                            <td>
                                &nbsp;
                                <asp:Label ID="lblNomeCoordenador" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="Largura150">
                                <asp:Label ID="Label38" runat="server" style="font-weight: 700" 
                                    Text="Matrícula:"></asp:Label>
                            </td>
                            <td>
                                &nbsp;
                                <asp:Label ID="lblMatriculaSiape" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="Largura150">
                                <asp:Label ID="Label41" runat="server" style="font-weight: 700" Text="CPF:"></asp:Label>
                            </td>
                            <td>
                                &nbsp;
                                <asp:Label ID="lblCpfCoordenador" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="Largura150">
                                <asp:Label ID="Label42" runat="server" style="font-weight: 700" Text="E-mail"></asp:Label>
                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" 
                                    ControlToValidate="txtEmailResponsavel" ErrorMessage="E-mail do responsável" 
                                    ForeColor="Red">*</asp:RequiredFieldValidator>
                            </td>
                            <td>
                                &nbsp;
                                <asp:TextBox ID="txtEmailResponsavel" runat="server" Width="500px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Largura150">
                                <asp:Label ID="Label43" runat="server" style="font-weight: 700" 
                                    Text="Telefone:"></asp:Label>
                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" 
                                    ControlToValidate="txtTelefoneResponsavel" 
                                    ErrorMessage="Telefone do Responsável" ForeColor="Red">*</asp:RequiredFieldValidator>
                            </td>
                            <td>
                                &nbsp;
                                <asp:TextBox ID="txtTelefoneResponsavel" runat="server" Width="180px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </p>
            </asp:Panel> 
    </asp:Panel>
    </ContentTemplate>
    </asp:UpdatePanel>

    <asp:Panel ID="PanelProcev" runat="server" Visible="false">
            
            <table id="tblsigproj" runat="server" class="Largura100Porcento">
                <tr>
                    <td class="Largura150">
                        <asp:Label ID="Label19" runat="server" style="font-weight: 700" 
                            Text="Número do SIGPROJ:"></asp:Label>
                        &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" 
                            ControlToValidate="txtNumeroSigProj" ErrorMessage="Número do SIGPROJ" 
                            ForeColor="Red">*</asp:RequiredFieldValidator>
                    </td>
                    <td>
                        &nbsp;
                        <asp:TextBox ID="txtNumeroSigProj" runat="server" Width="180px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="Largura150">
                        <asp:Label ID="Label20" runat="server" style="font-weight: 700" 
                            Text="Título do Projeto (Ação de Extensão):"></asp:Label>
                        &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" 
                            ControlToValidate="txtTituloProjetoSigproj" 
                            ErrorMessage="Título do Projeto (Ação de Extensão)" ForeColor="Red">*</asp:RequiredFieldValidator>
                    </td>
                    <td>
                        &nbsp;<asp:TextBox ID="txtTituloProjetoSigproj" runat="server" Width="694px"></asp:TextBox>
                    </td>
                </tr>
            </table>
        <asp:Panel ID="pnlResumoAtividades" runat="server" Visible="false">
         
            <br />
            <asp:Label ID="Label21" runat="server" style="font-weight: 700" 
                Text="RESUMO DAS ATIVIDADES A SEREM DESENVOLVIDAS NO SEMESTRE"></asp:Label>
            <br />
            <table style="width:100%;text-align:center">
                <tr style="background-color:Gray;color:White">
                    <td>
                        Período</td>
                    <td>
                        Segunda</td>
                    <td>
                        Terça</td>
                    <td>
                        Quarta</td>
                    <td>
                        Quinta</td>
                    <td>
                        Sexta</td>
                    <td>
                        Sábado</td>
                </tr>
                <tr>
                    <td>
                        Matutino</td>
                    <td>
                        <asp:TextBox ID="txtAtvSegMat" runat="server" Width="60px"></asp:TextBox>
                        <asp:MaskedEditExtender ID="txtAtvSegMat_MaskedEditExtender" runat="server" 
                            Enabled="True" 
                            Mask="99:99"
                            ClearMaskOnLostFocus="false"
                            UserTimeFormat="TwentyFourHour"
                            TargetControlID="txtAtvSegMat" />

                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" 
                            ControlToValidate="txtAtvSegMat" 
                            ErrorMessage="Atividades segunda-feira matutino" ForeColor="Red">*</asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:TextBox ID="txtAtvTerMat" runat="server" Width="60px"></asp:TextBox>
                        <asp:MaskedEditExtender ID="txtAtvTerMat_MaskedEditExtender" runat="server" 
                            Enabled="True" 
                            Mask="99:99"
                            ClearMaskOnLostFocus="false"
                            UserTimeFormat="TwentyFourHour"
                            TargetControlID="txtAtvTerMat">
                        </asp:MaskedEditExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" 
                            ControlToValidate="txtAtvTerMat" ErrorMessage="Atividades terça-feira matutino" 
                            ForeColor="Red">*</asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:TextBox ID="txtAtvQuaMat" runat="server" Width="60px"></asp:TextBox>
                        <asp:MaskedEditExtender ID="txtAtvQuaMat_MaskedEditExtender" runat="server" 
                            Enabled="True" 
                            Mask="99:99"
                            ClearMaskOnLostFocus="false"
                            UserTimeFormat="TwentyFourHour"
                            TargetControlID="txtAtvQuaMat">
                        </asp:MaskedEditExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" 
                            ControlToValidate="txtAtvQuaMat" 
                            ErrorMessage="Atividades quarta-feira matutino" ForeColor="Red">*</asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:TextBox ID="txtAtvQuiMat" runat="server" Width="60px"></asp:TextBox>
                        <asp:MaskedEditExtender ID="txtAtvQuiMat_MaskedEditExtender" runat="server" 
                            Enabled="True" 
                            Mask="99:99"
                            ClearMaskOnLostFocus="false"
                            UserTimeFormat="TwentyFourHour"
                            TargetControlID="txtAtvQuiMat">
                        </asp:MaskedEditExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" 
                            ControlToValidate="txtAtvQuiMat" 
                            ErrorMessage="Atividades quinta-feira matutino" ForeColor="Red">*</asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:TextBox ID="txtAtvSextMat" runat="server" Width="60px"></asp:TextBox>
                        <asp:MaskedEditExtender ID="txtAtvSextMat_MaskedEditExtender" runat="server" 
                            Enabled="True" 
                            Mask="99:99"
                            ClearMaskOnLostFocus="false"
                            UserTimeFormat="TwentyFourHour"
                            TargetControlID="txtAtvSextMat">
                        </asp:MaskedEditExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" 
                            ControlToValidate="txtAtvSextMat" 
                            ErrorMessage="Atividades sexta-feira matutino" ForeColor="Red">*</asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:TextBox ID="txtAtvSabMat" runat="server" Height="23px" Width="60px"></asp:TextBox>
                        <asp:MaskedEditExtender ID="txtAtvSabMat_MaskedEditExtender" runat="server" 
                            Enabled="True" 
                            Mask="99:99"
                            ClearMaskOnLostFocus="false"
                            UserTimeFormat="TwentyFourHour"
                            TargetControlID="txtAtvSabMat">
                        </asp:MaskedEditExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" 
                            ControlToValidate="txtAtvSabMat" ErrorMessage="Atividades sábado matutino" 
                            ForeColor="Red">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Vespertino</td>
                    <td>
                        <asp:TextBox ID="txtAtvSegVesp" runat="server" Width="60px"></asp:TextBox>
                        <asp:MaskedEditExtender ID="txtAtvSegVesp_MaskedEditExtender" runat="server" 
                            Enabled="True" 
                            Mask="99:99"
                            ClearMaskOnLostFocus="false"
                            UserTimeFormat="TwentyFourHour"
                            TargetControlID="txtAtvSegVesp">
                        </asp:MaskedEditExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" 
                            ControlToValidate="txtAtvSegVesp" 
                            ErrorMessage="Atividades segunda-feira vespertino" ForeColor="Red">*</asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:TextBox ID="txtAtvTerVesp" runat="server" Width="60px"></asp:TextBox>
                        <asp:MaskedEditExtender ID="txtAtvTerVesp_MaskedEditExtender" runat="server" 
                            Enabled="True" 
                            Mask="99:99"
                            ClearMaskOnLostFocus="false"
                            UserTimeFormat="TwentyFourHour"
                            TargetControlID="txtAtvTerVesp">
                        </asp:MaskedEditExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" 
                            ControlToValidate="txtAtvTerVesp" 
                            ErrorMessage="Atividades terça-feira vespertino" ForeColor="Red">*</asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:TextBox ID="txtAtvQuaVesp" runat="server" Width="60px"></asp:TextBox>
                        <asp:MaskedEditExtender ID="txtAtvQuaVesp_MaskedEditExtender" runat="server" 
                            Enabled="True" 
                            Mask="99:99"
                            ClearMaskOnLostFocus="false"
                            UserTimeFormat="TwentyFourHour"
                            TargetControlID="txtAtvQuaVesp">
                        </asp:MaskedEditExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" 
                            ControlToValidate="txtAtvQuaVesp" 
                            ErrorMessage="Atividades quarta-feira vespertino" ForeColor="Red">*</asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:TextBox ID="txtAtvQuiVesp" runat="server" Width="60px"></asp:TextBox>
                        <asp:MaskedEditExtender ID="txtAtvQuiVesp_MaskedEditExtender" runat="server" 
                            Enabled="True" 
                            Mask="99:99"
                            ClearMaskOnLostFocus="false"
                            UserTimeFormat="TwentyFourHour"
                            TargetControlID="txtAtvQuiVesp">
                        </asp:MaskedEditExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" 
                            ControlToValidate="txtAtvQuiVesp" 
                            ErrorMessage="Atividades quinta-feira vespertino" ForeColor="Red">*</asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:TextBox ID="txtAtvSextVesp" runat="server" Width="60px"></asp:TextBox>
                        <asp:MaskedEditExtender ID="txtAtvSextVesp_MaskedEditExtender" runat="server" 
                            Enabled="True" 
                            Mask="99:99"
                            ClearMaskOnLostFocus="false"
                            UserTimeFormat="TwentyFourHour"
                            TargetControlID="txtAtvSextVesp">
                        </asp:MaskedEditExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" 
                            ControlToValidate="txtAtvSextVesp" 
                            ErrorMessage="Atividades sexta-feira vespertino" ForeColor="Red">*</asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:TextBox ID="txtAtvSabVesp" runat="server" Width="60px"></asp:TextBox>
                        <asp:MaskedEditExtender ID="txtAtvSabVesp_MaskedEditExtender" runat="server" 
                            Enabled="True" 
                            Mask="99:99"
                            ClearMaskOnLostFocus="false"
                            UserTimeFormat="TwentyFourHour"
                            TargetControlID="txtAtvSabVesp">
                        </asp:MaskedEditExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" 
                            ControlToValidate="txtAtvSabVesp" ErrorMessage="Atividades sábado vespertino" 
                            ForeColor="Red">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Noturno</td>
                    <td>
                        <asp:TextBox ID="txtAtvSegNot" runat="server" Width="60px"></asp:TextBox>
                        <asp:MaskedEditExtender ID="txtAtvSegNot_MaskedEditExtender" runat="server" 
                            Enabled="True" 
                            Mask="99:99"
                            ClearMaskOnLostFocus="false"
                            UserTimeFormat="TwentyFourHour"
                            TargetControlID="txtAtvSegNot">
                        </asp:MaskedEditExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" 
                            ControlToValidate="txtAtvSegNot" 
                            ErrorMessage="Atividades segunda-feira noturno" ForeColor="Red">*</asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:TextBox ID="txtAtvTerNot" runat="server" Width="60px"></asp:TextBox>
                        <asp:MaskedEditExtender ID="txtAtvTerNot_MaskedEditExtender" runat="server" 
                            Enabled="True" 
                            Mask="99:99"
                            ClearMaskOnLostFocus="false"
                            UserTimeFormat="TwentyFourHour"
                            TargetControlID="txtAtvTerNot">
                        </asp:MaskedEditExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" 
                            ControlToValidate="txtAtvTerNot" ErrorMessage="Atividades terça-feira noturno" 
                            ForeColor="Red">*</asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:TextBox ID="txtAtvQuaNot" runat="server" Width="60px"></asp:TextBox>
                        <asp:MaskedEditExtender ID="txtAtvQuaNot_MaskedEditExtender" runat="server" 
                            Enabled="True" 
                            Mask="99:99"
                            ClearMaskOnLostFocus="false"
                            UserTimeFormat="TwentyFourHour"
                            TargetControlID="txtAtvQuaNot">
                        </asp:MaskedEditExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server" 
                            ControlToValidate="txtAtvQuaNot" ErrorMessage="Atividades quarta-feira noturno" 
                            ForeColor="Red">*</asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:TextBox ID="txtAtvQuiNot" runat="server" Width="60px"></asp:TextBox>
                        <asp:MaskedEditExtender ID="txtAtvQuiNot_MaskedEditExtender" runat="server" 
                            Enabled="True" 
                            Mask="99:99"
                            ClearMaskOnLostFocus="false"
                            UserTimeFormat="TwentyFourHour"
                            TargetControlID="txtAtvQuiNot">
                        </asp:MaskedEditExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator27" runat="server" 
                            ControlToValidate="txtAtvQuiNot" ErrorMessage="Atividades quinta-feira noturno" 
                            ForeColor="Red">*</asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:TextBox ID="txtAtvSexNot" runat="server" Width="60px"></asp:TextBox>
                        <asp:MaskedEditExtender ID="txtAtvSexNot_MaskedEditExtender" runat="server" 
                            Enabled="True" 
                            Mask="99:99"
                            ClearMaskOnLostFocus="false"
                            UserTimeFormat="TwentyFourHour"
                            TargetControlID="txtAtvSexNot">
                        </asp:MaskedEditExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator28" runat="server" 
                            ControlToValidate="txtAtvSexNot" ErrorMessage="Atividades sexta-feira noturno" 
                            ForeColor="Red">*</asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:TextBox ID="txtAtvSabNot" runat="server" Width="60px"></asp:TextBox>
                        <asp:MaskedEditExtender ID="txtAtvSabNot_MaskedEditExtender" runat="server" 
                            Enabled="True" 
                            Mask="99:99"
                            ClearMaskOnLostFocus="false"
                            UserTimeFormat="TwentyFourHour"
                            TargetControlID="txtAtvSabNot">
                        </asp:MaskedEditExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator29" runat="server" 
                            ControlToValidate="txtAtvSabNot" ErrorMessage="Atividades sábado noturno" 
                            ForeColor="Red">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
            <div style="text-align:center;padding-top:10px;padding-bottom:10px">
            <asp:Label ID="Label2" runat="server" style="font-weight: 700" 
                Text="Obs.: Grade Horária mínima de 12 horas semanais"></asp:Label>

            </div>
            </asp:Panel>
    </asp:Panel>

    <asp:Panel ID="PanelProeg" runat="server" Visible="false">
        <div>
            
            <br />
            
        </div>
    </asp:Panel>
    <asp:Label ID="lblAlertaPropeq" runat="server" Text="" Visible="false"></asp:Label>
    <asp:Panel ID="PanelPropeq" runat="server" Visible="false">
        <asp:RadioButtonList ID="rdoProjetos" runat="server" AutoPostBack="True" 
            DataSourceID="ObjectDataSource2" DataTextField="titulo" 
            DataValueField="projetoUID" 
            onselectedindexchanged="rdoProjetos_SelectedIndexChanged">
            
        </asp:RadioButtonList>
        <asp:HiddenField ID="hdprojetopropeq" runat="server" />
        <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" 
            SelectMethod="GetProjetosMembro" TypeName="ufmt.ssbic.Business.ProjetoBO">
            <SelectParameters>
                <asp:ControlParameter ControlID="hdprojetopropeq" Name="cpf" 
                    PropertyName="Value" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </asp:Panel>


   <div id="divAcoesEtapa2" style="text-align:center;padding:10px" runat="server" visible="false">
       <asp:Button ID="btnProximoE3" runat="server" Text="Próxima Etapa" 
           onclick="btnProximoE3_Click" />
   </div>


    </asp:Panel>

   <div id="divConfirmarInscricao" style="text-align:center;padding:10px" runat="server" visible="false">
      <asp:Button ID="btnVoltar" runat="server" Text="Cancelar" 
           onclick="btnVoltarE1_Click" /> 
       <asp:Button ID="btnConfirmarInscricao" runat="server" 
           Text="Confirmar Inscrição" onclick="btnConfirmarInscricao_Click" />
   </div>


    </fieldset>

    </div>


    </ContentTemplate>
  </asp:UpdatePanel>
</asp:Content>
