﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.sig.entity;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;
using ufmt.ssbic.View.session;


namespace ufmt.ssbic.View.webapp.processoSeletivo
{
    public partial class FrmCadastrar : VerificaSession
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack) {

                FinanciadorBO objBO = new FinanciadorBO();
                string proreitoriaUID = Session["programaUID"].ToString();
                string area = Request.QueryString["area"];
                lblTitulo.Text = area;
                
            }
            
        }

        protected void IndiceZero(object sender, EventArgs e)
        {
            DropDownList objDropDownList = (DropDownList)sender; //Cast no sender para DropDownList
            objDropDownList.Items.Insert(0, new ListItem("Selecione","")); //Adiciona um novo Item
        }


        protected void btnVoltar_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("FrmListar.aspx?instrumentoSelecaoUID=" + Request.QueryString["instrumentoSelecaoUID"].ToString());
        }

        protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ProcessoSeletivoBO objBO = new ProcessoSeletivoBO();
                ProcessoSeletivo ps = new ProcessoSeletivo();

                ps.instrumentoSelecaoUID = int.Parse(Request.QueryString["instrumentoSelecaoUID"].ToString());
                ps.titulo = txtTitulo.Text;
                ps.descricao = txtDescricao.Text;
                DateTime dataAplicacao;
                DateTime dataResultado;

                if (DateTime.TryParse(txtDataAplicacao.Text, out dataAplicacao))
                    ps.dataAplicacao = DateTime.Parse(txtDataAplicacao.Text);
                else
                    ps.dataAplicacao = null;

                if (DateTime.TryParse(txtDataResultado.Text, out dataResultado))
                    ps.dataResultado = DateTime.Parse(txtDataResultado.Text);
                else
                    ps.dataResultado = null;

                objBO.Salvar(ps, 1);


                lblMsgResultado.Text = "Registro adicionado com sucesso.";
                PanelMsgResultado.CssClass = "success";
                PanelFormulario.Visible = false;
                btnSalvar.Visible = false;
                btnNovo.Visible = true;
            }
            catch (Exception err)
            {

                lblMsgResultado.Text = "Não foi possível efetuar a operação.  Por favor, entre em contato com o administrador do sistema.<br />" + err.Message.ToString();
                PanelMsgResultado.CssClass = "error";
                
            }

            PanelMsgResultado.Visible = true;
        }

        protected void btnNovo_Click(object sender, ImageClickEventArgs e)
        {
            ReiniciarCadastro();
        }

        private void ReiniciarCadastro()
        {
            PanelMsgResultado.Visible = false;
            PanelFormulario.Visible = true;
            btnSalvar.Visible = true;
            btnNovo.Visible = false;
           
           
        }



    }
}