﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true" CodeBehind="FrmInscricao.aspx.cs" Inherits="ufmt.ssbic.View.webapp.processoSeletivo.FrmInscricao" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="padding:5px;text-align:left;">
        <h1><span class="allcaps">Inscrição</span></h1>
</div>
<div id="containerMensagens">
    <asp:Panel ID="PanelMsgResultado" runat="server" Visible="false">
        <asp:Label ID="lblMsgResultado" runat="server"></asp:Label>
    </asp:Panel>
</div>
<p>
URL do Formulário de Inscrição: 
    <asp:HyperLink ID="hpUrlForm" runat="server" Target="_blank">[hpUrlForm]</asp:HyperLink>
</p>
<p>
<a href="FrmEditarDataFormularioMonitoria.aspx">Editar data de abertura e fechamento do formulário de inscrição</a>
</p>
<p>
    <asp:TextBox ID="txtRga" runat="server"></asp:TextBox>
</p>
    <p>
        <asp:DropDownList ID="DropDownList1" runat="server" AppendDataBoundItems="true"
            DataSourceID="ObjectDataSource2" DataTextField="nome" 
            DataValueField="cursoUID" AutoPostBack="True" 
            onselectedindexchanged="DropDownList1_SelectedIndexChanged">
            <asp:ListItem Value="0" Selected="True">Todos os Cursos</asp:ListItem>

        </asp:DropDownList>
        <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" 
            SelectMethod="GetCursosProcessoSeletivo" 
            TypeName="ufmt.ssbic.Business.CursoProcessoSeletivoBO">
            <SelectParameters>
                <asp:QueryStringParameter Name="processoSeletivoUID" 
                    QueryStringField="processoSeletivoUID" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <asp:DropDownList ID="DropDownList2" runat="server" AppendDataBoundItems="true" AutoPostBack="True" 
            onselectedindexchanged="DropDownList1_SelectedIndexChanged">
            <asp:ListItem Value="" Selected="True">Todos Status</asp:ListItem>

            <asp:ListItem Value="1">Status OK</asp:ListItem>

        </asp:DropDownList>
</p>
    <div style="margin-bottom:70px;">
    <div style="float:left;"> 
        <asp:Button ID="btBuscar" runat="server" onclick="btBuscar_Click" 
            Text="Buscar" />

        <asp:Button ID="btBuscarTodos" runat="server" 
            Text="Buscar Todos" onclick="btBuscarTodos_Click" />
    
    </div>
    
    <div style="float:right; text-align:right">        
        <asp:Button ID="btHomologacao" runat="server" 
            Text="Homologar bolsistas do 1º semestre" onclick="btHomologacao_Click" />

        <asp:Button ID="btHomologacao2" runat="server" 
            Text="Homologar bolsistas do 2º semestre" onclick="btHomologacao2_Click" />
        <asp:HiddenField ID="hdSemestre" runat="server" />
    </div>

</div>
    <asp:GridView ID="grdInscritos" runat="server" AutoGenerateColumns="False" 
        DataSourceID="ObjectDataSource1" onrowcommand="grdInscritos_RowCommand" 
        Width="100%">
        <Columns>
            <asp:BoundField DataField="Matricula" HeaderText="RGA" 
                SortExpression="Matricula" />
            <asp:BoundField DataField="nomeAluno" HeaderText="Nome" 
                SortExpression="nomeAluno" />
            <asp:BoundField DataField="Curso" HeaderText="Curso" 
                SortExpression="Curso" />
            <asp:BoundField DataField="nomeDisciplina" HeaderText="Disciplina" 
                SortExpression="nomeDisciplina" />
            <asp:CheckBoxField DataField="remunerada" HeaderText="Remunerada?" 
                SortExpression="remunerada" />
            <asp:TemplateField HeaderText="Período" SortExpression="periodoAtuacao">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# VerificaPeriodo((string)Eval("periodoAtuacao")) %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Editar">
                <ItemTemplate>
                    <asp:LinkButton CommandArgument='<%# Eval("monitorUID") %>' CommandName="editar" ID="lnkEditar" runat="server">Editar</asp:LinkButton>                      
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Excluir">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkExcluir" CommandArgument='<%# Eval("monitorUID") %>' CommandName="excluir" runat="server">Excluir</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Homologar">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkHomologarBolsista" CommandArgument='<%# Eval("monitorUID") %>' CommandName="homologarbolsista" Text='<%# VerificaHomologacao((int)Eval("bolsistaUID"), (int)Eval("monitorUID")) %>' runat="server"></asp:LinkButton>
                </ItemTemplate>            
            </asp:TemplateField>
        </Columns>
        
    </asp:GridView>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
        SelectMethod="GetMonitores" TypeName="ufmt.ssbic.Business.MonitorBO">
        <SelectParameters>
            <asp:ControlParameter ControlID="txtRga" Name="rga" PropertyName="Text" 
                Type="String" />
            <asp:QueryStringParameter Name="processoSeletivoUID" 
                QueryStringField="processoSeletivoUID" Type="Int32" />
            <asp:ControlParameter ControlID="DropDownList1" Name="cursoUID" 
                PropertyName="SelectedValue" Type="Int64" />
            <asp:ControlParameter ControlID="DropDownList2" Name="status" 
                PropertyName="SelectedValue" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>


    
               <asp:modalpopupextender 
                ID="MdlFolhaPagamento"
                BehaviorID="behavior"
                runat="server"
                TargetControlID="lnkModal"
                PopupControlID="pnlModal"
                BackgroundCssClass="modalBackground"
                DropShadow="True"
                DynamicServicePath="" 
                Enabled="True"
        
        />


        <asp:HyperLink ID="lnkModal" runat="server" Visible="true" Text=""  />

 <asp:Panel ID="pnlModal" runat="server" CssClass="modalPopup">
 <div id="ContainerDetalhes" style="width:800px">
          <div id="MenuDetalhes" style="background-color:White;padding-left:5px;padding-bottom:5px;height:40px">
            <div style="width:650px; float:left; height: 32px;">
                <h2 style="height: 35px">Escolha a folha de pagamento</h2>
            </div>
            <div style="float:left;width:130px;text-align:right; padding-top:10px;">
            <asp:ImageButton ID="ImageButton1" runat="server" 
                    ImageUrl="../../images/close24.png" 
                    OnclientClick="$find('behavior').hide(); return false;" ToolTip="Fechar Janela" 
                    ImageAlign="AbsMiddle" onclick="ImageButton1_Click" />
                &nbsp;<asp:LinkButton ID="LinkButtonFechar" runat="server" Visible="true" 
                    Text="Fechar" onclick="LinkButtonFechar_Click"   />
            </div>
            </div>
     <asp:GridView ID="grdFolhasPagamento" runat="server" AutoGenerateColumns="False" 
              DataSourceID="odsFolhaPagamento" Width="100%" 
              onrowcommand="grdFolhasPagamento_RowCommand">
         <Columns>
             <asp:TemplateField HeaderText="Selecionar">
                <ItemStyle Width="80px" />
                <ItemTemplate>
                    <asp:LinkButton ID="lnkHomologarTodos" runat="server" CausesValidation="False" 
                                            CommandArgument='<%# Eval("folhaPagamentoUID") %>' 
                                            CommandName="homologar" CssClass="minibutton">
                                            <span>
                                                <img src="../../images/select.png" alt="" />
                                                Selecionar
                                            </span>
                    </asp:LinkButton>
                </ItemTemplate>
             </asp:TemplateField>
             <asp:BoundField DataField="dataInicio" HeaderText="Início" 
                 SortExpression="dataInicio" />
             <asp:BoundField DataField="dataFim" HeaderText="Fim" SortExpression="dataFim" />
         </Columns>
     </asp:GridView>
     <asp:ObjectDataSource ID="odsFolhaPagamento" runat="server" 
              SelectMethod="GetUltimaFolhaPagamento" 
              TypeName="ufmt.ssbic.Business.MonitoriaController">
         <SelectParameters>
             <asp:QueryStringParameter Name="processoSeletivoUID" 
                 QueryStringField="processoSeletivoUID" Type="Int32" />
         </SelectParameters>
          </asp:ObjectDataSource>
     <asp:HiddenField ID="hdBolsistaUID" runat="server" />
     <asp:HiddenField ID="hdMonitorUID" runat="server" />
</div>
 </asp:Panel>
</asp:Content>
