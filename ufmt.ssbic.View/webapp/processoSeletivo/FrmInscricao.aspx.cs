﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.View.session;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;
using ufmt.sig.entity;

namespace ufmt.ssbic.View.webapp.processoSeletivo
{
    public partial class FrmInscricao : VerificaSession
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int processoSeletivoUID = int.Parse(Request.QueryString["processoSeletivoUID"].ToString());

            string appPath = string.Format("{0}://{1}{2}{3}",
              Request.Url.Scheme,
              Request.Url.Host,
              Request.Url.Port == 80 ? string.Empty : ":" + Request.Url.Port,
              Request.ApplicationPath);
            
            if (!appPath.EndsWith("/"))
                            appPath += "/";

            hpUrlForm.Text = appPath + "inscricao/monitoria/default.aspx?processo=" + processoSeletivoUID;
            hpUrlForm.NavigateUrl = appPath + "inscricao/monitoria/default.aspx?processo=" + processoSeletivoUID;
        }

        public string VerificaPeriodo(String periodoAtuacao)
        {
            String periodo;
            if (periodoAtuacao.Equals("1"))
                periodo = "1° Semestre";
            else
                if (periodoAtuacao.Equals("2"))
                    periodo = "2° Semestre";
                else
                    periodo = "Anual";

            return periodo;
        }

        protected void grdInscritos_RowCommand(object sender, GridViewCommandEventArgs e)
        {            
            try
            {
                if (e.CommandName == "editar")
                {
                    Response.Redirect("../inscritos/FrmEditar.aspx?monitorUID="+e.CommandArgument.ToString());
                }

                if (e.CommandName == "excluir")
                {
                    int monitorUID = int.Parse(e.CommandArgument.ToString());
                    this.ExcluirMonitor(monitorUID);
                }

                if (e.CommandName == "homologarbolsista")
                {
                    int monitorUID = int.Parse(e.CommandArgument.ToString());
                    hdMonitorUID.Value = monitorUID.ToString();
                    Monitor monitor = new MonitoriaController().GetMonitor(monitorUID);

                    hdBolsistaUID.Value = monitor.bolsistaUID.Value.ToString();

                    if (monitor.periodoAtuacao.Equals("A"))
                        hdSemestre.Value = "3";
                    else
                        hdSemestre.Value = monitor.periodoAtuacao;

                    MdlFolhaPagamento.Show();
                }
            }
            catch (Exception ex)
            {
                PanelMsgResultado.Visible = true;
                lblMsgResultado.Text = "Houve um erro inesperado. "+ex.Message;
            }
        }

        protected void ExcluirMonitor(int monitorUID)
        {
            try
            {
                MonitorBO monitorBO = new MonitorBO();
                monitorBO.Excluir(monitorUID);
            }
            catch (Exception ex)
            {
                PanelMsgResultado.Visible = true;
                lblMsgResultado.Text = "Não foi possível excluir o registro. "+ex.Message;
            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btBuscar_Click(object sender, EventArgs e)
        {
            grdInscritos.DataBind();
        }

        protected void btBuscarTodos_Click(object sender, EventArgs e)
        {
            txtRga.Text = String.Empty;
            grdInscritos.DataBind();
        }

        protected void btHomologacao_Click(object sender, EventArgs e)
        {
            hdSemestre.Value = "1";
            MdlFolhaPagamento.Show();
        }

        protected void btHomologacao2_Click(object sender, EventArgs e)
        {
            hdSemestre.Value = "2";
            MdlFolhaPagamento.Show();
        }

        protected void LinkButtonFechar_Click(object sender, EventArgs e)
        {
            MdlFolhaPagamento.Hide();
            hdBolsistaUID.Value = String.Empty;
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            MdlFolhaPagamento.Hide();
            hdBolsistaUID.Value = String.Empty;
        }

        protected void grdFolhasPagamento_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("homologar"))
            {
                int processoSeletivoUID = int.Parse(Request.QueryString["processoSeletivoUID"]);
                int folhaPagamentoUID = int.Parse(e.CommandArgument.ToString());

                int monitorUID = int.Parse(hdMonitorUID.Value);
                MonitorBO monitorBO = new MonitorBO();
                Monitor monitor = monitorBO.GetMonitor(monitorUID);

                if (monitor.remunerada.HasValue && monitor.remunerada.Value)
                {
                    BolsistaBO bolsistaBO = new BolsistaBO();

                    if (String.IsNullOrEmpty(hdBolsistaUID.Value))
                        bolsistaBO.HomologarBolsistas(null, processoSeletivoUID, folhaPagamentoUID, hdSemestre.Value);
                    else
                        bolsistaBO.HomologarBolsistas(int.Parse(hdBolsistaUID.Value), processoSeletivoUID, folhaPagamentoUID, hdSemestre.Value);

                }
                else
                {
                    Servidor orientador = new PessoaBO().GetServidorUnico(monitor.orientadorUID.Value);
                    Servidor coordenador = new PessoaBO().GetServidorUnico(monitor.coordenadorUID.Value);
                    monitor.ativo = true;
                    monitorBO.Atualizar(monitorUID, monitor, orientador, coordenador);
                }
                hdMonitorUID.Value = String.Empty;
                hdBolsistaUID.Value = String.Empty;
                grdInscritos.DataBind();
            }
        }

        private int GetProcessoSeletivoUID()
        {
            return int.Parse(Request.QueryString["processoSeletivoUID"]);
        }

        public String VerificaHomologacao(int bolsistaUID, int monitorUID)
        {
            String homologar = "";
            
            BolsistaFolhaPagamentoBO bfpBO = new BolsistaFolhaPagamentoBO();

            MonitoriaController controller = new MonitoriaController();
            
            
            Monitor monitor = new MonitorBO().GetMonitor(monitorUID);
            if (monitor.status.HasValue)
            {
                if (monitor.status.Value)
                {

                    BolsistaFolhaPagamento bfp = controller.GetBolsistaFolhaPagamentoPorBolsista(bolsistaUID);
                    if (bfp != null)
                    {
                        if (bfp.FolhaPagamento.ProcessoSeletivo.InstrumentoSelecao.situacao.HasValue)
                        {
                            if (bfp.FolhaPagamento.ProcessoSeletivo.InstrumentoSelecao.situacao.Value)
                            {
                                if (bfp.ativo)
                                {
                                    homologar = "";
                                }
                                else
                                {
                                    homologar = "Homologar";
                                }
                            }
                            else
                            {
                                homologar = "Homologar";
                            }
                        }
                        else
                        {
                            homologar = "Homologar";
                        }
                    }
                    else
                    {
                        homologar = "Homologar";
                    }
                } 
                else 
                {
                    homologar = "";
                }
            }

            if (!monitor.remunerada.Value)
            {
                if (monitor.ativo.Value)
                    homologar = "";
                else
                    homologar = "Homologar";
            }
         
            return homologar;         
        }

        
    }


}