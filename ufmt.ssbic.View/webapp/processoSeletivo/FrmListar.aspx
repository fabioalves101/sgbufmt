﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true" CodeBehind="FrmListar.aspx.cs" Inherits="ufmt.ssbic.View.webapp.processoSeletivo.FrmListar" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
  <ContentTemplate>

    <div style="padding:5px;text-align:left;">
        <h1><span class="allcaps"><asp:Label ID="Label1" runat="server"></asp:Label>

            </span></h1>
        <div id="containerMensagens">
                <asp:Panel ID="PanelMsgResultado" runat="server" Visible="false">
                    <asp:Label ID="lblMsgResultado" runat="server"></asp:Label>
                </asp:Panel>
        </div>
       
                                <fieldset style="padding: 10px">
                                    <legend>
                                        <img alt="Filtrar Registros" class="style2" 
                                            src="../../images/ico_filters.png" />
                                        Filtar Registros</legend>
                                         <asp:Panel ID="PanelFiltro" runat="server">
                                    &nbsp;<asp:TextBox ID="txtCriterio" runat="server" Width="503px" 
                                        EnableViewState="False" ViewStateMode="Disabled"></asp:TextBox>
                                    
       <asp:LinkButton ID="lnkConsultar" runat="server" CssClass="minibutton" onclick="lnkConsultar_Click">
              
                                       
    <span>

                                             
                                             Consultar



    </span>
                                             

                                           

                                             

                                             
</asp:LinkButton>

<asp:LinkButton ID="lnkMostrarTodos" runat="server" CssClass="minibutton" onclick="lnkMostrarTodos_Click">

                          
    <span>
        Mostrar Todos

    </span>
                                             
</asp:LinkButton>

                                             

</asp:Panel>


                                </fieldset>
                            
<asp:Panel ID="PanelAcoes" runat="server" style="padding:10px">
    
    <div style="clear:both"></div>
    

 <div class="addbutton" style="float:left;margin-right:5px"> 
     <asp:HyperLink ID="lnkNovoRegistro" runat="server" CssClass="minibutton">

    <span>

        

     


        

     <img alt="" src="../../images/add.png" border="0" />&nbsp;&nbsp; Adicionar Registro</span>




        </asp:HyperLink>
 </div>                                           

                                            
<div class="addbutton" style="float:left;" runat="server" ID="divExcluir"> 
<asp:LinkButton ID="lnkExcluir" runat="server" onclick="lnkExcluirSelecionados_Click" CssClass="minibutton" OnClientClick="javascript:return window.confirm('Tem certeza que deseja remover este registro?');">




    <span>
        

       

    <img src="../../images/delete2.png" 
                                                style="width: 16px; height: 16px" alt="Excluir selecionados"
                                                runat="server" id="imgExcluir" />&nbsp; 
    
Excluir selecionados


        
    </span>
    




</asp:LinkButton>
    
</div>

<div style="float:left;width:300px;margin-left:10px">
        <asp:Label ID="lblResultadoConsulta" runat="server"></asp:Label>
        &nbsp;<asp:Label ID="lblTotalRegistros" runat="server" CssClass="ArialBlack16"></asp:Label>
    </div>

<div style="clear:both" />
</asp:Panel>

        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
            AutoGenerateColumns="False" 
            OnPageIndexChanging="GridView1_PageIndexChanging" Width="100%" 
            onrowcreated="GridView1_RowCreated" CssClass="mGrid" 
            onrowcommand="GridView1_RowCommand">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Label ID="lblID" runat="server" Text='<%# Eval("processoSeletivoUID") %>' 
                            Visible="false"></asp:Label>
                        <asp:CheckBox ID="chkRegistro" runat="server" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="5%" />
                </asp:TemplateField>



                <asp:TemplateField HeaderText="Título" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>

                        &nbsp;<asp:HyperLink ID="lnkAtualizarRegistro" runat="server" 
                            ImageUrl="../../images/edit_20.png" 
                            NavigateUrl='<%# "FrmEditar.aspx?area=Processo Seletivo&registroUID=" + Eval("processoSeletivoUID") + "&instrumentoSelecaoUID=" + Eval("instrumentoSelecaoUID") %>'>Atualizar Registro</asp:HyperLink>
                        &nbsp;&nbsp;

                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="detalhes" Text='<%# Eval("titulo") %>' CommandArgument='<%# Eval("processoSeletivoUID") %>' />
                        <br />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="25%" />
                </asp:TemplateField>


                <asp:TemplateField HeaderText="Candidatos" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkGridCandidato" runat="server" CommandName="adicionarBolsista" 
                            Text="Homologar" CommandArgument='<%# Eval("processoSeletivoUID") %>' />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="10%" />
                </asp:TemplateField>



                <asp:TemplateField HeaderText="Instrumento de Seleção">
                    <ItemTemplate>
                                                <asp:Label ID="lblGridInsSelecao" runat="server" 
                            Text='<%# Eval("instrumentoSelecao") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" Width="15%" />
                </asp:TemplateField>



                <asp:TemplateField HeaderText="Data Aplicação">
                    <ItemTemplate>
                                                <asp:Label ID="lblGridBolsa" runat="server" 
                            Text='<%# Eval("dataAplicacao", "{0:dd/MM/yyyy}") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" Width="10%" />
                </asp:TemplateField>


                <asp:TemplateField HeaderText="Data Resultado">
                    <ItemTemplate>
                        <asp:Label ID="lblGridTipo" runat="server" 
                            Text='<%# Eval("dataResultado", "{0:dd/MM/yyyy}") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" Width="10%" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Inscrição">
                    <ItemTemplate>
                        <asp:HyperLink ID="hpInscricao" runat="server" NavigateUrl='<%# Eval("processoSeletivoUID", "FrmInscricao.aspx?processoSeletivoUID={0}") %>'>Gerenciar</asp:HyperLink>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" Width="5%" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Vagas por curso">
                    <ItemTemplate>
                        <asp:HyperLink ID="hpCursos" runat="server" NavigateUrl='<%# Eval("processoSeletivoUID", "../cursosprocessoseletivo/FrmListar.aspx?processoSeletivoUID={0}") %>'>Gerenciar</asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerTemplate>
                <asp:Panel ID="Panel1" runat="server" DefaultButton="cmdIr">
                    <div style="text-align: left">
                        <asp:ImageButton ID="cmdFirst" runat="server" CausesValidation="False" 
                            CommandArgument="First" CommandName="Page" 
                            ImageUrl="../../images/i_primeira_pagina.gif" ToolTip="Ir para a primeira página" />
                        &nbsp;<asp:ImageButton ID="cmdPrev" runat="server" CausesValidation="False" 
                            CommandArgument="Prev" CommandName="Page" 
                            ImageUrl="../../images/i_pagina_anterior.gif" ToolTip="Ir para a página anterior" />
                        &nbsp;&nbsp;&nbsp;&nbsp;<asp:ImageButton ID="cmdNext" runat="server" 
                            CausesValidation="False" CommandArgument="Next" CommandName="Page" 
                            ImageUrl="../../images/i_proxima_pagina.gif" ToolTip="Ir para a próxima página" />
                        &nbsp;<asp:ImageButton ID="cmdLast" runat="server" CausesValidation="False" 
                            CommandArgument="Last" CommandName="Page" 
                            ImageUrl="../../images/i_ultima_pagina.gif" ToolTip="Ir para a última página" />
                        &nbsp;&nbsp; Ir para a página
                        Ir para a página
                        <asp:TextBox ID="txtPagina" runat="server" 
                            Text="<% # GridView1.PageIndex + 1 %>" ValidationGroup="grid" Width="29px"></asp:TextBox>
                        &nbsp;<asp:ImageButton ID="cmdIr" runat="server" ImageUrl="../../images/btnIr.gif" 
                            OnClick="cmdIr2_Click" ValidationGroup="grid" />
                        <b>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                            ControlToValidate="txtPagina" Display="Dynamic" 
                            ErrorMessage="* Entre com um valor para continuar" Font-Bold="False" 
                            ValidationGroup="grid"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                            ControlToValidate="txtPagina" Display="Dynamic" 
                            ErrorMessage="* Digite somente números para continuar" Font-Bold="False" 
                            ValidationExpression="^[0-9]+$" ValidationGroup="grid">* Digite somente números* Digite somente números</asp:RegularExpressionValidator>
                        </b>
                    </div>
                    <div style="text-align: left">
                        Página&nbsp;Página&nbsp;<asp:Label ID="lblTotalPaginaAtual" runat="server" 
                            Font-Bold="True" Text="<%# GridView1.PageIndex + 1 %>"></asp:Label>
                        &nbsp;de&nbsp;&nbsp;de&nbsp;<asp:Label ID="lblTotalPaginas" runat="server" 
                            Text="<% # GridView1.PageCount %>"></asp:Label>
                        &nbsp;
                    </div>
                </asp:Panel>
            </PagerTemplate>
            <EmptyDataTemplate>
                <div 
                    style="text-align: center; padding-top: 20px; padding-bottom: 20px; font-size: 18px;
                                                    font-family: Arial Narrow; font-weight: bold">
                    <asp:Label ID="lblMsgNenhumRegistro" runat="server" 
                        Text="Nenhum registro localizado no critério da consulta."></asp:Label>
                </div>
            </EmptyDataTemplate>
        </asp:GridView>


               <asp:modalpopupextender 
                ID="ModalPopupExtender1"
                BehaviorID="behavior"
                runat="server"
                TargetControlID="lnkModal"
                PopupControlID="PanelDetalhes"
                BackgroundCssClass="modalBackground"
                DropShadow="True"
                DynamicServicePath="" 
                Enabled="True"
        
        />


        <asp:HyperLink ID="lnkModal" runat="server" Visible="true" Text=""  />

 <asp:Panel ID="PanelDetalhes" runat="server" CssClass="modalPopup">

                   <asp:UpdatePanel ID="UpdatePanelConsultarAluno" runat="server">
               <ContentTemplate>   

          <div id="ContainerDetalhes" style="width:800px">
          <div id="MenuDetalhes" style="background-color:White;padding-left:5px;padding-bottom:5px;height:40px">
            <div style="width:650px; float:left; height: 32px;">
                <h2 style="height: 35px"><asp:Label ID="lblTituloModal" runat="server"></asp:Label></h2>
            </div>
            <div style="float:left;width:130px;text-align:right; padding-top:10px;">
                <asp:ImageButton ID="ImageButton1" runat="server" 
                    ImageUrl="../../images/close24.png" OnclientClick="$find('behavior').hide(); return false;" ToolTip="Fechar Janela" ImageAlign="AbsMiddle" />
                &nbsp;<asp:LinkButton ID="LinkButtonFechar" runat="server" Visible="true" 
                    Text="Fechar" onclick="LinkButtonFechar_Click"   />
            </div>
          </div>
           <div id="corpoDetalhes" style="border:1px solid #cccccc;height:450px;padding-top:10px;background-color:#f7f7f7">
            



               <asp:Panel ID="PanelModalDetalhes" runat="server" Visible="false">
                    <table class="Largura100Porcento">
                   <tr>
                       <td class="Largura150">
                           <asp:Label ID="Label9" runat="server" style="font-weight: 700" 
                               Text="Instrumento de Seleção:"></asp:Label>
                       </td>
                       <td>
                           <asp:Label ID="LabelDetalhe1" runat="server"></asp:Label>
                       </td>
                   </tr>
                   <tr>
                       <td class="Largura150">
                           <asp:Label ID="Label10" runat="server" style="font-weight: 700" Text="Título:"></asp:Label>
                       </td>
                       <td>
                           <asp:Label ID="LabelDetalhe2" runat="server"></asp:Label>
                       </td>
                   </tr>
                   <tr>
                       <td class="Largura150">
                           <asp:Label ID="Label6" runat="server" style="font-weight: 700" 
                               Text="Data Aplicação:"></asp:Label>
                       </td>
                       <td>
                           <asp:Label ID="LabelDetalhe3" runat="server"></asp:Label>
                       </td>
                   </tr>
                   <tr>
                       <td class="Largura150">
                           <asp:Label ID="Label7" runat="server" style="font-weight: 700" 
                               Text="Data Resultado:"></asp:Label>
                       </td>
                       <td>
                           <asp:Label ID="LabelDetalhe4" runat="server"></asp:Label>
                       </td>
                   </tr>
               </table>
                </asp:Panel>

               <asp:Panel ID="PanelModalAluno" runat="server" Visible="false" style="padding:10px" DefaultButton="btBuscar">
                   
                   
                    <div id="divModalConsultaAluno">
                        <strong>Localizar Aluno por
                        <asp:RadioButtonList ID="rblTipoConsultaAluno" runat="server" 
                            RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="True" 
                            onselectedindexchanged="rblTipoConsultaAluno_SelectedIndexChanged">
                            <asp:ListItem Selected="True" Value="0">Nome</asp:ListItem>
                            <asp:ListItem Value="1">Matrícula</asp:ListItem>
                        </asp:RadioButtonList>
                        &nbsp;<asp:RegularExpressionValidator ID="REVAluno" runat="server" 
                            ControlToValidate="txtAluno" Display="Dynamic" 
                            ErrorMessage="* Digite somente números" ForeColor="Red" 
                            ValidationExpression="^[0-9]+$" Enabled="false">*Digite somente números</asp:RegularExpressionValidator>
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server" 
                            AssociatedUpdatePanelID="UpdatePanelConsultarAluno">
                            <ProgressTemplate>
                                <div>
                                    &nbsp;<asp:Image ID="Image6" runat="server" 
                                        ImageUrl="~/images/loading_circle.gif" />
                                    &nbsp;Consultando...
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        </strong><br />
                        <asp:TextBox ID="txtAluno" runat="server" Width="497px"></asp:TextBox>




                        &nbsp;<asp:ImageButton ID="btBuscar" runat="server" CssClass="imgBtBuscar" 
                            ImageUrl="~/media/images/busca.png" onclick="btBuscar_Click" />


                        <asp:HiddenField ID="hddProcessoSeletivoUID" runat="server" />

                    </div>
     
<asp:Panel ID="PanelResultadoConsulta" runat="server" Visible="false">    
    <fieldset>
    <legend>Alunos Localizados (<asp:Label ID="lblTotalAlunos" runat="server" 
            style="font-weight: 700"></asp:Label>)</legend>
    <asp:GridView ID="gdrAlunos" runat="server" CssClass="mGrid"
         PagerStyle-CssClass="pgr"
         AlternatingRowStyle-CssClass="alt" AllowPaging="True" 
        EmptyDataText="Vazio" AutoGenerateColumns="False" PageSize="5" 
        onpageindexchanging="gdrAlunos_PageIndexChanging" onrowcommand="gdrAlunos_RowCommand"
         >
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>

        <Columns>
            
            <asp:TemplateField HeaderText="Selecionar" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="60px">
                <ItemTemplate>
                    <asp:LinkButton CssClass="minibutton" ID="LinkButton1" runat="server" CausesValidation="False" 
                                CommandName="adicionar" CommandArgument='<%# Eval("Matricula") + "," + Eval("Nome") %>'>
                            <span>
                                <img src="../../images/select.png" alt="" />
                                Selecionar
                            </span>
                            </asp:LinkButton>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" Width="60px" />
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Matricula" SortExpression="Matricula">

                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Matricula") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Nome" SortExpression="Nome">

                <ItemTemplate>
                    <asp:Label ID="lblNome" runat="server" Text='<%# Eval("Nome") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CPF" SortExpression="CPF">
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("CPF") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Tipo" SortExpression="Tipo">
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("Tipo") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>

        <EmptyDataTemplate>
        <div style="padding:20px;text-align:center">
            Nenhum aluno localizado no critério da consulta.&nbsp; Verifique os dígitos e tente 
            novamente.
        </div>
        </EmptyDataTemplate>

        <PagerStyle CssClass="pgr" />

    </asp:GridView>

</fieldset>
</asp:Panel>

<asp:Panel ID="PanelAlunosSelecionados" runat="server" Visible="false">
    <fieldset>
<legend>Alunos Selecionados (<asp:Label ID="lblTotalAlunosSelecionados" 
        runat="server" style="font-weight: 700"></asp:Label>)</legend>
    
    
    <div style="float:left;width:590px">
    <asp:ListBox ID="lbAlunosSelecionados" runat="server" Height="100px" 
        Width="590px"></asp:ListBox>

    </div>
    
    <div style="float:left;margin-left:10px">
    <div>
        <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" 
            CssClass="minibutton" onclick="LinkButton2_Click" Visible="False">
            <span style="width:130px">
                <img src="../../images/select.png" alt="" />
                Confirmar Alunos
            </span>
        </asp:LinkButton>
     </div>
     <div>
         <asp:LinkButton ID="LinkButton3" runat="server" CausesValidation="False" 
             CssClass="minibutton" onclick="LinkButton3_Click">
                <span style="width:130px">
                    <img src="../../images/cancel.png" alt="" />
                    Excluir Selecionados
                </span>
         </asp:LinkButton>
     </div>
</div>
</fieldset>
</asp:Panel>




</asp:Panel>


           </div> 


          </div>   
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>



    </div>

  </ContentTemplate>
  </asp:UpdatePanel>


</asp:Content>
