﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Web.Configuration;
using ufmt.ssbic.View.session;

namespace ufmt.ssbic.View.webapp.processoSeletivo
{
    public partial class FrmEditarDataFormularioMonitoria : VerificaSession
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                String strDataBloqueio = this.GetValue("dataBloqueio");
                String strDataInicio = this.GetValue("dataInicio");


                if (!String.IsNullOrEmpty(strDataBloqueio) && !String.IsNullOrEmpty(strDataInicio))
                {
                    DateTime dataInicio = DateTime.Parse(strDataInicio);
                    DateTime dataBloqueio = DateTime.Parse(strDataBloqueio);

                    txtAbertura.Text = dataInicio.ToShortDateString();
                    txtFechamento.Text = dataBloqueio.ToShortDateString();
                }
                else
                {
                    pnlForm.Visible = false;

                    lblMessage.Text = "Valor de inicio e fim indefinido. Comunique o administrador do sistema.";
                    pnlMessage.Visible = true;
                }
            }
        }

        public string GetValue(string SettingName)
        {
            //Se For NULL, ou seja, nao existir retorna "Nunhum valor encontrado"
            return ConfigurationManager.AppSettings[SettingName]
                ?? null;
        }
        public void SetValue(string SettingName, string value)
        {
            try
            {
                Configuration config = WebConfigurationManager.OpenWebConfiguration("~");

                //Se não existir vai ser adicionada automaticamente
                if (config.AppSettings.Settings[SettingName] == null)
                    throw new Exception("Configuração não encontrada");
                else
                {
                    //Seta o valor
                    config.AppSettings.Settings[SettingName].Value = value;
                    //Salva a configuração
                    config.Save();
                    //Atualiza o appSettings
                    ConfigurationManager.RefreshSection("appSettings");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void btSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validar();
                
                String strDataFechamento = DateTime.Parse(txtFechamento.Text).ToString();
                String strDataAbertura = DateTime.Parse(txtAbertura.Text).ToString();

                this.SetValue("dataBloqueio", strDataFechamento);
                this.SetValue("dataInicio", strDataAbertura);

                String msg = "O sistema será reiniciado para que as alterações realizadas tenham efeito.";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('" + msg + "'); window.location = '../../Logout.aspx';", true);
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                pnlMessage.Visible = true;
            }
        }

        private void Validar()
        {
            try
            {
                DateTime dataInicio;
                if (!DateTime.TryParse(txtAbertura.Text, out dataInicio))
                {
                    throw new Exception("A data de abertura não está no formato correto( dd/MM/yyyy ).");
                }

                DateTime dataFim;
                if (!DateTime.TryParse(txtFechamento.Text, out dataFim))
                {
                    throw new Exception("A data de fechamento não está no formato correto( dd/MM/yyyy ).");
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}