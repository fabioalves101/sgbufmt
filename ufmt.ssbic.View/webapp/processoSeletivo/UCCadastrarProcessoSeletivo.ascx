﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCCadastrarProcessoSeletivo.ascx.cs" Inherits="ufmt.ssbic.View.webapp.processoSeletivo.UCCadastrarProcessoSeletivo" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Panel ID="PanelFormulario" runat="server">


<div class="containerFormulario">
<div id="containerValidacao">
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" 
        HeaderText="Para continuar, preencha corretamente os campos abaixo:" 
        CssClass="ResumoErro" />
</div>
    

         <table class="Largura100Porcento">
        <tr>
            <td class="Largura150">
            <asp:Label ID="Label3" runat="server" 
            style="font-weight: 700" Text="Título:"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="txtTitulo" ErrorMessage="Título" 
                    ForeColor="Red">*</asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:TextBox ID="txtTitulo" runat="server" Width="487px"></asp:TextBox>
            </td>
        </tr>
             <tr>
                 <td class="Largura150">
                     <asp:Label ID="Label4" runat="server" style="font-weight: 700" 
                         Text="Descrição:"></asp:Label>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                         ControlToValidate="txtDescricao" ErrorMessage="Descrição" ForeColor="Red">*</asp:RequiredFieldValidator>
                 </td>
                 <td>
                     <asp:TextBox ID="txtDescricao" runat="server" Width="487px" Height="91px" 
                         TextMode="MultiLine"></asp:TextBox>
                 </td>
             </tr>
             <tr>
                 <td class="Largura150">
                     <asp:Label ID="Label5" runat="server" style="font-weight: 700" 
                         Text="Data de Início:"></asp:Label>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                         ControlToValidate="txtDataAplicacao" ErrorMessage="Data de Aplicação" 
                         ForeColor="Red">*</asp:RequiredFieldValidator>
                 </td>
                 <td>
                     <asp:TextBox ID="txtDataAplicacao" runat="server" Width="92px"></asp:TextBox>

                                 <asp:CalendarExtender ID="txtDataAplicacao_CalendarExtender" runat="server" 
                                    Enabled="True" TargetControlID="txtDataAplicacao" Format="dd/MM/yyyy" PopupButtonID="imgCalendarioData1" >
                                </asp:CalendarExtender>

                                 <asp:Image ID="imgCalendarioData1" runat="server" 
                         ImageUrl="~/images/Calendar_scheduleHS.png" />

                                 <asp:MaskedEditValidator ID="MEVData1" runat="server" 
                                      ControlExtender="txtDataAplicacao_MaskedEditExtender" ControlToValidate="txtDataAplicacao" 
                                      Display="Dynamic" ForeColor="Red" 
                                      InvalidValueMessage="Data no formato inválido" IsValidEmpty="True" 
                                      CssClass="failureNotification"></asp:MaskedEditValidator>

                                 <asp:MaskedEditExtender ID="txtDataAplicacao_MaskedEditExtender" runat="server" 
                                       CultureDateFormat="pt-br" Enabled="True" Mask="99/99/9999" MaskType="Date" 
                                       TargetControlID="txtDataAplicacao">
                                 </asp:MaskedEditExtender> 

                 </td>
             </tr>
             <tr>
                 <td class="Largura150">
                     <asp:Label ID="Label6" runat="server" style="font-weight: 700" 
                         Text="Data Final:"></asp:Label>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                         ControlToValidate="txtDataResultado" ErrorMessage="Data do Resultado" 
                         ForeColor="Red">*</asp:RequiredFieldValidator>
                 </td>
                 <td>

                     <asp:TextBox ID="txtDataResultado" runat="server" Width="92px"></asp:TextBox>

                     <asp:Image ID="imgCalendarioData2" runat="server" 
                         ImageUrl="~/images/Calendar_scheduleHS.png" />

                     <asp:MaskedEditValidator ID="MEVData2" runat="server" 
                         ControlExtender="txtDataResultado_MaskedEditExtender" 
                         ControlToValidate="txtDataResultado" CssClass="failureNotification" 
                         Display="Dynamic" ForeColor="Red"
                         InvalidValueMessage="Data no formato inválido" IsValidEmpty="True">
                      </asp:MaskedEditValidator>


                    <asp:MaskedEditExtender ID="txtDataResultado_MaskedEditExtender" runat="server" 
                        CultureDateFormat="pt-br" Enabled="True" Mask="99/99/9999" MaskType="Date" 
                        TargetControlID="txtDataResultado">
                    </asp:MaskedEditExtender>
                     <asp:CalendarExtender ID="CalendarExtender_txtDataResultado" runat="server"
                     Enabled="True" TargetControlID="txtDataResultado" Format="dd/MM/yyyy" PopupButtonID="imgCalendarioData2">
                     </asp:CalendarExtender>

                    



                 </td>
             </tr>
        </table>

</div>

</asp:Panel>
