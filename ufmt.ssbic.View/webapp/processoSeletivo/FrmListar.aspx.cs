﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;
using ufmt.ssbic.View.session;
using System.Collections;
using System.Globalization;

namespace ufmt.ssbic.View.webapp.processoSeletivo
{
    public partial class FrmListar : VerificaSession
    {
        public String Criterio { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                BolsaBO objBO = new BolsaBO();
                string programaUID = Session["programaUID"].ToString();
                string proreitoria = objBO.GetPrograma(int.Parse(programaUID));
                string area = "Processo Seletivo";
                lblTotalAlunosSelecionados.Text = "0";

                lblResultadoConsulta.Text = "<b>Mostrando todos</b>";
                PreecherGridView(Request.QueryString["instrumentoSelecaoUID"].ToString(), txtCriterio.Text, false);
                lnkNovoRegistro.NavigateUrl = "FrmCadastrar.aspx?area=" + area + "&instrumentoSelecaoUID=" + Request.QueryString["instrumentoSelecaoUID"];
                Label1.Text = "Processo Seletivo &raquo;" + proreitoria;
                verificaItens();
            }
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {


            GridView1.PageIndex = e.NewPageIndex;
            PreecherGridView(Request.QueryString["instrumentoSelecaoUID"].ToString(), txtCriterio.Text, false);

        }

        protected void PreecherGridView(String instrumentoSelecaoUID, String criterio, bool visible)
        {

            PanelMsgResultado.Visible = visible;

            ProcessoSeletivoBO objBO = new ProcessoSeletivoBO();
            List<vwProcessoSeletivo> registros = null;


            //Consulta
            if (!String.IsNullOrEmpty(criterio))
            {
                registros = objBO.FindRegistros(int.Parse(instrumentoSelecaoUID), criterio);


            }
            //Listagem padrão
            else
            {

                registros = objBO.GetRegistros(int.Parse(instrumentoSelecaoUID));


            }

            GridView1.DataSource = registros;
            lblTotalRegistros.Text = registros.Count.ToString() + " registro(s) disponível(is)";
            GridView1.DataBind();


            if (registros.Count > 0)
            {

                ImageButton btnPrev = (ImageButton)GridView1.BottomPagerRow.FindControl("cmdPrev");
                if (GridView1.PageIndex == 0)
                    btnPrev.Enabled = false;
                else
                    btnPrev.Enabled = true;

                divExcluir.Visible = true;

            }
            else
            {

                divExcluir.Visible = false;

            }


            if (Session["permissao"].ToString().Contains("MONITORIA"))
            {
                GridView1.Columns[6].Visible = true;
            }
            else
            {
                GridView1.Columns[6].Visible = false;
            }


        }

        protected void cmdIr2_Click(object sender, ImageClickEventArgs e)
        {
            TextBox txt = (TextBox)GridView1.BottomPagerRow.FindControl("txtPagina");
            GridView1.PageIndex = (Convert.ToInt32(txt.Text) - 1);
            PreecherGridView(Request.QueryString["instrumentoSelecaoUID"].ToString(), txtCriterio.Text, false);

        }

        protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                /*
                HyperLink lnkTituloResolucao = (HyperLink)e.Row.FindControl("lnkTituloResolucao");
                HyperLink lnkAtualizarRegistro = (HyperLink)e.Row.FindControl("lnkAtualizarRegistro");
                Label lblAssunto = (Label)e.Row.FindControl("lblAssunto");
               
                lnkAtualizarRegistro.NavigateUrl = "FrmEditar.aspx?registroUID=" + (int?)DataBinder.Eval(e.Row.DataItem, "bolsaUID") + "&proreitoriaUID=" + (int?)DataBinder.Eval(e.Row.DataItem, "proreitoriaUID");
                */

            }



        }

        protected void lnkExcluirSelecionados_Click(object sender, EventArgs e)
        {
            bool resultado = false;
            ProcessoSeletivoBO objBO = new ProcessoSeletivoBO();

            try
            {

                for (int i = 0; i <= GridView1.Rows.Count - 1; i++)
                {
                    CheckBox chk = new CheckBox();
                    chk = (CheckBox)GridView1.Rows[i].FindControl("chkRegistro");
                    Label lblID = (Label)GridView1.Rows[i].FindControl("lblID");

                    if (chk.Checked)
                    {
                        resultado = objBO.Excluir(int.Parse(lblID.Text));
                    }

                }

                lblMsgResultado.Text = "Registro(s) excluído(s) com sucesso.";
                PanelMsgResultado.CssClass = "success";

            }
            catch (Exception err)
            {

                lblMsgResultado.Text = "Não foi possível excluir o(s) registro(s) selecionado(s).  Por favor, entre em contato com o administrador do sistema.<br />" + err.Message.ToString();
                PanelMsgResultado.CssClass = "error";

            }

            PanelMsgResultado.Visible = true;
            PreecherGridView(Request.QueryString["instrumentoSelecaoUID"].ToString(), txtCriterio.Text, true);
        }


        protected void lnkConsultar_Click(object sender, EventArgs e)
        {

            PreecherGridView(Request.QueryString["instrumentoSelecaoUID"].ToString(), txtCriterio.Text, false);

        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {


                if (e.CommandName == "detalhes")
                {

                    ProcessoSeletivoBO objBO = new ProcessoSeletivoBO();
                    vwProcessoSeletivo ps = objBO.GetProcessoSeletivo(int.Parse(e.CommandArgument.ToString()));
                    LabelDetalhe1.Text = ps.instrumentoSelecao;
                    LabelDetalhe2.Text = ps.titulo;
                    LabelDetalhe3.Text = String.Format("{0:dd/MM/yyyy}", ps.dataAplicacao);
                    LabelDetalhe4.Text = String.Format("{0:dd/MM/yyyy}", ps.dataResultado);
                    PanelModalDetalhes.Visible = true;
                    PanelModalAluno.Visible = false;
                    lblTituloModal.Text = "Detalhes do Registro";
                }

                if (e.CommandName == "adicionarBolsista")
                {

                    Response.Redirect("~/webapp/folha/FrmListar.aspx?processoSeletivoUID=" + e.CommandArgument.ToString());

                    //BolsistaBO objBO = new BolsistaBO();
                    //List<vwBolsista> bolsistas = objBO.GetRegistros(int.Parse(e.CommandArgument.ToString()));

                    //if (bolsistas.Count > 0)
                    //{

                    //    lbAlunosSelecionados.DataSource = bolsistas;
                    //    lbAlunosSelecionados.DataTextField = "detalheRegistro";
                    //    lbAlunosSelecionados.DataValueField = "bolsistaUID";
                    //    lbAlunosSelecionados.DataBind();
                    //    PanelAlunosSelecionados.Visible = true;
                    //    lblTotalAlunosSelecionados.Text = bolsistas.Count.ToString();
                    //    LinkButton3.OnClientClick = "javascript: return window.confirm('Tem certeza que deseja remover esse bolsista?')";
                    //}
                    //else {

                    //    PanelAlunosSelecionados.Visible = false;
                    //    LinkButton3.OnClientClick = "javascript:alert('Nenhum aluno disponível');return false;";
                    //}

                    //ModalPopupExtender1.Show();
                    //PanelModalDetalhes.Visible = false;
                    //PanelModalAluno.Visible = true;
                    //lblTituloModal.Text = "Adicionar Candidatos a Bolsa";
                    //hddProcessoSeletivoUID.Value = e.CommandArgument.ToString();
                    //PanelResultadoConsulta.Visible = false;
                    
                  }


                ModalPopupExtender1.Show();
                PanelMsgResultado.Visible = false;

        }

        protected void lnkMostrarTodos_Click(object sender, EventArgs e)
        {
            PreecherGridView(Request.QueryString["instrumentoSelecaoUID"].ToString(), null, false);
            txtCriterio.Text = String.Empty;
        }

        protected void btBuscar_Click(object sender, ImageClickEventArgs e)
        {
            PreencherGridAluno();
        }

        protected void gdrAlunos_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            if (e.CommandName == "adicionar")
            {
                
                
                
                string[] dados = e.CommandArgument.ToString().Split(',');
                BolsistaBO objBO = new BolsistaBO();
                Bolsista bolsista = new Bolsista();
                BolsistaProcessoSeletivo bps = new BolsistaProcessoSeletivo();


                if (objBO.VerificaRGA(dados[0], int.Parse(hddProcessoSeletivoUID.Value)) == 0)
                {
                    bolsista.registroAluno = dados[0];
                    bps.processoSeletivoUID = int.Parse(hddProcessoSeletivoUID.Value);
                    bps.situacao = 0;
                    lbAlunosSelecionados.Items.Add(new ListItem(dados[0] + " | " + dados[1], objBO.Salvar(bolsista, bps).ToString()));

                }

                /*
                if(!lbAlunosSelecionados.Items.Contains(new ListItem(dados[0] + " | " + dados[1], dados[0]))){

                }*/
                ModalPopupExtender1.Show();
                lblTotalAlunosSelecionados.Text = lbAlunosSelecionados.Items.Count.ToString();
                verificaItens();
            }
        }

        protected void gdrAlunos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gdrAlunos.PageIndex = e.NewPageIndex;
            PreencherGridAluno();
        }


        protected void PreencherGridAluno() {

            AlunoBO objBO = new AlunoBO();
            int totalRegistros = 0;
            List<vwAluno> registros = null;
            if (rblTipoConsultaAluno.SelectedValue.Equals("0"))
                registros = objBO.FindRegistros(txtAluno.Text, 0);
            else
                registros = objBO.FindRegistros(null, decimal.Parse(txtAluno.Text));

            gdrAlunos.DataSource = registros;
            gdrAlunos.DataBind();
            totalRegistros = registros.Count;
            PanelResultadoConsulta.Visible = true;
            PanelAlunosSelecionados.Visible = true;
            PanelResultadoConsulta.Visible = true;
            ModalPopupExtender1.Show();
            
            lblTotalAlunos.Text = totalRegistros.ToString();
            


        }

        protected void LinkButton3_Click(object sender, EventArgs e)
        {


            if (lbAlunosSelecionados.SelectedItem != null)
            {

                    string bolsistaUID = lbAlunosSelecionados.SelectedItem.Value;
                    lbAlunosSelecionados.Items.Remove(lbAlunosSelecionados.SelectedItem);
                    lblTotalAlunosSelecionados.Text = lbAlunosSelecionados.Items.Count.ToString();
                    BolsistaBO objBO = new BolsistaBO();
                    objBO.Excluir(int.Parse(bolsistaUID), int.Parse(hddProcessoSeletivoUID.Value));

            }
            ModalPopupExtender1.Show();
            verificaItens();
        }


        protected void verificaItens()
        {


            if (lbAlunosSelecionados.Items.Count == 0)
            {
                LinkButton2.OnClientClick = "javascript:alert('Nenhum aluno disponível');return false;";
                LinkButton3.OnClientClick = "javascript:alert('Nenhum aluno disponível');return false;";

            }
            else
            {

                LinkButton2.OnClientClick = "javascript: return window.confirm('Tem certeza que deseja adicionar esses alunos?')";
                LinkButton3.OnClientClick = "javascript: return window.confirm('Tem certeza que deseja remover esse bolsista?')";
            }


        }

        protected void LinkButton2_Click(object sender, EventArgs e)
        {

            /*
            BolsistaBO objBO = new BolsistaBO();
            Bolsista bolsista = new Bolsista();
            BolsistaProcessoSeletivo bps = new BolsistaProcessoSeletivo();

            for (int i = 0; i < lbAlunosSelecionados.Items.Count; i++) {

                bolsista.registroAluno = lbAlunosSelecionados.Items[i].Value;
                bps.processoSeletivoUID = int.Parse(hddProcessoSeletivoUID.Value);
                bps.situacao = 0;
                
                if(objBO.VerificaRGA(lbAlunosSelecionados.Items[i].Value) != 0 ) {

                    objBO.Salvar(bolsista, bps);
                }
                
            
            }

            
            lblMsgResultado.Text = "Registro(s) adicionados(s) com sucesso.";
            PanelMsgResultado.CssClass = "success";
            PanelMsgResultado.Visible = true;
            lbAlunosSelecionados.Items.Clear();
            txtAluno.Text = String.Empty;
            */
        }

        protected void LinkButtonFechar_Click(object sender, EventArgs e)
        {
            lbAlunosSelecionados.Items.Clear();
            txtAluno.Text = String.Empty;
            ModalPopupExtender1.Hide();
        }

        protected void rblTipoConsultaAluno_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if(rblTipoConsultaAluno.Items[1].Selected)
                REVAluno.Enabled = true;
            else
                REVAluno.Enabled = false;
            ModalPopupExtender1.Show();
        }

    }



}