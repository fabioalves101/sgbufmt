﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ufmt.ssbic.View.webapp.processoSeletivo
{
    public partial class UCCadastrarProcessoSeletivo : System.Web.UI.UserControl
    {
        public String Titulo { get; set; }
        public String Descricao { get; set; }
        public DateTime? DataInicio { get; set; }
        public DateTime? DataFinal { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void IndiceZero(object sender, EventArgs e)
        {
            DropDownList objDropDownList = (DropDownList)sender; //Cast no sender para DropDownList
            objDropDownList.Items.Insert(0, new ListItem("Selecione", "")); //Adiciona um novo Item
        }
              


        private void ReiniciarCadastro()
        {            
            PanelFormulario.Visible = true;
        }

        public void SetAtributos()
        {
            Titulo = txtTitulo.Text;
            Descricao = txtDescricao.Text;

            DateTime dataAplicacao;
            DateTime dataResultado;

            if (DateTime.TryParse(txtDataAplicacao.Text, out dataAplicacao))
                DataInicio = DateTime.Parse(txtDataAplicacao.Text);
            else
                DataInicio = null;

            if (DateTime.TryParse(txtDataResultado.Text, out dataResultado))
                DataFinal = DateTime.Parse(txtDataResultado.Text);
            else
                DataFinal = null;

        }

    }
}