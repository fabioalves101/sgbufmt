﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;
using ufmt.ssbic.View.session;

namespace ufmt.ssbic.View.webapp.processoSeletivo
{
    public partial class FrmEditar : VerificaSession
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                ProcessoSeletivoBO objBO = new ProcessoSeletivoBO();

                string proreitoriaUID = Session["programaUID"].ToString();
                string area = Request.QueryString["area"];
                lblTitulo.Text = area;
                string registroUID = Request.QueryString["registroUID"];
                vwProcessoSeletivo ps = objBO.GetProcessoSeletivo(int.Parse(registroUID));
                hddCodigoRegistro.Value = ps.processoSeletivoUID.ToString();
                txtDataAplicacao.Text = ps.dataAplicacao.ToString();
                txtDataResultado.Text = ps.dataResultado.ToString();
                txtDescricao.Text = ps.descricao;
                txtTitulo.Text = ps.titulo;

            }

        }

        protected void IndiceZero(object sender, EventArgs e)
        {
            DropDownList objDropDownList = (DropDownList)sender; //Cast no sender para DropDownList
            objDropDownList.Items.Insert(0, new ListItem("Selecione", "")); //Adiciona um novo Item
        }
        protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ProcessoSeletivoBO objBO = new ProcessoSeletivoBO();
                ProcessoSeletivo ps = new ProcessoSeletivo();
                ps.processoSeletivoUID = int.Parse(hddCodigoRegistro.Value);
                ps.instrumentoSelecaoUID = int.Parse(Request.QueryString["instrumentoSelecaoUID"].ToString());
                ps.titulo = txtTitulo.Text;
                ps.descricao = txtDescricao.Text;
                DateTime dataAplicacao;
                DateTime dataResultado;

                if (DateTime.TryParse(txtDataAplicacao.Text, out dataAplicacao))
                    ps.dataAplicacao = DateTime.Parse(txtDataAplicacao.Text);
                else
                    ps.dataAplicacao = null;

                if (DateTime.TryParse(txtDataResultado.Text, out dataResultado))
                    ps.dataResultado = DateTime.Parse(txtDataResultado.Text);
                else
                    ps.dataResultado = null;
                objBO.Salvar(ps, 0);

                lblMsgResultado.Text = "Registro atualizado com sucesso.";
                PanelMsgResultado.CssClass = "success";
                PanelFormulario.Visible = false;
                btnSalvar.Visible = false;

            }
            catch (Exception err)
            {

                lblMsgResultado.Text = "Não foi possível efetuar a operação.  Por favor, entre em contato com o administrador do sistema.<br />" + err.Message.ToString();
                PanelMsgResultado.CssClass = "error";
            }

            PanelMsgResultado.Visible = true;
        }

        protected void btnVoltar_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("FrmListar.aspx?instrumentoSelecaoUID=" + Request.QueryString["instrumentoSelecaoUID"].ToString());
        }
    }
}