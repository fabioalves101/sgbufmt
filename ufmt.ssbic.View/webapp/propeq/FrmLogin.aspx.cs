﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.sig.entity;

namespace ufmt.ssbic.View.webapp.propeq
{
    public partial class FrmLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Image ImageTopo = (Image)Page.Master.FindControl("ImageTopo");
                BolsaBO objBO = new BolsaBO();
                lblProReitoria.Text = objBO.GetPrograma(int.Parse(Session["programaUID"].ToString()));
                //lblIntro.Text = TextoIntro;
                ImageTopo.ImageUrl = "~/images/ssbic_topo.png";
            }
        }

        protected void btEntrar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                String cpf = login.Text;
                String passwd = senha.Text;

                int permissoesPropeq = 36;

                //REALIZA O LOGIN DO USUÁRIO
                AutenticacaoBO autenticacaoBO = new AutenticacaoBO();
                Usuario usuario = autenticacaoBO.Login(cpf, passwd);

                //GRAVA ID DO USUÁRIO EM SESSÃO
                Session["usuarioUID"] = usuario.UsuarioUID;

                //SOLICITA PERMISSÕES DO USUÁRIO LOGADO
                List<PermissaoUsuario> permissoesUsuario = autenticacaoBO.VerificaPermissoes(usuario.UsuarioUID);
                PermissaoUsuario permissaoUsuario = permissoesUsuario.FirstOrDefault<PermissaoUsuario>();


                if (permissaoUsuario != null)
                {
                    if (usuario.DataProximaTrocaSenha <= DateTime.Now)
                    {
                        pnlLogin.Visible = false;
                        pnlNovaSenha.Visible = true;
                        btEntrar.Enabled = false;
                    }
                    else // Verifica permissão por Pró Reitoria
                    {
                        VerificaPermissaoAcesso(permissaoUsuario, permissoesPropeq, 3);
                    }
                }// Não tem permissão para esse aplicação 14
                else
                {
                    throw new Exception("O usuário não possui permissão para essa aplicação.");
                }

            }
            catch (Exception ex)
            {
                panelAviso.Visible = true;
                lblAlerta.Visible = true;
                lblAlerta.Text = ex.Message;
            }

        }

        private void VerificaPermissaoAcesso(PermissaoUsuario permissaoUsuario, int permissaoProReitoria, int programaUID)
        {           
            if (permissaoUsuario.Permissao.PermissaoUID == permissaoProReitoria)
            {
                Session["programaUID"] = programaUID.ToString();
                Response.Redirect("projeto/Projetos.aspx");
            } else throw new Exception("O usuário não possui permissão para essa aplicação.");
        }

        protected void btAlterar_Click(object sender, EventArgs e)
        {

        }
    }
}