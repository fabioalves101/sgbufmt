﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.View
{
    public partial class CadastrarAluno : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int projetoUID = int.Parse(Request.QueryString["projetoUID"]);
            hpVoltar.NavigateUrl = "Alunos.aspx?projetoUID=" + projetoUID;
        }

        protected void btBuscar_Click(object sender, ImageClickEventArgs e)
        {
            pnlAlunos.Visible = true;
            this.PreencherGridAluno();
            
        }

        protected void gdrAlunos_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridView gridview = (GridView)sender;
            Int64 matricula = Int64.Parse(gridview.SelectedValue.ToString());

            AlunoProjetoBO alunoProjetoBO = new AlunoProjetoBO();

            vwAluno aluno = alunoProjetoBO.GetAluno(matricula);

            lblNome.Text = aluno.Nome;
            lblMatricula.Text = aluno.Matricula.ToString();
            lblTipo.Text = aluno.Tipo;

            pnlMembros.Visible = true;
            pnlAlunos.Visible = false;

        }

        protected void btSalvar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                int projetoUID = int.Parse(Request.QueryString["projetoUID"]);
                int pessoaMembroUID = int.Parse(ddlMembros.SelectedValue);
                Int64 matricula = Int64.Parse(lblMatricula.Text);

                MembroBO membroBO = new MembroBO();
                Membro membro = membroBO.GetMembro(pessoaMembroUID, projetoUID);

                AlunoProjetoBO alunoProjetoBO = new AlunoProjetoBO();

                alunoProjetoBO.ValidarRegras(matricula, pessoaMembroUID, projetoUID);

                alunoProjetoBO.SalvarAluno(projetoUID, membro.membroUID, matricula);

                Response.Redirect("Alunos.aspx?projetoUID="+projetoUID);
            }
            catch (Exception ex)
            {
                panelAviso.Visible = true;
                lblAlerta.Text = ex.Message;
            }
        }

        protected void PreencherGridAluno()
        {

            AlunoBO objBO = new AlunoBO();
            int totalRegistros = 0;
            List<vwAluno> registros = null;
            if (ddlAluno.SelectedValue.Equals("0"))
                registros = objBO.FindRegistros(txtAluno.Text, 0);
            else
                registros = objBO.FindRegistros(null, decimal.Parse(txtAluno.Text));

            gdrAlunos.DataSource = registros;
            gdrAlunos.DataBind();
            totalRegistros = registros.Count;
        }

    }
}