﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Alunos.aspx.cs" Inherits="ufmt.ssbic.View.Alunos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:Panel ID="panelAviso" runat="server" Visible="false">    
        <div class="pnlAviso">
                    <img alt="" src="/media/images/alerta.png" border="0" />&nbsp;&nbsp;
                    <asp:Label ID="lblAlerta" runat="server" Text="" Visible="true"></asp:Label>
        </div>
</asp:Panel>

    <h1>Gerenciar alunos do projeto</h1>
<fieldset>
    <legend>Projeto</legend>
    Nome do Projeto: <asp:Label ID="lblProjeto" runat="server" 
        Font-Bold="True"></asp:Label>
    <br />
    Coordenador: <asp:Label ID="lblCoordenador" runat="server" 
        Font-Bold="True"></asp:Label>

</fieldset>
<fieldset>
<legend>Alunos</legend>
<div class="addbutton">
    <asp:HyperLink ID="hpLink" CssClass="minibutton" runat="server">
    <span>
        <img alt="" src="/media/images/add.png" border="0" />&nbsp;&nbsp;
	    Cadastrar Novo Aluno
    </span>
</asp:HyperLink>

</div>
    <asp:GridView ID="gdrAlunosProjeto" runat="server" AutoGenerateColumns="False" 
        DataSourceID="ObjectDataSource1"
         CssClass="mGrid"
         PagerStyle-CssClass="pgr"
         AlternatingRowStyle-CssClass="alt" AllowPaging="True" 
        DataKeyNames="alunoProjetoUID" onselectedindexchanged="gdrAlunosProjeto_SelectedIndexChanged" 
        >
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>

        <Columns>
            <asp:TemplateField HeaderText="Remover" ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:LinkButton CssClass="minibutton" ID="lnkRemover" runat="server" CausesValidation="False" 
                                CommandName="Select">
                            <span>
                                <img src="media/images/remove.png" alt="" />
                                Remover
                            </span>
                            </asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Matricula" HeaderText="Matrícula" 
                SortExpression="Matricula" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="Nome" HeaderText="Nome" SortExpression="Nome" />
            <asp:BoundField DataField="Tipo" HeaderText="Tipo" SortExpression="Tipo" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="responsavel" HeaderText="Responsável" 
                SortExpression="responsavel" />
        </Columns>

<PagerStyle CssClass="pgr"></PagerStyle>
    </asp:GridView>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
        SelectMethod="GetAlunosProjeto" TypeName="ufmt.ssbic.Business.AlunoProjetoBO">
        <SelectParameters>
            <asp:QueryStringParameter Name="projetoUID" QueryStringField="projetoUID" 
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
</fieldset>

</asp:Content>
