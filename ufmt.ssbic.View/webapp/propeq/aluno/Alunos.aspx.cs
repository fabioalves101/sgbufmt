﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;


namespace ufmt.ssbic.View
{
    public partial class Alunos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int projetoUID = int.Parse(Request.QueryString["projetoUID"]);
            hpLink.NavigateUrl = "CadastrarAluno.aspx?projetoUID=" + projetoUID;

            ProjetoBO projetoBO = new ProjetoBO();
            Projeto projeto = projetoBO.GetProjeto(projetoUID);

            lblProjeto.Text = projeto.titulo;

            MembroBO membroBO = new MembroBO();
            PessoaMembro coordenador = membroBO.GetCoordenadorProjeto(projetoUID);

            lblCoordenador.Text = coordenador.nome;
        }

        protected void gdrAlunosProjeto_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridView gridview = (GridView)sender;
            int alunoProjetoUID = int.Parse(gridview.SelectedValue.ToString());

            AlunoProjetoBO alunoProjetoBO = new AlunoProjetoBO();
            alunoProjetoBO.ExcluirAlunoProjeto(alunoProjetoUID);

            gdrAlunosProjeto.DataBind();

            panelAviso.Visible = true;
            lblAlerta.Text = "Aluno removido com sucesso";
        }
    }
}