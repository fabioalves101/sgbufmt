﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;
using ufmt.sig.entity;

namespace ufmt.ssbic.View
{
    public partial class Membros : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                long projetoUID = long.Parse(Request.QueryString["projetoUID"]);

                ProjetoBO projetoBO = new ProjetoBO();
                Projeto projeto = projetoBO.GetProjeto(projetoUID);

                lblTitulo.Text = projeto.titulo;

                MembroBO membroBO = new MembroBO();
                PessoaMembro pessoaMembro = membroBO.GetCoordenadorProjeto(projetoUID);
                lblCoordenador.Text = pessoaMembro.nome;

                pnlServidor.Visible = false;
            }

        }

        protected void rdoServidor_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdoServidor.SelectedValue == "0")
            {
                pnlNaoServidor.Visible = true;
                pnlServidor.Visible = false;
            }
            else
            {
                pnlNaoServidor.Visible = false;
                pnlServidor.Visible = true;
            }
        }

        protected void btBuscar_Click(object sender, ImageClickEventArgs e)
        {
            
            pnlListaServidores.Visible = true;
            gdrListaServidores.DataBind();
        }

        protected void gdrListaServidores_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridView gridview = (GridView)sender;
            Int64 servidorUID = int.Parse(gridview.SelectedValue.ToString());

            PessoaBO pessoaB = new PessoaBO();
            Pessoa pessoa = pessoaB.GetPessoa(servidorUID);

            pnlMembroSelecionado.Visible = true;
            lblMembro.Visible = true;
            lblMembro.Text = "Servidor selecionado: " + pessoa.Nome;
            hdMembro.Value = pessoa.PessoaUID.ToString();

            pnlListaServidores.Visible = false;
        }

        protected void btAdicionar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ValidarFormulario();
                if (rdoServidor.SelectedValue == "1")
                {
                    SalvarMembroServidor();
                }
                else
                {
                    SalvarMembroExterno();
                }

                gdrListaMembros.DataBind();

                pnlAviso.Visible = true;
                lblAlerta.Visible = true;
                lblAlerta.Text = "Membro adicionado ao projeto com sucesso.";
            }
            catch (Exception ex)
            {
                pnlAviso.Visible = true;
                lblAlerta.Visible = true;
                lblAlerta.Text = "Houve um erro ao adicionar o membro ao projeto. "+ex.Message;
            }
        }

        protected void SalvarMembroServidor()
        {
            int projetoUID = int.Parse(Request.QueryString["projetoUID"]);
            long pessoaUID = long.Parse(hdMembro.Value);
            int cargaHoraria = int.Parse(txtCargaHorariaServidor.Text);

            MembroBO membroBO = new MembroBO();
            membroBO.SalvarMembroProjeto(pessoaUID, projetoUID, cargaHoraria);
        }

        protected void SalvarMembroExterno()
        {
            int projetoUID = int.Parse(Request.QueryString["projetoUID"]);

            String nome = txtNome.Text;
            String instituicao = txtInstituicao.Text;
            int cargaHoraria = int.Parse(txtCargaHoraria.Text);
            int tipoTitulacaoUID = int.Parse(ddlTitulacao.SelectedValue);
            String cpf = txtCpf.Text;

            MembroBO membroBO = new MembroBO();
            membroBO.SalvarMembroProjeto(nome, instituicao, tipoTitulacaoUID, cargaHoraria, projetoUID, cpf);
        }

        protected void imgbtBuscaCpf_Click(object sender, ImageClickEventArgs e)
        {
            String cpf = txtCpf.Text;
            MembroBO membroBO = new MembroBO();
            PessoaMembro pessoaMembro = membroBO.GetPessoaMembro(cpf);

            if (pessoaMembro != null)
            {
                txtNome.Text = pessoaMembro.nome;
                ddlTitulacao.SelectedValue = pessoaMembro.tipoTitulacaoUID.ToString();
                txtInstituicao.Text = pessoaMembro.instituicao;
            }
            else
            {
                lblAviso.Text = "Nenhuma pessoa encontrada com o cpf digitado. Preencha os campos abaixo para cadastrar.";
                txtNome.Text = String.Empty;
                ddlTitulacao.SelectedValue = String.Empty;
                txtInstituicao.Text = String.Empty;
            }
        }

        protected void gdrListaMembros_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridView gridview = (GridView)sender;
                int pessoaMembroUID = int.Parse(gridview.SelectedValue.ToString());

                int projetoUID = int.Parse(Request.QueryString["projetoUID"]);

                MembroBO membroBO = new MembroBO();

                lblAvisoListaMembros.Visible = true;

                if (membroBO.ExcluirMembro(pessoaMembroUID, projetoUID))
                {
                    lblAvisoListaMembros.Text = "Membro removido com sucesso";

                    pnlAviso.Visible = true;
                    lblAlerta.Visible = true;
                    lblAlerta.Text = "Membro removido do projeto com sucesso.";
                }

                gdrListaMembros.DataBind();
            }
            catch (Exception ex)
            {
                lblAvisoListaMembros.Text = ex.Message;
            }
        }

        public void ValidarFormulario()
        {
            try
            {
                if (rdoServidor.SelectedValue.Equals("0"))
                {
                    if (String.IsNullOrEmpty(txtNome.Text))
                        throw new Exception("O nome do membro precisa ser preenchido");

                    if (String.IsNullOrEmpty(txtInstituicao.Text))
                        throw new Exception("O nome da instituição precisa ser preenchido");
                                        
                    if (String.IsNullOrEmpty(txtCargaHoraria.Text))
                        throw new Exception("A carga horária precisa ser preenchida");

                    int carga = int.Parse(txtCargaHoraria.Text);

                    if (carga > 5 || carga < 1)
                        throw new Exception("A carga horária foi preenchida de maneira incorreta");

                    if (String.IsNullOrEmpty(txtCpf.Text))
                        throw new Exception("O CPF precisa ser preenchido");
                }
                else
                {
                    if (String.IsNullOrEmpty(txtCargaHorariaServidor.Text))
                        throw new Exception("A carga horária precisa ser preenchida");

                    int cargaServidor = int.Parse(txtCargaHorariaServidor.Text);

                    if (cargaServidor > 5 || cargaServidor < 1)
                        throw new Exception("A carga horária foi preenchida de maneira incorreta");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

       
    }
}