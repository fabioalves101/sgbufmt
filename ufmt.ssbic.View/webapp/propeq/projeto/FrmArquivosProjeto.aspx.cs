﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.DataAccess;
using ufmt.ssbic.Business;
using ufmt.ssbic.View.componentes;
using System.Configuration;

namespace ufmt.ssbic.View.webapp.propeq.projeto
{
    public partial class FrmArquivosProjeto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void grvListaProjetos_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridView gridview = (GridView)sender;
            int arquivoProjetoUID = int.Parse(gridview.SelectedValue.ToString());
            ProjetoBO projetoBO = new ProjetoBO();

            ArquivoProjeto arquivoProjeto = projetoBO.GetArquivoProjeto(arquivoProjetoUID);
            String caminho = Server.MapPath("~/arquivos/projetos");

            if (System.IO.File.Exists(caminho + "\\" + arquivoProjeto.nomeArquivo))
            {
                System.IO.File.Delete(caminho + "\\" + arquivoProjeto.nomeArquivo);
            }

            projetoBO.ExcluirArquivoProjeto(arquivoProjetoUID);

            grvListaProjetos.DataBind();

            pnlAviso.Visible = true;
            lblAviso.Text = "Arquivo excluído com sucesso";
            
        }

        protected void btSalvar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ValidarFormulario();

                bool hasArquivo = false;

                int projetoUID = int.Parse(Request.QueryString["projetoUID"]);
                ProjetoBO projetoBO = new ProjetoBO();
                Projeto projeto = projetoBO.GetProjeto(projetoUID);

                ObjUpload resultadoUpload = new ObjUpload();
                String descricaoArquivo = String.Empty;
                if (fUpload.HasFile)
                {
                    string caminho = Server.MapPath("~\\" + ConfigurationManager.AppSettings["diretorioUpload"].ToString() + "\\");
                    Upload objUpload = new Upload();
                    resultadoUpload = objUpload.UploadFile(fUpload, "3500000", ".pdf,.doc,.docx,.xls,.odt", caminho);

                    String msg = String.Empty;
                    if (resultadoUpload.MsgErro != null)//ocorreu um erro
                    {
                        throw new Exception("Houve um problema ao realizar o upload do arquivo. " + resultadoUpload.MsgErro);
                    }
                    else
                    {
                        descricaoArquivo = txtDescricaoArquivo.Text;
                        hasArquivo = true;
                    }
                }

                if (hasArquivo)
                {
                    projetoBO.SalvarArquivoProjeto(projeto, resultadoUpload.NomeArquivo, descricaoArquivo);
                    grvListaProjetos.DataBind();
                }
            }
            catch (Exception ex)
            {
                pnlAviso.Visible = true;
                lblAviso.Text = "Não foi possível salvar o arquivo. "+ex.Message;
            }

        }

        protected void ValidarFormulario()
        {
            if (!fUpload.HasFile)
                throw new Exception("É necessário selecionar um arquivo.");
            if (String.IsNullOrEmpty(txtDescricaoArquivo.Text))
                throw new Exception("è necessário preencher a descrição do arquivo.");
        }


        
    }
}