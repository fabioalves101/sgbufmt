﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Projetos.aspx.cs" Inherits="ufmt.ssbic.View.Projetos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="pnlAviso" runat="server" Visible="false">
    <div class="pnlAviso">
            <img alt="" src="../../../media/images/alerta.png" border="0" />&nbsp;&nbsp;
            <asp:Label ID="lblAviso" runat="server" Text="" Visible="false"></asp:Label>
    </div>
    </asp:Panel>
<div class="addbutton"> 
<a class="minibutton" href="CadastrarProjeto.aspx">
    <span>
        <img alt="" src="../../../media/images/add.png" border="0" />&nbsp;&nbsp;
	    Cadastrar Novo Projeto
    </span>
</a>
</div>
<fieldset>
<legend>Buscar:</legend>
Título: 
    <asp:TextBox ID="txtTitulo" runat="server" Width="300px"></asp:TextBox>

&nbsp;

Coordenador:
    <asp:TextBox ID="txtCoordenador" runat="server" Width="300px"></asp:TextBox>

<asp:ImageButton ID="btBuscar" runat="server" 
            ImageUrl="../../../media/images/busca.png" CssClass="imgBtBuscar" 
            onclick="btBuscar_Click" />
</fieldset>


    <asp:Panel ID="pnlListaProjetos" runat="server">
        <asp:GridView ID="grvListaProjetos" runat="server" AutoGenerateColumns="False" 
            DataSourceID="objListaProjetos"
            CssClass="mGrid"
            PagerStyle-CssClass="pgr"
            AlternatingRowStyle-CssClass="alt" AllowPaging="True" 
            DataKeyNames="projetoUID" onselectedindexchanged="grvListaProjetos_SelectedIndexChanged"
            >
            <AlternatingRowStyle CssClass="alt" />
           <Columns>
                <asp:TemplateField HeaderText="Opções" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px">
                    <ItemTemplate>
                        <asp:HyperLink ID="hpEditar" NavigateUrl='<%# Bind("projetoUID", "EditarProjeto.aspx?projetoUID={0}")%>' runat="server">
                            <img title="Editar Projeto" alt="Editar" src="../../../media/images/edit.png" />
                        </asp:HyperLink>
                        &nbsp;&nbsp;
                        <asp:HyperLink ID="hpMembros" NavigateUrl='<%# Bind("projetoUID", "Membros.aspx?projetoUID={0}")%>' runat="server">
                            <img title="Gerenciar Membros" alt="Gerenciar Membros" src="../../../media/images/membros.png" />
                        </asp:HyperLink>
                        &nbsp;&nbsp;
                        <asp:LinkButton ID="lnkBtExcluir" runat="server" CausesValidation="False" 
                                CommandName="Select" 
                            onclientclick="javascript:return window.confirm('Tem certeza que deseja excluir o projeto?');">
                            <span>
                                <img src="../../../media/images/delete.png" alt="Excluir Projeto" title="Excluir Projeto" />                                
                            </span>
                        </asp:LinkButton>
                        &nbsp;&nbsp;
                        <asp:HyperLink ID="hpAlunos" NavigateUrl='<%# Bind("projetoUID", "FrmArquivosProjeto.aspx?projetoUID={0}")%>' runat="server">
                                <img src="../../../media/images/folder.png" alt="Gerenciar Arquivos" title="Gerenciar Arquivos" />                                
                        </asp:HyperLink>
                        
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Registro CAP">
                    <ItemTemplate>
                        <asp:Label ID="lblRegistroCap" Text='<%# Bind("registroCap")%>' runat="server"></asp:Label>
                        /
                        <asp:Label ID="lblAnoCap" Text='<%# Bind("anoCap")%>' runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:BoundField DataField="titulo" HeaderText="Título" 
                    SortExpression="titulo" />
                <asp:BoundField DataField="inicio" dataformatstring="{0:dd/M/yyyy}" 
                    HeaderText="Início" HtmlEncode="false" SortExpression="inicio" />
                <asp:BoundField DataField="fim" dataformatstring="{0:dd/M/yyyy}" 
                    HeaderText="Fim" HtmlEncode="false" SortExpression="fim" />
                <asp:BoundField DataField="coordenador" HeaderText="Coordenador" 
                    SortExpression="coordenador" />
            </Columns>
            <PagerStyle CssClass="pgr" />
        </asp:GridView>
        <asp:ObjectDataSource ID="objListaProjetos" runat="server" 
            SelectMethod="GetProjetosCoordenador" 
            TypeName="ufmt.ssbic.Business.ProjetoBO">
            <SelectParameters>
                <asp:ControlParameter ControlID="txtTitulo" Name="titulo" PropertyName="Text" 
                    Type="String" />
                <asp:ControlParameter ControlID="txtCoordenador" Name="coordenador" 
                    PropertyName="Text" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </asp:Panel>

</asp:Content>
