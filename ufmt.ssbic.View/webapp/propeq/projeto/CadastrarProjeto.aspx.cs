﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.sig.entity;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;
using System.Configuration;
using ufmt.ssbic.View.componentes;


namespace ufmt.ssbic.View
{
    public partial class CadastrarProjeto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            ProjetoBO projetoBO = new ProjetoBO();
            Projeto ultimoProjeto = projetoBO.UltimoProjeto();

            txtAnoRegistro.Text = ultimoProjeto.anoCap.ToString();
            txtNumeroRegistro.Text = (ultimoProjeto.registroCap + 1).ToString();
            
        }

        protected void chkConvenio_CheckedChanged(object sender, EventArgs e)
        {
            if (chkConvenio.Checked)
            {
                pnlConvenio.Visible = true;
            }
            else
            {
                pnlConvenio.Visible = false;
            }
        }

        protected void chkFinanciamento_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFinanciamento.Checked)
            {
                pnlFinanciamento.Visible = true;
            }
            else
            {
                pnlFinanciamento.Visible = false;
            }
        }

        protected void btBuscar_Click(object sender, ImageClickEventArgs e)
        {
            pnlListaServidores.Visible = true;
            gdrListaServidores.DataBind();
        }

        protected void gdrListaServidores_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridView gridview = (GridView)sender;
            Int64 servidorUID = int.Parse(gridview.SelectedValue.ToString());

            PessoaBO pessoaB = new PessoaBO();
            Pessoa pessoa = pessoaB.GetPessoa(servidorUID);

            pnlCoordenadorSelecionado.Visible = true;
            lblCoordenadorSelecionado.Visible = true;
            lblCoordenadorSelecionado.Text = "Servidor selecionado: " + pessoa.Nome;
            hdCoordenadorSelecionado.Value = pessoa.PessoaUID.ToString();

            pnlListaServidores.Visible = false;
        }

        protected void btSalvar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ValidarFormulario();

                bool hasArquivo = false;
                ObjUpload resultadoUpload = new ObjUpload();
                String descricaoArquivo = String.Empty;
                if (fUpload.HasFile)
                {
                    string caminho = Server.MapPath("~\\" + ConfigurationManager.AppSettings["diretorioUpload"].ToString() + "\\");
                    Upload objUpload = new Upload();
                    resultadoUpload = objUpload.UploadFile(fUpload, "3500000", ".pdf,.doc,.docx,.xls,.odt", caminho);

                    String msg = String.Empty;
                    if (resultadoUpload.MsgErro != null)//ocorreu um erro
                    {
                        throw new Exception("Houve um problema ao realizar o upload do arquivo. " + resultadoUpload.MsgErro);
                    }
                    else
                    {
                        descricaoArquivo = txtDescricaoArquivo.Text;
                        hasArquivo = true;
                    }
                }
                
                int nRegistroCap = int.Parse(txtNumeroRegistro.Text);
                int anoRegistroCap = int.Parse(txtAnoRegistro.Text);

                String titulo = txtTitulo.Text;

                DateTime inicio = DateTime.Parse(txtInicio.Text);
                DateTime fim = DateTime.Parse(txtFim.Text);                                

                bool hasConvenio = false;
                String convenio = String.Empty;
                if (chkConvenio.Checked)
                {
                    hasConvenio = true;
                    convenio = txtConvenio.Text;
                }

                bool hasFinanciamento = false;
                String financiador = String.Empty;
                float valorFinanciamento = 0;
                if (chkFinanciamento.Checked)
                {
                    hasFinanciamento = true;
                    financiador = txtFinanciador.Text;
                    valorFinanciamento = float.Parse(txtValorFinanciamento.Text);
                }

                int areaConhecimentoUID = int.Parse(ddlAreaConhecimento.SelectedValue);

                ProjetoBO projetoBO = new ProjetoBO();

                bool produtoTecnologico = false;
                if (chkProdutoTecnologico.Checked)
                {
                    produtoTecnologico = true;
                }

                bool hasInteracao = false;
                int tipoInteracao = 0;
                if (chkInteracao.Checked)
                {
                    hasInteracao = true;
                    tipoInteracao = int.Parse(rdoInteracaoInstituicao.SelectedValue);
                }

                Projeto projeto =
                    projetoBO.SalvarProjeto(
                        nRegistroCap, anoRegistroCap, titulo, inicio, fim,
                        hasConvenio, convenio,
                        hasFinanciamento, financiador, valorFinanciamento, areaConhecimentoUID, produtoTecnologico,
                        hasInteracao, tipoInteracao,
                        hasArquivo, resultadoUpload.NomeArquivo, descricaoArquivo
                        );

                long pessoaUID = long.Parse(hdCoordenadorSelecionado.Value);               
                
                int cargaHoraria = int.Parse(txtCargaHoraria.Text);                

                MembroBO membroBO = new MembroBO();
                membroBO.SalvarCoordenador(pessoaUID, projeto, cargaHoraria);


                Response.Redirect("Projetos.aspx?sucesso=true");
            }
            catch (Exception ex)
            {
                panelAviso.Visible = true;
                lblAlerta.Text = "Houve um erro ao cadastrar o projeto. "+ex.Message;
            }
        }

        public void ValidarFormulario()
        {
            try
            {
                if (chkInteracao.Checked)
                { 
                    if(String.IsNullOrEmpty(rdoInteracaoInstituicao.SelectedValue))
                        throw new Exception("O tipo de instituição em que há interãção deve ser preenchido");
                }

                if (String.IsNullOrEmpty(txtNumeroRegistro.Text))
                    throw new Exception("O número de registro deve ser preenchido");

                if (String.IsNullOrEmpty(txtAnoRegistro.Text))
                    throw new Exception("O ano de registro deve ser preenchido");

                if (String.IsNullOrEmpty(txtTitulo.Text))
                    throw new Exception("O título precisa ser preenchido.");

                if (String.IsNullOrEmpty(txtInicio.Text))
                    throw new Exception("O início do projeto precisa ser preenchido");

                if (String.IsNullOrEmpty(txtFim.Text))
                    throw new Exception("O fim do projeto precisa ser preenchido");

                DateTime inicio = DateTime.Parse(txtInicio.Text);
                DateTime fim = DateTime.Parse(txtFim.Text);

                if (inicio >= fim)
                    throw new Exception("O fim do projeto precisa ser maior que o início.");

                if (String.IsNullOrEmpty(hdCoordenadorSelecionado.Value))
                    throw new Exception("Um coordenador precisa ser selecionado");

                if (String.IsNullOrEmpty(txtCargaHoraria.Text))
                    throw new Exception("A carga horária precisa ser preenchida");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void chkInteracao_CheckedChanged(object sender, EventArgs e)
        {
            if(chkInteracao.Checked)
                pnlInteracao.Visible = true;
            else
                pnlInteracao.Visible = false;
        }

    }
}