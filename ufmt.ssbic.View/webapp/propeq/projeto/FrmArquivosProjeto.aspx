﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="FrmArquivosProjeto.aspx.cs" Inherits="ufmt.ssbic.View.webapp.propeq.projeto.FrmArquivosProjeto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <asp:Panel ID="pnlAviso" runat="server" Visible="false">
    <div class="pnlAviso">
            <img alt="" src="../../../media/images/alerta.png" border="0" />&nbsp;&nbsp;
            <asp:Label ID="lblAviso" runat="server" Text="" Visible="true"></asp:Label>
    </div>
    </asp:Panel>
<fieldset>
    <legend><strong>Cadastrar novo arquivo</strong></legend>
    <p>
        Arquivo: <asp:FileUpload ID="fUpload" runat="server" />
    </p>
    <p>
        Descrição do Arquivo: <asp:TextBox ID="txtDescricaoArquivo" runat="server" Width="300px"></asp:TextBox>
    </p>
    <p>
        <asp:ImageButton onmouseover="~/media/images/savebutton_hover.png" 
    onmouseout="~/media/images/savebutton.png" ID="btSalvar" runat="server" 
    ImageUrl="~/media/images/savebutton.png" onclick="btSalvar_Click" />
    </p>
</fieldset>


    <asp:Panel ID="pnlListaProjetos" runat="server">
        <asp:GridView ID="grvListaProjetos" runat="server" AutoGenerateColumns="False" 
            DataSourceID="objListaProjetos"
            CssClass="mGrid"
            PagerStyle-CssClass="pgr"
            AlternatingRowStyle-CssClass="alt" AllowPaging="True" 
            onselectedindexchanged="grvListaProjetos_SelectedIndexChanged" DataKeyNames="arquivoProjetoUID"
            
            >
            <AlternatingRowStyle CssClass="alt" />
           <Columns>
                <asp:BoundField DataField="descricao" 
                    HeaderText="Nome do Arquivo" SortExpression="descricao" />
                <asp:TemplateField HeaderText="Download" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="70px">
                    <ItemTemplate>
                        <asp:HyperLink ID="hpAlunos" NavigateUrl='<%# Bind("nomeArquivo", "../../../arquivos/projetos/{0}")%>' runat="server">
                                <img src="../../../media/images/download.png" alt="Download" title="Realizar download do arquivo" />                                
                        </asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Excluir" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="70px">
                    <ItemTemplate>
                         <asp:LinkButton ID="lnkBtExcluir" runat="server" CausesValidation="False" 
                                CommandName="Select"
                            onclientclick="javascript:return window.confirm('Tem certeza que deseja excluir o arquivo?');">
                            <span>
                                <img src="../../../media/images/delete.png" alt="Excluir Arquivo" title="Excluir Arquivo" />                                
                            </span>
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="pgr" />
        </asp:GridView>
        <asp:ObjectDataSource ID="objListaProjetos" runat="server" 
            SelectMethod="GetArquivosProjeto" 
            TypeName="ufmt.ssbic.Business.ProjetoBO">
            <SelectParameters>
                <asp:QueryStringParameter Name="projetoUID" QueryStringField="projetoUID" 
                    Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </asp:Panel>
</asp:Content>
