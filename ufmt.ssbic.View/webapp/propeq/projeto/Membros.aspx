﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Membros.aspx.cs" Inherits="ufmt.ssbic.View.Membros" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <h1>Gerenciar membros do projeto</h1>

 <asp:Panel ID="pnlAviso" runat="server" Visible="false">
    <div class="pnlAviso">
            <img alt="" src="/media/images/alerta.png" border="0" />&nbsp;&nbsp;
            <asp:Label ID="lblAlerta" runat="server" Text="" Visible="false"></asp:Label>
    </div>
 </asp:Panel>

<fieldset>
 <legend>Projeto</legend>
    <asp:Panel ID="pnlProjeto" runat="server">
        <p>
            Título: 
            <asp:Label ID="lblTitulo" runat="server"></asp:Label>
        </p>
        <p>
            Coordenador: 
            <asp:Label ID="lblCoordenador" runat="server"></asp:Label>
        </p>
    </asp:Panel>
</fieldset>
<fieldset>
    <legend>Adicionar Membro</legend>
    <asp:Panel ID="pnlAdicionarMembro" runat="server">        
        <p>
            Servidor da UFMT?<asp:RadioButtonList ID="rdoServidor" runat="server" 
                AutoPostBack="True" RepeatDirection="Horizontal" 
                onselectedindexchanged="rdoServidor_SelectedIndexChanged">
                <asp:ListItem Value="1">Sim</asp:ListItem>
                <asp:ListItem Selected="True" Value="0">Não</asp:ListItem>
            </asp:RadioButtonList>
        </p>
        <asp:Panel ID="pnlNaoServidor" runat="server">
            <p>
                CPF:
                <asp:TextBox ID="txtCpf" runat="server"></asp:TextBox>
                <asp:ImageButton ID="imgbtBuscaCpf" runat="server" 
            ImageUrl="~/media/images/busca.png" CssClass="imgBtBuscar" 
                    onclick="imgbtBuscaCpf_Click" />
                <br />
                <asp:Label ID="lblAviso" runat="server" 
                    Text="Digite o CPF para procurar pessoas cadastradas" Font-Bold="True"></asp:Label>
            </p>
            <p>
                Nome: 
                <asp:TextBox ID="txtNome" runat="server" Width="500"></asp:TextBox>
            </p>
            
            <p>
                Titulação:
                <asp:DropDownList ID="ddlTitulacao" runat="server" DataSourceID="sqlDSIdbufmt" 
                    DataTextField="descricao" DataValueField="tipoTitulacaoUID">
                </asp:DropDownList>
                <asp:SqlDataSource ID="sqlDSIdbufmt" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:idbufmt %>" 
                    SelectCommand="SELECT [descricao], [tipoTitulacaoUID] FROM [TipoTitulacao]">
                </asp:SqlDataSource>
            </p>
            <p>
                Instituição:
                <asp:TextBox ID="txtInstituicao" runat="server" Width="400px"></asp:TextBox>
            </p>
            <p>
                Carga Horária:
                <asp:TextBox ID="txtCargaHoraria" runat="server" Width="40px"></asp:TextBox>
                <asp:NumericUpDownExtender ID="txtCargaHoraria_NumericUpDownExtender" 
                    runat="server" Enabled="True" Maximum="5" Minimum="1" RefValues="" 
                    ServiceDownMethod="" ServiceDownPath="" ServiceUpMethod="" Tag="" 
                    TargetButtonDownID="" TargetButtonUpID="" TargetControlID="txtCargaHoraria" 
                    Width="40">
                </asp:NumericUpDownExtender>
            </p>
        </asp:Panel>
        <asp:Panel ID="pnlServidor" runat="server">
        
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updPnlListaServidores">
        <ProgressTemplate>
        <div style="text-align:center">
            <img alt="Carregando" src="media/images/ajax-loader.gif" />
        </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updPnlListaServidores" runat="server">
        
    <ContentTemplate>
    <asp:Panel ID="pnlCoordenador" runat="server">
        <asp:Panel ID="pnlMembroSelecionado" runat="server" Visible="false">
            <p style="font-weight:bold"> 
                <asp:Label ID="lblMembro" runat="server" Text="" Visible="False"></asp:Label>
                <asp:HiddenField ID="hdMembro" runat="server" />
            </p>
            <p>
                <asp:Label ID="lblCargaHoraria" runat="server" Text="Carga Horária: "></asp:Label>
                <asp:TextBox ID="txtCargaHorariaServidor" runat="server" Width="40px"></asp:TextBox>
                <asp:NumericUpDownExtender ID="txtCargaHorariaServidor_NumericUpDownExtender" 
                    runat="server" Enabled="True" Maximum="5" Minimum="1" RefValues="" 
                    ServiceDownMethod="" ServiceDownPath="" ServiceUpMethod="" Tag="" 
                    TargetButtonDownID="" TargetButtonUpID="" 
                    TargetControlID="txtCargaHorariaServidor" Width="40">
                </asp:NumericUpDownExtender>
            </p>
        </asp:Panel>
    <p>
        <asp:Label ID="lblBuscaCoordenador" runat="server" Text="Buscar: "></asp:Label>       
        <asp:TextBox ID="txtBuscaCoordenador" runat="server" Width="400px"></asp:TextBox>
        <asp:DropDownList ID="ddlBuscaCoordenador" runat="server">
            <asp:ListItem>Nome</asp:ListItem>
            <asp:ListItem>CPF</asp:ListItem>
            <asp:ListItem Value="siape">Registro SIAPE</asp:ListItem>
        </asp:DropDownList>
        <asp:ImageButton ID="btBuscar" runat="server" 
            ImageUrl="~/media/images/busca.png" CssClass="imgBtBuscar" 
            onclick="btBuscar_Click" />
    </p>
        
        <asp:Panel ID="pnlListaServidores" runat="server" Visible="False">
        Selecione o coordenador.
            <asp:GridView ID="gdrListaServidores" runat="server" 
                AutoGenerateColumns="False" DataSourceID="ObjectDataSource1" 
                AllowPaging="True" Width="100%"
                CssClass="mGrid"
                PagerStyle-CssClass="pgr"
                AlternatingRowStyle-CssClass="alt" DataKeyNames="ServidorUID" onselectedindexchanged="gdrListaServidores_SelectedIndexChanged"
                >            
                <AlternatingRowStyle CssClass="alt" />
                <Columns>                    
                    <asp:TemplateField HeaderText="Selecionar" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="120px">
                        <ItemTemplate>
                            <asp:LinkButton CssClass="minibutton" ID="LinkButton1" runat="server" CausesValidation="False" 
                                CommandName="Select">
                            <span>
                                <img src="media/images/select.png" alt="" />
                                Selecionar
                            </span>
                            </asp:LinkButton>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="120px" />
                    </asp:TemplateField>
                    
                    <asp:BoundField DataField="Registro" HeaderText="Registro SIAPE" ItemStyle-HorizontalAlign="Center" 
                        SortExpression="Registro" ItemStyle-Width="120px">
                    <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                        Nome do Servidor
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='  <%# Eval("Pessoa.Nome")%>'></asp:Label>
                        
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle CssClass="pgr" />
            </asp:GridView>
            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
                SelectMethod="GetPessoas" TypeName="ufmt.ssbic.Business.PessoaBO">
                <SelectParameters>
                    <asp:ControlParameter ControlID="txtBuscaCoordenador" DefaultValue="null" 
                        Name="parametro" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="ddlBuscaCoordenador" DefaultValue="null" 
                        Name="tipoParametro" PropertyName="SelectedValue" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
            
        </asp:Panel>       
        
    </asp:Panel>
    </ContentTemplate>
    </asp:UpdatePanel>

        </asp:Panel>

        <fieldset>
            <asp:ImageButton ID="btAdicionar" runat="server" 
            ImageUrl="~/media/images/addbutton.png" onclick="btAdicionar_Click" />
        </fieldset>
    </asp:Panel>
</fieldset>
<fieldset>
    <legend>Membros do Projeto</legend>

    <asp:Panel ID="pnlListaMembros" runat="server">
        <asp:Label ID="lblAvisoListaMembros" runat="server" Visible="false" Text=""></asp:Label>

        <asp:GridView ID="gdrListaMembros" runat="server"
         AutoGenerateColumns="False" DataSourceID="odsListaMembros" 
                AllowPaging="True" Width="100%"
                CssClass="mGrid"
                PagerStyle-CssClass="pgr"
                AlternatingRowStyle-CssClass="alt" DataKeyNames="pessoaMembroUID" onselectedindexchanged="gdrListaMembros_SelectedIndexChanged"
        >
            <AlternatingRowStyle CssClass="alt" />
            <Columns>
            <asp:TemplateField HeaderText="Remover" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="120px">
                        <ItemTemplate>
                            <asp:LinkButton CssClass="minibutton" ID="LinkButton1" runat="server" CausesValidation="False" 
                                CommandName="Select">
                            <span>
                                <img src="media/images/remove.png" alt="" />
                                Remover
                            </span>
                            </asp:LinkButton>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="120px" />
                    </asp:TemplateField>
                <asp:BoundField DataField="registro" HeaderText="Registro" 
                    SortExpression="registro" />
                <asp:BoundField DataField="nome" HeaderText="Nome" SortExpression="nome" />
                <asp:BoundField DataField="instituicao" HeaderText="Instituição" 
                    SortExpression="instituicao" />
            </Columns>
            <PagerStyle CssClass="pgr" />
        </asp:GridView>
        <asp:ObjectDataSource ID="odsListaMembros" runat="server" 
            SelectMethod="GetMembrosProjeto" TypeName="ufmt.ssbic.Business.MembroBO">
            <SelectParameters>
                <asp:QueryStringParameter Name="projetoUID" QueryStringField="projetoUID" 
                    Type="Int32" />
                <asp:Parameter DefaultValue="2" Name="funcaoProjetoUID" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </asp:Panel>
</fieldset>

</asp:Content>
