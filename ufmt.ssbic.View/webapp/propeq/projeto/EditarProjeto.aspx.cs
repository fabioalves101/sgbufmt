﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.sig.entity;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.View
{
    public partial class EditarProjeto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                long projetoUID = long.Parse(Request.QueryString["projetoUID"]);

                ProjetoBO projetoBO = new ProjetoBO();
                Projeto projeto = projetoBO.GetProjeto(projetoUID);

                txtNumeroRegistro.Text = projeto.registroCap.ToString();
                txtAnoRegistro.Text = projeto.anoCap.ToString();

                txtTitulo.Text = projeto.titulo;
                txtInicio.Text = projeto.inicio.ToShortDateString();
                txtFim.Text = projeto.fim.Value.ToShortDateString();

                if(projeto.areaConhecimentoUID != null)
                    ddlAreaConhecimento.SelectedValue = projeto.areaConhecimentoUID.ToString();

                if (projeto.Convenio.Count > 0)
                {
                    chkConvenio.Checked = true;
                    pnlConvenio.Visible = true;
                    txtConvenio.Text = projeto.Convenio.First<Convenio>().convenio1;
                    hdConvenio.Value = projeto.Convenio.First<Convenio>().convenioUID.ToString();
                }

                if (projeto.Financiamento.Count > 0)
                {
                    chkFinanciamento.Checked = true;
                    pnlFinanciamento.Visible = true;
                    txtFinanciador.Text = projeto.Financiamento.First<Financiamento>().financiador;
                    txtValorFinanciamento.Text = projeto.Financiamento.First<Financiamento>().valor.ToString();
                    hdFinanciamento.Value = projeto.Financiamento.First<Financiamento>().financiamentoUID.ToString();
                }

                if (projeto.produtoTecnologico.HasValue)
                {
                    if(projeto.produtoTecnologico.Value)
                        chkProdutoTecnologico.Checked = true;
                }

                if (projeto.InteracaoProjetoInstituicao.Count > 0)
                {
                    chkInteracao.Checked = true;
                    pnlInteracao.Visible = true;
                    rdoInteracaoInstituicao.SelectedValue = projeto.InteracaoProjetoInstituicao.FirstOrDefault<InteracaoProjetoInstituicao>().tipoInstituicao.ToString();
                    hdInteracao.Value = projeto.InteracaoProjetoInstituicao.FirstOrDefault<InteracaoProjetoInstituicao>().interacaoProjetoInstituicaoUID.ToString();
                }

                MembroBO membroBO = new MembroBO();
                PessoaMembro pessoaMembro = membroBO.GetCoordenadorProjeto(projetoUID);

                pnlCoordenadorSelecionado.Visible = true;
                lblCoordenadorSelecionado.Visible = true;
                lblCoordenadorSelecionado.Text = "Coordenador atual: " + pessoaMembro.nome;
                txtCargaHoraria.Text = pessoaMembro.Membro.FirstOrDefault<Membro>().cargaHoraria.ToString();
                hdCoordenadorSelecionado.Value = pessoaMembro.pessoaUID.ToString();

                
            }

        }

        protected void chkConvenio_CheckedChanged(object sender, EventArgs e)
        {
            if (chkConvenio.Checked)
            {
                pnlConvenio.Visible = true;
            }
            else
            {
                pnlConvenio.Visible = false;
            }
        }

        protected void chkFinanciamento_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFinanciamento.Checked)
            {
                pnlFinanciamento.Visible = true;
            }
            else
            {
                pnlFinanciamento.Visible = false;
            }
        }

        protected void btBuscar_Click(object sender, ImageClickEventArgs e)
        {
            pnlListaServidores.Visible = true;
            gdrListaServidores.DataBind();
        }

        protected void gdrListaServidores_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridView gridview = (GridView)sender;
            Int64 servidorUID = int.Parse(gridview.SelectedValue.ToString());

            PessoaBO pessoaB = new PessoaBO();
            Pessoa pessoa = pessoaB.GetPessoa(servidorUID);

            pnlCoordenadorSelecionado.Visible = true;
            lblCoordenadorSelecionado.Visible = true;
            lblCoordenadorSelecionado.Text = "Servidor selecionado: " + pessoa.Nome;
            hdCoordenadorSelecionado.Value = pessoa.PessoaUID.ToString();

            pnlListaServidores.Visible = false;
        }

        protected void btSalvar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                int projetoUID = int.Parse(Request.QueryString["projetoUID"]);
                int nRegistroCap = int.Parse(txtNumeroRegistro.Text);
                int anoRegistroCap = int.Parse(txtAnoRegistro.Text);

                String titulo = txtTitulo.Text;

                DateTime inicio = DateTime.Parse(txtInicio.Text);
                DateTime fim = DateTime.Parse(txtFim.Text);

                bool hasConvenio = false;
                String convenio = String.Empty;
                int? convenioUID = null;
                if (chkConvenio.Checked)
                {
                    hasConvenio = true;
                    convenio = txtConvenio.Text;

                    if (!String.IsNullOrEmpty(hdConvenio.Value))
                        convenioUID = int.Parse(hdConvenio.Value);
                }

                bool hasFinanciamento = false;
                String financiador = String.Empty;
                Decimal valorFinanciamento = 0;
                int? financiamentoUID = null;
                if (chkFinanciamento.Checked)
                {
                    hasFinanciamento = true;
                    financiador = txtFinanciador.Text;
                    valorFinanciamento = Decimal.Parse(txtValorFinanciamento.Text);

                    if (!String.IsNullOrEmpty(hdFinanciamento.Value))
                        financiamentoUID = int.Parse(hdFinanciamento.Value);
                }

                bool hasInteracao = false;
                int tipoInteracao = 0;
                int? interacaoUID = null;
                if (chkInteracao.Checked)
                {
                    hasInteracao = true;
                    tipoInteracao = int.Parse(rdoInteracaoInstituicao.SelectedValue);
                    if (!String.IsNullOrEmpty(hdInteracao.Value))
                        interacaoUID = int.Parse(hdInteracao.Value);
                }

                int areaConhecimentoUID = int.Parse(ddlAreaConhecimento.SelectedValue);

                bool produtoTecnologico = false;
                if (chkProdutoTecnologico.Checked)
                    produtoTecnologico = true;

                ProjetoBO projetoBO = new ProjetoBO();

                Projeto projeto =
                    projetoBO.AtualizarProjeto(
                        projetoUID,
                        nRegistroCap, anoRegistroCap, titulo, inicio, fim,
                        hasConvenio, convenioUID, convenio,
                        hasFinanciamento, financiamentoUID, financiador, valorFinanciamento, areaConhecimentoUID,
                        produtoTecnologico,
                        hasInteracao, interacaoUID, tipoInteracao
                        );

                long pessoaUID = long.Parse(hdCoordenadorSelecionado.Value);
                int cargaHoraria = int.Parse(txtCargaHoraria.Text);

                MembroBO membroBO = new MembroBO();
                membroBO.AtualizarCoordenador(pessoaUID, projeto, cargaHoraria);

                Response.Redirect("Projetos.aspx");
            }
            catch (Exception ex)
            {
                panelAviso.Visible = true;
                lblAlerta.Text = "Houve um erro ao cadastrar o projeto. " + ex.Message;
            }
        }

        public void ValidarFormulario()
        {
            try
            {
                if (String.IsNullOrEmpty(txtNumeroRegistro.Text))
                    throw new Exception("O número de registro deve ser preenchido");

                if (String.IsNullOrEmpty(txtAnoRegistro.Text))
                    throw new Exception("O ano de registro deve ser preenchido");

                if (String.IsNullOrEmpty(txtTitulo.Text))
                    throw new Exception("O título precisa ser preenchido.");

                if (String.IsNullOrEmpty(txtInicio.Text))
                    throw new Exception("O início do projeto precisa ser preenchido");

                if (String.IsNullOrEmpty(txtFim.Text))
                    throw new Exception("O fim do projeto precisa ser preenchido");

                DateTime inicio = DateTime.Parse(txtInicio.Text);
                DateTime fim = DateTime.Parse(txtFim.Text);

                if (inicio >= fim)
                    throw new Exception("O fim do projeto precisa ser maior que o início.");

                if (String.IsNullOrEmpty(hdCoordenadorSelecionado.Value))
                    throw new Exception("Um coordenador precisa ser selecionado");

                if (String.IsNullOrEmpty(txtCargaHoraria.Text))
                    throw new Exception("A carga horária precisa ser preenchida");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void chkInteracao_CheckedChanged(object sender, EventArgs e)
        {
            if (chkInteracao.Checked)
                pnlInteracao.Visible = true;
            else
                pnlInteracao.Visible = false;
        }
    }
}