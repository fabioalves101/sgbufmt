﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.View
{
    public partial class Projetos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session.Remove("administracao");

            if (!String.IsNullOrEmpty(Request.QueryString["sucesso"]))
                {
                    if (Request.QueryString["sucesso"].Equals("true"))
                    {
                        pnlAviso.Visible = true;
                        lblAviso.Visible = true;
                        lblAviso.Text = "Projeto cadastrado com sucesso.";
                    }
                }
            
        }

        protected void grvListaProjetos_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridView gridview = (GridView)sender;
            int projetoUID = int.Parse(gridview.SelectedValue.ToString());

            ProjetoBO projetoBO = new ProjetoBO();

            Projeto projeto = projetoBO.GetProjeto(projetoUID);
            String caminho = Server.MapPath("~/arquivos/projetos");

            foreach(ArquivoProjeto arquivo in projeto.ArquivoProjeto)
            {
                if (System.IO.File.Exists(caminho +"\\"+ arquivo.nomeArquivo))
                {
                    System.IO.File.Delete(caminho +"\\"+ arquivo.nomeArquivo);
                }
            }

            projetoBO.ExcluirProjeto(projetoUID);

            pnlAviso.Visible = true;
            lblAviso.Visible = true;
            lblAviso.Text = "Projeto removido com sucesso.";

            grvListaProjetos.DataBind();
        }

        protected void btBuscar_Click(object sender, ImageClickEventArgs e)
        {
            grvListaProjetos.DataBind();
        }
    }
}