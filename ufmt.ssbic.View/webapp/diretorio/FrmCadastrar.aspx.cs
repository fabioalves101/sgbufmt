﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.sig.entity;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;
using ufmt.ssbic.View.session;


namespace ufmt.ssbic.View.webapp.diretorio
{
    public partial class FrmCadastrar : VerificaSession
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack) {

                FinanciadorBO objBO = new FinanciadorBO();
                string proreitoriaUID = Session["programaUID"].ToString();
                string area = Request.QueryString["area"];
                lblTitulo.Text = area;
                
                ddlFinanciador.DataSource = objBO.GetTipoFinanciador();
                ddlFinanciador.DataTextField = "descricao";
                ddlFinanciador.DataValueField = "financiadorUID";
                ddlFinanciador.DataBound += new EventHandler(IndiceZero);
                ddlFinanciador.DataBind();


            }
            
        }

        protected void IndiceZero(object sender, EventArgs e)
        {
            DropDownList objDropDownList = (DropDownList)sender; //Cast no sender para DropDownList
            objDropDownList.Items.Insert(0, new ListItem("Selecione","")); //Adiciona um novo Item
        }


        protected void btnVoltar_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("FrmListar.aspx?instrumentoSelecaoUID=" + Request.QueryString["instrumentoSelecaoUID"].ToString());
        }

        protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                FinanciadorBO objBO = new FinanciadorBO();
                FinanciadorInstrumentoSelecao fin = new FinanciadorInstrumentoSelecao();

                fin.instrumentoSelecaoUID = int.Parse(Request.QueryString["instrumentoSelecaoUID"].ToString());
                fin.financiadorUID = int.Parse(ddlFinanciador.SelectedValue);
                fin.quantidadeBolsas = int.Parse(txtQuantidadeBolsa.Text);
                fin.valorBolsa = double.Parse(txtValor.Text);
                objBO.Salvar(fin, 1);


                lblMsgResultado.Text = "Registro adicionado com sucesso.";
                PanelMsgResultado.CssClass = "success";
                PanelFormulario.Visible = false;
                btnSalvar.Visible = false;
                btnNovo.Visible = true;
            }
            catch (Exception err)
            {

                lblMsgResultado.Text = "Não foi possível efetuar a operação.  Por favor, entre em contato com o administrador do sistema.<br />" + err.Message.ToString();
                PanelMsgResultado.CssClass = "error";
                
            }

            PanelMsgResultado.Visible = true;
        }

        protected void btnNovo_Click(object sender, ImageClickEventArgs e)
        {
            ReiniciarCadastro();
        }

        private void ReiniciarCadastro()
        {
            PanelMsgResultado.Visible = false;
            PanelFormulario.Visible = true;
            btnSalvar.Visible = true;
            btnNovo.Visible = false;
           
           
        }



    }
}