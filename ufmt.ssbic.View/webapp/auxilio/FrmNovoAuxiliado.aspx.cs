﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.View.session;
using ufmt.ssbic.DataAccess;
using ufmt.ssbic.Business;

namespace ufmt.ssbic.View.webapp.auxilio
{
    public partial class FrmNovoAuxiliado : VerificaSession
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Request.QueryString["message"]))
            {
                PanelMsgResultado.Visible = true;
                lblMsgResultado.Text = "O Aluno selecionado já possui auxilios cadastrados";
            }
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            Aluno aluno = UCBuscarAluno1.GetAluno();
            int folhaPagamentoUID = int.Parse(Request.QueryString["folhaPagamentoUID"]);
            if (!this.ExisteAuxiliado(aluno, folhaPagamentoUID))
            {
                Auxiliado auxiliado = null;
                if (rdoImportaDados.SelectedValue.Equals("1"))
                {
                    int? bancoUID = null;
                    if (!String.IsNullOrEmpty(aluno.CodigoFebrabram))
                    {
                        Banco banco = new BancoBO().GetBanco(aluno.CodigoFebrabram);
                        bancoUID = banco.bancoUID;
                    }

                    auxiliado = new Auxiliado()
                    {
                        cpf = aluno.CPF,
                        email = aluno.Email,
                        registroAluno = aluno.Matricula.ToString(),
                        bancoUID = bancoUID,
                        agencia = aluno.Agencia,
                        numeroConta = aluno.ContaCorrente
                    };
                }
                else
                {
                    int bancoUID = int.Parse(ddlBanco.SelectedValue);
                    string agencia = txtAgencia.Text;
                    string conta = txtConta.Text;

                    auxiliado = new Auxiliado()
                    {
                        cpf = aluno.CPF,
                        email = aluno.Email,
                        registroAluno = aluno.Matricula.ToString(),
                        bancoUID = bancoUID,
                        agencia = agencia,
                        numeroConta = conta
                    };

                }


                List<Auxilio> listAuxilios = new List<Auxilio>();
                AuxilioController aController = new AuxilioController();

                List<Auxilio> auxiliosSelecionados = new List<Auxilio>();

                foreach (ListItem item in chkAuxilios.Items)
                {
                    if (item.Selected)
                    {
                        auxiliosSelecionados.Add(
                            aController.GetAuxilio(int.Parse(item.Value))
                            );
                    }
                }


                aController.CadastrarAuxiliado(auxiliado, auxiliosSelecionados, folhaPagamentoUID);
                
                Response.Redirect("FrmListar.aspx?folhaPagamentoUID=" + folhaPagamentoUID);
            }
            else
            {
                Response.Redirect("FrmNovoAuxiliado.aspx?folhaPagamentoUID=" + folhaPagamentoUID+"&message=existe");
            }
        }

        private bool ExisteAuxiliado(Aluno aluno, int folhaPagamentoUID)
        {
            AuxilioController ac = new AuxilioController();
            AuxilioFolhaPagamento auxilioFP = ac.GetAuxilioFolhaPagamento(aluno.CPF, folhaPagamentoUID);
            if (auxilioFP != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected void rdoImportaDados_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdoImportaDados.SelectedValue.Equals("2"))
            {
                pnlDadosBancarios.Visible = true;
            }
            else
            {
                pnlDadosBancarios.Visible = false;
            }
        }
    }
}