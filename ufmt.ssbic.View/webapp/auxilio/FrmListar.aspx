﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true" CodeBehind="FrmListar.aspx.cs" Inherits="ufmt.ssbic.View.webapp.auxilio.FrmListar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<style>
.mGrid td {color:#000;}
.mGrid td a{color:#000;}
</style>

<!-- BREADCRUMB -->
        <asp:Panel ID="breadcrumb" CssClass="breadcrumb" runat="server" Visible="true">
        Você está em: <strong>Fluxo de Bolsistas</strong> &raquo;
                <asp:HyperLink NavigateUrl="~/webapp/editais/FrmListar.aspx" ID="HyperLink2" runat="server">Instrumentos de Seleção em aberto</asp:HyperLink>
                <strong>&raquo;</strong>
                <asp:HyperLink NavigateUrl="~/webapp/editais/FrmListar.aspx" ID="hpetapas" runat="server">Etapas Mensais</asp:HyperLink>
                <strong>&raquo;</strong>
                <asp:Label ID="Label4" Text="Gerenciamento dos bolsistas" runat="server" Font-Bold="True"></asp:Label>
            </asp:Panel>
<!-- FIM BREADCRUMB -->

<div>
    <fieldset>
        <legend>Buscar</legend>
        Nome: <asp:TextBox ID="txtBuscaNome" runat="server" Width="478px"></asp:TextBox>
        <asp:Button ID="btBuscarAuxiliado" runat="server" Text="Buscar" 
            onclick="btBuscarAuxiliado_Click" />
        <asp:Button ID="btMostrar" runat="server" Text="Mostrar Todos" 
            onclick="btMostrar_Click" />
    </fieldset>
    
</div>

<div style="padding: 5px; text-align: left;">
                <h1>
                    <span class="allcaps">
                        Administração dos Auxiliados » Edital de Assistência Estudantil 2013/01
                    </span>
                </h1>               
                

                <div id="containerMensagens">
                    <asp:Panel ID="PanelMsgResultado" runat="server" Visible="false">
                        <asp:Label ID="lblMsgResultado" runat="server"></asp:Label>
                    </asp:Panel>
                </div>

    <asp:LinkButton ID="lnkbtNovo" runat="server" CssClass="minibutton" OnClick="btNovo_Click">
                    <span>Cadastrar novo auxíliado</span>
                    </asp:LinkButton>

                    

<div style="margin-top:20px;">
<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        DataSourceID="odsListAuxiliados" Width="100%" CssClass="mGrid" 
        onrowcommand="GridView1_RowCommand">
        
        <Columns>
            <asp:TemplateField>
                <HeaderTemplate>
                    Nome do Aluno
                </HeaderTemplate>
                <ItemTemplate>
                    <%# ((ufmt.ssbic.DataAccess.AuxiliadoAlunoSiga)Container.DataItem).Aluno.Nome %>
                </ItemTemplate>
                
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    Auxílios
                </HeaderTemplate>
                <ItemTemplate>
                    <%# 
                        ((ufmt.ssbic.DataAccess.AuxiliadoAlunoSiga)Container.DataItem).ListAuxilioAluno.ElementAtOrDefault(0) != null ? 
                    ((ufmt.ssbic.DataAccess.AuxiliadoAlunoSiga)Container.DataItem).ListAuxilioAluno.ElementAt(0).Auxilio_1.nome :
                                            ""
                    %>
                    <br />
                    <%# 
                        ((ufmt.ssbic.DataAccess.AuxiliadoAlunoSiga)Container.DataItem).ListAuxilioAluno.ElementAtOrDefault(1) != null ? 
                    ((ufmt.ssbic.DataAccess.AuxiliadoAlunoSiga)Container.DataItem).ListAuxilioAluno.ElementAt(1).Auxilio_1.nome :
                                            ""
                    %>
                    <br />
                    <%# 
                        ((ufmt.ssbic.DataAccess.AuxiliadoAlunoSiga)Container.DataItem).ListAuxilioAluno.ElementAtOrDefault(2) != null ? 
                    ((ufmt.ssbic.DataAccess.AuxiliadoAlunoSiga)Container.DataItem).ListAuxilioAluno.ElementAt(2).Auxilio_1.nome :
                                            ""
                    %>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField>
                <HeaderTemplate>
                    Situação
                </HeaderTemplate>
                <ItemTemplate>
                    <%# ((ufmt.ssbic.DataAccess.AuxiliadoAlunoSiga)Container.DataItem).AuxilioFolhaPagamento.ativo == true ? "Ativo" : "Inativo" %>
                </ItemTemplate>
                
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    Opções
                </HeaderTemplate>
                <ItemTemplate>
                <asp:HyperLink ID="lnkAtualizarRegistro" runat="server" 
                    NavigateUrl='<%# "FrmEditar.aspx?auxiliadoUID=" + 
                        ((ufmt.ssbic.DataAccess.AuxiliadoAlunoSiga)Container.DataItem).Auxiliado.auxiliadoUID + 
                        "&folhaPagamentoUID=" + 
                        Request.QueryString["folhaPagamentoUID"]
                         %>'>
                    Editar</asp:HyperLink>

                    |
                    <asp:LinkButton ID="lnkExcluir" runat="server"  CommandName="excluir" Text="Excluir" CommandArgument='<%# Eval("AuxiliadoUID") %>' />
<%--
                    <asp:HyperLink ID="HyperLink1" runat="server" 
                    NavigateUrl='<%# "FrmEditar.aspx?auxiliadoUID=" + 
                        ((ufmt.ssbic.DataAccess.AuxiliadoAlunoSiga)Container.DataItem).Auxiliado.auxiliadoUID + 
                        "&folhaPagamentoUID=" + 
                        Request.QueryString["folhaPagamentoUID"]
                         %>'>
                    Excluir</asp:HyperLink>--%>

                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
</asp:GridView>
</div>

    <asp:ObjectDataSource ID="odsListAuxiliados" runat="server"
         SelectMethod="GetAuxiliados" 
                            TypeName="ufmt.ssbic.Business.AuxilioController">
                            <SelectParameters>
                                <asp:QueryStringParameter Name="folhaPagamentoUID" 
                                    QueryStringField="folhaPagamentoUID" Type="Int32" />
                               
                            </SelectParameters>
    </asp:ObjectDataSource>

    </div>
</asp:Content>
