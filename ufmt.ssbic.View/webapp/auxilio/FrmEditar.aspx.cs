﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.View.webapp.auxilio
{
    public partial class FrmEditar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int auxiliadoUID = int.Parse(Request.QueryString["auxiliadoUID"]);
                AuxilioController controller = new AuxilioController();
                vwAluno aluno = controller.GetAlunoAuxiliado(auxiliadoUID);

                lblNomeAluno.Text = aluno.Nome;
                lblRGA.Text = aluno.Matricula.ToString();
                lblCPF.Text = aluno.CPF;
                lblCurso.Text = aluno.Curso;
                lblCampus.Text = aluno.Campus;

                Auxiliado auxiliado = controller.GetAuxiliado(auxiliadoUID);

                txtAgencia.Text = auxiliado.agencia;
                txtConta.Text = auxiliado.numeroConta;

                if (auxiliado.bancoUID.HasValue)
                    ddlBanco.SelectedValue = auxiliado.bancoUID.ToString();
            }
        }

        protected void chkAuxilios_DataBound(object sender, EventArgs e)
        {
            AuxilioController controller = new AuxilioController();
            List<AuxilioAluno> auxiliosAluno = controller.GetAuxiliosAuxiliado(int.Parse(Request.QueryString["auxiliadoUID"]));

            foreach (ListItem chk in chkAuxilios.Items)
            {
                AuxilioAluno auxilioAluno = auxiliosAluno.Where(a => a.auxilioUID == int.Parse(chk.Value)).FirstOrDefault();
                if (auxilioAluno != null)
                {
                    chk.Selected = true;
                }
            }

        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            AuxilioController controller = new AuxilioController();
            int auxiliadoUID = int.Parse(Request.QueryString["auxiliadoUID"]);
            vwAluno aluno = controller.GetAlunoAuxiliado(auxiliadoUID);

            Auxiliado auxiliado = controller.GetAuxiliado(auxiliadoUID);
            
            if (rdoImportaDados.SelectedValue.Equals("1"))
            {
                int? bancoUID = null;
                if (!String.IsNullOrEmpty(aluno.CodigoFebrabram))
                {
                    Banco banco = new BancoBO().GetBanco(aluno.CodigoFebrabram);
                    bancoUID = banco.bancoUID;
                }

                auxiliado.bancoUID = bancoUID;
                auxiliado.agencia = aluno.Agencia;
                auxiliado.numeroConta = aluno.ContaCorrente;                        
            }
            else
            {
                int bancoUID = int.Parse(ddlBanco.SelectedValue);
                string agencia = txtAgencia.Text;
                string conta = txtConta.Text;
                auxiliado.bancoUID = bancoUID;
                auxiliado.agencia = agencia;
                auxiliado.numeroConta = conta;                
            }

            List<Auxilio> listAuxilios = new List<Auxilio>();            

            List<Auxilio> auxiliosSelecionados = new List<Auxilio>();

            foreach (ListItem item in chkAuxilios.Items)
            {
                if (item.Selected)
                {
                    auxiliosSelecionados.Add(
                        controller.GetAuxilio(int.Parse(item.Value))
                        );
                }
            }

            int folhaPagamentoUID = int.Parse(Request.QueryString["folhaPagamentoUID"]);
            controller.AtualizarAuxiliado(auxiliado, auxiliosSelecionados, folhaPagamentoUID);

            Response.Redirect("FrmListar.aspx?folhaPagamentoUID=" + folhaPagamentoUID);
        }

        protected void rdoImportaDados_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdoImportaDados.SelectedValue.Equals("2"))
            {
                pnlDadosBancarios.Visible = true;
            }
            else
            {
                pnlDadosBancarios.Visible = false;
            }
        }
    }
}