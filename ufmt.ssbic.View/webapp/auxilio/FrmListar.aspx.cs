﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.View.session;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.View.webapp.auxilio
{
    public partial class FrmListar : VerificaSession
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int folhaPagamentoUID = int.Parse(Request.QueryString["folhaPagamentoUID"]);

                new AuxilioController().AtualizarVinculoInstitucional(folhaPagamentoUID);
            }
        }

        protected void btNovo_Click(object sender, EventArgs e)
        {
            Response.Redirect("FrmNovoAuxiliado.aspx?folhaPagamentoUID=" + Request.QueryString["folhaPagamentoUID"]);
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("excluir"))
            {
                int auxiliadoUID = int.Parse(e.CommandArgument.ToString());
                int folhaPagamentoUID = int.Parse(Request.QueryString["folhaPagamentoUID"]);

                AuxilioController controller = new AuxilioController();
                controller.Excluir(auxiliadoUID, folhaPagamentoUID);
                GridView1.DataBind();
            }
        }

        protected void btBuscarAuxiliado_Click(object sender, EventArgs e)
        {
            int folhaPagamentoUID = int.Parse(Request.QueryString["folhaPagamentoUID"]);
            string nome = txtBuscaNome.Text;
            AuxilioController ac = new AuxilioController();
            List<AuxiliadoAlunoSiga> alunos = ac.GetAuxiliados(folhaPagamentoUID, nome);

            GridView1.DataSource = alunos;
            GridView1.DataSourceID = null;
            GridView1.DataBind();
        }

        protected void btMostrar_Click(object sender, EventArgs e)
        {
            GridView1.DataSource = null;
            GridView1.DataSourceID = "odsListAuxiliados";
            GridView1.DataBind();
        }
    }
}