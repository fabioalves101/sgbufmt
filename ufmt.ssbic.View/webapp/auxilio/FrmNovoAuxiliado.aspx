﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true" CodeBehind="FrmNovoAuxiliado.aspx.cs" Inherits="ufmt.ssbic.View.webapp.auxilio.FrmNovoAuxiliado" %>
<%@ Register src="../../controles/UCBuscarAluno.ascx" tagname="UCBuscarAluno" tagprefix="uc1" %>
<%@ Register src="../../controles/UCDadosBancariosAluno.ascx" tagname="UCDadosBancariosAluno" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<style>
.mGrid td {color:#000;}
.mGrid td a{color:#000;}
</style>

<!-- BREADCRUMB -->
        <asp:Panel ID="breadcrumb" CssClass="breadcrumb" runat="server" Visible="true">
        Você está em: <strong>Fluxo de Bolsistas</strong> &raquo;
                <asp:HyperLink NavigateUrl="~/webapp/editais/FrmListar.aspx" ID="HyperLink2" runat="server">Instrumentos de Seleção em aberto</asp:HyperLink>
                <strong>&raquo;</strong>
                <asp:HyperLink NavigateUrl="~/webapp/editais/FrmListar.aspx" ID="hpetapas" runat="server">Etapas Mensais</asp:HyperLink>
                <strong>&raquo;</strong>
                <asp:Label ID="Label4" Text="Gerenciamento dos bolsistas" runat="server" Font-Bold="True"></asp:Label>
            </asp:Panel>
<!-- FIM BREADCRUMB -->

<div style="padding: 5px; text-align: left;">
                <h1>
                    <span class="allcaps">
                        Cadastrar novo auxiliado » Edital de Assistência Estudantil 2013/01
                    </span>
                </h1>
                

                <div id="containerMensagens">
                    <asp:Panel ID="PanelMsgResultado" runat="server" Visible="false">
                        <asp:Label ID="lblMsgResultado" runat="server"></asp:Label>
                    </asp:Panel>
                </div>
<h2>Cadastrar novo auxiliado</h2>
    <asp:Panel ID="PanelFormulario" runat="server">
    <uc1:UCBuscarAluno ID="UCBuscarAluno1" runat="server" />
    
    <div class="containerFormulario">
    
    <asp:CheckBoxList ID="chkAuxilios" runat="server" DataSourceID="odsAuxilios" 
            DataTextField="nome" DataValueField="auxilioUID">
    </asp:CheckBoxList>
        <asp:ObjectDataSource ID="odsAuxilios" runat="server" 
            SelectMethod="GetAuxilios" TypeName="ufmt.ssbic.Business.AuxilioController">
        </asp:ObjectDataSource>
        <br />
    <hr />
        <asp:RadioButtonList ID="rdoImportaDados" runat="server" AutoPostBack="True" 
            RepeatDirection="Horizontal" 
            onselectedindexchanged="rdoImportaDados_SelectedIndexChanged">
            <asp:ListItem Value="1">Importar do SIGA</asp:ListItem>
            <asp:ListItem Value="2">Cadastrar Dados Bancários</asp:ListItem>
        </asp:RadioButtonList>
        <asp:Panel ID="pnlDadosBancarios" runat="server" Visible="False">
        <p>
            Banco: 
            <asp:DropDownList ID="ddlBanco" runat="server" DataSourceID="odsBancos" 
                DataTextField="banco" DataValueField="bancoUID">
            </asp:DropDownList>
            <asp:ObjectDataSource ID="odsBancos" runat="server" SelectMethod="GetBancos" 
                TypeName="ufmt.ssbic.Business.BancoBO"></asp:ObjectDataSource>
        </p>
            
        <p>
            Agência:<asp:TextBox ID="txtAgencia" runat="server"></asp:TextBox>
        </p>
        <p>
            Número da Conta:<asp:TextBox ID="txtConta" runat="server"></asp:TextBox>
        </p>
        </asp:Panel>
        <hr />
        <asp:Button ID="btnSalvar" runat="server" Text="Salvar" 
            onclick="btnSalvar_Click" />    
    </div>

    </asp:Panel>
    </div>
</asp:Content>
