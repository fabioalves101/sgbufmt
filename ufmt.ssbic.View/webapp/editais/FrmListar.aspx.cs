﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;
using ufmt.ssbic.View.session;
using System.Collections;

namespace ufmt.ssbic.View.webapp.editais
{
    public partial class FrmListar : VerificaSession
    {
        public int? ProReitoriaUID { get; set; }
        public String Criterio { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                BolsaBO objBO = new BolsaBO();
                //string programaUID = Session["programaUID"].ToString();
                //string proreitoria = objBO.GetPrograma(int.Parse(programaUID));
                
                lblResultadoConsulta.Text = "<b>Mostrando todos</b>";
                PreecherGridView(Session["programaUID"].ToString(), txtCriterio.Text, false);

                Label1.Text = "Administração dos Instrumentos de Seleção em aberto &raquo;";// +proreitoria;
            }
        }

        protected void PreecherGridView(string programaUID, String criterio, bool visible)
        {

            PanelMsgResultado.Visible = visible;
            if (!String.IsNullOrEmpty(programaUID))
                this.ProReitoriaUID = int.Parse(programaUID);
            else
                this.ProReitoriaUID = null;

            if (this.ProReitoriaUID == 5)
                this.ProReitoriaUID = null;

            if (this.ProReitoriaUID == 6)
                this.ProReitoriaUID = null;


            InstrumentoSelecaoBO objBO = new InstrumentoSelecaoBO();
            List<vwInstrumentoSelecaoBolsa> registros = null;

            long? cursoUID = null;
            InstrumentoSelecaoResponsavel responsavelIS = null;
            if (Session["permissao"].ToString().Contains("COORDENADOR"))
            {
                responsavelIS = objBO.GetResponsavelPorInstrumentoSelecao(long.Parse(Session["usuarioUID"].ToString()));
                if(responsavelIS != null)
                    cursoUID = responsavelIS.cursoUID;
            }
            if (responsavelIS != null)
            {
                
                registros = objBO.GetInstrumentosPorResponsavel(this.ProReitoriaUID, criterio, cursoUID, responsavelIS);
                
            } 
            else 
            {
                //Consulta
                if (!String.IsNullOrEmpty(criterio))
                {
                    registros = objBO.FindRegistrosISB(this.ProReitoriaUID, criterio, cursoUID);
                }
                else
                {
                    registros = objBO.FindRegistrosISB(this.ProReitoriaUID, null, cursoUID);
                }
            }
            GdrEditais.DataSource = registros;
            lblTotalRegistros.Text = registros.Count.ToString() + " registro(s) disponível(is)";
            VerificarPermissoesModulos();

            GdrEditais.DataBind();


            if (registros.Count > 0)
            {

                /*ImageButton btnPrev = (ImageButton)GdrEditais.BottomPagerRow.FindControl("cmdPrev");
                if (GdrEditais.PageIndex == 0)
                    btnPrev.Enabled = false;
                else
                    btnPrev.Enabled = true;
                */
            }



        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "gerarFolha")
            {
                hdProcessoSeletivo.Value = e.CommandArgument.ToString();
                ModalPopupExtender1.Show();
            }

            if (e.CommandName == "fecharInstrumento")
            {
                ProcessoSeletivoBO procBo = new ProcessoSeletivoBO();
                vwProcessoSeletivo processoSeletivo = procBo.GetProcessoSeletivo(int.Parse(e.CommandArgument.ToString()));
                
                InstrumentoSelecaoBO isBO = new InstrumentoSelecaoBO();
                isBO.FecharInstrumentoSelecao(processoSeletivo.instrumentoSelecaoUID);
                PreecherGridView(Session["programaUID"].ToString(), txtCriterio.Text, false);

                string msg = "Instrumento de Seleção fechado com sucesso.";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('" + msg + "');", true);
            }

            
        }

        protected void lnkConsultar_Click(object sender, EventArgs e)
        {
            PreecherGridView(Session["programaUID"].ToString(), txtCriterio.Text, false);
        }

        protected void lnkMostrarTodos_Click(object sender, EventArgs e)
        {
            PreecherGridView(Session["programaUID"].ToString(), null, false);
        }

        public String verificarSituacao(bool status)
        {
            String situacao;
            if (status)
                situacao = "Em Andamento";
            else
                situacao = "Concluído";

            return situacao;

        }

        protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
        {
            string msg = "Folha de pagamento gerada com sucesso.";
            DateTime dataInicio = DateTime.Parse( txtDataInicio.Text);
            DateTime dataFim = DateTime.Parse(txtDataFinal.Text);

            int processoSeletivoUID = int.Parse(hdProcessoSeletivo.Value.ToString());
            try
            {
                FolhaPagamentoBO folhaBO = new FolhaPagamentoBO();
                folhaBO.SalvarFolhaPagamento(processoSeletivoUID, dataInicio, dataFim);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('"+ msg +"');", true);
        }

        protected void VerificarPermissoesModulos()
        {
            Session["administracao"] = "true";
            if (Session["permissao"].ToString().Contains("ADMINISTRADOR"))
            {//ADMINISTRAÇÃO
                GdrEditais.Columns[3].Visible = false;
                GdrEditais.Columns[4].Visible = true;
            }
            else if (Session["permissao"].ToString().Contains("COORDENADOR"))
            {//COORDENADOR
                GdrEditais.Columns[3].Visible = false;
                GdrEditais.Columns[4].Visible = false;
            }
            else
            { //FINANCEIRO
                GdrEditais.Columns[3].Visible = false;
                GdrEditais.Columns[4].Visible = false;
            }
        }

        protected String VerificarInstrumentoConcluido(bool situacao, String stringRetorno)
        {
            if (situacao)
                return stringRetorno;
            else
                return "";
        }


    }

}