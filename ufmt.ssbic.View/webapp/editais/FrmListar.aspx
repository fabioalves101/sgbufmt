﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="FrmListar.aspx.cs" Inherits="ufmt.ssbic.View.webapp.editais.FrmListar" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

        <!-- BREADCRUMB -->
                <asp:Panel ID="breadcrumb" CssClass="breadcrumb" runat="server" Visible="true">
                Você está em: <strong>Fluxo de Bolsistas</strong> &raquo;
                        <asp:Label ID="Label4" Text="Instrumentos de Seleção em aberto" runat="server" Font-Bold="True"></asp:Label>
                    </asp:Panel>

        <!-- FIM BREADCRUMB -->
            <div style="padding: 5px; text-align: left;">
                <h1>
                    <span class="allcaps">
                        <asp:Label ID="Label1" runat="server"></asp:Label>
                    </span>
                </h1>
                

                <div id="containerMensagens">
                    <asp:Panel ID="PanelMsgResultado" runat="server" Visible="false">
                        <asp:Label ID="lblMsgResultado" runat="server"></asp:Label>
                    </asp:Panel>
                </div>
                <fieldset style="padding: 10px">
                    <legend>
                        <img alt="Filtrar Registros" class="style2" src="../../images/ico_filters.png" />
                        Filtar Registros</legend>
                    <asp:Panel ID="PanelFiltro" runat="server">
                        &nbsp;<asp:TextBox ID="txtCriterio" runat="server" Width="503px" EnableViewState="False"
                            ViewStateMode="Disabled"></asp:TextBox>
                        <asp:LinkButton ID="lnkConsultar" runat="server" CssClass="minibutton" 
                            onclick="lnkConsultar_Click" CausesValidation="false">
                                       
    <span>Consultar</span>                   
                        </asp:LinkButton>
                        <asp:LinkButton ID="lnkMostrarTodos" runat="server" CssClass="minibutton" 
                            onclick="lnkMostrarTodos_Click" CausesValidation="false">
  
    <span>
        Mostrar Todos
    </span>
                                             
                        </asp:LinkButton>
                    </asp:Panel>
                </fieldset>
                <!--GRIDVIEW -->
                <asp:Panel ID="PanelAcoes" runat="server" Style="padding: 10px">
                    <div style="clear: both">
                        <asp:GridView ID="GdrEditais" runat="server"
                        AutoGenerateColumns="False" 
                        OnPageIndexChanging="GridView1_PageIndexChanging" Width="100%" CssClass="mGrid" 
                        onrowcommand="GridView1_RowCommand">
                        <Columns>
                            <asp:TemplateField HeaderText="Descrição" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                
                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="detalhes" 
                                        Text='<%# Eval("descricao") %>' 
                                        CommandArgument='<%# Eval("instrumentoSelecaoUID") %>' 
                                        CausesValidation="False" /> <%--- 
                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="detalhes" 
                                        Text='<%# Eval("descricaoProcesso") %>' 
                                        CommandArgument='<%# Eval("instrumentoSelecaoUID") %>' 
                                        CausesValidation="False" />--%>
                                    <br />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Width="40%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Situação" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>                
                                    <asp:Label ID="Label3" runat="server" Text='<%# verificarSituacao((bool)Eval("situacao")) %>'></asp:Label>
                                    <br />
                                </ItemTemplate>
                                
                                <ItemStyle HorizontalAlign="Center" Width="20%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Administrar" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    
                                    <asp:HyperLink ID="HyperLink1" NavigateUrl='<%# Eval("processoSeletivoUID", "../folha/FrmListar.aspx?processoSeletivoUID={0}") %>' Text='<%# VerificarInstrumentoConcluido((bool)Eval("situacao"), "Administrar") %>' runat="server"></asp:HyperLink>
                                                               
                                    
                                    <br />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="20%" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Operações" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton3" CausesValidation="false" CommandArgument='<%# Eval("processoSeletivoUID") %>' CommandName="gerarFolha" runat="server">Gerar Próxima Folha de Pagamento</asp:LinkButton>               
                                    <br />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="20%" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton OnClientClick="return confirm('Tem certeza que deseja fechar o instrumento de seleção?');" 
                                    ID="lnkFechar" CausesValidation="false" CommandArgument='<%# Eval("processoSeletivoUID") %>' CommandName="fecharInstrumento" Text='<%# VerificarInstrumentoConcluido((bool)Eval("situacao"), "Fechar Instrumento de Seleção") %>' runat="server"></asp:LinkButton>               
                                    <br />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="20%" />
                            </asp:TemplateField>

                        </Columns>
                        </asp:GridView>
                    </div>
                    <div class="addbutton" style="float: left; margin-right: 5px">
                        <asp:Label ID="lblResultadoConsulta" runat="server"></asp:Label>
                        &nbsp;<asp:Label ID="lblTotalRegistros" runat="server" CssClass="ArialBlack16"></asp:Label>
                    </div>
                    <div style="clear: both" />
                    </asp:Panel>
            <!-- FIM GRIDVIEW -->    
            <!-- PAINEL MODAL POPUP -->        
                
               <asp:modalpopupextender 
                ID="ModalPopupExtender1"
                BehaviorID="behavior"
                runat="server"
                TargetControlID="lnkModal"
                PopupControlID="PanelDetalhes"
                BackgroundCssClass="modalBackground"
                DropShadow="True"
                DynamicServicePath="" 
                Enabled="True"
        
        />

        <asp:HyperLink ID="lnkModal" runat="server" Visible="true" Text=""  />

 <asp:Panel ID="PanelDetalhes" runat="server" CssClass="modalPopup" style="display:none">
          <div id="ContainerDetalhes" style="width:800px">
          <div id="MenuDetalhes" style="background-color:White;padding-left:5px;padding-bottom:5px;height:40px">
            <div style="width:650px; float:left; height: 32px;">
                <h2 style="height: 35px"><asp:Label ID="Label2" runat="server" Text="Label">Gerar próxima folha de pagamento</asp:Label></h2>
            </div>
            <div style="float:left;width:130px;text-align:right; padding-top:10px;">
                <asp:ImageButton ID="ImageButton1" runat="server" 
                    ImageUrl="../../images/close24.png" OnclientClick="$find('behavior').hide(); return false;" ToolTip="Fechar Janela" ImageAlign="AbsMiddle" />
                &nbsp;<asp:LinkButton ID="LinkButtonFechar" runat="server" Visible="true" Text="Fechar" OnclientClick="$find('behavior').hide(); return false;"   />
            </div>
          </div>
           <div id="corpoDetalhes" style="border:1px solid #cccccc;height:450px;padding-top:10px;background-color:#f7f7f7">
            
               <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                   CssClass="ResumoErro" ForeColor="Red" 
                   HeaderText="Para continuar, preencha corretamente os campos abaixo:" />
            
               <table class="Largura100Porcento">
                  
                   <tr>
                       <td class="Largura150">
                           <asp:Label ID="Label5" runat="server" style="font-weight: 700" 
                               Text="Data de Início:"></asp:Label>
                           <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                               ControlToValidate="txtDataInicio" ErrorMessage="Data de Aplicação" 
                               ForeColor="Red">*</asp:RequiredFieldValidator>
                       </td>
                       <td>
                           <asp:TextBox ID="txtDataInicio" runat="server" Width="92px"></asp:TextBox>
                           <asp:CalendarExtender ID="txtDataAplicacao_CalendarExtender" runat="server" 
                               Enabled="True" Format="dd/MM/yyyy" PopupButtonID="imgCalendarioData1" 
                               TargetControlID="txtDataInicio">
                           </asp:CalendarExtender>
                           <asp:MaskedEditExtender ID="txtDataAplicacao_MaskedEditExtender" runat="server" 
                               CultureDateFormat="pt-br" Enabled="True" Mask="99/99/9999" MaskType="Date" 
                               TargetControlID="txtDataInicio">
                           </asp:MaskedEditExtender>
                           <asp:Image ID="imgCalendarioData1" runat="server" 
                               ImageUrl="~/images/Calendar_scheduleHS.png" />
                           <asp:MaskedEditValidator ID="MEVData1" runat="server" 
                               ControlExtender="txtDataAplicacao_MaskedEditExtender" 
                               ControlToValidate="txtDataInicio" CssClass="failureNotification" 
                               Display="Dynamic" ForeColor="Red" 
                               InvalidValueMessage="Data no formato inválido" IsValidEmpty="True"></asp:MaskedEditValidator>
                       </td>
                   </tr>
                   <tr>
                       <td class="Largura150">
                           <asp:Label ID="Label10" runat="server" style="font-weight: 700" 
                               Text="Data Final:"></asp:Label>
                           <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                               ControlToValidate="txtDataFinal" ErrorMessage="Data do Resultado" 
                               ForeColor="Red">*</asp:RequiredFieldValidator>
                       </td>
                       <td>
                           <asp:TextBox ID="txtDataFinal" runat="server" Width="92px"></asp:TextBox>
                           <asp:MaskedEditExtender ID="txtDataResultado_MaskedEditExtender" runat="server" 
                               CultureDateFormat="pt-br" Enabled="True" Mask="99/99/9999" MaskType="Date" 
                               TargetControlID="txtDataFinal">
                           </asp:MaskedEditExtender>
                           <asp:CalendarExtender ID="CalendarExtender_txtDataResultado" runat="server" 
                               Enabled="True" Format="dd/MM/yyyy" PopupButtonID="imgCalendarioData2" 
                               TargetControlID="txtDataFinal">
                           </asp:CalendarExtender>
                           <asp:Image ID="imgCalendarioData2" runat="server" 
                               ImageUrl="~/images/Calendar_scheduleHS.png" />
                           <asp:MaskedEditValidator ID="MEVData2" runat="server" 
                               ControlExtender="txtDataResultado_MaskedEditExtender" 
                               ControlToValidate="txtDataFinal" CssClass="failureNotification" 
                               Display="Dynamic" ForeColor="Red" 
                               InvalidValueMessage="Data no formato inválido" IsValidEmpty="True">
                      </asp:MaskedEditValidator>
                       </td>
                   </tr>
                
               </table>
               <p>
    <asp:ImageButton onmouseover="~/images/savebutton_hover.png" 
    onmouseout="~/images/savebutton.png" ID="btnSalvar" runat="server" 
    ImageUrl="~/images/savebutton.png" onclick="btnSalvar_Click" 
        style="height: 28px" />

        </p>

               <asp:HiddenField ID="hdProcessoSeletivo" runat="server" />
           </div> 


          </div>      
</asp:Panel>
                <!-- FIM MODAL POPUP -->
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
