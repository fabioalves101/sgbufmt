﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="ProjetosProfessor.aspx.cs" Inherits="ufmt.ssbic.View.relatorios.ProjetosProfessor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<h1>Projetos por Professor</h1>
    Buscar: <asp:TextBox ID="txtBusca" runat="server" Width="400px"></asp:TextBox>
    <asp:DropDownList ID="ddlTipoBusca" runat="server">
        <asp:ListItem Value="1">Nome</asp:ListItem>
        <asp:ListItem Value="2">CPF</asp:ListItem>
    </asp:DropDownList>
    <asp:Button ID="btBuscar" runat="server" Text="Buscar" 
        onclick="btBuscar_Click" />
        
    <asp:GridView ID="grdProfessor" runat="server" AllowPaging="True" 
        AutoGenerateColumns="False" DataSourceID="odsProfessorProjeto"
         Width="100%"
                CssClass="mGrid"
                PagerStyle-CssClass="pgr" DataKeyNames="pessoaMembroUID" onselectedindexchanged="grdProfessor_SelectedIndexChanged"
        >
        <Columns>
            <asp:TemplateField HeaderText="Selecionar" ItemStyle-Width="60px">
                <ItemTemplate>
                <asp:LinkButton CssClass="minibutton" ID="LinkButton2" runat="server" CausesValidation="False" 
                                CommandName="Select">
                            <span>
                                <img src="../media/images/select.png" alt="" />
                                Selecionar
                            </span>
                </asp:LinkButton>

                </ItemTemplate>

<ItemStyle Width="60px"></ItemStyle>
            </asp:TemplateField>
            <asp:BoundField DataField="registro" HeaderText="SIAPE" 
                SortExpression="registro" />
            <asp:BoundField DataField="cpf" HeaderText="CPF" SortExpression="cpf" />
            <asp:BoundField DataField="nome" HeaderText="Nome" SortExpression="nome" />
            <asp:BoundField DataField="instituicao" HeaderText="Instituição" 
                SortExpression="instituicao" />
        </Columns>

<PagerStyle CssClass="pgr"></PagerStyle>
    </asp:GridView>

    <asp:ObjectDataSource ID="odsProfessorProjeto" runat="server" 
        SelectMethod="BuscaPessoaMembro" TypeName="ufmt.ssbic.Business.MembroBO">
        <SelectParameters>
            <asp:ControlParameter ControlID="txtBusca" Name="param" PropertyName="Text" 
                Type="String" />
            <asp:ControlParameter ControlID="ddlTipoBusca" Name="tipoParam" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

</asp:Content>
