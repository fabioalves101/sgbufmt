﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Stimulsoft.Report;
using Stimulsoft.Report.Web;

namespace ufmt.ssbic.View.relatorios
{
    public partial class ProjetosFinanciados : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string descricao = String.Empty;
            GerarRelatorio(string.Empty, "ProjetosFinanciados");
        }

        public void GerarRelatorio(string basedados, string nomeRelatorio)
        {
            StiReport report = new StiReport();
            report.Load(AppDomain.CurrentDomain.BaseDirectory + "reports\\" + nomeRelatorio + ".mrt");
            report.Compile();
            StiReportResponse.ResponseAsPdf(this, report, true);
        }
    }
}