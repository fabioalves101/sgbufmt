﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.View.session;
using ufmt.ssbic.Business;
using Stimulsoft.Report;
using Stimulsoft.Report.Web;

namespace ufmt.ssbic.View.webapp.relatorio
{
    public partial class FrmRelatorio : VerificaSession
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                /* TODO: Aplicar links de relatórios para os seguintes itens:

                    Por tipo de bolsa (PIBIC-PIBIT ou Ação Afirmativa)
                    Por professor
                    Por instituto
                    Por departamento
                    Por curso em que o aluno está matriculado
                    Por fonte de financiamento (o aluno será locado por nós após seleção, portanto este item só será apresentado para os APROVADOS)

                */

                

                if (!String.IsNullOrEmpty(Request.QueryString["folhaPagamentoUID"]))
                {
                    if (!String.IsNullOrEmpty(Request.QueryString["suplementar"]))
                    {
                        GerarRelatorio("FolhaDePagamentoFinanceiroSuplementar", int.Parse(Request.QueryString["folhaPagamentoUID"]));
                    }
                    else if (!String.IsNullOrEmpty(Request.QueryString["administrador"]))
                    {
                        GerarRelatorio("FolhaDePagamentoAdministrador", int.Parse(Request.QueryString["folhaPagamentoUID"]));
                    }
                    else
                    {
                        GerarRelatorio("FolhaDePagamentoFinanceiro", int.Parse(Request.QueryString["folhaPagamentoUID"]));
                    }
                }

                BolsaBO objBO = new BolsaBO();
                string programaUID = Session["programaUID"].ToString();
                string proreitoria = objBO.GetPrograma(int.Parse(programaUID));
                Label1.Text = "Relatórios &raquo;" + proreitoria;

                if (programaUID == "3")
                {

                    PanelRelatorioPropeq.Visible = true;

                }
                else {

                    PanelRelatorioPropeq.Visible = false;
                }

            }
            
        }



        public void GerarRelatorio(string nomeRelatorio, int folhaPagamentoUID)
        {
            StiReport report = new StiReport();
            report.Load(AppDomain.CurrentDomain.BaseDirectory + "reports\\" + nomeRelatorio + ".mrt");
            report.Compile();

            report.CompiledReport.DataSources["BolsistasFolhaPagamento"].Parameters["@folhaPagamentoUID"].ParameterValue = folhaPagamentoUID;

            StiReportResponse.ResponseAsPdf(this, report, true);
        }

        protected void btnGerarRelatorio_Click(object sender, EventArgs e)
        {
            GerarRelatorio(rblRelatorioBolsa.SelectedValue, int.Parse(Request.QueryString["folhaPagamentoUID"]));
        }

        protected void lnk_Click(object sender, EventArgs e)
        {
            int programaUID = int.Parse(Session["programaUID"].ToString());
        }



    }
}