﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true" CodeBehind="FrmRelatorioPagamentoBolsista.aspx.cs" Inherits="ufmt.ssbic.View.webapp.relatorio.FrmRelatorioPagamentoBolsista" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<h1>Relatório de Bolsistas Por Coordenador</h1>
Instrumento de Seleção: 
    <asp:DropDownList ID="ddlInstrumentosSelecao" runat="server" 
        DataSourceID="ObjectDataSource1" DataTextField="descricao" 
        DataValueField="instrumentoSelecaoUID">
    </asp:DropDownList>
     
     Ano: 
    <asp:TextBox ID="txtAno" Width="40px" runat="server"></asp:TextBox>

    <asp:Button ID="Button1" runat="server" Text="Gerar Relatório" 
        onclick="Button1_Click" />
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
        SelectMethod="GetListInstrumentoSelecaoPorPrograma" 
        TypeName="ufmt.ssbic.Business.RelatorioController">
        <SelectParameters>
            <asp:SessionParameter Name="programaUID" SessionField="programaUID" 
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
