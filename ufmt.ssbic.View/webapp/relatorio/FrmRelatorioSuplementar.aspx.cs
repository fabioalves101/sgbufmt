﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.View.session;
using ufmt.ssbic.Business;
using Stimulsoft.Report;
using Stimulsoft.Report.Web;

namespace ufmt.ssbic.View.webapp.relatorio
{
    public partial class FrmRelatorioSuplementar : VerificaSession
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!String.IsNullOrEmpty(Request.QueryString["folhaSuplementarUID"]))
                {
                    GerarRelatorio("FolhaSuplementarAdministrador", int.Parse(Request.QueryString["folhaSuplementarUID"]));
                }
            }
        }

        public void GerarRelatorio(string nomeRelatorio, int folhaSuplementarUID)
        {
            StiReport report = new StiReport();
            report.Load(AppDomain.CurrentDomain.BaseDirectory + "reports\\" + nomeRelatorio + ".mrt");
            report.Compile();

            report.CompiledReport.DataSources["BolsistasFolhaPagamento"].Parameters["@folhaPagamentoUID"].ParameterValue = folhaSuplementarUID;

            StiReportResponse.ResponseAsPdf(this, report, true);
        }
        
    }
}