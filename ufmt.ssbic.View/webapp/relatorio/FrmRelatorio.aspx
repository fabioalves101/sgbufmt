﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true" CodeBehind="FrmRelatorio.aspx.cs" Inherits="ufmt.ssbic.View.webapp.relatorio.FrmRelatorio" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<h1>Relatórios</h1>
<asp:Panel ID="PanelRelatorios" runat="server" Visible="true">
    <ul>
        <li>
            <h3>
            <a href="FrmRelatorioBolsistaCoordenador.aspx">
                Relatório de Bolsistas com Coordenador
            </a>
            </h3>
        </li>

        <li>
            <h3>
            <a href="FrmRelatorioPagamentoBolsista.aspx">
                Relatório de Pagamento de Bolsistas por Ano
            </a>
            </h3>
        </li>
    </ul>
</asp:Panel>
    <asp:Panel ID="pnlRel" runat="server" Visible="false">
        <h1><span class="allcaps"><asp:Label ID="Label1" runat="server"></asp:Label>
            </span></h1>
        <div id="containerMensagens">
                <asp:Panel ID="PanelMsgResultado" runat="server" Visible="false">
                    <asp:Label ID="lblMsgResultado" runat="server"></asp:Label>
                </asp:Panel>
            </div>
        <div style="text-align:center; padding:20px">
            <asp:RadioButtonList ID="rblRelatorioBolsa" runat="server" 
                RepeatDirection="Horizontal">
                   <asp:ListItem Text="Tipo de bolsa" Value="TipoBolsa"></asp:ListItem>
                   <asp:ListItem Text="Professor" Value="ProfessorBolsa"></asp:ListItem>
                   <asp:ListItem Text="Bolsista" Value="AlunoBolsa"></asp:ListItem>
                   <asp:ListItem Text="Fonte de financiamento" Value="FonteFinanciamentoBolsa"></asp:ListItem>
            </asp:RadioButtonList>
            
            <br />
            <asp:Button ID="btnGerarRelatorio" runat="server" Text="Gerar Relatório" 
                onclick="btnGerarRelatorio_Click" />
        </div>
        </asp:Panel>
        <asp:Panel ID="PanelRelatorioPropeq" runat="server" Visible="false">

            <p>
    <asp:HyperLink ID="hpProjetosPeriodo" runat="server" 
        NavigateUrl="~/webapp/relatorio/ProjetosPeriodo.aspx">Projetos por Período</asp:HyperLink>
</p>

<p>
    <asp:HyperLink ID="HyperLink1" runat="server" 
        NavigateUrl="~/webapp/relatorio/ProjetosFinanciados.aspx">Projetos Financiados</asp:HyperLink>
</p>

<p>
    <asp:HyperLink ID="HyperLink2" runat="server" 
        NavigateUrl="~/webapp/relatorio/ProjetosProfessor.aspx">Projetos Por Professor (Coordenador / Membro)</asp:HyperLink>
</p>

<p>
    <asp:HyperLink ID="HyperLink3" runat="server" 
        NavigateUrl="~/webapp/relatorio/ProjetosUnidadeEspecifica.aspx">Projetos Por Unidade Específica</asp:HyperLink>
</p>
            
        </asp:Panel>


</asp:Content>

