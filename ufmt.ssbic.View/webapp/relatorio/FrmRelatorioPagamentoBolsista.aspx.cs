﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;
using Stimulsoft.Report;
using Stimulsoft.Report.Web;
using ufmt.ssbic.View.session;

namespace ufmt.ssbic.View.webapp.relatorio
{
    public partial class FrmRelatorioPagamentoBolsista : VerificaSession
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            int instrumentoSelecaoUID = int.Parse(ddlInstrumentosSelecao.SelectedValue);
            int programaUID = int.Parse(Session["programaUID"].ToString());

            int ano = 0;
            
            if (int.TryParse(txtAno.Text, out ano))
            {

                RelatorioController controller = new RelatorioController();
                Proreitoria programa = controller.GetPrograma(programaUID);

                this.GerarRelatorio("PagamentoDeBolsistas", instrumentoSelecaoUID, ano);
            }
        }

        public void GerarRelatorio(string nomeRelatorio, int instrumentoSelecaoUID, int ano)
        {
            StiReport report = new StiReport();
            report.Load(AppDomain.CurrentDomain.BaseDirectory + "reports\\" + nomeRelatorio + ".mrt");
            report.Compile();
            
            report.CompiledReport.DataSources["DataSource1"]
                .Parameters["@InstrumentoSelecaoUID"].ParameterValue = instrumentoSelecaoUID;

            report.CompiledReport.DataSources["DataSource1"]
                .Parameters["@Ano"].ParameterValue = ano;

            StiReportResponse.ResponseAsPdf(this, report, true);
        }
    }
}