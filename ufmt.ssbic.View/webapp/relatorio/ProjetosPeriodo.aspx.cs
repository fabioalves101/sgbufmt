﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Stimulsoft.Report;
using Stimulsoft.Report.Web;

namespace ufmt.ssbic.View.relatorios
{
    public partial class ProjetosPeriodo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btFiltrar_Click(object sender, EventArgs e)
        {
            DateTime inicio = DateTime.Parse(txtInicio.Text);
            DateTime fim = DateTime.Parse(txtFim.Text);

            GerarRelatorio(String.Empty, "Projetos", inicio, fim);
        }

        public void GerarRelatorio(string basedados, string nomeRelatorio, DateTime dataInicio, DateTime dataFim)
        {
            StiReport report = new StiReport();
            report.Load(AppDomain.CurrentDomain.BaseDirectory + "reports\\" + nomeRelatorio + ".mrt");
            report.Compile();

            report.CompiledReport.DataSources["ProcedureProjetos"].Parameters["@Inicio"].ParameterValue = dataInicio;
            report.CompiledReport.DataSources["ProcedureProjetos"].Parameters["@Fim"].ParameterValue = dataFim;

            StiReportResponse.ResponseAsPdf(this, report, true);
        }
    }
}