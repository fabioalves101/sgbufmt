﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Stimulsoft.Report;
using Stimulsoft.Report.Web;
using ufmt.ssbic.Business;

namespace ufmt.ssbic.View.relatorios
{
    public partial class CoordenadorProjetos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }
        public void GerarRelatorio(string basedados, string nomeRelatorio, int coordenadorUID)
        {
            StiReport report = new StiReport();
            report.Load(AppDomain.CurrentDomain.BaseDirectory + "reports\\" + nomeRelatorio + ".mrt");
            report.Compile();

            report.CompiledReport.DataSources["ProcedureCoordenadorProjeto"].Parameters["@CoordenadorUID"].ParameterValue = coordenadorUID;

            StiReportResponse.ResponseAsPdf(this, report, true);
        }

        protected void gdrListaServidores_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridView gridview = (GridView)sender;
            int pessoaMembroUID = int.Parse(gridview.SelectedValue.ToString());

            string descricao = String.Empty;
            GerarRelatorio(string.Empty, "CoordenadorProjeto", pessoaMembroUID);
        }
    }
}