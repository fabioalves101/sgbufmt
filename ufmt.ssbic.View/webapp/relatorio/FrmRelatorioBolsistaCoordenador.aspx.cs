﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;
using Stimulsoft.Report;
using Stimulsoft.Report.Web;
using ufmt.ssbic.View.session;

namespace ufmt.ssbic.View.webapp.relatorio
{
    public partial class FrmRelatorioBolsistaCoordenador : VerificaSession
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            int instrumentoSelecaoUID = int.Parse(ddlInstrumentosSelecao.SelectedValue);
            int programaUID = int.Parse(Session["programaUID"].ToString());

            RelatorioController controller = new RelatorioController();
            Proreitoria programa = controller.GetPrograma(programaUID);

            if (programa.descricao.Equals("PROCEV"))
            {
                this.GerarRelatorio("BolsistasPorCoordenadorCodex", instrumentoSelecaoUID, programaUID);
            }
            else
            {
                this.GerarRelatorio("BolsistasPorCoordenador", instrumentoSelecaoUID, programaUID);
            }
        }

        public void GerarRelatorio(string nomeRelatorio, int instrumentoSelecaoUID, int programaUID)
        {
            StiReport report = new StiReport();
            report.Load(AppDomain.CurrentDomain.BaseDirectory + "reports\\" + nomeRelatorio + ".mrt");
            report.Compile();
            
            report.CompiledReport.DataSources["BolsistasPorCoordenador"]
                .Parameters["@ProreitoriaUID"].ParameterValue = programaUID;
            
            report.CompiledReport.DataSources["BolsistasPorCoordenador"]
                .Parameters["@InstrumentoSelecaoUID"].ParameterValue = instrumentoSelecaoUID;

            StiReportResponse.ResponseAsPdf(this, report, true);
        }
    }
}