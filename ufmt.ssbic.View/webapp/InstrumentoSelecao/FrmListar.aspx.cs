﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;
using ufmt.ssbic.View.session;
using System.Collections;

namespace ufmt.ssbic.View.webapp.instrumentoSelecao
{
    public partial class FrmListar : VerificaSession
    {
        public int? ProReitoriaUID { get; set; }
        public String Criterio { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                BolsaBO objBO = new BolsaBO();
                string programaUID = Session["programaUID"].ToString();
                string proreitoria = objBO.GetPrograma(int.Parse(programaUID));
                string area = "Instrumento de Seleção";

                

                lblResultadoConsulta.Text = "<b>Mostrando todos</b>";
                PreecherGridView(Session["programaUID"].ToString(), txtCriterio.Text, false);
                lnkNovoRegistro.NavigateUrl = "FrmCadastrar.aspx?area=" + area;
                Label1.Text = "Instrumento de Seleção &raquo;" + proreitoria;
            }
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {


            GridView1.PageIndex = e.NewPageIndex;
            PreecherGridView(Session["programaUID"].ToString(), txtCriterio.Text, false);

        }

        protected void PreecherGridView(String proreitoriaUID, String criterio, bool visible)
        {

            PanelMsgResultado.Visible = visible;
            if (!String.IsNullOrEmpty(proreitoriaUID))
                this.ProReitoriaUID = int.Parse(proreitoriaUID);
            else
                this.ProReitoriaUID = null;

            InstrumentoSelecaoBO objBO = new InstrumentoSelecaoBO();
            List<vwInstrumentoSelecao> registros = null;


            //Consulta
            if (!String.IsNullOrEmpty(criterio))
            {
                registros = objBO.FindRegistros(this.ProReitoriaUID, criterio);

                 
            }
             //Listagem padrão
            else {

                registros = objBO.GetRegistros(this.ProReitoriaUID);


            }

            GridView1.DataSource = registros;
            lblTotalRegistros.Text = registros.Count.ToString() + " registro(s) disponível(is)";
            GridView1.DataBind();


            if (registros.Count > 0)
            {

                ImageButton btnPrev = (ImageButton)GridView1.BottomPagerRow.FindControl("cmdPrev");
                if (GridView1.PageIndex == 0)
                    btnPrev.Enabled = false;
                else
                    btnPrev.Enabled = true;

                divExcluir.Visible = true;
                
            }
            else
            {

                divExcluir.Visible = false;

            }

            

        }

        protected void cmdIr2_Click(object sender, ImageClickEventArgs e)
        {
            TextBox txt = (TextBox)GridView1.BottomPagerRow.FindControl("txtPagina");
            GridView1.PageIndex = (Convert.ToInt32(txt.Text) - 1);
            PreecherGridView(Session["programaUID"].ToString(), txtCriterio.Text, false);

        }

        protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                /*
                HyperLink lnkTituloResolucao = (HyperLink)e.Row.FindControl("lnkTituloResolucao");
                HyperLink lnkAtualizarRegistro = (HyperLink)e.Row.FindControl("lnkAtualizarRegistro");
                Label lblAssunto = (Label)e.Row.FindControl("lblAssunto");
               
                lnkAtualizarRegistro.NavigateUrl = "FrmEditar.aspx?registroUID=" + (int?)DataBinder.Eval(e.Row.DataItem, "bolsaUID") + "&proreitoriaUID=" + (int?)DataBinder.Eval(e.Row.DataItem, "proreitoriaUID");
                */

            }
        }

        protected void lnkExcluirSelecionados_Click(object sender, EventArgs e)
        {
            bool resultado = false;
            InstrumentoSelecaoBO objBO = new InstrumentoSelecaoBO();

            try
            {
                for (int i = 0; i <= GridView1.Rows.Count - 1; i++)
                {
                    CheckBox chk = new CheckBox();
                    chk = (CheckBox)GridView1.Rows[i].FindControl("chkRegistro");
                    Label lblID = (Label)GridView1.Rows[i].FindControl("lblID");

                    if (chk.Checked)
                    {
                        ArquivoInstrumentoSelecao arquivo = objBO.GetArquivoIs(int.Parse(lblID.Text));

                        if (arquivo != null)
                        {
                            String caminho = Server.MapPath("~/arquivos/instrumentosselecao");
                            if (System.IO.File.Exists(caminho + "\\" + arquivo.nomeArquivo))
                            {
                                System.IO.File.Delete(caminho + "\\" + arquivo.nomeArquivo);
                            }
                        }

                        resultado = objBO.Excluir(int.Parse(lblID.Text));
                    }

                }

                lblMsgResultado.Text = "Registro(s) excluído(s) com sucesso.";
                PanelMsgResultado.CssClass = "success";

            }
            catch (Exception err)
            {

                lblMsgResultado.Text = "Não foi possível excluir o(s) registro(s) selecionado(s).  Por favor, entre em contato com o administrador do sistema.<br />" + err.Message.ToString();
                PanelMsgResultado.CssClass = "error";

            } 

            PanelMsgResultado.Visible = true;
            PreecherGridView(Session["programaUID"].ToString(), txtCriterio.Text, true);
        }


        protected void lnkConsultar_Click(object sender, EventArgs e)
        {

            PreecherGridView(Session["programaUID"].ToString(), txtCriterio.Text, false);

        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "detalhes") {

                InstrumentoSelecaoBO objBO = new InstrumentoSelecaoBO();
                vwInstrumentoSelecao instrumentoSelecao = objBO.GetInstrumentoSelecao(int.Parse(e.CommandArgument.ToString()));
                LabelDetalhe1.Text = instrumentoSelecao.descricao;
                LabelDetalhe2.Text = instrumentoSelecao.bolsa;
                LabelDetalhe3.Text = instrumentoSelecao.tipoInstrumentoSelecao;
                LabelDetalhe4.Text = instrumentoSelecao.proreitoria;
                ModalPopupExtender1.Show();
                PanelMsgResultado.Visible = false;
            }

            if (e.CommandName == "financiador")
            {

                Response.Redirect("~/webapp/financiador/FrmListar.aspx?instrumentoSelecaoUID=" + e.CommandArgument.ToString());
            }

            if (e.CommandName == "processoSeletivo")
            {

                Response.Redirect("~/webapp/processoSeletivo/FrmListar.aspx?instrumentoSelecaoUID=" + e.CommandArgument.ToString());
            }

            if (e.CommandName == "responsaveis")
            {
                Response.Redirect("~/webapp/responsaveis/FrmListar.aspx?instrumentoSelecaoUID=" + e.CommandArgument.ToString());
            }



        }

        protected void lnkMostrarTodos_Click(object sender, EventArgs e)
        {
            PreecherGridView(Session["programaUID"].ToString(), null, false);
            txtCriterio.Text = String.Empty;
        }


    }



}