﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;
using ufmt.ssbic.View.session;
using System.Configuration;
using ufmt.ssbic.View.componentes;

namespace ufmt.ssbic.View.webapp.instrumentoSelecao
{
    public partial class FrmEditar : VerificaSession
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                InstrumentoSelecaoBO objBO = new InstrumentoSelecaoBO();

                string proreitoriaUID = Session["programaUID"].ToString();

                string area = Request.QueryString["area"];
                lblTitulo.Text = area;
                string registroUID = Request.QueryString["registroUID"];
                ddlTipo.DataSource = objBO.GetTipoInstrumentoSelecao();
                ddlTipo.DataTextField = "descricao";
                ddlTipo.DataValueField = "tipoInstrumentoSelecaoUID";
                ddlTipo.DataBound += new EventHandler(IndiceZero);
                ddlTipo.DataBind();

                BolsaBO objBolsa = new BolsaBO();
                ddlBolsa.DataSource = objBolsa.GetRegistros(int.Parse(proreitoriaUID));
                ddlBolsa.DataTextField = "descricao";
                ddlBolsa.DataValueField = "bolsaUID";
                ddlBolsa.DataBound += new EventHandler(IndiceZero);
                ddlBolsa.DataBind();

                vwInstrumentoSelecao insSel = objBO.GetInstrumentoSelecao(int.Parse(registroUID));
                txtDescricao.Text = insSel.descricao;
                ddlBolsa.SelectedValue = insSel.bolsaUID.ToString();
                ddlTipo.SelectedValue = insSel.tipoInstrumentoSelecaoUID.ToString();
                hddCodigoRegistro.Value = insSel.instrumentoSelecaoUID.ToString();
                
                ArquivoInstrumentoSelecao arquivo = objBO.GetArquivoIs(insSel.instrumentoSelecaoUID);
                if (arquivo != null)
                {
                    string caminho = this.BaseSiteUrl + "arquivos/instrumentosselecao/";
                    hpArquivoAtual.NavigateUrl = caminho + arquivo.nomeArquivo;
                }


            }

        }

        protected void IndiceZero(object sender, EventArgs e)
        {
            DropDownList objDropDownList = (DropDownList)sender; //Cast no sender para DropDownList
            objDropDownList.Items.Insert(0, new ListItem("Selecione", "")); //Adiciona um novo Item
        }
        protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                InstrumentoSelecaoBO objBO = new InstrumentoSelecaoBO();

                InstrumentoSelecao instrumento = objBO.GetInstrumento(int.Parse(hddCodigoRegistro.Value));

                InstrumentoSelecao insSel = new InstrumentoSelecao();
                insSel.instrumentoSelecaoUID = int.Parse(hddCodigoRegistro.Value);
                insSel.descricao = txtDescricao.Text;
                insSel.proreitoriaUID = int.Parse(Session["programaUID"].ToString());
                insSel.tipoInstrumentoSelecaoUID = int.Parse(ddlTipo.SelectedValue);
                insSel.bolsaUID = int.Parse(ddlBolsa.SelectedValue);
                insSel.situacao = instrumento.situacao;                

                bool hasArquivo = false;
                ObjUpload resultadoUpload = new ObjUpload();
                String descricaoArquivo = String.Empty;
                if (fUpload.HasFile)
                {
                    string caminho = Server.MapPath("~\\" + ConfigurationManager.AppSettings["diretorioUploadEditais"].ToString() + "\\");
                    ArquivoInstrumentoSelecao arquivo = objBO.GetArquivoIs(insSel.instrumentoSelecaoUID);
                    if (arquivo != null)
                    {
                        objBO.ExcluirArquivoIS(insSel.instrumentoSelecaoUID);
                    
                        if (System.IO.File.Exists(caminho + arquivo.nomeArquivo))
                        {
                            System.IO.File.Delete(caminho + arquivo.nomeArquivo);
                        }
                    }
                    
                    Upload objUpload = new Upload();
                    resultadoUpload = objUpload.UploadFile(fUpload, "3500000", ".pdf,.doc,.docx,.xls,.odt", caminho);

                    String msg = String.Empty;
                    if (resultadoUpload.MsgErro != null)//ocorreu um erro
                    {
                        throw new Exception("Houve um problema ao realizar o upload do arquivo. " + resultadoUpload.MsgErro);
                    }
                    else
                    {
                        hasArquivo = true;
                    }
                }

                objBO.Salvar(insSel, hasArquivo, resultadoUpload.NomeArquivo, 0);
                lblMsgResultado.Text = "Registro atualizado com sucesso.";
                PanelMsgResultado.CssClass = "success";
                PanelFormulario.Visible = false;
                btnSalvar.Visible = false;

            }
            catch (Exception err)
            {

                lblMsgResultado.Text = "Não foi possível efetuar a operação.  Por favor, entre em contato com o administrador do sistema.<br />" + err.Message.ToString();
                PanelMsgResultado.CssClass = "error";
            }

            PanelMsgResultado.Visible = true;
        }

        protected void btnVoltar_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("FrmListar.aspx");
        }

        public string BaseSiteUrl
        {
            get
            {
                HttpContext context = HttpContext.Current;
                string baseUrl = context.Request.Url.Scheme + "://" + context.Request.Url.Authority + context.Request.ApplicationPath.TrimEnd('/') + '/';
                return baseUrl;
            }
        }
    }
}