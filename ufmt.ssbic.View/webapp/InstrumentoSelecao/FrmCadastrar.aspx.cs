﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.sig.entity;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;
using ufmt.ssbic.View.session;
using ufmt.ssbic.View.componentes;
using System.Configuration;


namespace ufmt.ssbic.View.webapp.instrumentoSelecao
{
    public partial class FrmCadastrar : VerificaSession
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack) {
                
                string area = Request.QueryString["area"];
                lblTitulo.Text = area;
            }

            if (!Session["permissao"].ToString().Contains("MONITORIA"))
            {
                TabContainer1.Tabs.Remove(TabCursos);
            }
            
        }

        protected void IndiceZero(object sender, EventArgs e)
        {
            DropDownList objDropDownList = (DropDownList)sender; //Cast no sender para DropDownList
            objDropDownList.Items.Insert(0, new ListItem("Selecione","")); //Adiciona um novo Item
        }


        protected void btnVoltar_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("FrmListar.aspx");
        }

        protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
        {         
            try
            {
                FrmCadastrarInstrumentoSelecao1.setAtributos();
                string descricaoInstrumentoSelecao = FrmCadastrarInstrumentoSelecao1.Descricao;
                int tipoBolsaUID = FrmCadastrarInstrumentoSelecao1.TipoBolsaUID;
                int tipoInstrumentoSelecaoUID = FrmCadastrarInstrumentoSelecao1.TipoInstrumentoSelecaoUID;
                bool hasArquivo = FrmCadastrarInstrumentoSelecao1.HasArquivo;
                String nomeArquivo = FrmCadastrarInstrumentoSelecao1.NomeArquivo;

                List<ufmt.ssbic.View.controles.CursoQtdeVagas> vagasPorCurso = (List<ufmt.ssbic.View.controles.CursoQtdeVagas>)Session["vagasPorCurso"];
                

                InstrumentoSelecaoBO objBO = new InstrumentoSelecaoBO();
                InstrumentoSelecao insSel = new InstrumentoSelecao();
                insSel.descricao = descricaoInstrumentoSelecao;
                insSel.proreitoriaUID = int.Parse(Session["programaUID"].ToString());
                insSel.tipoInstrumentoSelecaoUID = tipoInstrumentoSelecaoUID;
                insSel.bolsaUID = tipoBolsaUID;
                insSel.situacao = true;

                int instrumentoSelecaoUID = objBO.Salvar(insSel, hasArquivo, nomeArquivo, 1);

                UCCadastrarFinanciador1.SetAtributos();
                int financiadorUID = UCCadastrarFinanciador1.FinanciadorUID;
                string tipoOfertaBolsa = UCCadastrarFinanciador1.TipoBolsa;
                int quantidadBolsas = UCCadastrarFinanciador1.QuantidadeBolsas;
                int quantidadeBolsasRestantes = UCCadastrarFinanciador1.QuantidadeRestantes;
                double? valorBolsa = UCCadastrarFinanciador1.ValorBolsa;
                int? cursoFinanciadorUID = UCCadastrarFinanciador1.CursoUID;
                
                FinanciadorBO financiadorBO = new FinanciadorBO();
                FinanciadorInstrumentoSelecao financiador = new FinanciadorInstrumentoSelecao();
                financiador.instrumentoSelecaoUID = instrumentoSelecaoUID;
                financiador.financiadorUID = financiadorUID;
                financiador.tipoOferta = int.Parse(tipoOfertaBolsa);
                financiador.valorBolsa = valorBolsa;
                financiador.quantidadeBolsas = quantidadBolsas;
                financiador.quantidadeRestantes = quantidadeBolsasRestantes;
                financiador.cursoUID = cursoFinanciadorUID;
                financiadorBO.Salvar(financiador, 1);

                UCCadastrarProcessoSeletivo1.SetAtributos();
                string tituloProcessoSeletivo = UCCadastrarProcessoSeletivo1.Titulo;
                string descricaoProcessoSeletivo = UCCadastrarProcessoSeletivo1.Descricao;
                DateTime? dataInicioProcessoSeletivo = UCCadastrarProcessoSeletivo1.DataInicio;
                DateTime? dataFinalProcessoSeletivo = UCCadastrarProcessoSeletivo1.DataFinal;

                ProcessoSeletivoBO procSeletivoBO = new ProcessoSeletivoBO();
                ProcessoSeletivo processoSeletivo = new ProcessoSeletivo();
                processoSeletivo.instrumentoSelecaoUID = instrumentoSelecaoUID;
                processoSeletivo.titulo = tituloProcessoSeletivo;
                processoSeletivo.descricao = descricaoProcessoSeletivo;
                processoSeletivo.dataAplicacao = dataInicioProcessoSeletivo;
                processoSeletivo.dataResultado = dataFinalProcessoSeletivo;
                int processoSeletivoUID = procSeletivoBO.Salvar(processoSeletivo, 1);


                if (vagasPorCurso != null)
                {
                    CursoProcessoSeletivoBO cursoProcBO = new CursoProcessoSeletivoBO();

                    List<CursoProcessoSeletivo> listaCursosProc = new List<CursoProcessoSeletivo>();

                    foreach (ufmt.ssbic.View.controles.CursoQtdeVagas cursoVagas in vagasPorCurso)
                    {
                        CursoProcessoSeletivo cursoProc = new CursoProcessoSeletivo();
                        cursoProc.cursoUID = cursoVagas.curso.CursoUID;
                        cursoProc.processoSeletivoUID = processoSeletivoUID;
                        cursoProc.vagas = cursoVagas.qtdeVagas;

                        listaCursosProc.Add(cursoProc);
                    }

                    cursoProcBO.SalvarVagasPorCurso(listaCursosProc);
                }
                /*
                UCCadastrarResponsaveis1.SetAtributos();
                long responsavelUID = UCCadastrarResponsaveis1.ServidorUID;
                long cursoResponsavelUID = UCCadastrarResponsaveis1.CursoUID;
                 */

                lblMsgResultado.Text = "Registro adicionado com sucesso.";
                PanelMsgResultado.CssClass = "success";
                TabContainer1.Visible = false;
                btnSalvar.Visible = false;
                btnNovo.Visible = true;

                Session["vagasPorCurso"] = null;
            }
            catch (Exception err)
            {

                lblMsgResultado.Text = "Não foi possível efetuar a operação.  Por favor, entre em contato com o administrador do sistema.<br />" + err.Message.ToString();
                PanelMsgResultado.CssClass = "error";

            }

            PanelMsgResultado.Visible = true;
        }

        protected void btnNovo_Click(object sender, ImageClickEventArgs e)
        {
            ReiniciarCadastro();
        }

        private void ReiniciarCadastro()
        {
            Response.Redirect("FrmCadastrar.aspx?area=Instrumento de Seleção");
        }

    }
}