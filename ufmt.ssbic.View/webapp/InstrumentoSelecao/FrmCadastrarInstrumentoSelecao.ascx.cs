﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.ssbic.View.componentes;
using System.Configuration;

namespace ufmt.ssbic.View.webapp.instrumentoSelecao
{
    public partial class FrmCadastrarInstrumentoSelecao : System.Web.UI.UserControl
    {
        public String Descricao { get; set; }
        public int TipoInstrumentoSelecaoUID { get; set; }
        public int TipoBolsaUID { get; set; }
        public bool HasArquivo = false;
        public String NomeArquivo { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                InstrumentoSelecaoBO objBO = new InstrumentoSelecaoBO();
                string proreitoriaUID = Session["programaUID"].ToString();
                string area = Request.QueryString["area"];
                

                ddlTipo.DataSource = objBO.GetTipoInstrumentoSelecao();
                ddlTipo.DataTextField = "descricao";
                ddlTipo.DataValueField = "tipoInstrumentoSelecaoUID";
                ddlTipo.DataBound += new EventHandler(IndiceZero);
                ddlTipo.DataBind();

                BolsaBO objBolsa = new BolsaBO();
                ddlBolsa.DataSource = objBolsa.GetRegistros(int.Parse(proreitoriaUID));
                ddlBolsa.DataTextField = "descricao";
                ddlBolsa.DataValueField = "bolsaUID";
                ddlBolsa.DataBound += new EventHandler(IndiceZero);
                ddlBolsa.DataBind();               

            }
        }

        protected void IndiceZero(object sender, EventArgs e)
        {
            DropDownList objDropDownList = (DropDownList)sender; //Cast no sender para DropDownList
            objDropDownList.Items.Insert(0, new ListItem("Selecione", "")); //Adiciona um novo Item
        }

        private void ReiniciarCadastro()
        {
            PanelMsgResultado.Visible = false;
            PanelFormulario.Visible = true;
            txtDescricao.Text = String.Empty;

        }

        public void setAtributos() {         
            Descricao = txtDescricao.Text;
            TipoBolsaUID = int.Parse(ddlBolsa.SelectedValue);
            TipoInstrumentoSelecaoUID = int.Parse(ddlTipo.SelectedValue);

            ObjUpload resultadoUpload = new ObjUpload();
            String descricaoArquivo = String.Empty;
            if (fUpload.HasFile)
            {
                string caminho = Server.MapPath("~\\" + ConfigurationManager.AppSettings["diretorioUploadEditais"].ToString() + "\\");
                Upload objUpload = new Upload();
                resultadoUpload = objUpload.UploadFile(fUpload, "3500000", ".pdf,.doc,.docx,.xls,.odt", caminho);

                String msg = String.Empty;
                if (resultadoUpload.MsgErro != null)//ocorreu um erro
                {
                    throw new Exception("Houve um problema ao realizar o upload do arquivo. " + resultadoUpload.MsgErro);
                }
                else
                {
                    HasArquivo = true;
                }
            }

            NomeArquivo = resultadoUpload.NomeArquivo;
        }
    }
}