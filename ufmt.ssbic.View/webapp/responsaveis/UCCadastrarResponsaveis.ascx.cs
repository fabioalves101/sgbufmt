﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.sig.entity;

namespace ufmt.ssbic.View.webapp.responsaveis
{
    public partial class UCCadastrarResponsaveis : System.Web.UI.UserControl
    {
        public long ServidorUID;
        public long CursoUID;

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btBuscar_Click(object sender, ImageClickEventArgs e)
        {
            pnlListaServidores.Visible = true;
            gdrListaServidores.DataBind();
        }

        protected void gdrListaServidores_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridView gridview = (GridView)sender;
            Int64 servidorUID = int.Parse(gridview.SelectedValue.ToString());

            PessoaBO pessoaB = new PessoaBO();
            Pessoa pessoa = pessoaB.GetPessoa(servidorUID);

            pnlCoordenadorSelecionado.Visible = true;
            lblCoordenadorSelecionado.Visible = true;
            lblCoordenadorSelecionado.Text = "Servidor selecionado: " + pessoa.Nome;
            lblCpf.Text = "CPF: " + pessoa.Cpf;
            lblRegistro.Text = "E-mail: " + pessoa.Mail;

            hdCoordenadorSelecionado.Value = pessoa.PessoaUID.ToString();

            pnlListaServidores.Visible = false;
            pnlBuscaResponsavel.Visible = false;
        }


        public void PreencherCursos(String nome)
        {
            CursoBO cursoBO = new CursoBO();
            List<Curso> cursos = cursoBO.GetCursos(1, nome);

            GridView1.DataSource = cursos;
            //lblTotalRegistros.Text = registros.Count.ToString() + " registro(s) disponível(is)";
            GridView1.DataBind();
        }
        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                hdCursoUID.Value = e.CommandArgument.ToString();

                CursoBO cursoBO = new CursoBO();
                Curso curso = cursoBO.GetCurso(int.Parse(hdCursoUID.Value.ToString()));

                lblnomecurso.Text = "<strong>Curso vinculado: " + curso.Nome + "</strong>";
                pnlCurso.Visible = false;
                fieldsetCurso.Visible = false;
            }
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void btBuscar_Click(object sender, EventArgs e)
        {
            PreencherCursos(txtNomeCurso.Text);
        }
        protected void GridView1_PageIndexChanging1(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            PreencherCursos(txtNomeCurso.Text);
        }
        protected void btnNaoAplica_Click(object sender, EventArgs e)
        {
            pnlCurso.Visible = false;
        }

        protected void rdoResponsavelCurso_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdoResponsavelCurso.SelectedValue.Equals("1"))
            {
                pnlCurso.Visible = true;                
            }
            else
            {
                pnlCurso.Visible = false;
            }
        }

        protected void btReiniciar_Click(object sender, EventArgs e)
        {
            Response.Redirect("FrmCadastrar.aspx?instrumentoSelecaoUID=" + Request.QueryString["instrumentoSelecaoUID"].ToString());
        }

        public void SetAtributos()
        {
            ServidorUID = long.Parse(hdCoordenadorSelecionado.Value);
            CursoUID = long.Parse(hdCursoUID.Value);
        }

    }
}