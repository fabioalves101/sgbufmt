﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;
using ufmt.ssbic.View.session;
using System.Collections;
using ufmt.sig.entity;

namespace ufmt.ssbic.View.webapp.responsaveis
{
    public partial class FrmListar : VerificaSession
    {
        public int? ProReitoriaUID { get; set; }
        public String Criterio { get; set; }
        public int? ProcessoSeletivoUID { get; set; }
        public FolhaPagamento folhaPagamento { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblResultadoConsulta.Text = "<b>Mostrando todos</b>";
                //PreecherGridView(Session["programaUID"].ToString(), int.Parse(Request.QueryString["folhaPagamentoUID"]), false);
                Label1.Text = "Administração dos Responáveis (Professor/Coordenador) &raquo;";

                lnkNovoRegistro.NavigateUrl = "~/webapp/responsaveis/FrmCadastrar.aspx?instrumentoSelecaoUID="+Request.QueryString["instrumentoSelecaoUID"];

                PreecherGridView(int.Parse(Request.QueryString["instrumentoSelecaoUID"]), false);
            }
        }

        protected void PreecherGridView(int instrumentoSelecaoUID, bool visible)
        {
            PanelMsgResultado.Visible = visible;


            InstrumentoSelecaoBO objBO = new InstrumentoSelecaoBO();
            List<vwInstrumentoSelecaoResponsavel> registros = null;
            
            //Consulta
            //if (!String.IsNullOrEmpty(folhaPagamentoUID))
            //{
            //    registros = objBO.GetBolsistasFolhaPagamento(folhaPagamentoUID); //FindRegistrosISB(this.ProReitoriaUID, criterio);
            //}
            //else
            //{
            //    registros = objBO.GetBolsistasFolhaPagamento(int.Parse(Request.QueryString["folhaPagamentoUID"])); //registros = objBO.FindRegistrosISB(this.ProReitoriaUID, null);
            //}

            registros = objBO.GetResponsaveisPorInstrumentoSelecao(instrumentoSelecaoUID); //FindRegistrosISB(this.ProReitoriaUID, criterio);

            GdrEditais.DataSource = registros;
            lblTotalRegistros.Text = registros.Count.ToString() + " registro(s) disponível(is)";
            GdrEditais.DataBind();
            
            if (registros.Count > 0)
            {
                /*ImageButton btnPrev = (ImageButton)GdrEditais.BottomPagerRow.FindControl("cmdPrev");
                if (GdrEditais.PageIndex == 0)
                    btnPrev.Enabled = false;
                else
                    btnPrev.Enabled = true;
                */
            }
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                int id = int.Parse(e.CommandArgument.ToString());
                InstrumentoSelecaoBO ISBo = new InstrumentoSelecaoBO();
                ISBo.ExcluirInstrumentoSelecaoResponsavel(id);

                PreecherGridView(int.Parse(Request.QueryString["instrumentoSelecaoUID"]), false);
                GdrEditais.DataBind();
            }

            
        }

        protected void lnkConsultar_Click(object sender, EventArgs e)
        {
            // PreecherGridView(Session["programaUID"].ToString(), txtCriterio.Text, false);
        }

        protected void lnkMostrarTodos_Click(object sender, EventArgs e)
        {
            //   PreecherGridView(Session["programaUID"].ToString(), null, false);
        }

        public String VerificarSituacao(int bolsistaUID)
        {
            String situacao;

            BolsistaProcessoSeletivoBO objBO = new BolsistaProcessoSeletivoBO();
            BolsistaProcessoSeletivo bolsistaProcesso = objBO.GetBolsistaProcessoSeletivo(bolsistaUID, this.ProcessoSeletivoUID.Value);

            if (bolsistaProcesso != null)
            {
                if (bolsistaProcesso.situacao.Value == 1)
                    situacao = "Ativo";
                else
                    situacao = "Inativo";
            }
            else
                situacao = "Não foi possível verificar";

            return situacao;

        }

        protected void lnkNovoBolsista_Click(object sender, EventArgs e)
        {
            int folhaPagamentoUID = int.Parse(Request.QueryString["folhaPagamentoUID"]);

            FolhaPagamentoBO folhaBO = new FolhaPagamentoBO();
            FolhaPagamento folha = folhaBO.GetFolhaPagamento(folhaPagamentoUID);

            Response.Redirect("../processoSeletivo/FrmProcessoSeletivo.aspx?processoSeletivoUID=" + folha.processoSeletivoUID + "&folhaPagamentoUID=" + folhaPagamentoUID.ToString());
        }

        public String VerificarPermissaoAutorizacaoPagamento(Boolean pagamentoAutorizado)
        {
            String texto = "";
                        
            Session["administracao"] = "true";
            if (Session["permissao"].ToString().Contains("ADMINISTRADOR"))
            {
                texto = "";
            }
            else if (Session["permissao"].ToString().Contains("COORDENADOR"))
            {
                texto = "";
            }
            else
            { //FINANCEIRO
                if(!pagamentoAutorizado)
                    texto = "| Confirmar Pagamento";
            }

            return texto;
        }





        protected void btAceitar_Click(object sender, EventArgs e)
        {
            int folhaPagamentoUID = int.Parse(Request.QueryString["folhaPagamentoUID"]);

            FolhaPagamentoBO folhaBO = new FolhaPagamentoBO();
            folhaBO.AtualizarFolhaPagamento(folhaPagamentoUID, null, 3, null, null, null, false);

            FolhaPagamento folha = folhaBO.GetFolhaPagamento(folhaPagamentoUID);
            Response.Redirect("../folha/FrmListar.aspx?processoSeletivoUID=" + folha.processoSeletivoUID);
        }

        protected void GdrEditais_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }




    }

}