﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true" CodeBehind="FrmCadastrar.aspx.cs" Inherits="ufmt.ssbic.View.webapp.responsaveis.FrmCadastro" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<h1>&nbsp;Novo Registro &raquo;
        <asp:Label ID="lblTitulo" runat="server"></asp:Label>
    </h1>

        <div id="containerMensagens">
                <asp:Panel ID="PanelMsgResultado" runat="server" Visible="false">
                    <asp:Label ID="lblMsgResultado" runat="server"></asp:Label>
                </asp:Panel>
        </div>

<asp:Panel ID="PanelFormulario" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div class="containerFormulario" style="padding-left:10px;">
        <asp:Panel ID="pnlCoordenadorSelecionado" runat="server" Visible="False">        
            <p style="font-weight:bold"> 
                <asp:Label ID="lblCoordenadorSelecionado" runat="server" Text="" Visible="false"></asp:Label>
            </p>
           
            <p style="font-weight:bold"> 
                <asp:Label ID="lblCpf" runat="server" Text=""></asp:Label>
            </p>
             <p style="font-weight:bold"> 
                <asp:Label ID="lblRegistro" runat="server" Text=""></asp:Label>
            </p>
                <asp:HiddenField ID="hdCoordenadorSelecionado" runat="server" />
            </p>
            <p>
                <asp:Label ID="lblnomecurso" runat="server" Text=""></asp:Label>
            </p>

        <!-- INICIO VINCULAR CURSO -->
        <fieldset runat="server" id="fieldsetCurso">
            <legend>Vincular responsável à curso</legend>
            <asp:RadioButtonList ID="rdoResponsavelCurso" runat="server" 
                AutoPostBack="True" 
                onselectedindexchanged="rdoResponsavelCurso_SelectedIndexChanged">

                <asp:ListItem Value="0" Selected="True">Não se aplica</asp:ListItem>
                <asp:ListItem Value="1">Selecionar curso</asp:ListItem>

            </asp:RadioButtonList>
            
            <asp:Panel ID="pnlCurso" runat="server" Visible="false">
        <fieldset>
            <legend>Buscar curso</legend>
            Nome: 
            <asp:TextBox ID="txtNomeCurso" runat="server" Width="300px"></asp:TextBox>
            <asp:Button ID="Button1" runat="server" Text="Buscar Curso" 
                onclick="btBuscar_Click" CausesValidation="false" />
            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
            AutoGenerateColumns="False" CssClass="mGrid" 
                onpageindexchanging="GridView1_PageIndexChanging1" OnRowCommand="GridView1_RowCommand">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton CssClass="minibutton" ID="LinkButton1" runat="server" CausesValidation="False" 
                                CommandName="Select" CommandArgument='<%# Eval("cursoUID") %>'>
                            <span>
                                <img src="../../media/images/select.png" alt="" />
                                Selecionar
                            </span>
                            </asp:LinkButton>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="10%" />
                </asp:TemplateField>  
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Eval("nome") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="90%" />
                </asp:TemplateField>               
            </Columns>
            <PagerTemplate>
                <asp:Panel ID="Panel1" runat="server" DefaultButton="cmdIr">
                    <div style="text-align: left">
                        <asp:ImageButton ID="cmdFirst" runat="server" CausesValidation="False" 
                            CommandArgument="First" CommandName="Page" 
                            ImageUrl="../../images/i_primeira_pagina.gif" ToolTip="Ir para a primeira página" />
                        &nbsp;<asp:ImageButton ID="cmdPrev" runat="server" CausesValidation="False" 
                            CommandArgument="Prev" CommandName="Page" 
                            ImageUrl="../../images/i_pagina_anterior.gif" ToolTip="Ir para a página anterior" />
                        &nbsp;&nbsp;&nbsp;&nbsp;<asp:ImageButton ID="cmdNext" runat="server" 
                            CausesValidation="False" CommandArgument="Next" CommandName="Page" 
                            ImageUrl="../../images/i_proxima_pagina.gif" ToolTip="Ir para a próxima página" />
                        &nbsp;<asp:ImageButton ID="cmdLast" runat="server" CausesValidation="False" 
                            CommandArgument="Last" CommandName="Page" 
                            ImageUrl="../../images/i_ultima_pagina.gif" ToolTip="Ir para a última página" />
                        &nbsp;&nbsp; Ir para a página
                        Ir para a página
                        <asp:TextBox ID="txtPagina" runat="server" 
                            Text="<% # GridView1.PageIndex + 1 %>" ValidationGroup="grid" Width="29px"></asp:TextBox>
                        &nbsp;<asp:ImageButton ID="cmdIr" runat="server" ImageUrl="../../images/btnIr.gif" 
                            ValidationGroup="grid" />
                        <b>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                            ControlToValidate="txtPagina" Display="Dynamic" 
                            ErrorMessage="* Entre com um valor para continuar" Font-Bold="False" 
                            ValidationGroup="grid"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                            ControlToValidate="txtPagina" Display="Dynamic" 
                            ErrorMessage="* Digite somente números para continuar" Font-Bold="False" 
                            ValidationExpression="^[0-9]+$" ValidationGroup="grid">* Digite somente números* Digite somente números</asp:RegularExpressionValidator>
                        </b>
                    </div>
                    <div style="text-align: left">
                        Página&nbsp;Página&nbsp;<asp:Label ID="lblTotalPaginaAtual" runat="server" 
                            Font-Bold="True" Text="<%# GridView1.PageIndex + 1 %>"></asp:Label>
                        &nbsp;de&nbsp;&nbsp;de&nbsp;<asp:Label ID="lblTotalPaginas" runat="server" 
                            Text="<% # GridView1.PageCount %>"></asp:Label>
                        &nbsp;
                    </div>
                </asp:Panel>
            </PagerTemplate>
            <EmptyDataTemplate>
                <div 
                    style="text-align: center; padding-top: 20px; padding-bottom: 20px; font-size: 18px;
                                                    font-family: Arial Narrow; font-weight: bold">
                    <asp:Label ID="lblMsgNenhumRegistro" runat="server" 
                        Text="Nenhum registro localizado no critério da consulta."></asp:Label>
                </div>
            </EmptyDataTemplate>
        </asp:GridView>


        </fieldset>
    </asp:Panel>
    <asp:HiddenField ID="hdCursoUID" runat="server" />
          
        </fieldset>
        <!-- FIM CURSO -->
            
        </asp:Panel>
            <asp:Panel ID="pnlBuscaResponsavel" runat="server">
            
             <p>
                <asp:Label ID="lblBuscaCoordenador" runat="server" Text="Buscar: "></asp:Label>       
                <asp:TextBox ID="txtBuscaCoordenador" runat="server" Width="400px"></asp:TextBox>
                <asp:DropDownList ID="ddlBuscaCoordenador" runat="server">
                    <asp:ListItem>Nome</asp:ListItem>
                    <asp:ListItem>CPF</asp:ListItem>
                    <asp:ListItem Value="siape">Registro SIAPE</asp:ListItem>
                </asp:DropDownList>
                <asp:ImageButton ID="btBuscar" runat="server" 
                    ImageUrl="~/media/images/busca.png" CssClass="imgBtBuscar" 
                    onclick="btBuscar_Click" />
            </p>

            <asp:Panel ID="pnlListaServidores" runat="server" Visible="False">
            Selecione o coordenador.
                <asp:GridView ID="gdrListaServidores" runat="server" 
                    AutoGenerateColumns="False" DataSourceID="ObjectDataSource1" 
                    AllowPaging="True" Width="100%"
                    CssClass="mGrid"
                    PagerStyle-CssClass="pgr"
                    AlternatingRowStyle-CssClass="alt" DataKeyNames="ServidorUID" onselectedindexchanged="gdrListaServidores_SelectedIndexChanged"
                    >            
                    <AlternatingRowStyle CssClass="alt" />
                    <Columns>                    
                        <asp:TemplateField HeaderText="Selecionar" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="120px">
                            <ItemTemplate>
                                <asp:LinkButton CssClass="minibutton" ID="LinkButton1" runat="server" CausesValidation="False" 
                                    CommandName="Select">
                                <span>
                                    <img src="../../media/images/select.png" alt="" />
                                    Selecionar
                                </span>
                                </asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" Width="120px" />
                        </asp:TemplateField>
                    
                        <asp:BoundField DataField="Registro" HeaderText="Registro SIAPE" ItemStyle-HorizontalAlign="Center" 
                            SortExpression="Registro" ItemStyle-Width="120px">
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                            Nome do Servidor
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='  <%# Eval("Pessoa.Nome")%>'></asp:Label>
                        
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle CssClass="pgr" />
                </asp:GridView>
                <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
                    SelectMethod="GetPessoas" TypeName="ufmt.ssbic.Business.PessoaBO">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="txtBuscaCoordenador" DefaultValue="null" 
                            Name="parametro" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="ddlBuscaCoordenador" DefaultValue="null" 
                            Name="tipoParametro" PropertyName="SelectedValue" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            
            </asp:Panel>
        </asp:Panel>
        </div>

        <p>
                <asp:Button ID="btConfirmar" runat="server" Text="Confirmar" Visible="false"
                    onclick="btConfirmar_Click" />
                <asp:Button ID="btReiniciar" runat="server" Text="Reiniciar Cadastro" 
                    onclick="btReiniciar_Click" />
            </p>

    </ContentTemplate>
    </asp:UpdatePanel>

</asp:Panel>


</asp:Content>
