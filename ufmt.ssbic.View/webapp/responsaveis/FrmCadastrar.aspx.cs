﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.sig.entity;
using ufmt.ssbic.View.session;

namespace ufmt.ssbic.View.webapp.responsaveis
{
    public partial class FrmCadastro : VerificaSession
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblTitulo.Text = "Responsáveis";
        }

        protected void btBuscar_Click(object sender, ImageClickEventArgs e)
        {
            pnlListaServidores.Visible = true;
            gdrListaServidores.DataBind();
        }

        protected void gdrListaServidores_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridView gridview = (GridView)sender;
            Int64 servidorUID = int.Parse(gridview.SelectedValue.ToString());

            PessoaBO pessoaB = new PessoaBO();
            Pessoa pessoa = pessoaB.GetPessoa(servidorUID);

            pnlCoordenadorSelecionado.Visible = true;
            lblCoordenadorSelecionado.Visible = true;
            lblCoordenadorSelecionado.Text = "Servidor selecionado: " + pessoa.Nome;
            lblCpf.Text = "CPF: " + pessoa.Cpf;
            lblRegistro.Text = "E-mail: " + pessoa.Mail;

            hdCoordenadorSelecionado.Value = pessoa.PessoaUID.ToString();

            pnlListaServidores.Visible = false;
            pnlBuscaResponsavel.Visible = false;
            btConfirmar.Visible = true;
        }

        protected void btConfirmar_Click(object sender, EventArgs e)
        {
            AutenticacaoBO autenticacaoBO = new AutenticacaoBO();
            Usuario usuario = autenticacaoBO.GetUsuarioPorPessoa(int.Parse(hdCoordenadorSelecionado.Value));

            if (usuario != null)
            {
                int instrumentoSelecaoUID = int.Parse(Request.QueryString["instrumentoSelecaoUID"]);

                int? cursoUID = null;
                if(!String.IsNullOrEmpty(hdCursoUID.Value.ToString()))
                {
                    cursoUID = int.Parse(hdCursoUID.Value.ToString());
                }
                InstrumentoSelecaoBO ISBo = new InstrumentoSelecaoBO();
                ISBo.SalvarInstrumentoSelecaoResponsavel(instrumentoSelecaoUID, usuario.UsuarioUID, cursoUID);

                Response.Redirect("FrmListar.aspx?instrumentoSelecaoUID=" + instrumentoSelecaoUID);
            }
            else
            {
                lblMsgResultado.Text = "A pessoa selecionada não possui usuário cadastrado no sistema. Solicite o cadastro pela mesma através do link http://sistemas.ufmt.br/ufmt.ssbic";
            }
        }

        public void PreencherCursos(String nome)
        {
            CursoBO cursoBO = new CursoBO();
            List<Curso> cursos = cursoBO.GetCursos(1, nome);

            GridView1.DataSource = cursos;
            //lblTotalRegistros.Text = registros.Count.ToString() + " registro(s) disponível(is)";
            GridView1.DataBind();
        }
        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                hdCursoUID.Value = e.CommandArgument.ToString();
                
                CursoBO cursoBO = new CursoBO();
                Curso curso = cursoBO.GetCurso(int.Parse(hdCursoUID.Value.ToString()));

                lblnomecurso.Text = "<strong>Curso vinculado: " + curso.Nome + "</strong>";
                pnlCurso.Visible = false;
                fieldsetCurso.Visible = false;
                btConfirmar.Visible = true;
            }
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void btBuscar_Click(object sender, EventArgs e)
        {
            PreencherCursos(txtNomeCurso.Text);
        }
        protected void GridView1_PageIndexChanging1(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            PreencherCursos(txtNomeCurso.Text);
        }
        protected void btnNaoAplica_Click(object sender, EventArgs e)
        {
            pnlCurso.Visible = false;
        }

        protected void rdoResponsavelCurso_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdoResponsavelCurso.SelectedValue.Equals("1"))
            {
                pnlCurso.Visible = true;
                btConfirmar.Visible = false;
            }
            else
            {
                pnlCurso.Visible = false;
            }
        }

        protected void btReiniciar_Click(object sender, EventArgs e)
        {
            Response.Redirect("FrmCadastrar.aspx?instrumentoSelecaoUID="+Request.QueryString["instrumentoSelecaoUID"].ToString());
        }



    }


}