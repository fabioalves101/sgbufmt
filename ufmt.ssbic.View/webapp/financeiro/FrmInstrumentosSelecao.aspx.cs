﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.View.session;

namespace ufmt.ssbic.View.webapp.financeiro
{
    public partial class FrmInstrumentosSelecao : VerificaSession
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btBuscar_Click(object sender, EventArgs e)
        {
            grdInstrumentosSelecao.DataBind();
        }
    }
}