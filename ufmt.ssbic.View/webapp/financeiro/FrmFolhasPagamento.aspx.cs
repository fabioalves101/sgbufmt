﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.ssbic.View.session;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.View.webapp.financeiro
{
    public partial class FrmFolhasPagamento : VerificaSession
    {
        FinanceiroController controller; 

        public FrmFolhasPagamento()
        {
            this.controller = new FinanceiroController();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            int instrumentoSelecaoUID = int.Parse(Request.QueryString["is"]);

            hdInstrumentoSelecaoUID.Value = instrumentoSelecaoUID.ToString();
            
            UCBreadcrumbFinanceiro1.instrumentoSelecaoUID = instrumentoSelecaoUID;
        }
               

        protected String GetMesFolhaPagamento(DateTime dataFim)
        {
            return dataFim.Month + "/" + dataFim.Year;
        }

        protected void grdFolhaPagamento_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("imprimir"))
            {
                int folhaPagamentoUID = int.Parse(e.CommandArgument.ToString());

                FinanceiroController controller = new FinanceiroController();
                InstrumentoSelecao instrumentoSelecao = controller.GetInstrumentoSelecaoPorFolhaPagamento(folhaPagamentoUID);

                if (instrumentoSelecao.proreitoriaUID == 8)
                {
                    Response.Redirect("../relatorio/FrmRelatorioPRAE.aspx?folhaPagamentoUID=" + e.CommandArgument.ToString());
                }
                else
                {
                    Response.Redirect("../relatorio/FrmRelatorio.aspx?folhaPagamentoUID=" + e.CommandArgument.ToString());
                }
            }
        }

        protected String VerificarSuplementar(int folhaPagamentoUID)
        {
            String navigateURL = String.Empty;
            if (this.controller.VerificaFolhaSuplementar(folhaPagamentoUID))
            {
                navigateURL = "Gerenciar Folha Suplementar";
            }

            return navigateURL;
        }

        protected void lnkFolhasSuplementares_Click(object sender, EventArgs e)
        {

            FinanceiroController fin = new FinanceiroController();
            ProcessoSeletivo ps = fin.GetProcessoSeletivoPorInstrumentoSelecao(int.Parse(Request.QueryString["is"]));

            Response.Redirect("FrmSuplementar.aspx?processoSeletivoUID=" + ps.processoSeletivoUID);
        }
    }
}