﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;
using ufmt.ssbic.View.session;

namespace ufmt.ssbic.View.webapp.financeiro
{
    public partial class FrmBolsistasFolhaSuplementar : VerificaSession
    {
        private FinanceiroController controller;

        public FrmBolsistasFolhaSuplementar()
        {
            this.controller = new FinanceiroController();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            int folhaPagamentoUID = int.Parse(Request.QueryString["fp"]);

            hdFolhaPagamentoUID.Value = folhaPagamentoUID.ToString();

            UCBreadcrumbFinanceiro1.folhaPagamentoUID = folhaPagamentoUID;

            InstrumentoSelecao instrumento = this.controller.GetInstrumentoSelecaoPorFolhaPagamento(folhaPagamentoUID);
            UCBreadcrumbFinanceiro1.instrumentoSelecaoUID = instrumento.instrumentoSelecaoUID;
        }

        protected vwAlunoSiga GetAluno(String matricula)
        {
            if (!String.IsNullOrEmpty(matricula))
            {
                vwAlunoSiga aluno = this.controller.GetAlunoPorMatricula(decimal.Parse(matricula));
                if (aluno != null)
                    return aluno;
                else
                {
                    vwAlunoSiga al = new vwAlunoSiga();
                    al.CPF = "Não encontrado";
                    al.Nome = "Não encontrado";

                    return al;
                }
            }
            else
            {
                vwAlunoSiga al = new vwAlunoSiga();
                al.CPF = "Não encontrado";
                al.Nome = "Não encontrado";
                return al;
            }
        }

        protected String GetBanco(int? bancoUID)
        {
            if(bancoUID.HasValue)
            {
                Banco banco = this.controller.GetBanco(bancoUID.Value);

                return banco.numero + " - " + banco.banco1;
            }
            else
            {
                return "Não encontrado";
            }
        }

        protected void grdFolhaPagamento_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("problemas"))
            {
                hdbolsistaUID.Value = e.CommandArgument.ToString();
                mdlPanelRecusa.Show();
            }

            if (e.CommandName.Equals("realizado"))
            {
                int bolsistaUID = int.Parse(e.CommandArgument.ToString());
                int folhaPagamentoUID = int.Parse(hdFolhaPagamentoUID.Value);
                this.controller.PagamentoRealizado(bolsistaUID, folhaPagamentoUID);

                VerificarFechamentoFolhaPagamento(folhaPagamentoUID);

                grdFolhaPagamento.DataBind();
            }

            if (e.CommandName.Equals("parecer"))
            {
                int bolsistaUID = int.Parse(e.CommandArgument.ToString());
                BolsistaFolhaPagamento bfp = this.controller.GetBolsistaFolhaPagamentoPorBolsista(bolsistaUID);

                lblParecer.Text = bfp.parecer;

                mdlPanelParecer.Show();
            }
        }

        protected void salvarParecer_Click(object sender, EventArgs e)
        {
            this.controller.PagamentoNaoAutorizado(int.Parse(hdbolsistaUID.Value),
                int.Parse(hdFolhaPagamentoUID.Value), txtParecerRecusa.Text);

            grdFolhaPagamento.DataBind();
        }

        protected void btCancelarParecer_Click(object sender, EventArgs e)
        {

        }

        protected void grdFolhaPagamento_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Object obj = DataBinder.Eval(e.Row.DataItem, "bolsistaUID");
                if (obj != null)
                {
                    bool pagamentoRecusado =  this.controller.VerificaPagamentoNaoRealizado(int.Parse(obj.ToString()), 
                                                                                int.Parse(hdFolhaPagamentoUID.Value));

                    if (pagamentoRecusado)
                    {
                        System.Drawing.Color color = System.Drawing.ColorTranslator.FromHtml(@"#FFC1C1");

                        e.Row.BackColor = color;
                    }
                }

                
            } 
        }


        protected bool VerificaLinkProblemasVisible(int bolsistaUID)
        {
            bool pagamentoRecusado = this.controller.VerificaPagamentoNaoRealizado(bolsistaUID,
                                                 int.Parse(hdFolhaPagamentoUID.Value));

            if (pagamentoRecusado)
                return false;
            else
                return true;
        }

        protected bool VerificaLinkOkVisible(int bolsistaUID)
        {
            bool pagamentoRecusado = this.controller.VerificaPagamentoNaoRealizado(bolsistaUID,
                                                 int.Parse(hdFolhaPagamentoUID.Value));

            if (pagamentoRecusado)
                return true;
            else
                return false;
        }

        protected void btnImprimir_Click(object sender, EventArgs e)
        {
            Response.Redirect("../relatorio/FrmRelatorio.aspx?folhaPagamentoUID="+hdFolhaPagamentoUID.Value+
                "&suplementar=1");
        }

        protected void btFecharPnl_Click(object sender, EventArgs e)
        {
            mdlPanelParecer.Hide();
            grdFolhaPagamento.DataBind();
        }

        protected void btFecharFolha_Click(object sender, EventArgs e)
        {
            int folhaPagamentoUID = int.Parse(hdFolhaPagamentoUID.Value);
            this.controller.AtualizarEtapaFinanceiro(folhaPagamentoUID, 3);
        }

        private void VerificarFechamentoFolhaPagamento(int folhaPagamentoUID)
        {
            try
            {
                List<BolsistaFolhaPagamento> listBfp =
                    this.controller.GetBolsistaFolhaPagamentoPorFolhaPagamento(folhaPagamentoUID);

                bool pagamentoFinalizado = true;

                foreach (BolsistaFolhaPagamento bfp in listBfp)
                {
                    if (bfp.pagamentoRecusado)
                        pagamentoFinalizado = false;
                }


                if (pagamentoFinalizado)
                {
                    lblFechamentoFolha.Text = "Confirma a finalização do pagamento dessa folha de pagamento?";
                    mdlFechamentoFolha.Show();
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}