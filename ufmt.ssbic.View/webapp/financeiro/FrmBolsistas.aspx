﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true" CodeBehind="FrmBolsistas.aspx.cs" Inherits="ufmt.ssbic.View.webapp.financeiro.FrmBolsistas" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register src="../../controles/UCBreadcrumbFinanceiro.ascx" tagname="UCBreadcrumbFinanceiro" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
.mGrid td 
{
    height:50px;
}
</style>

    <div>
    <uc1:UCBreadcrumbFinanceiro ID="UCBreadcrumbFinanceiro1" runat="server" />
    </div>
    
    <div>
    <h1>
    Bolsistas na Folha de Pagamento
    </h1>
    <h2>Valor da Bolsa: 
        <asp:Label runat="server" ID="valorBolsa"></asp:Label></h2>
    </div>
    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </asp:Panel>

    <div>
        <asp:Button ID="btnImprimir" runat="server" Text="Imprimir Folha de Pagamento" 
            onclick="btnImprimir_Click" />
        
        <asp:Button ID="btnFinalizar" runat="server" 
            Text="Finalizar Pagamento dos Bolsistas" onclick="btnFinalizar_Click" />
    &nbsp;<asp:Label ID="lblFolhaFechada" runat="server" 
            Text="Folha de Pagamento Fechada" Visible="False"></asp:Label>
        <asp:Button ID="btTextoBolsistas" runat="server" 
            Text="Gerar Arquivo Texto dos Bolsistas" onclick="btTextoBolsistas_Click" />
    </div>
    <asp:GridView ID="grdFolhaPagamento" runat="server" AutoGenerateColumns="False" 
        DataSourceID="odsFolhaPagamento" Width="100%" CssClass="mGrid"
        onrowcommand="grdFolhaPagamento_RowCommand" OnRowCreated="grdFolhaPagamento_RowCreated">
        <Columns>
        <asp:TemplateField>
            <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
            </asp:TemplateField>
         <asp:TemplateField HeaderText="Nome">
                <ItemTemplate>
                    <asp:Label ID="lblNome" runat="server" Text='<%# GetAluno((String)Eval("registroAluno")).Nome %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="registroAluno" HeaderText="Matrícula" 
                SortExpression="registroAluno" />
            <asp:TemplateField HeaderText="CPF">
                <ItemTemplate>
                    <asp:Label ID="lblCPF" runat="server" Text='<%# GetAluno((String)Eval("registroAluno")).CPF %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            
            
            <asp:TemplateField HeaderText="Banco">
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# GetBanco((Object)Container.DataItem) %>'></asp:Label>
                </ItemTemplate>                
            </asp:TemplateField>
            <asp:BoundField DataField="agencia" HeaderText="Agência" 
                SortExpression="agencia" />
            <asp:BoundField DataField="numeroConta" HeaderText="Número da Conta" 
                SortExpression="numeroConta" />
            
            <asp:BoundField DataField="email" HeaderText="E-Mail" SortExpression="email" />
            
            <asp:TemplateField HeaderText="Opções">
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" CommandName="problemas" CommandArgument='<%#(int)Eval("bolsistaUID") %>' runat="server" Visible='<%# VerificaLinkProblemasVisible((Object)Container.DataItem) %>'>
                        <img src='../../images/error_bolsista.png' alt="Problemas no Pagamento" title="Problemas no Pagamento" width="20px" />
                    </asp:LinkButton>

                    

                    <asp:LinkButton ID="LinkButton2" CommandName="realizado" CommandArgument='<%#(int)Eval("bolsistaUID") %>' runat="server" Visible='<%# VerificaLinkOkVisible((Object)Container.DataItem) %>'>
                        <img src='../../images/ok.png' alt="Pagamento Realizado" title="Pagamento Realizado" width="20px" />
                    </asp:LinkButton>
                    <br />
                    <asp:LinkButton ID="LinkButton3" CommandName="parecer" CommandArgument='<%#(int)Eval("bolsistaUID") %>' runat="server" Visible='<%# VerificaLinkOkVisible((Object)Container.DataItem) %>'>
                        <img src='../../images/parecer.png' alt="Parecer" title="Parecer" width="20px" />
                    </asp:LinkButton>

                </ItemTemplate>
            </asp:TemplateField>
            
        </Columns>
    </asp:GridView>
    <asp:ObjectDataSource ID="odsFolhaPagamento" runat="server" 
        SelectMethod="GetBolsistas" TypeName="ufmt.ssbic.Business.FinanceiroController">
        <SelectParameters>
            <asp:ControlParameter ControlID="hdFolhaPagamentoUID" Name="folhaPagamentoUID" 
                PropertyName="Value" Type="Int32" />
            <asp:Parameter DefaultValue="false" Name="suplementar" Type="Boolean" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:HiddenField ID="hdFolhaPagamentoUID" runat="server" />


    <asp:modalpopupextender 
                ID="mdlPanelRecusa"
                runat="server"
                TargetControlID="lnkModalRecusa"
                PopupControlID="pnlParecerRecusaPagamento"
                BackgroundCssClass="modalBackground"
                DynamicServicePath="" 
                Enabled="True"
                CancelControlID="btCancelarParecer"
        />

        <asp:HyperLink ID="lnkModalRecusa" runat="server" Visible="true" Text=""  />

    <asp:Panel ID="pnlParecerRecusaPagamento" runat="server" Style="">
        <asp:HiddenField ID="hdbolsistaUID" runat="server" />
        <div style="width:500px; height:300px; background:#FFF">
            <h1>Parecer de Pagamento Não Realizado</h1>
            <p style="padding:30px;">
            Digite aqui o parecer:
            <br />           
                <asp:TextBox ID="txtParecerRecusa" runat="server" Height="100px" 
                    TextMode="MultiLine" Width="380px"></asp:TextBox>
            </p>
            <p style="text-align:center">
                <asp:Button ID="btSalvarParecer" runat="server" Text="Salvar" 
                    onclick="salvarParecer_Click" />
                <asp:Button ID="btCancelarParecer" runat="server" Text="Cancelar" 
                    onclick="btCancelarParecer_Click" />
            </p>
        </div>
    </asp:Panel>


    <asp:modalpopupextender 
                ID="mdlPanelParecer"
                runat="server"
                TargetControlID="lnkModalParecer"
                PopupControlID="pnlParecerPagamento"
                BackgroundCssClass="modalBackground"
                DynamicServicePath="" 
                Enabled="True"
                CancelControlID="btCancelarParecer"
        />

        <asp:HyperLink ID="lnkModalParecer" runat="server" Visible="true" Text=""  />

    <asp:Panel ID="pnlParecerPagamento" runat="server" Style="">
        <div style="width:500px; height:300px; background:#FFF">
            <h1>Parecer de Pagamento Não Realizado</h1>
            <p style="padding:30px;">
                <asp:Label ID="lblParecer" runat="server"></asp:Label>
            </p>
            <p>
                
                    <asp:Button ID="btFecharPnl" runat="server" Text="Fechar" 
                        onclick="btFecharPnl_Click" />
            </p>
        </div>
    </asp:Panel>

<asp:HyperLink ID="lnkModalFechamentoFolha" runat="server" Visible="true" Text=""  />


    <asp:modalpopupextender 
                ID="mdlFechamentoFolha"
                runat="server"
                TargetControlID="lnkModalFechamentoFolha"
                PopupControlID="pnlFechamentoFolha"
                BackgroundCssClass="modalBackground"
                DynamicServicePath="" 
                Enabled="True"
                CancelControlID="btCancelarFecharFolha"
        />
    <asp:Panel ID="pnlFechamentoFolha" runat="server" Style="">
        <div style="width:500px; height:300px; background:#FFF">
            <h1>Fechamento da Folha de Pagamento</h1>
            <p style="padding:30px; font-size:14px">
                <asp:Label ID="lblFechamentoFolha" runat="server"></asp:Label>
            </p>
            <p style="text-align:center">
                <asp:Button ID="btFecharFolha" runat="server" Text="Confirmar" 
                    onclick="btFecharFolha_Click" />
                <asp:Button ID="btCancelarFecharFolha" runat="server" Text="Cancelar" />
            </p>
        </div>
    </asp:Panel>

</asp:Content>
