﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Stimulsoft.Report;
using Stimulsoft.Report.Web;
using ufmt.ssbic.DataAccess;
using ufmt.ssbic.Business;

namespace ufmt.ssbic.View.webapp.financeiro
{
    public partial class FrmRelatorioPRAE : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
             int folhaPagamentoUID = int.Parse(Request.QueryString["folhaPagamentoUID"]);

            BolsistaController bcontroller = new BolsistaController();
            List<BolsistaFolhaPagamento> bolsistas =
                bcontroller.GetBolsistasPorFolhaPagamento(folhaPagamentoUID, true);

            AuxilioController acontrollerr = new AuxilioController();
            List<AuxilioFolhaPagamento> auxiliados = acontrollerr.GetAuxiliadosFolhaPagamento(folhaPagamentoUID, true);
            
            acontrollerr.LimparBolsistaAuxilioFolhaPagamento();

            List<BolsistaAuxilioFolhaPagamento> listBolsistasAuxilios = new List<BolsistaAuxilioFolhaPagamento>();

            bool uniao = false;
            foreach (BolsistaFolhaPagamento bolsista in bolsistas)
            {
                foreach (AuxilioFolhaPagamento auxiliado in auxiliados)
                {
                    if (bolsista.Bolsista.registroAluno == auxiliado.Auxiliado.registroAluno)
                    {
                        //cria novo objeto juntando as informações
                       
                        if (!(listBolsistasAuxilios.Where(l => l.auxiliadoUID == auxiliado.auxiliadoUID).Count() > 0))
                        {
                            List<AuxilioAluno> listAuxilios = auxiliado.Auxiliado.AuxilioAluno.ToList();
                            decimal valor = 0;
                            foreach (AuxilioAluno auxilio in listAuxilios)
                            {
                                valor += auxilio.Auxilio_1.valor.Value;
                            }

                            uniao = true;
                            BolsistaAuxilioFolhaPagamento baf = new BolsistaAuxilioFolhaPagamento()
                            {
                                auxiliadoUID = auxiliado.auxiliadoUID,
                                bolsistaUID = bolsista.bolsistaUID,
                                folhaPagamentoUID = folhaPagamentoUID,
                                pagamentoAutorizado = false,
                                pagamentoRecusado = false,
                                registroAluno = auxiliado.Auxiliado.registroAluno,
                                valor = valor + decimal.Parse("400"),
                                bancoUID = auxiliado.Auxiliado.bancoUID,
                                agencia = auxiliado.Auxiliado.agencia,
                                numeroConta = auxiliado.Auxiliado.numeroConta
                            };

                            
                            listBolsistasAuxilios.Add(baf);
                        }
                    }
                    else
                    {
                        //cria novo objeto com a informação de auxiliado
                        List<AuxilioAluno> listAuxilios = auxiliado.Auxiliado.AuxilioAluno.ToList();
                        decimal valor = 0;
                        foreach (AuxilioAluno auxilio in listAuxilios)
                        {
                            valor += auxilio.Auxilio_1.valor.Value;
                        }

                        if (!(listBolsistasAuxilios.Where(l => l.auxiliadoUID == auxiliado.auxiliadoUID).Count() > 0))
                        {
                            BolsistaAuxilioFolhaPagamento baf = new BolsistaAuxilioFolhaPagamento()
                            {
                                auxiliadoUID = auxiliado.auxiliadoUID,
                                folhaPagamentoUID = folhaPagamentoUID,
                                pagamentoAutorizado = false,
                                pagamentoRecusado = false,
                                registroAluno = auxiliado.Auxiliado.registroAluno,
                                valor = valor,
                                bancoUID = auxiliado.Auxiliado.bancoUID,
                                agencia = auxiliado.Auxiliado.agencia,
                                numeroConta = auxiliado.Auxiliado.numeroConta
                            };
                            listBolsistasAuxilios.Add(baf);
                        }
                    }                    
                }

                if (!uniao)
                {
                    //cria novo objeto com a informação de bolsista
                    if (!(listBolsistasAuxilios.Where(l => l.bolsistaUID == bolsista.bolsistaUID).Count() > 0))
                    {
                        BolsistaAuxilioFolhaPagamento baf = new BolsistaAuxilioFolhaPagamento()
                        {
                            bolsistaUID = bolsista.bolsistaUID,
                            folhaPagamentoUID = folhaPagamentoUID,
                            pagamentoAutorizado = false,
                            pagamentoRecusado = false,
                            registroAluno = bolsista.Bolsista.registroAluno,
                            valor = decimal.Parse("400"),
                            bancoUID = bolsista.Bolsista.bancoUID,
                            agencia = bolsista.Bolsista.agencia,
                            numeroConta = bolsista.Bolsista.numeroConta
                        };
                        listBolsistasAuxilios.Add(baf);
                    }                    
                }
                uniao = false;
            }

            acontrollerr.SalvarBolsistaAuxilioFolhaPagamento(listBolsistasAuxilios);

            if (!String.IsNullOrEmpty(Request.QueryString["folhaPagamentoUID"]))
            {
                GerarRelatorio("FolhaDePagamentoAdministradorPRAE", int.Parse(Request.QueryString["folhaPagamentoUID"]));
            }
        }

        public void GerarRelatorio(string nomeRelatorio, int folhaPagamentoUID)
        {
            StiReport report = new StiReport();
            report.Load(AppDomain.CurrentDomain.BaseDirectory + "reports\\" + nomeRelatorio + ".mrt");
            report.Compile();

            report.CompiledReport.DataSources["BolsistasFolhaPagamento"].Parameters["@folhaPagamentoUID"].ParameterValue = folhaPagamentoUID;

            StiReportResponse.ResponseAsPdf(this, report, true);
        }
    }
}