﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;
using System.ComponentModel;
using System.IO;
using ufmt.ssbic.View.util;
using ufmt.ssbic.View.session;

namespace ufmt.ssbic.View.webapp.financeiro
{
    public partial class FrmBolsistas : VerificaSession
    {
        private FinanceiroController controller;

        public FrmBolsistas()
        {
            this.controller = new FinanceiroController();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            int folhaPagamentoUID = int.Parse(Request.QueryString["fp"]);

            hdFolhaPagamentoUID.Value = folhaPagamentoUID.ToString();

            UCBreadcrumbFinanceiro1.folhaPagamentoUID = folhaPagamentoUID;

            InstrumentoSelecao instrumento = this.controller.GetInstrumentoSelecaoPorFolhaPagamento(folhaPagamentoUID);
            UCBreadcrumbFinanceiro1.instrumentoSelecaoUID = instrumento.instrumentoSelecaoUID;
                        
            FinanciadorBO fBO = new FinanciadorBO();
            FinanciadorInstrumentoSelecao fis = fBO.GetFinanciadorInstrumentoSelecao(instrumento.instrumentoSelecaoUID);
            valorBolsa.Text = fis.valorBolsa.Value.ToString();

            if (this.controller.VerificaFolhaPagamentoFechada(folhaPagamentoUID))
            {
                btnFinalizar.Visible = false;
                lblFolhaFechada.Visible = true;
            }

        }



        protected vwAlunoSiga GetAluno(String matricula)
        {
            if (!String.IsNullOrEmpty(matricula))
            {
                vwAlunoSiga aluno = this.controller.GetAlunoPorMatricula(decimal.Parse(matricula));
                if (aluno != null)
                    return aluno;
                else
                {
                    vwAlunoSiga al = new vwAlunoSiga();
                    al.CPF = "Não encontrado";
                    al.Nome = "Não encontrado";

                    return al;
                }
            }
            else
            {
                vwAlunoSiga al = new vwAlunoSiga();
                al.CPF = "Não encontrado";
                al.Nome = "Não encontrado";
                return al;
            }
        }

        protected String GetBanco(Object obj)
        {
            Bolsista bolsista = (Bolsista)obj;

            if (bolsista.Banco != null)
            {
                return bolsista.Banco.numero + " - " + bolsista.Banco.banco1;
            }
            else
            {
                return "Não encontrado";
            }
        }

        protected void grdFolhaPagamento_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("problemas"))
            {
                hdbolsistaUID.Value = e.CommandArgument.ToString();
                mdlPanelRecusa.Show();
            }

            if (e.CommandName.Equals("realizado"))
            {
                int bolsistaUID = int.Parse(e.CommandArgument.ToString());
                int folhaPagamentoUID = int.Parse(hdFolhaPagamentoUID.Value);
                this.controller.PagamentoRealizado(bolsistaUID, folhaPagamentoUID);

                VerificarFechamentoFolhaPagamento(folhaPagamentoUID);

                grdFolhaPagamento.DataBind();
            }

            if (e.CommandName.Equals("parecer"))
            {
                int bolsistaUID = int.Parse(e.CommandArgument.ToString());
                BolsistaFolhaPagamento bfp = this.controller.GetBolsistaFolhaPagamentoPorBolsista(bolsistaUID);

                lblParecer.Text = bfp.parecer;

                mdlPanelParecer.Show();
            }
        }

        private void VerificarFechamentoFolhaPagamento(int folhaPagamentoUID)
        {
            try
            {
                List<BolsistaFolhaPagamento> listBfp = 
                    this.controller.GetBolsistaFolhaPagamentoPorFolhaPagamento(folhaPagamentoUID);

                bool pagamentoFinalizado = true;

                foreach (BolsistaFolhaPagamento bfp in listBfp)
                {
                    if (bfp.pagamentoRecusado)
                        pagamentoFinalizado = false;
                }


                if (pagamentoFinalizado)
                {
                        lblFechamentoFolha.Text = "Confirma a finalização do pagamento dessa folha de pagamento?";
                        mdlFechamentoFolha.Show();
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }            
        }

        protected void salvarParecer_Click(object sender, EventArgs e)
        {
            try
            {
                this.controller.PagamentoNaoAutorizado(int.Parse(hdbolsistaUID.Value),
                    int.Parse(hdFolhaPagamentoUID.Value), txtParecerRecusa.Text);


                this.controller.AtualizarEtapaFinanceiro(int.Parse(hdFolhaPagamentoUID.Value), 0);


                grdFolhaPagamento.DataBind();
            }
            catch (Exception ex)
            {
                this.LancarExcecao(ex.Message, true);
            }
        }

        protected void btCancelarParecer_Click(object sender, EventArgs e)
        {

        }

        protected void grdFolhaPagamento_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Bolsista bolsista = (Bolsista)e.Row.DataItem;
                
                Object obj = DataBinder.Eval(e.Row.DataItem, "bolsistaUID");
                if (obj != null)
                {
                    bool pagamentoRecusado = false;

                    pagamentoRecusado = bolsista.BolsistaFolhaPagamento.FirstOrDefault().pagamentoRecusado;
                    

                    if (pagamentoRecusado)
                    {
                        System.Drawing.Color color = System.Drawing.ColorTranslator.FromHtml(@"#FFC1C1");

                        e.Row.BackColor = color;
                    }
                        
                }                
            } 
        }

        protected bool VerificaLinkProblemasVisible(Object obj)
        {

            Bolsista bolsista = (Bolsista)obj;

            BolsistaFolhaPagamento bolsistaFolha = bolsista.BolsistaFolhaPagamento.FirstOrDefault();

            bool pagamentoRecusado = true;
            if (bolsistaFolha.pagamentoRecusado)
            {
                pagamentoRecusado = false;                
            }

            return pagamentoRecusado;
        }

        protected bool VerificaLinkOkVisible(Object obj)
        {

            Bolsista bolsista = (Bolsista)obj;

            BolsistaFolhaPagamento bolsistaFolha = bolsista.BolsistaFolhaPagamento.FirstOrDefault();

            bool pagamentoRecusado = false;
            if (bolsistaFolha.pagamentoRecusado)
            {
                pagamentoRecusado = true;                
            } 

            return pagamentoRecusado;
        }

        protected void btnImprimir_Click(object sender, EventArgs e)
        {
            Response.Redirect("../relatorio/FrmRelatorio.aspx?folhaPagamentoUID="+hdFolhaPagamentoUID.Value);
        }

        protected void btnFinalizar_Click(object sender, EventArgs e)
        {
            try
            {
                this.controller.FecharFolhaPagamento(int.Parse(hdFolhaPagamentoUID.Value));

                Response.Redirect("FrmBolsistas.aspx?fp="+hdFolhaPagamentoUID.Value);
            }
            catch (Exception ex)
            {
                this.LancarExcecao(ex.Message, true);
            }
        }

        private void LancarExcecao(String mensagem, bool visibilidade)
        {
            pnlMessage.Visible = visibilidade;
            lblMessage.Text = mensagem;
        }

        protected void btTextoValores_Click(object sender, EventArgs e)
        {
            String docValorPhysicalPath = Server.MapPath("~") + "arquivos/download/document.txt";
            FileInfo fi = new FileInfo(docValorPhysicalPath);

            InstrumentoSelecao instrumento = this.controller.GetInstrumentoSelecaoPorFolhaPagamento(
                                                                int.Parse(hdFolhaPagamentoUID.Value));

            decimal valor = Convert.ToDecimal(instrumento.FinanciadorInstrumentoSelecao.FirstOrDefault().valorBolsa);

            using (StreamWriter text = fi.CreateText())
            {
                for (int i = 0; i < 7; i++)
                {
                    text.WriteLine(valor.ToString());
                    text.Write(text.NewLine);
                }
            }

            FileUtil.ForceDownload(Response, "valores.txt", docValorPhysicalPath);
        }

        protected void btTextoBolsistas_Click(object sender, EventArgs e)
        {
            String docValorPhysicalPath = Server.MapPath("~") + "/arquivos/download/document.txt";
            FileInfo fi = new FileInfo(docValorPhysicalPath);

            IList<Bolsista> listBolsistas = this.controller.GetBolsistas(int.Parse(hdFolhaPagamentoUID.Value), false);

            InstrumentoSelecao instrumento = this.controller.GetInstrumentoSelecaoPorFolhaPagamento(
                                                                int.Parse(hdFolhaPagamentoUID.Value));
            decimal valor = Convert.ToDecimal(instrumento.FinanciadorInstrumentoSelecao.FirstOrDefault().valorBolsa);

            using (StreamWriter text = fi.CreateText())
            {
                int i = 0;
                text.WriteLine("Bolsistas:");
                foreach(Bolsista bolsista in listBolsistas)
                {                    
                    text.Write(this.GetAluno(bolsista.registroAluno).CPF+"   ");
                    text.Write("    ");
                    if (bolsista.Banco != null)
                    {
                        int tamanhoBanco = bolsista.Banco.numero.Value.ToString().Length;

                        int j = 3 - tamanhoBanco;

                        while (j > 0)
                        {
                            text.Write("0");
                            j--;
                        }
                        
                        text.Write(bolsista.Banco.numero);
                    }
                    else
                        text.Write("   ");
                    text.Write(" ");

                    String nAgencia = bolsista.agencia.ToString();
                    text.Write(nAgencia);
                    text.Write(" ");

                    String nConta = bolsista.numeroConta.ToString();
                    text.Write(nConta.Replace("-", "").Replace(".", ""));
                    text.Write("         ");
                    text.Write(text.NewLine);
                    text.Write(text.NewLine);


                    i++;

                    if (i == 6)
                    {
                        i = 0;
                        text.Write(text.NewLine);
                        text.Write(text.NewLine);
                        text.WriteLine("---------------------------------------------------------");
                        text.WriteLine("Valores:");
                        text.Write(text.NewLine);
                        text.Write(text.NewLine);
                        
                        for (int k = 0; k < 7; k++)
                        {
                            text.WriteLine(valor.ToString());
                            text.Write(text.NewLine);
                        }
                        text.Write(text.NewLine);
                        text.Write(text.NewLine);
                        text.Write("---------------------------------------------------------");
                        text.Write(text.NewLine);
                        text.Write(text.NewLine);
                    }

                    
                }
            }

            FileUtil.ForceDownload(Response, "bolsistas.txt", docValorPhysicalPath);
        }

        protected void btFecharPnl_Click(object sender, EventArgs e)
        {
            mdlPanelParecer.Hide();
            grdFolhaPagamento.DataBind();
        }

        protected void btFecharFolha_Click(object sender, EventArgs e)
        {
            int folhaPagamentoUID = int.Parse(hdFolhaPagamentoUID.Value);
            this.controller.AtualizarEtapaFinanceiro(folhaPagamentoUID, 3);
        }

    }
}