﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.DataAccess;
using ufmt.ssbic.Business;

namespace ufmt.ssbic.View.webapp.financeiro
{
    public partial class FrmBolsistasSuplementar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void lnkNovoBolsista_Click(object sender, EventArgs e)
        {
            Response.Redirect("FrmNovoBolsista.aspx?folhaSuplementarUID=" + Request.QueryString["folhaSuplementarUID"]);
        }

        public string GetNomeAluno(string cpf)
        {
            AlunoBO alunoBO = new AlunoBO();
            return alunoBO.GetAlunoPorCPF(cpf).Nome;
        }

        protected void grdAlunosFolha_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("excluir"))
            {
                int bolsistaFolhaSuplementarUID = int.Parse(e.CommandArgument.ToString());
                SuplementarController scontroller = new SuplementarController();
                scontroller.ExcluirBolsistaFolhaSuplementar(bolsistaFolhaSuplementarUID);
                
                grdAlunosFolha.DataBind();
            }
        }

        protected void lnkImprimir_Click(object sender, EventArgs e)
        {
            Response.Redirect("../relatorio/FrmRelatorioSuplementar.aspx?folhaSuplementarUID=" + Request.QueryString["folhaSuplementarUID"]);
        }
    }
}