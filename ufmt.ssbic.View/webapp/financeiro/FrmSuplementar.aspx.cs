﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;
using ufmt.ssbic.View.session;
using System.Collections;

namespace ufmt.ssbic.View.webapp.financeiro
{
    public partial class FrmSuplementar : VerificaSession
    {
        public int? ProcessoSeletivoUID { get; set; }
        public String Criterio { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                BolsaBO objBO = new BolsaBO();
                string programaUID = Session["programaUID"].ToString();
                string proreitoria = objBO.GetPrograma(int.Parse(programaUID));
                                
                PreecherGridView(Request.QueryString["processoSeletivoUID"].ToString(), null, false);

                hdProcessoSeletivo.Value = Request.QueryString["processoSeletivoUID"].ToString();

                Label1.Text = "Administração dos Instrumentos de Seleção &raquo; Etapas mensais";
            }
        }

        protected void PreecherGridView(String processoSeletivoUID, DateTime? dataInicio, bool visible)
        {

            PanelMsgResultado.Visible = visible;
            if (!String.IsNullOrEmpty(processoSeletivoUID))
                this.ProcessoSeletivoUID = int.Parse(processoSeletivoUID);
            else
                this.ProcessoSeletivoUID = null;

            FolhaPagamentoBO objBO = new FolhaPagamentoBO();
            List<FolhaPagamento> registros = null;


            

            //Consulta
            if (!dataInicio.HasValue)
            {
                registros = objBO.GetFolhasPagamento(this.ProcessoSeletivoUID);
            }
            else
            {
                registros = objBO.GetFolhasPagamento(this.ProcessoSeletivoUID, dataInicio.Value);
            }

            //GdrFolha.DataSource = registros;
            //lblTotalRegistros.Text = registros.Count.ToString() + " registro(s) disponível(is)";
            //GdrFolha.DataBind();



            if (Session["permissao"].ToString().Contains("FINANCEIRO"))
            {
              //  GdrFolha.Columns[6].Visible = true;
              //  GdrFolha.Columns[5].Visible = false;
              //  lnkGerarFolha.Visible = false;
            }
            else if (Session["permissao"].ToString().Contains("COORDENADOR"))
            {
            //    lnkGerarFolha.Visible = false;
//                GdrFolha.Columns[6].Visible = false;
  //              GdrFolha.Columns[5].Visible = false;
            }
            else
            {
    //            GdrFolha.Columns[6].Visible = false;
      //          GdrFolha.Columns[5].Visible = false;
            }

            if (Session["permissao"].ToString().Contains("MONITORIA"))
            {
        //        GdrFolha.Columns[6].Visible = true;
            }

            if (Session["permissao"].ToString().Contains("CARE"))
            {
         //       GdrFolha.Columns[5].Visible = true;
       //         GdrFolha.Columns[8].Visible = true;
            }


            if (registros.Count > 0)
            {

                /*ImageButton btnPrev = (ImageButton)GdrEditais.BottomPagerRow.FindControl("cmdPrev");
                if (GdrEditais.PageIndex == 0)
                    btnPrev.Enabled = false;
                else
                    btnPrev.Enabled = true;
                */
            }



        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

       


        public String verificarSituacao(int status)
        {
            String situacao;
            if (status == 0)
                situacao = "Aguardando";
            else if(status == 1)
                situacao = "Aberto";
            else
                situacao = "Concluído";

            return situacao;
        }

        public String VerificarParecer(String parecer)
        {
            String link;
            if (String.IsNullOrEmpty(parecer))
                link = "";
            else
                link = "[parecer]";

            return link;
        }

        protected void GdrFolha_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "parecer2")
            {
                FolhaPagamentoBO objBO = new FolhaPagamentoBO();
                FolhaPagamento folha = objBO.GetFolhaPagamento(int.Parse(e.CommandArgument.ToString()));
        //        lblParecer.Text = folha.parecerE2;
          //      lblTituloParecer.Text = "Etapa 2 - Homologação - Parecer";
            //    ModalPopupExtender1.Show();
            }

            if (e.CommandName == "parecer3")
            {
                FolhaPagamentoBO objBO = new FolhaPagamentoBO();
                FolhaPagamento folha = objBO.GetFolhaPagamento(int.Parse(e.CommandArgument.ToString()));
                //   lblParecer.Text = folha.parecerE3;
                //  lblTituloParecer.Text = "Etapa 3 - Financeiro - Parecer";
                // ModalPopupExtender1.Show();
            }
        }

        protected void lnkGerarFolha_Click(object sender, EventArgs e)
        {
            hdProcessoSeletivo.Value = Request.QueryString["processoSeletivoUID"].ToString();
            pnlNovaFolha.Visible = true;
        }

        protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
        {
            string msg = "Folha de pagamento gerada com sucesso.";
         //   DateTime dataInicio = DateTime.Parse(txtDataInicio.Text);
         //   DateTime dataFim = DateTime.Parse(txtDataFinal.Text);

            DateTime dataInicio = DateTime.Parse(
                "01/" + ddlMesFolha.SelectedValue + "/" + ddlAnoFolha.SelectedValue);

            int ultimoDia = DateTime.DaysInMonth(int.Parse(ddlAnoFolha.SelectedValue), 
                                                 int.Parse(ddlMesFolha.SelectedValue));

            DateTime dataFim = DateTime.Parse(
                ultimoDia.ToString() + "/" + ddlMesFolha.SelectedValue + "/" + ddlAnoFolha.SelectedValue);

            int processoSeletivoUID = int.Parse(hdProcessoSeletivo.Value.ToString());

            ProcessoSeletivoBO proBO = new ProcessoSeletivoBO();
            ProcessoSeletivo processo = proBO.GetProcessoSeletivoPorId(processoSeletivoUID);
            

            try
            {
                FolhaPagamentoBO folhaBO = new FolhaPagamentoBO();
                folhaBO.SalvarFolhaPagamentoSuplementar(processoSeletivoUID, dataInicio, dataFim);

                pnlNovaFolha.Visible = false;
                PreecherGridView(Request.QueryString["processoSeletivoUID"].ToString(), null, false);
                //   GdrFolha.DataBind();
                
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('" + msg + "');", true);
            
        }

        protected void lnkFolhaSuplementar_Click(object sender, EventArgs e)
        {
            pnlNovaFolha.Visible = true;
        }        
    }

}