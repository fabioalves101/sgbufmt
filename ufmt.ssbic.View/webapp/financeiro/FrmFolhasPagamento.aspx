﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true" CodeBehind="FrmFolhasPagamento.aspx.cs" Inherits="ufmt.ssbic.View.webapp.financeiro.FrmFolhasPagamento" %>
<%@ Register src="../../controles/UCBreadcrumbFinanceiro.ascx" tagname="UCBreadcrumbFinanceiro" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<style>
.mGrid td 
{
    height:50px;
}
</style>
    
<div>

    <uc1:UCBreadcrumbFinanceiro ID="UCBreadcrumbFinanceiro1" runat="server" />

</div>
<div>
    <h1>Folhas de Pagamento</h1>
</div>
    <asp:LinkButton ID="lnkFolhasSuplementares" runat="server" 
        CssClass="minibutton" onclick="lnkFolhasSuplementares_Click">
        <span>
            Visualizar Folhas Suplementares
        </span>
    </asp:LinkButton>

    <asp:GridView ID="grdFolhaPagamento" runat="server" AutoGenerateColumns="False" 
        DataSourceID="odsFolhaPagamento" Width="100%" CssClass="mGrid" 
        onrowcommand="grdFolhaPagamento_RowCommand">
        <Columns>
            <asp:TemplateField HeaderText="Mês">
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# GetMesFolhaPagamento((DateTime)Eval("dataFim")) %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:CheckBoxField DataField="fechada" HeaderText="Finalizada" 
                SortExpression="fechada" />
            <asp:TemplateField HeaderText="Suplementar" SortExpression="suplementar">
            <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLink2" NavigateUrl='<%# Eval("folhaPagamentoUID", "FrmBolsistasFolhaSuplementar.aspx?fp={0}") %>' runat="server"><%# VerificarSuplementar((int)Eval("folhaPagamentoUID")) %></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Imprimir">
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" CommandName="imprimir" CommandArgument='<%# Eval("folhaPagamentoUID") %>' runat="server">
                        <img src="../../images/print.png" alt="Imprimir Folha" title="Imprimir Folha de Pagamento" />
                    </asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Gerenciar Bolsistas da Folha">
                <ItemStyle HorizontalAlign="Center" Width="20%" />
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# Eval("folhaPagamentoUID", "FrmBolsistas.aspx?fp={0}") %>'>
                    <img src="../../images/folha.png" alt="Gerenciar Bolsistas da Folha" title="Gerenciar Bolsistas da Folha" />                    
                    
                    </asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>

        </Columns>
    </asp:GridView>
    <asp:ObjectDataSource ID="odsFolhaPagamento" runat="server" 
        SelectMethod="GetFolhasPagamento" 
        TypeName="ufmt.ssbic.Business.FinanceiroController">
        <SelectParameters>
            <asp:ControlParameter ControlID="hdInstrumentoSelecaoUID" 
                Name="instrumentoSelecaoUID" PropertyName="Value" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:HiddenField ID="hdInstrumentoSelecaoUID" runat="server" />
</asp:Content>
