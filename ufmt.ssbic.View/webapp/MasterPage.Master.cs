﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.sig.entity;

namespace ufmt.ssbic.View.webapp.bolsa
{
    public partial class MasterPage : System.Web.UI.MasterPage
    {
        public string UsuarioUID 
        {
            get { return Session["usuarioUID"].ToString(); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                lnkSair.NavigateUrl = "~/Logout.aspx";
                long usuarioUID = long.Parse(Session["usuarioUID"].ToString());
                AutenticacaoBO autenticacaoBO = new AutenticacaoBO();
                Usuario usuario = autenticacaoBO.GetUsuario(usuarioUID);
                usuarioLogado.Text = usuario.Pessoa.Nome;

            }
            catch (Exception ex)
            {
                Response.Redirect("~/Login.aspx?proreitoriaUID=" + Request.QueryString["proreitoriaUID"]);
            }
        }
    }
}