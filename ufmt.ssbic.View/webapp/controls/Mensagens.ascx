﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Mensagens.ascx.cs"
    Inherits="ufmt.ssbic.View.webapp.controls.Mensagens" %>

<!-- Painel Erro -->
<div id="Panel_error" clientidmode="Static" runat="server" class="error">
    <div style="float: left; width: 90%">
        <strong>
            <asp:Label ID="lblMsgErro" runat="server" Text="Erro!"></asp:Label>
            <br />
            <br />
        </strong>
        <asp:Label ID="lblTipoErro" runat="server" Text="Erro!"></asp:Label>
    </div>
    <div style="float: right; width: 10%; text-align: right">
        <a id="fecharErro" onclick="document.getElementById('Panel_error').style.display = 'none';" class="fechar">fechar</a></div>
</div>
<!-- Fim Painel Erro -->

<!-- Painel Sucesso -->
<div id="Panel_success" clientidmode="Static" runat="server" class="success">
    <div style="float: left; width: 90%">
        <strong style="color: #008000; font-size: larger">
            <asp:Label ID="lblMsgSucesso" runat="server" Text="Sucesso!"></asp:Label>
            <br />
        </strong>
        <br />
            <asp:Label ID="lblTipoSucesso" runat="server" Text="Sucesso!" style="color: #008000;"></asp:Label>
    </div>
    <div style="float: right; width: 10%; text-align: right">
        <a id="fecharSucesso" onclick="document.getElementById('Panel_success').style.display = 'none';" class="fechar">fechar</a>
    </div>
</div>
<!-- Fim Painel Sucesso -->

<!-- Painel Alerta -->
<div id="Panel_alert" clientidmode="Static" runat="server" class="warning">
    <div style="float: left; width: 90%">
        <strong style="color: #754F00; font-size: larger">
            <asp:Label ID="lblMsgAlerta" runat="server" Text="Alerta!"></asp:Label>
            <br />
        </strong>
        <br />
            <asp:Label ID="lblTipoAlerta" runat="server" Text="Alerta!" style="color: #754F00;"></asp:Label>
    </div>
    <div style="float: right; width: 10%; text-align: right">
        <a id="fecharAlerta" onclick="document.getElementById('Panel_alert').style.display = 'none';" class="fechar">fechar</a>
    </div>
</div>
<!-- Fim Painel Alerta -->

<!-- Painel Info -->
<div id="Panel_info" clientidmode="Static" runat="server" class="info" visible="false">
    <div style="float: left; width: 90%">
        <strong style="color: #003399; font-size: larger">
            <asp:Label ID="lblMsginformacao" runat="server" Text="Informação!"></asp:Label>
            <br />
        </strong>
        <br />
            <asp:Label ID="lblTipoInformacao" runat="server" Text="Informação!" style="color: #003399;"></asp:Label>
    </div>
    <div style="float: right; width: 10%; text-align: right">
        <a id="fecharInfo" onclick="document.getElementById('Panel_info').style.display = 'none';" class="fechar">fechar</a>
    </div>
</div>
<!-- Fim Painel Info -->