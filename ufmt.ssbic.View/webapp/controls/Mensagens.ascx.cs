﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ufmt.ssbic.View.webapp.controls
{
    public partial class Mensagens : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Panel_success.Style.Add("display", "none");
            Panel_alert.Style.Add("display", "none");
            Panel_error.Style.Add("display", "none");
            Panel_info.Style.Add("display", "none");
        }

        /// <summary>
        /// Limpa todos os paineis definindo como ocultos e com os textos padrões
        /// </summary>
        public void limparPaineis()
        {           
            Panel_alert.Style.Add("display", "none");
            lblMsgAlerta.Visible = false;
            lblMsgAlerta.Text = "Alerta!";
            lblTipoAlerta.Visible = false;
            lblTipoAlerta.Text = "Alerta!";

            Panel_error.Style.Add("display", "none");
            lblMsgErro.Visible = false;
            lblMsgErro.Text = "Erro!";
            lblTipoErro.Visible = false;
            lblTipoErro.Text = "Erro!";

            Panel_info.Style.Add("display", "none");
            lblMsginformacao.Visible = false;
            lblMsginformacao.Text = "Informação!";
            lblTipoInformacao.Visible = false;
            lblTipoInformacao.Text = "Informação!";
            
            Panel_success.Style.Add("display", "none");
            lblMsgSucesso.Visible = false;
            lblMsgSucesso.Text = "Sucesso!";
            lblTipoSucesso.Visible = false;
            lblTipoSucesso.Text = "Sucesso!";
        }

        /// <summary>
        /// Habilita determinado painel
        /// </summary>
        /// <param name="nomePainel">Nome do painel a ser habilitado</param>
        /// <param name="mensagemPrincipal">Mensagem que aparecerá em destaque</param>
        /// <param name="mensagemSecundaria">Mensagem que aparecerá logo abaixo da principal</param>
        /// <param name="exclusivo">Caso apenas o painel desejado será exibido</param>
        public void HabilitarPainel(String nomePainel, String mensagemPrincipal, String mensagemSecundaria, bool exclusivo)
        {

            if (exclusivo)
            {
                limparPaineis();
            }

            if (nomePainel.ToLower().Equals("alerta"))
            {
                Panel_alert.Style.Add("display", "block");
                lblMsgAlerta.Visible = true;
                lblMsgAlerta.Text = mensagemPrincipal;
                lblTipoAlerta.Visible = true;
                lblTipoAlerta.Text = mensagemSecundaria;
                
            }else if (nomePainel.ToLower().Equals("erro"))
            {
                Panel_error.Style.Add("display", "block");
                lblMsgErro.Visible = true;
                lblMsgErro.Text = mensagemPrincipal;
                lblTipoErro.Visible = true;
                lblTipoErro.Text = mensagemSecundaria;
            
            }else if (nomePainel.ToLower().Equals("informacao"))
            {
                Panel_info.Style.Add("display", "block");
                lblMsginformacao.Visible = true;
                lblMsginformacao.Text = mensagemPrincipal;
                lblTipoInformacao.Visible = true;
                lblTipoInformacao.Text = mensagemSecundaria;
            
            }else if (nomePainel.ToLower().Equals("sucesso"))
            {
                Panel_success.Style.Add("display", "block");
                lblMsgSucesso.Visible = true;
                lblMsgSucesso.Text = mensagemPrincipal;
                lblTipoSucesso.Visible = true;
                lblTipoSucesso.Text = mensagemSecundaria;
            
            }
        }
    }
}