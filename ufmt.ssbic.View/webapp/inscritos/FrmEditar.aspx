﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true" CodeBehind="FrmEditar.aspx.cs" Inherits="ufmt.ssbic.View.webapp.inscritos.FrmEditar" %>
<%@ Register src="../../controles/UCDadosBancariosAluno.ascx" tagname="UCDadosBancariosAluno" tagprefix="uc1" %>
<%@ Register src="../../controles/UCBuscarServidor.ascx" tagname="UCBuscarServidor" tagprefix="uc2" %>
<%@ Register src="../../controles/UCBuscarAluno.ascx" tagname="UCBuscarAluno" tagprefix="uc3" %>
<%@ Register src="../../controles/UCBuscarDisciplinas.ascx" tagname="UCBuscarDisciplinas" tagprefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<h1><span class="allcaps">Editar aluno inscrito</span></h1>
<div id="containerMensagens">
    <asp:Panel ID="PanelMsgResultado" runat="server" Visible="false">
        <asp:Label ID="lblMsgResultado" runat="server"></asp:Label>
    </asp:Panel>
        <div>
            <fieldset>
                <legend>Professor Orientador:</legend>
                <asp:Panel ID="pnlDadosOrientador" runat="server">
                    <p>
                    <asp:Label ID="lblOrientador" runat="server"></asp:Label>
                    </p>
                    <p>
                    <asp:LinkButton ID="lnkOrientador" CausesValidation="false" runat="server" onclick="lnkOrientador_Click">Alterar</asp:LinkButton>
                    </p>
                </asp:Panel>
                <asp:Panel ID="pnlOrientador" runat="server" Visible="false">
                    
                    <uc2:UCBuscarServidor ID="UCBuscarOrientador" runat="server" />
                    
                </asp:Panel>
            </fieldset>
        </div>
        <div>
            <fieldset>
                <legend>Coordenador:</legend>
                <asp:Panel ID="pnlDadosCoordenador" runat="server">
                    <p>
                    <asp:Label ID="lblCoordenador" runat="server"></asp:Label>
                    </p>
                    <p>
                    <asp:LinkButton ID="lnkCoordenador" CausesValidation="false" runat="server" onclick="lnkCoordenador_Click">Alterar</asp:LinkButton>
                    </p>
                </asp:Panel>
                <asp:Panel ID="pnlCoordenador" runat="server" Visible="false">
                    <uc2:UCBuscarServidor ID="UCBuscarCoordenador" runat="server" />
                </asp:Panel>

            </fieldset>
        </div>
        <div>
        <fieldset>
                <legend>Modalidade:</legend>
                <asp:RadioButtonList ID="rdoModalidade" runat="server" 
                    RepeatDirection="Horizontal" Width="500px">
                    <asp:ListItem Value="1">Voluntária</asp:ListItem>
                    <asp:ListItem Value="2">Remunerada</asp:ListItem>
                </asp:RadioButtonList>
            </fieldset>
        </div>
        <div>
            <fieldset>
                <legend>Período:</legend>
                <asp:RadioButtonList ID="rdoPeriodo" runat="server" 
                    RepeatDirection="Horizontal" Width="300px">
                    <asp:ListItem Value="1">1° Semestre</asp:ListItem>
                    <asp:ListItem Value="2">2° Semestre</asp:ListItem>
                    <asp:ListItem Value="3">Anual</asp:ListItem>
                </asp:RadioButtonList>
            </fieldset>
        </div>
        <div>
            <fieldset>
                <legend>Atuação:</legend>
                <p>
                Início: 
                    <asp:TextBox ID="txtInicio" runat="server"></asp:TextBox>
                </p>
                <p>
                Final: 
                    <asp:TextBox ID="txtFinal" runat="server"></asp:TextBox>
                </p>
                
            </fieldset>
        </div>

        <div>
            <uc1:UCDadosBancariosAluno ID="UCDadosBancariosAluno1" runat="server" />
        </div>
        <div>

            <asp:Panel ID="pnlDisciplinas" runat="server" Visible="true"> 
            
            <uc4:UCBuscarDisciplinas ID="UCBuscarDisciplinas1" runat="server" />

            
            </asp:Panel>
            
        </div>
        <div>
            <fieldset>
                <legend>Status:</legend>
                <asp:RadioButtonList ID="rdoStatus" runat="server" 
                    RepeatDirection="Horizontal" Width="300px">
                    <asp:ListItem Value="1">OK</asp:ListItem>
                    <asp:ListItem Value="0">Não OK</asp:ListItem>
                </asp:RadioButtonList>
            </fieldset>
        </div>
        <div style="text-align:center">
            <asp:Button ID="btAtualizar" runat="server" Text="Atualizar" Height="30px" 
                onclick="btAtualizar_Click" Width="193px" />
        
        </div>

</div>
    
</asp:Content>
