﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.View.session;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;
using ufmt.sig.entity;

namespace ufmt.ssbic.View.webapp.inscritos
{
    public partial class FrmEditar : VerificaSession
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MonitorBO monitorBO = new MonitorBO();
                Monitor monitor = monitorBO.GetMonitor(GetMonitorUID());

                

                PessoaBO pessoaBO = new PessoaBO();
                Servidor orientador = null;
                if (monitor.orientadorUID.HasValue)
                {
                    orientador = pessoaBO.GetServidorUnico(monitor.orientadorUID.Value);
                    lblOrientador.Text = orientador.Pessoa.Nome;
                    UCBuscarOrientador.servidor = orientador;
                    UCBuscarOrientador.SetServidor(orientador.ServidorUID);
                }
                else
                {
                    lblOrientador.Text = "Não definido";
                }

                Servidor coordenador = null;
                if (monitor.coordenadorUID.HasValue)
                {
                    coordenador = pessoaBO.GetServidorUnico(monitor.coordenadorUID.Value);
                    lblCoordenador.Text = coordenador.Pessoa.Nome;
                    UCBuscarCoordenador.servidor = coordenador;
                    UCBuscarCoordenador.SetServidor(coordenador.ServidorUID);
                }
                else
                {
                    lblCoordenador.Text = "Não definido";
                }


                if (monitor.remunerada.Value)
                    rdoModalidade.SelectedValue = "2";
                else
                    rdoModalidade.SelectedValue = "1";

                if (monitor.periodoAtuacao.Equals("1"))
                    rdoPeriodo.SelectedValue = "1";
                else
                {
                    if (monitor.periodoAtuacao.Equals("2"))
                        rdoPeriodo.SelectedValue = "2";
                    else
                        rdoPeriodo.SelectedValue = "3";
                }

                if (monitor.status.HasValue)
                {
                    if(monitor.status.Value)
                        rdoStatus.SelectedValue = "1";
                    else
                        rdoStatus.SelectedValue = "0";
                }
                else
                    rdoStatus.SelectedValue = "0";


                if (monitor.inicio.HasValue)
                    txtInicio.Text = monitor.inicio.Value.ToShortDateString();

                if (monitor.final.HasValue)
                    txtFinal.Text = monitor.final.Value.ToShortDateString();

                BolsistaBO bolsistaBO = new BolsistaBO();
                Bolsista bolsista = bolsistaBO.GetBolsistaPorId(monitor.bolsistaUID.Value);

                AlunoBO alunoBO = new AlunoBO();
                vwAlunoSiga vwaluno = alunoBO.GetAluno(decimal.Parse(bolsista.registroAluno));

                Aluno aluno = new Aluno();
                aluno.Campus = vwaluno.Campus;
                aluno.CodCampus = vwaluno.CodCampus;
                aluno.CodCurso = vwaluno.CodCurso;
                aluno.CPF = vwaluno.CPF;
                aluno.Curso = vwaluno.Curso;
                aluno.Email = vwaluno.Email;
                aluno.Matricula = vwaluno.Matricula;
                aluno.Nome = vwaluno.Nome;
                aluno.Tipo = vwaluno.Tipo;

                UCDadosBancariosAluno1.aluno = aluno;
                UCDadosBancariosAluno1.Agencia = bolsista.agencia;
                UCDadosBancariosAluno1.Conta = bolsista.numeroConta;

                if(bolsista.bancoUID.HasValue)
                    UCDadosBancariosAluno1.BancoUID = bolsista.bancoUID.Value;
                


                UCDadosBancariosAluno1.SetDadosBancarios();


                CursoBO cursoBO = new CursoBO();
                ufmt.sig.entity.MatrizDisciplinar matriz = cursoBO.GetMatrizDisciplinar(int.Parse(monitor.disciplinaUID.Value.ToString()));

                UCBuscarDisciplinas1.rga = aluno.Matricula.ToString();
                UCBuscarDisciplinas1.media = 5.0;
                UCBuscarDisciplinas1.nomeCurso = aluno.Curso;

                ((Panel)UCBuscarDisciplinas1.FindControl("pnlGridview")).Visible = false;
                ((Panel)UCBuscarDisciplinas1.FindControl("pnlDisciplina")).Visible = true;

                ((Label)UCBuscarDisciplinas1.FindControl("lblCodigoDiscplina")).Text = matriz.CodigoExterno.Value.ToString();
                ((Label)UCBuscarDisciplinas1.FindControl("lblNomeDisciplina")).Text = matriz.Nome;

                ((HiddenField)UCBuscarDisciplinas1.FindControl("hdMatrizDisciplinarUID")).Value = matriz.MatrizDisciplinarUID.ToString();
                UCBuscarDisciplinas1.disciplina = matriz;
            }            
        }

        protected int GetMonitorUID()
        {
            return int.Parse(Request.QueryString["monitorUID"]);
        }

        protected void lnkOrientador_Click(object sender, EventArgs e)
        {
            pnlDadosOrientador.Visible = false;

            UCBuscarOrientador.labelBuscaServidor = "Buscar Orientador";
            pnlOrientador.Visible = true;
        }

        protected void lnkCoordenador_Click(object sender, EventArgs e)
        {
            pnlDadosCoordenador.Visible = false;

            UCBuscarCoordenador.labelBuscaServidor = "Buscar Coordenador";
            pnlCoordenador.Visible = true;
        }

        protected void btAtualizar_Click(object sender, EventArgs e)
        {  
                MonitorBO monitorBO = new MonitorBO();
                Monitor monitor = monitorBO.GetMonitor(GetMonitorUID());
                Servidor orientador = null;
                try
                {
                    orientador = UCBuscarOrientador.GetServidor();
                }
                catch (Exception ex)
                {
                    orientador = new Servidor();
                    orientador.ServidorUID = monitor.orientadorUID.Value;
                }

                Servidor coordenador = null;
                try
                {
                    coordenador = UCBuscarCoordenador.GetServidor();
                }
                catch (Exception ex)
                {
                    coordenador = new Servidor();
                    coordenador.ServidorUID = monitor.coordenadorUID.Value;
                }

                Monitor newMonitor = new Monitor();
                newMonitor.cursoUID = monitor.cursoUID;
                newMonitor.bolsistaUID = monitor.bolsistaUID;

                int disciplinaUID = int.Parse(((HiddenField)UCBuscarDisciplinas1.FindControl("hdMatrizDisciplinarUID")).Value);

                newMonitor.disciplinaUID = disciplinaUID;
                if (rdoModalidade.SelectedItem.Value == "2")
                    newMonitor.remunerada = true;
                else
                {
                    newMonitor.remunerada = false;
                    newMonitor.ativo = true;
                }

                if (rdoPeriodo.SelectedItem.Value == "1")
                    newMonitor.periodoAtuacao = "1";
                else
                {
                    if (rdoPeriodo.SelectedItem.Value == "2")
                        newMonitor.periodoAtuacao = "2";
                    else
                        newMonitor.periodoAtuacao = "3";
                }

                if (!String.IsNullOrEmpty(txtInicio.Text))
                    newMonitor.inicio = Convert.ToDateTime(txtInicio.Text);
                if (!String.IsNullOrEmpty(txtFinal.Text))
                    newMonitor.final = Convert.ToDateTime(txtFinal.Text);
                newMonitor.ativo = false;
                newMonitor.processoSeletivoUID = monitor.processoSeletivoUID;
                newMonitor.obs = monitor.obs;

                if (rdoStatus.SelectedItem.Value == "1")
                    newMonitor.status = true;
                else
                    newMonitor.status = false;

                BolsistaBO bolsistaBO = new BolsistaBO();
                Bolsista bolsista = bolsistaBO.GetBolsistaPorId(monitor.bolsistaUID.Value);
               
                bolsista.agencia = UCDadosBancariosAluno1.Agencia;
                bolsista.numeroConta = UCDadosBancariosAluno1.Conta;

                TextBox txtEmail = (TextBox)UCDadosBancariosAluno1.FindControl("txtEmailAluno");
                bolsista.email = txtEmail.Text;

                if (UCDadosBancariosAluno1.BancoUID != 0)
                    bolsista.bancoUID = UCDadosBancariosAluno1.BancoUID;
                else
                    bolsista.bancoUID = null;

                AlunoBO alunoBO = new AlunoBO();
                vwAlunoSiga vwaluno = alunoBO.GetAluno(decimal.Parse(bolsista.registroAluno));

                bolsista.registroAluno = vwaluno.Matricula.ToString();
            
                
                monitorBO.Atualizar(GetMonitorUID(), newMonitor, orientador, coordenador);
                bolsistaBO.AtualizarBolsista(bolsista.bolsistaUID, bolsista.email, bolsista.numeroConta, bolsista.bancoUID, bolsista.agencia);
                
                Response.Redirect("../processoSeletivo/FrmInscricao.aspx?processoSeletivoUID="+monitor.processoSeletivoUID);
        /*    
        }
            catch (Exception ex)
            {
                PanelMsgResultado.Visible = true;
                lblMsgResultado.Text = "Não foi possível atualizar o registro. " + ex.Message;
            }
         */
            
        }
    }
}