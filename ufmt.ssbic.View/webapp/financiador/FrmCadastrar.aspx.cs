﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.sig.entity;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;
using ufmt.ssbic.View.session;


namespace ufmt.ssbic.View.webapp.financiador
{
    public partial class FrmCadastrar : VerificaSession
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack) {


                string proreitoriaUID = Session["programaUID"].ToString();
                string area = Request.QueryString["area"];
                lblTitulo.Text = area;
                
                LoadDdlFinanciador();

            }
            
        }

        protected void LoadDdlFinanciador()
        {
            FinanciadorBO objBO = new FinanciadorBO();
            ddlFinanciador.DataSource = objBO.GetTipoFinanciador();
            ddlFinanciador.DataTextField = "descricao";
            ddlFinanciador.DataValueField = "financiadorUID";
            ddlFinanciador.DataBound += new EventHandler(IndiceZero);
            ddlFinanciador.DataBind();
        }

        protected void IndiceZero(object sender, EventArgs e)
        {
            DropDownList objDropDownList = (DropDownList)sender; //Cast no sender para DropDownList
            objDropDownList.Items.Insert(0, new ListItem("Selecione","")); //Adiciona um novo Item
        }


        protected void btnVoltar_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("FrmListar.aspx?instrumentoSelecaoUID=" + Request.QueryString["instrumentoSelecaoUID"].ToString());
        }

        protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                FinanciadorBO objBO = new FinanciadorBO();
                FinanciadorInstrumentoSelecao fin = new FinanciadorInstrumentoSelecao();

                fin.instrumentoSelecaoUID = int.Parse(Request.QueryString["instrumentoSelecaoUID"].ToString());
                fin.financiadorUID = int.Parse(ddlFinanciador.SelectedValue);
                fin.quantidadeBolsas = int.Parse(txtQuantidadeBolsa.Text);
                fin.quantidadeRestantes = int.Parse(txtQuantidadeBolsa.Text);
                
                if(!String.IsNullOrEmpty(hdCursoUID.Value.ToString()))
                {
                    fin.cursoUID = int.Parse(hdCursoUID.Value.ToString());
                }
                //Remunerado
                if (rblTipoBolsa.SelectedValue == "1")
                    fin.valorBolsa = double.Parse(txtValor.Text);
                else //Voluntário
                    fin.valorBolsa = null;

                fin.tipoOferta = int.Parse(rblTipoBolsa.SelectedItem.Value);
                objBO.Salvar(fin, 1);

                lblMsgResultado.Text = "Registro adicionado com sucesso.";
                PanelMsgResultado.CssClass = "success";
                PanelFormulario.Visible = false;
                btnSalvar.Visible = false;
                btnNovo.Visible = true;
            }
            catch (Exception err)
            {

                lblMsgResultado.Text = "Não foi possível efetuar a operação.  Por favor, entre em contato com o administrador do sistema.<br />" + err.Message.ToString();
                PanelMsgResultado.CssClass = "error";
                
            }

            PanelMsgResultado.Visible = true;
        }

        protected void btnNovo_Click(object sender, ImageClickEventArgs e)
        {
            ReiniciarCadastro();
        }

        private void ReiniciarCadastro()
        {
            PanelMsgResultado.Visible = false;
            PanelFormulario.Visible = true;
            btnSalvar.Visible = true;
            btnNovo.Visible = false;
           
           
        }

        protected void rblTipoBolsa_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Remunerado
            if (rblTipoBolsa.SelectedValue == "1")
            {
                txtValor.Enabled = true;
                RequiredFieldValidator5.Enabled = true;
            }
            else
            {//Voluntário
                txtValor.Enabled = false;
                RequiredFieldValidator5.Enabled = false;
            }
        }

        public void PreencherCursos(String nome)
        {
            CursoBO cursoBO = new CursoBO();
            List<Curso> cursos = cursoBO.GetCursos(1, nome);

            GridView1.DataSource = cursos;
            //lblTotalRegistros.Text = registros.Count.ToString() + " registro(s) disponível(is)";
            GridView1.DataBind();
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                hdCursoUID.Value = e.CommandArgument.ToString();
                trtable.Visible = true;

                CursoBO cursoBO = new CursoBO();
                Curso curso = cursoBO.GetCurso(int.Parse(hdCursoUID.Value.ToString()));

                lblnomecurso.Text = curso.Nome;
                pnlCurso.Visible = false;
            }
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void btVincularCurso_Click(object sender, EventArgs e)
        {
            pnlCurso.Visible = true;
        }

        protected void btBuscar_Click(object sender, EventArgs e)
        {
            PreencherCursos(txtNomeCurso.Text);
        }

        protected void GridView1_PageIndexChanging1(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            PreencherCursos(txtNomeCurso.Text);
        }

        protected void btnNaoAplica_Click(object sender, EventArgs e)
        {
            Panel2.Visible = false;
        }

        protected void imgBtnEdit_Click(object sender, ImageClickEventArgs e)
        {
            this.CollapsiblePanelExtender1.Collapsed = false;
            this.CollapsiblePanelExtender1.ClientState = "false";
            txtNovaInstituicao.Text = ddlFinanciador.SelectedItem.Text;
            lblNovaInstituicao.Text = "Alterar Financiador";
            hdd_op_i.Value = "atualizar";
        }

        protected void imgBtnFinanciador_Click(object sender, ImageClickEventArgs e)
        {
            txtNovaInstituicao.Text = String.Empty;
            lblNovaInstituicao.Text = "Nova Instituição";
            hdd_op_i.Value = "cadastrar";
            this.CollapsiblePanelExtender1.Collapsed = false;
            this.CollapsiblePanelExtender1.ClientState = "false";
        }

        protected void btnNovaInstituicao_Click(object sender, EventArgs e)
        {
            int bancoUID = 0;
            String descricao = String.Empty;

            bancoUID = int.Parse(ddBanco.SelectedValue);
            descricao = txtNovaInstituicao.Text;
            
            FinanciadorBO financiadorBO = new FinanciadorBO();

            Financiador financiador = new Financiador();
            financiador.descricao = descricao;
            financiador.bancoUID = bancoUID;

            if (hdd_op_i.Value.Equals("cadastrar"))
            {                
                financiadorBO.Salvar(financiador, 1);
            }
            else
            {
                financiador.financiadorUID = int.Parse(ddlFinanciador.SelectedValue);
                financiadorBO.Salvar(financiador, 2);
            }

            LoadDdlFinanciador();
            this.CollapsiblePanelExtender1.Collapsed = true;
            this.CollapsiblePanelExtender1.ClientState = "true";
        }






    }
}