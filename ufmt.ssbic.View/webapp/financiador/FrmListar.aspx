﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true" CodeBehind="FrmListar.aspx.cs" Inherits="ufmt.ssbic.View.webapp.financiador.FrmListar" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
  <ContentTemplate>

    <div style="padding:5px;text-align:left;">
        <h1><span class="allcaps"><asp:Label ID="Label1" runat="server"></asp:Label>

            </span></h1>
        <div id="containerMensagens">
                <asp:Panel ID="PanelMsgResultado" runat="server" Visible="false">
                    <asp:Label ID="lblMsgResultado" runat="server"></asp:Label>
                </asp:Panel>
        </div>
       
                                <fieldset style="padding: 10px">
                                    <legend>
                                        <img alt="Filtrar Registros" class="style2" 
                                            src="../../images/ico_filters.png" />
                                        Filtar Registros</legend>
                                         <asp:Panel ID="PanelFiltro" runat="server">
                                    &nbsp;<asp:TextBox ID="txtCriterio" runat="server" Width="503px" 
                                        EnableViewState="False" ViewStateMode="Disabled"></asp:TextBox>
                                    
       <asp:LinkButton ID="lnkConsultar" runat="server" CssClass="minibutton" onclick="lnkConsultar_Click">
              
                                       
    <span>

                                             
                                             Consultar



    </span>
                                             

                                           

                                             

                                             
</asp:LinkButton>

<asp:LinkButton ID="lnkMostrarTodos" runat="server" CssClass="minibutton" onclick="lnkMostrarTodos_Click">

                          
    <span>
        Mostrar Todos

    </span>
                                             
</asp:LinkButton>

                                             

</asp:Panel>


                                </fieldset>
                            
<asp:Panel ID="PanelAcoes" runat="server" style="padding:10px">
    
    <div style="clear:both"></div>
    

 <div class="addbutton" style="float:left;margin-right:5px"> 
     <asp:HyperLink ID="lnkNovoRegistro" runat="server" CssClass="minibutton">

    <span>

        

     


        

     <img alt="" src="../../images/add.png" border="0" />&nbsp;&nbsp; Adicionar Registro</span>




        </asp:HyperLink>
 </div>                                           

                                            
<div class="addbutton" style="float:left;" runat="server" ID="divExcluir"> 
<asp:LinkButton ID="lnkExcluir" runat="server" onclick="lnkExcluirSelecionados_Click" CssClass="minibutton" OnClientClick="javascript:return window.confirm('Tem certeza que deseja remover este registro?');">




    <span>
        

       

    <img src="../../images/delete2.png" 
                                                style="width: 16px; height: 16px" alt="Excluir selecionados"
                                                runat="server" id="imgExcluir" />&nbsp; 
    
Excluir selecionados


        
    </span>
    




</asp:LinkButton>
    
</div>

<div style="float:left;width:300px;margin-left:10px">
        <asp:Label ID="lblResultadoConsulta" runat="server"></asp:Label>
        &nbsp;<asp:Label ID="lblTotalRegistros" runat="server" CssClass="ArialBlack16"></asp:Label>
    </div>

<div style="clear:both" />
</asp:Panel>

        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
            AutoGenerateColumns="False" 
            OnPageIndexChanging="GridView1_PageIndexChanging" Width="100%" 
            onrowcreated="GridView1_RowCreated" CssClass="mGrid" 
            onrowcommand="GridView1_RowCommand">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Label ID="lblID" runat="server" Text='<%# Eval("financiadorInstrumentoSelecaoUID") %>' 
                            Visible="false"></asp:Label>
                        <asp:CheckBox ID="chkRegistro" runat="server" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="5%" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Descrição" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>

                        &nbsp;<asp:HyperLink ID="lnkAtualizarRegistro" runat="server" 
                            ImageUrl="../../images/edit_20.png" 
                            NavigateUrl='<%# "FrmEditar.aspx?area=Financiador&registroUID=" + Eval("financiadorInstrumentoSelecaoUID") + "&instrumentoSelecaoUID=" + Eval("instrumentoSelecaoUID") %>'>Atualizar Registro</asp:HyperLink>
                        &nbsp;&nbsp;

                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="detalhes" Text='<%# Eval("descricao") %>' CommandArgument='<%# Eval("financiadorInstrumentoSelecaoUID") %>' />
                        <br />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="40%" />
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Curso Relacionado" SortExpression="quantidadeBolsas">
                    <ItemTemplate>
                                                <asp:Label ID="lblCurso" runat="server" 
                            Text='<%# VerificarCurso((String)Eval("nomeCurso")) %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" Width="20%" />
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Quantidade Bolsa" SortExpression="quantidadeBolsas">
                    <ItemTemplate>
                                                <asp:Label ID="lblGridBolsa" runat="server" 
                            Text='<%# Eval("quantidadeBolsas") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" Width="10%" />
                </asp:TemplateField>


                <asp:TemplateField HeaderText="Valor Bolsa" SortExpression="valorBolsa">
                    <ItemTemplate>
                        <asp:Label ID="lblGridTipo" runat="server" 
                            Text='<%# Eval("valorBolsa", "{0:C2}") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" Width="10%" />
                </asp:TemplateField>
            </Columns>
            <PagerTemplate>
                <asp:Panel ID="Panel1" runat="server" DefaultButton="cmdIr">
                    <div style="text-align: left">
                        <asp:ImageButton ID="cmdFirst" runat="server" CausesValidation="False" 
                            CommandArgument="First" CommandName="Page" 
                            ImageUrl="../../images/i_primeira_pagina.gif" ToolTip="Ir para a primeira página" />
                        &nbsp;<asp:ImageButton ID="cmdPrev" runat="server" CausesValidation="False" 
                            CommandArgument="Prev" CommandName="Page" 
                            ImageUrl="../../images/i_pagina_anterior.gif" ToolTip="Ir para a página anterior" />
                        &nbsp;&nbsp;&nbsp;&nbsp;<asp:ImageButton ID="cmdNext" runat="server" 
                            CausesValidation="False" CommandArgument="Next" CommandName="Page" 
                            ImageUrl="../../images/i_proxima_pagina.gif" ToolTip="Ir para a próxima página" />
                        &nbsp;<asp:ImageButton ID="cmdLast" runat="server" CausesValidation="False" 
                            CommandArgument="Last" CommandName="Page" 
                            ImageUrl="../../images/i_ultima_pagina.gif" ToolTip="Ir para a última página" />
                        &nbsp;&nbsp; Ir para a página
                        Ir para a página
                        <asp:TextBox ID="txtPagina" runat="server" 
                            Text="<% # GridView1.PageIndex + 1 %>" ValidationGroup="grid" Width="29px"></asp:TextBox>
                        &nbsp;<asp:ImageButton ID="cmdIr" runat="server" ImageUrl="../../images/btnIr.gif" 
                            OnClick="cmdIr2_Click" ValidationGroup="grid" />
                        <b>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                            ControlToValidate="txtPagina" Display="Dynamic" 
                            ErrorMessage="* Entre com um valor para continuar" Font-Bold="False" 
                            ValidationGroup="grid"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                            ControlToValidate="txtPagina" Display="Dynamic" 
                            ErrorMessage="* Digite somente números para continuar" Font-Bold="False" 
                            ValidationExpression="^[0-9]+$" ValidationGroup="grid">* Digite somente números* Digite somente números</asp:RegularExpressionValidator>
                        </b>
                    </div>
                    <div style="text-align: left">
                        Página&nbsp;Página&nbsp;<asp:Label ID="lblTotalPaginaAtual" runat="server" 
                            Font-Bold="True" Text="<%# GridView1.PageIndex + 1 %>"></asp:Label>
                        &nbsp;de&nbsp;&nbsp;de&nbsp;<asp:Label ID="lblTotalPaginas" runat="server" 
                            Text="<% # GridView1.PageCount %>"></asp:Label>
                        &nbsp;
                    </div>
                </asp:Panel>
            </PagerTemplate>
            <EmptyDataTemplate>
                <div 
                    style="text-align: center; padding-top: 20px; padding-bottom: 20px; font-size: 18px;
                                                    font-family: Arial Narrow; font-weight: bold">
                    <asp:Label ID="lblMsgNenhumRegistro" runat="server" 
                        Text="Nenhum registro localizado no critério da consulta."></asp:Label>
                </div>
            </EmptyDataTemplate>
        </asp:GridView>


               <asp:modalpopupextender 
                ID="ModalPopupExtender1"
                BehaviorID="behavior"
                runat="server"
                TargetControlID="lnkModal"
                PopupControlID="PanelDetalhes"
                BackgroundCssClass="modalBackground"
                DropShadow="True"
                DynamicServicePath="" 
                Enabled="True"
        
        />

        <asp:HyperLink ID="lnkModal" runat="server" Visible="true" Text=""  />

 <asp:Panel ID="PanelDetalhes" runat="server" CssClass="modalPopup">
          <div id="ContainerDetalhes" style="width:800px">
          <div id="MenuDetalhes" style="background-color:White;padding-left:5px;padding-bottom:5px;height:40px">
            <div style="width:650px; float:left; height: 32px;">
                <h2 style="height: 35px"><asp:Label ID="Label2" runat="server" Text="Label">Detalhes do Registro</asp:Label></h2>
            </div>
            <div style="float:left;width:130px;text-align:right; padding-top:10px;">
                <asp:ImageButton ID="ImageButton1" runat="server" 
                    ImageUrl="../../images/close24.png" OnclientClick="$find('behavior').hide(); return false;" ToolTip="Fechar Janela" ImageAlign="AbsMiddle" />
                &nbsp;<asp:LinkButton ID="LinkButtonFechar" runat="server" Visible="true" Text="Fechar" OnclientClick="$find('behavior').hide(); return false;"   />
            </div>
          </div>
           <div id="corpoDetalhes" style="border:1px solid #cccccc;height:450px;padding-top:10px;background-color:#f7f7f7">
            
               <table class="Largura100Porcento">
                   <tr>
                       <td class="Largura150">
                           <asp:Label ID="Label9" runat="server" style="font-weight: 700" 
                               Text="Descrição:"></asp:Label>
                       </td>
                       <td>
                           <asp:Label ID="LabelDetalhe1" runat="server"></asp:Label>
                       </td>
                   </tr>
                   <tr>
                       <td class="Largura150">
                           <asp:Label ID="Label6" runat="server" style="font-weight: 700" 
                               Text="Quantidade de Bolsas:"></asp:Label>
                       </td>
                       <td>
                           <asp:Label ID="LabelDetalhe2" runat="server"></asp:Label>
                       </td>
                   </tr>
                   <tr>
                       <td class="Largura150">
                           <asp:Label ID="Label7" runat="server" style="font-weight: 700" 
                               Text="Valor da Bolsa:"></asp:Label>
                       </td>
                       <td>
                           <asp:Label ID="LabelDetalhe3" runat="server"></asp:Label>
                       </td>
                   </tr>
                   <tr>
                       <td class="Largura150">
                           <asp:Label ID="Label10" runat="server" style="font-weight: 700" 
                               Text="Telefone:"></asp:Label>
                       </td>
                       <td>
                           <asp:Label ID="LabelDetalhe4" runat="server"></asp:Label>
                       </td>
                   </tr>
                   <tr>
                       <td class="Largura150">
                           <asp:Label ID="Label11" runat="server" style="font-weight: 700" Text="Site:"></asp:Label>
                       </td>
                       <td>
                           <asp:Label ID="LabelDetalhe5" runat="server"></asp:Label>
                       </td>
                   </tr>
                   <tr>
                       <td class="Largura150">
                           <asp:Label ID="Label12" runat="server" style="font-weight: 700" Text="E-mail:"></asp:Label>
                       </td>
                       <td>
                           <asp:Label ID="LabelDetalhe6" runat="server"></asp:Label>
                       </td>
                   </tr>
                   <tr>
                       <td class="Largura150">
                           <asp:Label ID="Label13" runat="server" style="font-weight: 700" 
                               Text="Responsável:"></asp:Label>
                       </td>
                       <td>
                           <asp:Label ID="LabelDetalhe7" runat="server"></asp:Label>
                       </td>
                   </tr>
               </table>
            
           </div> 


          </div>      
</asp:Panel>



    </div>

  </ContentTemplate>
  </asp:UpdatePanel>


</asp:Content>
