﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;
using ufmt.sig.entity;

namespace ufmt.ssbic.View.webapp.financiador
{
    public partial class UCCadastrarFinanciador : System.Web.UI.UserControl
    {
        public int FinanciadorUID { get; set; }
        public String TipoBolsa { get; set; }
        public double? ValorBolsa { get; set; }
        public int QuantidadeBolsas { get; set; }
        public int QuantidadeRestantes { get; set; }
        public int? CursoUID { get; set; }        

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {


                LoadDdlFinanciador();

            }
        }

        protected void LoadDdlFinanciador()
        {
            FinanciadorBO objBO = new FinanciadorBO();
            ddlFinanciador.DataSource = objBO.GetTipoFinanciador();
            ddlFinanciador.DataTextField = "descricao";
            ddlFinanciador.DataValueField = "financiadorUID";
            ddlFinanciador.DataBound += new EventHandler(IndiceZero);
            ddlFinanciador.DataBind();
        }

        protected void IndiceZero(object sender, EventArgs e)
        {
            DropDownList objDropDownList = (DropDownList)sender; //Cast no sender para DropDownList
            objDropDownList.Items.Insert(0, new ListItem("Selecione","")); //Adiciona um novo Item
        }

        private void ReiniciarCadastro()
        {
            PanelFormulario.Visible = true;        

        }

        protected void rblTipoBolsa_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Remunerado
            if (rblTipoBolsa.SelectedValue == "1")
            {
                txtValor.Enabled = true;
                RequiredFieldValidator5.Enabled = true;
            }
            else
            {//Voluntário
                txtValor.Enabled = false;
                RequiredFieldValidator5.Enabled = false;
            }
        }

        public void PreencherCursos(String nome)
        {
            CursoBO cursoBO = new CursoBO();
            List<Curso> cursos = cursoBO.GetCursos(1, nome);

            GridView1.DataSource = cursos;
            //lblTotalRegistros.Text = registros.Count.ToString() + " registro(s) disponível(is)";
            GridView1.DataBind();
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                hdCursoUID.Value = e.CommandArgument.ToString();
                trtable.Visible = true;

                CursoBO cursoBO = new CursoBO();
                Curso curso = cursoBO.GetCurso(int.Parse(hdCursoUID.Value.ToString()));

                lblnomecurso.Text = curso.Nome;
                pnlCurso.Visible = false;
            }
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void btVincularCurso_Click(object sender, EventArgs e)
        {
            pnlCurso.Visible = true;
        }

        protected void btBuscar_Click(object sender, EventArgs e)
        {
            PreencherCursos(txtNomeCurso.Text);
        }

        protected void GridView1_PageIndexChanging1(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            PreencherCursos(txtNomeCurso.Text);
        }

        protected void btnNaoAplica_Click(object sender, EventArgs e)
        {
            Panel2.Visible = false;
        }

        protected void imgBtnEdit_Click(object sender, ImageClickEventArgs e)
        {
            this.CollapsiblePanelExtender1.Collapsed = false;
            this.CollapsiblePanelExtender1.ClientState = "false";
            txtNovaInstituicao.Text = ddlFinanciador.SelectedItem.Text;
            lblNovaInstituicao.Text = "Alterar Financiador";
            hdd_op_i.Value = "atualizar";
        }

        protected void imgBtnFinanciador_Click(object sender, ImageClickEventArgs e)
        {
            txtNovaInstituicao.Text = String.Empty;
            lblNovaInstituicao.Text = "Nova Instituição";
            hdd_op_i.Value = "cadastrar";
            this.CollapsiblePanelExtender1.Collapsed = false;
            this.CollapsiblePanelExtender1.ClientState = "false";
        }

        protected void btnNovaInstituicao_Click(object sender, EventArgs e)
        {
            int bancoUID = 0;
            String descricao = String.Empty;

            bancoUID = int.Parse(ddBanco.SelectedValue);
            descricao = txtNovaInstituicao.Text;

            FinanciadorBO financiadorBO = new FinanciadorBO();

            Financiador financiador = new Financiador();
            financiador.descricao = descricao;
            financiador.bancoUID = bancoUID;

            if (hdd_op_i.Value.Equals("cadastrar"))
            {
                financiadorBO.Salvar(financiador, 1);
            }
            else
            {
                financiador.financiadorUID = int.Parse(ddlFinanciador.SelectedValue);
                financiadorBO.Salvar(financiador, 2);
            }

            LoadDdlFinanciador();
            this.CollapsiblePanelExtender1.Collapsed = true;
            this.CollapsiblePanelExtender1.ClientState = "true";
        }

        public void SetAtributos()
        {
            FinanciadorUID = int.Parse(ddlFinanciador.SelectedValue);
            
            TipoBolsa = rblTipoBolsa.SelectedValue;
            
            //Remunerado
            if (rblTipoBolsa.SelectedValue == "1")
                ValorBolsa = double.Parse(txtValor.Text);
            else //Voluntário
                ValorBolsa = null;

            QuantidadeBolsas = int.Parse(txtQuantidadeBolsa.Text);
            QuantidadeRestantes = int.Parse(txtQuantidadeBolsa.Text);

            if (!String.IsNullOrEmpty(hdCursoUID.Value))
                CursoUID = int.Parse(hdCursoUID.Value);
            else
                CursoUID = null;
        }


        
    }
}