﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;
using ufmt.ssbic.View.session;
using System.Collections;
using System.Globalization;

namespace ufmt.ssbic.View.webapp.financiador
{
    public partial class FrmListar : VerificaSession
    {
        public String Criterio { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                BolsaBO objBO = new BolsaBO();
                string programaUID = Session["programaUID"].ToString();
                string proreitoria = objBO.GetPrograma(int.Parse(programaUID));
                string area = "Financiador de Bolsas";
                

                lblResultadoConsulta.Text = "<b>Mostrando todos</b>";
                PreecherGridView(Request.QueryString["instrumentoSelecaoUID"].ToString(), txtCriterio.Text, false);
                lnkNovoRegistro.NavigateUrl = "FrmCadastrar.aspx?area=" + area + "&instrumentoSelecaoUID=" + Request.QueryString["instrumentoSelecaoUID"];
                Label1.Text = "Financiador de Bolsas &raquo;" + proreitoria;
            }
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {


            GridView1.PageIndex = e.NewPageIndex;
            PreecherGridView(Request.QueryString["instrumentoSelecaoUID"].ToString(), txtCriterio.Text, false);

        }

        protected void PreecherGridView(String instrumentoSelecaoUID, String criterio, bool visible)
        {

            PanelMsgResultado.Visible = visible;

            FinanciadorBO objBO = new FinanciadorBO();
            List<vwFinanciador> registros = null;


            //Consulta
            if (!String.IsNullOrEmpty(criterio))
            {
                registros = objBO.FindRegistros(int.Parse(instrumentoSelecaoUID), criterio);


            }
            //Listagem padrão
            else
            {
                registros = objBO.GetRegistros(int.Parse(instrumentoSelecaoUID));
            }

            GridView1.DataSource = registros;
            lblTotalRegistros.Text = registros.Count.ToString() + " registro(s) disponível(is)";
            GridView1.DataBind();


            if (registros.Count > 0)
            {

                ImageButton btnPrev = (ImageButton)GridView1.BottomPagerRow.FindControl("cmdPrev");
                if (GridView1.PageIndex == 0)
                    btnPrev.Enabled = false;
                else
                    btnPrev.Enabled = true;

                divExcluir.Visible = true;

            }
            else
            {

                divExcluir.Visible = false;

            }



        }

        protected void cmdIr2_Click(object sender, ImageClickEventArgs e)
        {
            TextBox txt = (TextBox)GridView1.BottomPagerRow.FindControl("txtPagina");
            GridView1.PageIndex = (Convert.ToInt32(txt.Text) - 1);
            PreecherGridView(Request.QueryString["instrumentoSelecaoUID"].ToString(), txtCriterio.Text, false);

        }

        protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                /*
                HyperLink lnkTituloResolucao = (HyperLink)e.Row.FindControl("lnkTituloResolucao");
                HyperLink lnkAtualizarRegistro = (HyperLink)e.Row.FindControl("lnkAtualizarRegistro");
                Label lblAssunto = (Label)e.Row.FindControl("lblAssunto");
               
                lnkAtualizarRegistro.NavigateUrl = "FrmEditar.aspx?registroUID=" + (int?)DataBinder.Eval(e.Row.DataItem, "bolsaUID") + "&proreitoriaUID=" + (int?)DataBinder.Eval(e.Row.DataItem, "proreitoriaUID");
                */

            }



        }

        protected void lnkExcluirSelecionados_Click(object sender, EventArgs e)
        {
            bool resultado = false;
            FinanciadorBO objBO = new FinanciadorBO();

            try
            {

                for (int i = 0; i <= GridView1.Rows.Count - 1; i++)
                {
                    CheckBox chk = new CheckBox();
                    chk = (CheckBox)GridView1.Rows[i].FindControl("chkRegistro");
                    Label lblID = (Label)GridView1.Rows[i].FindControl("lblID");

                    if (chk.Checked)
                    {
                        resultado = objBO.Excluir(int.Parse(lblID.Text));
                    }

                }

                lblMsgResultado.Text = "Registro(s) excluído(s) com sucesso.";
                PanelMsgResultado.CssClass = "success";

            }
            catch (Exception err)
            {

                lblMsgResultado.Text = "Não foi possível excluir o(s) registro(s) selecionado(s).  Por favor, entre em contato com o administrador do sistema.<br />" + err.Message.ToString();
                PanelMsgResultado.CssClass = "error";

            }

            PanelMsgResultado.Visible = true;
            PreecherGridView(Request.QueryString["instrumentoSelecaoUID"].ToString(), txtCriterio.Text, true);
        }


        protected void lnkConsultar_Click(object sender, EventArgs e)
        {

            PreecherGridView(Request.QueryString["instrumentoSelecaoUID"].ToString(), txtCriterio.Text, false);

        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "detalhes")
            {

                FinanciadorBO objBO = new FinanciadorBO();
                vwFinanciador fis = objBO.GetFinanciador(int.Parse(e.CommandArgument.ToString()));
                LabelDetalhe1.Text = fis.descricao;
                LabelDetalhe2.Text = fis.quantidadeBolsas.ToString();
                LabelDetalhe3.Text = String.Format(CultureInfo.GetCultureInfo("pt-BR").NumberFormat, "{0:C2}", fis.valorBolsa);
                LabelDetalhe4.Text = fis.telefone;
                LabelDetalhe5.Text = fis.site;
                LabelDetalhe6.Text = fis.email;
                LabelDetalhe7.Text = fis.responsavel;


                ModalPopupExtender1.Show();
                PanelMsgResultado.Visible = false;
            }


        }

        protected void lnkMostrarTodos_Click(object sender, EventArgs e)
        {
            PreecherGridView(Request.QueryString["instrumentoSelecaoUID"].ToString(), null, false);
            txtCriterio.Text = String.Empty;
        }

        public String VerificarCurso(String nomeCurso)
        {
            String curso = String.Empty;

            if (String.IsNullOrEmpty(nomeCurso))
            {
                curso = "N / A";
            }
            else
            {
                curso = nomeCurso;
            }

            return curso;
        }


    }



}