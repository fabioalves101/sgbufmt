﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;
using ufmt.ssbic.View.session;

namespace ufmt.ssbic.View.webapp.financiador
{
    public partial class FrmEditar : VerificaSession
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                FinanciadorBO objBO = new FinanciadorBO();

                string proreitoriaUID = Session["programaUID"].ToString();

                string area = Request.QueryString["area"];
                lblTitulo.Text = area;
                string registroUID = Request.QueryString["registroUID"];
                ddlFinanciador.DataSource = objBO.GetTipoFinanciador();
                ddlFinanciador.DataTextField = "descricao";
                ddlFinanciador.DataValueField = "financiadorUID";
                ddlFinanciador.DataBound += new EventHandler(IndiceZero);
                ddlFinanciador.DataBind();

                vwFinanciador fin = objBO.GetFinanciador(int.Parse(registroUID));
                ddlFinanciador.SelectedValue = fin.financiadorUID.ToString();
                txtQuantidadeBolsa.Text = fin.quantidadeBolsas.ToString();
                txtValor.Text = fin.valorBolsa.ToString();

                ViewState["quantidadeRestantes"] = fin.quantidadeRestantes.ToString();

                if (fin.tipoOferta == 1)
                {
                    rblTipoBolsa.Items[0].Selected = true;
                    txtValor.Enabled = true;
                    RequiredFieldValidator5.Enabled = true;
                }
                else
                {
                    rblTipoBolsa.Items[1].Selected = true;
                    txtValor.Enabled = false;
                    RequiredFieldValidator5.Enabled = false;
                }

                hddCodigoRegistro.Value = fin.financiadorInstrumentoSelecaoUID.ToString();

            }

        }

        protected void IndiceZero(object sender, EventArgs e)
        {
            DropDownList objDropDownList = (DropDownList)sender; //Cast no sender para DropDownList
            objDropDownList.Items.Insert(0, new ListItem("Selecione", "")); //Adiciona um novo Item
        }
        protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                FinanciadorBO objBO = new FinanciadorBO();
                FinanciadorInstrumentoSelecao fin = new FinanciadorInstrumentoSelecao();
                fin.financiadorInstrumentoSelecaoUID = int.Parse(hddCodigoRegistro.Value);
                fin.instrumentoSelecaoUID = int.Parse(Request.QueryString["instrumentoSelecaoUID"].ToString());
                fin.financiadorUID = int.Parse(ddlFinanciador.SelectedValue);
                fin.quantidadeBolsas = int.Parse(txtQuantidadeBolsa.Text);
                fin.quantidadeRestantes = int.Parse(ViewState["quantidadeRestantes"].ToString());

                //TODO: controlar o total disponível para novo valor de número de bolsas
                //if (int.Parse(txtQuantidadeBolsa.Text) >= int.Parse(ViewState["quantidadeRestantes"].ToString()))
                //{
                //    fin.quantidadeRestantes = int.Parse(ViewState["quantidadeRestantes"].ToString());
                //}
                //else {

                //    fin.quantidadeRestantes = int.Parse(ViewState["quantidadeRestantes"].ToString());
                //}


                //Remunerado
                if (rblTipoBolsa.SelectedValue == "1")
                    fin.valorBolsa = double.Parse(txtValor.Text);
                else //Voluntário
                    fin.valorBolsa = null;

                fin.tipoOferta = int.Parse(rblTipoBolsa.SelectedItem.Value);
                objBO.Salvar(fin, 0);
                lblMsgResultado.Text = "Registro atualizado com sucesso.";
                PanelMsgResultado.CssClass = "success";
                PanelFormulario.Visible = false;
                btnSalvar.Visible = false;

            }
            catch (Exception err)
            {

                lblMsgResultado.Text = "Não foi possível efetuar a operação.  Por favor, entre em contato com o administrador do sistema.<br />" + err.Message.ToString();
                PanelMsgResultado.CssClass = "error";
            }

            PanelMsgResultado.Visible = true;
        }

        protected void btnVoltar_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("FrmListar.aspx?instrumentoSelecaoUID=" + Request.QueryString["instrumentoSelecaoUID"].ToString());
        }

        protected void rblTipoBolsa_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Remunerado
            if (rblTipoBolsa.SelectedValue == "1")
            {
                txtValor.Enabled = true;
                RequiredFieldValidator5.Enabled = true;
            }
            else
            {//Voluntário
                txtValor.Enabled = false;
                RequiredFieldValidator5.Enabled = false;
                txtValor.Text = String.Empty;
            }
        }
    }
}