﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.View.session;
using ufmt.ssbic.View.controles;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.View.webapp.cursosprocessoseletivo
{
    public partial class FrmNovo : VerificaSession
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btVagasCurso_Click(object sender, EventArgs e)
        {

            if (UCVagasCurso1.getRows() > 0)
            {

                int processoSeletivoUID = int.Parse(Request.QueryString["processoSeletivoUID"]);

                List<CursoQtdeVagas> vagasPorCurso = (List<CursoQtdeVagas>)Session["vagasPorCurso"];

                CursoProcessoSeletivoBO cursoProcBO = new CursoProcessoSeletivoBO();

                List<CursoProcessoSeletivo> listaCursosProc = new List<CursoProcessoSeletivo>();

                foreach (ufmt.ssbic.View.controles.CursoQtdeVagas cursoVagas in vagasPorCurso)
                {
                    CursoProcessoSeletivo cursoProc = new CursoProcessoSeletivo();
                    cursoProc.cursoUID = cursoVagas.curso.CursoUID;
                    cursoProc.processoSeletivoUID = processoSeletivoUID;
                    cursoProc.vagas = cursoVagas.qtdeVagas;

                    listaCursosProc.Add(cursoProc);
                }

                cursoProcBO.SalvarVagasPorCurso(listaCursosProc);

                Session["vagasPorCurso"] = null;

                Response.Redirect("FrmListar.aspx?processoSeletivoUID=" + processoSeletivoUID);
            }
        }
    }
}