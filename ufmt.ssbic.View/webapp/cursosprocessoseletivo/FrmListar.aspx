﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true" CodeBehind="FrmListar.aspx.cs" Inherits="ufmt.ssbic.View.webapp.cursosprocessoseletivo.FrmListar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<h1><span class="allcaps">Vagas por curso</span></h1>
<div id="containerMensagens">
                <asp:Panel ID="PanelMsgResultado" runat="server" Visible="false">
                    <asp:Label ID="lblMsgResultado" runat="server"></asp:Label>
                </asp:Panel>
        </div>
 <div class="addbutton" style="margin-right:5px">
     <asp:LinkButton ID="lnkNovo" runat="server" onclick="lnkNovo_Click" CssClass="minibutton">
    <span>
     <img alt="" src="../../images/add.png" border="0" />&nbsp;&nbsp; Adicionar Registro
     </span>
     </asp:LinkButton>
 </div>    
    <asp:GridView ID="grdCursosVagas" runat="server" AutoGenerateColumns="False" Width="900px"
        DataSourceID="ObjectDataSource1" onrowcommand="grdCursosVagas_RowCommand">
        <Columns>
            <asp:BoundField DataField="nome" HeaderText="Curso" SortExpression="nome" />
           
           <asp:BoundField DataField="campus" ItemStyle-Width="340" HeaderText="Campus" SortExpression="campus" />
           
            <asp:BoundField ItemStyle-Width="30px" DataField="vagas" HeaderText="Vagas" ItemStyle-HorizontalAlign="Center"
                SortExpression="vagas" >
          
<ItemStyle Width="30px"></ItemStyle>
            </asp:BoundField>
            <asp:TemplateField HeaderText="Excluir" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:LinkButton CommandArgument='<%# Eval("cursoProcessoSeletivoUID") %>' CommandName="excluir" ID="lnkExcluir" runat="server">Excluir</asp:LinkButton>
                </ItemTemplate>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
            </asp:TemplateField>
          
        </Columns>
    </asp:GridView>



    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
        SelectMethod="GetCursosProcessoSeletivo" 
        TypeName="ufmt.ssbic.Business.CursoProcessoSeletivoBO">
        <SelectParameters>
            <asp:QueryStringParameter Name="processoSeletivoUID" 
                QueryStringField="processoSeletivoUID" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

</asp:Content>
