﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.View.session;
using ufmt.ssbic.Business;

namespace ufmt.ssbic.View.webapp.cursosprocessoseletivo
{
    public partial class FrmListar : VerificaSession
    {
        int processoSeletivoUID;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void lnkNovo_Click(object sender, EventArgs e)
        {
            Response.Redirect("FrmNovo.aspx?processoSeletivoUID=" + GetProcessoSeletivoUID());
        }

        protected void grdCursosVagas_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            PanelMsgResultado.Visible = true;
            try
            {
                if (e.CommandName == "excluir")
                {
                    int cursoProcessoSeletivoUID = int.Parse(e.CommandArgument.ToString());
                    CursoProcessoSeletivoBO cursoProcBO = new CursoProcessoSeletivoBO();
                    cursoProcBO.Excluir(cursoProcessoSeletivoUID);
                    grdCursosVagas.DataBind();
                    lblMsgResultado.Text = "Registro excluído com sucesso";
                }
            } catch(Exception ex) {
                lblMsgResultado.Text = "Houve um erro ao excluir o registro. "+ex.Message;
            }
        }

        protected int GetProcessoSeletivoUID()
        {
            return int.Parse(Request.QueryString["processoSeletivoUID"]);
        }
    }
}