﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.View.session;
using ufmt.ssbic.DataAccess;
using ufmt.ssbic.Business;

namespace ufmt.ssbic.View.webapp.auxilio
{
    public partial class FrmNovoBolsista : VerificaSession
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            Aluno aluno = UCBuscarAluno1.GetAluno();
            int folhaSuplementarUID = int.Parse(Request.QueryString["folhaSuplementarUID"]);
            if (!this.AlunoCadastrado(aluno, folhaSuplementarUID))
            {
                Bolsista bolsista = null;

                decimal valor = 0;
                if (decimal.TryParse(txtValor.Text, out valor))
                {
                    if (rdoImportaDados.SelectedValue.Equals("1"))
                    {
                        int? bancoUID = null;
                        if (!String.IsNullOrEmpty(aluno.CodigoFebrabram))
                        {
                            Banco banco = new BancoBO().GetBanco(aluno.CodigoFebrabram);
                            bancoUID = banco.bancoUID;
                        }

                        bolsista = new Bolsista()
                        {
                            cpf = aluno.CPF,
                            email = aluno.Email,
                            registroAluno = aluno.Matricula.ToString(),
                            bancoUID = bancoUID,
                            agencia = aluno.Agencia,
                            numeroConta = aluno.ContaCorrente
                        };
                    }
                    else
                    {
                        int bancoUID = int.Parse(ddlBanco.SelectedValue);
                        string agencia = txtAgencia.Text;
                        string conta = txtConta.Text;

                        bolsista = new Bolsista()
                        {
                            cpf = aluno.CPF,
                            email = aluno.Email,
                            registroAluno = aluno.Matricula.ToString(),
                            bancoUID = bancoUID,
                            agencia = agencia,
                            numeroConta = conta
                        };
                    }

                    SuplementarController scontroller = new SuplementarController();
                    scontroller.CadastrarAluno(bolsista, folhaSuplementarUID, valor);
                    Response.Redirect("FrmBolsistas.aspx?folhaSuplementarUID=" + folhaSuplementarUID);
                }
                else
                {
                    PanelMsgResultado.Visible = true;
                    lblMsgResultado.Text = "O valor precisa ser informado";
                }
                
            }
            else
            {
                PanelMsgResultado.Visible = true;
                lblMsgResultado.Text = "O Aluno selecionado já possui dados cadastrados";
            }
        }

        private bool AlunoCadastrado(Aluno aluno, int folhaSuplementarUID)
        {
            SuplementarController sc = new SuplementarController();
            BolsistaFolhaSuplementar bfs = sc.GetAlunoEmFolha(aluno.CPF, folhaSuplementarUID);
            if (bfs != null && bfs.ativo == true)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        private bool ExisteAuxiliado(Aluno aluno, int folhaPagamentoUID)
        {
            AuxilioController ac = new AuxilioController();
            AuxilioFolhaPagamento auxilioFP = ac.GetAuxilioFolhaPagamento(aluno.CPF, folhaPagamentoUID);
            if (auxilioFP != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected void rdoImportaDados_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdoImportaDados.SelectedValue.Equals("2"))
            {
                pnlDadosBancarios.Visible = true;
            }
            else
            {
                pnlDadosBancarios.Visible = false;
            }
        }
    }
}