﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.sig.entity;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.View.webapp
{
    public partial class UCGetServidor : System.Web.UI.UserControl
    {
        public Servidor servidor;
        private BolsistaBO bolsistaBO;
        private ResponsavelBO responsavelBO;

        public UCGetServidor()
        {
            this.bolsistaBO = new BolsistaBO();
            this.responsavelBO = new ResponsavelBO();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InfoServidorResponsavel();
            }
        }

        public void InfoServidorResponsavel()
        {
            try
            {
                Servidor servidorResponsavel = GetServidorResponsavel();
                lblnomeatual.Text = servidorResponsavel.Pessoa.Nome;
                lblcpfatual.Text = servidorResponsavel.Pessoa.Cpf;
                lblmatriculaatual.Text = servidorResponsavel.Registro;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('" + msg + "');", true);                
            }
        }

        protected void PreencherGridview(String criterio, String tipoCriterio, bool visible)
        {
            PessoaBO pessoaBO = new PessoaBO();
            IList<Servidor> servidores = pessoaBO.GetPessoas(criterio, tipoCriterio);

            gdrListaServidores.DataSource = servidores;
            pnlListaServidores.Visible = visible;
            gdrListaServidores.DataBind();
            
        }

        public Servidor GetServidorResponsavel()
        {
            try
            {
                Servidor servidorResponsavel = null;

                Bolsista bolsista = this.bolsistaBO.GetBolsistaPorId(int.Parse(Request.QueryString["bolsistaUID"]));
                int? responsavelUID = bolsista.BolsistaProcessoSeletivo.FirstOrDefault<BolsistaProcessoSeletivo>().responsavelUID;

                if (responsavelUID != null)
                {
                    
                    Responsavel responsavel = this.responsavelBO.GetResponsavel(responsavelUID.Value);

                    AutenticacaoBO authBO = new AutenticacaoBO();
                    servidorResponsavel = authBO.GetServidor(responsavel.matricula);
                    hdrespatual.Value = responsavelUID.ToString();
                }
                else
                {
                    throw new Exception("Não foi possível encontrar responsável por bolsista");
                }

                return servidorResponsavel;
            }
            catch (Exception ex)
            {
                
                throw new Exception("Não foi possível encontrar responsável por bolsista", ex);
            }
        }


        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            PreencherGridview(txtBuscaCoordenador.Text, ddlBuscaCoordenador.SelectedValue, true);
        }

        protected void gdrListaServidores_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridView gridview = (GridView)sender;
            Int64 servidorUID = int.Parse(gridview.SelectedValue.ToString());

            PessoaBO pessoaB = new PessoaBO();
            Pessoa pessoa = pessoaB.GetPessoa(servidorUID);
            servidor = pessoaB.GetServidor(pessoa.PessoaUID);

            lblnome.Text = pessoa.Nome;
            lblcpf.Text = pessoa.Cpf;
            lblmatricula.Text = servidor.Registro;

            hdrespnovo.Value = servidor.ServidorUID.ToString();

            pnlInfoServidor.Visible = true;
            pnlListaServidores.Visible = false;
        }

        protected void btConfirmar_Click(object sender, EventArgs e)
        {            
            string msg = string.Empty;
            try
            {
                AutenticacaoBO authBO = new AutenticacaoBO();
                Servidor servidor = authBO.GetServidor(long.Parse(hdrespnovo.Value));

                Responsavel responsavel = new Responsavel();
                responsavel.matricula = servidor.Registro;
                responsavel.telefone = servidor.Pessoa.TelefonePrimario;
                responsavel.email = servidor.Pessoa.Mail;
                responsavel.responsavelUID = int.Parse(hdrespatual.Value);
                this.responsavelBO.Atualizar(responsavel);

                pnlInfoServidor.Visible = false;
                this.InfoServidorResponsavel();
                PanelMsgResultadoUC.Visible = true;
                msg = "Responsável atualizado com sucesso.";
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('" + msg + "');", true);
        }
    }
}