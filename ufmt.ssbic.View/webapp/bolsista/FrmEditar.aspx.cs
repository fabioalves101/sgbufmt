﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.View.session;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;

namespace ufmt.ssbic.View.webapp.bolsista
{
    public partial class FrmEditar : VerificaSession
    {
        private BolsistaBO bolsistaBO;
        private AlunoBO alunoBO;
        public FrmEditar()
        {
            this.bolsistaBO = new BolsistaBO();
            this.alunoBO = new AlunoBO();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Session["permissao"].ToString().Contains("MONITORIA"))
            {
                if (!IsPostBack)
                {
                    Bolsista bolsista = this.bolsistaBO.GetBolsistaPorId(int.Parse(Request.QueryString["bolsistaUID"]));
                    vwAlunoSiga aluno = this.alunoBO.GetAluno(decimal.Parse(bolsista.registroAluno));

                    lblTitulo.Text = aluno.Nome;

                    this.GetBolsistaProcessoSeletivo();
                }
            }
            else
            {
                TabContainer1.Visible = false;
                pnlMonitoria.Visible = true;

                HiddenField hdUrlRedirect = (HiddenField) UCDadosMonitor1.FindControl("hdUrlRedirect");
                hdUrlRedirect.Value = "../bolsista/FrmListar.aspx?folhaPagamentoUID=" + 
                                        int.Parse(Request.QueryString["folhaPagamentoUID"]);
            }
        }

        protected void GetBolsistaProcessoSeletivo()
        {
            BolsistaProcessoSeletivoBO bolsistabpsBO = new BolsistaProcessoSeletivoBO();
            int bolsistaUID = int.Parse(Request.QueryString["bolsistaUID"]);
            int processoSeletivoUID = int.Parse(Request.QueryString["processoSeletivoUID"]);
            int folhaPagamentoUID = int.Parse(Request.QueryString["folhaPagamentoUID"]);
            
            BolsistaProcessoSeletivo bps = bolsistabpsBO.GetBolsistaProcessoSeletivo(bolsistaUID, processoSeletivoUID);


            BolsistaController controller = new BolsistaController();            
            BolsistaFolhaPagamento bfp = controller.GetBolsistaPorFolhaPagamento(bolsistaUID, folhaPagamentoUID);

            string situacaoBolsista = "ATIVO";
            if (!bfp.ativo)
            {
                situacaoBolsista = "INATIVO";
                rdoSituacaoBolsista.SelectedValue = "0";
            }

            lblSituacaoBolsista.Text = "O bolsista encontra-se " + situacaoBolsista;

            TextJustificativaSituacao.Text = bfp.justificativaSituacao;            

        }

        protected void btConfirmar_Click(object sender, EventArgs e)
        {
            string msg = string.Empty;
            try
            {
                int bolsistaUID = int.Parse(Request.QueryString["bolsistaUID"]);
                int processoSeletivoUID = int.Parse(Request.QueryString["processoSeletivoUID"]);
                int folhaPagamentoUID = int.Parse(Request.QueryString["folhaPagamentoUID"]);
                int situacao = int.Parse(rdoSituacaoBolsista.SelectedValue);

                BolsistaController controller = new BolsistaController();
                controller.AtualizarSituacaoBolsista(bolsistaUID, folhaPagamentoUID, situacao, TextJustificativaSituacao.Text);

                this.GetBolsistaProcessoSeletivo();
                pnlSituacaoBolsista.Visible = false;

                msg = "Situação atualizada com sucesso.";
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('" + msg + "');", true);
        }

        protected void lnkAlterarSituacaoBolsista_Click(object sender, EventArgs e)
        {
            pnlSituacaoBolsista.Visible = true;
        }

        protected void lnkVoltar_Click(object sender, EventArgs e)
        {
            Response.Redirect("FrmListar.aspx?folhaPagamentoUID=" + Request.QueryString["folhaPagamentoUID"]);
        }
    }
}