﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;
using ufmt.ssbic.View.session;

namespace ufmt.ssbic.View.webapp.bolsista
{
    public partial class FrmEditarBolsita : VerificaSession
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                ProcessoSeletivoBO objBO = new ProcessoSeletivoBO();

                string proreitoriaUID = Session["programaUID"].ToString();
                string area = Request.QueryString["area"];
                lblTitulo.Text = area;
                string registroUID = Request.QueryString["bolsistaUID"];

            }

        }

        protected void IndiceZero(object sender, EventArgs e)
        {
            DropDownList objDropDownList = (DropDownList)sender; //Cast no sender para DropDownList
            objDropDownList.Items.Insert(0, new ListItem("Selecione", "")); //Adiciona um novo Item
        }

    }
}