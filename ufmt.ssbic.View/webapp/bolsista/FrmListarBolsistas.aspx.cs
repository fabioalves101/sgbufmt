﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.Business;
using ufmt.ssbic.DataAccess;
using ufmt.ssbic.View.session;
using System.Collections;
using ufmt.sig.entity;

namespace ufmt.ssbic.View.webapp.bolsista
{
    public partial class FrmListarBolsistas : VerificaSession
    {
        public int? ProReitoriaUID { get; set; }
        public String Criterio { get; set; }
        public int? ProcessoSeletivoUID { get; set; }
        public FolhaPagamento folhaPagamento { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                new BolsistaController().AtualizarVinculoInstitucionalBolsistas(
                    int.Parse(Request.QueryString["folhaPagamentoUID"]));

                BolsaBO objBO = new BolsaBO();
                string programaUID = Session["programaUID"].ToString();
                string proreitoria = objBO.GetPrograma(int.Parse(programaUID));
                
                lblResultadoConsulta.Text = "<b>Mostrando todos</b>";

                FolhaPagamentoBO folhaBO = new FolhaPagamentoBO();
                FolhaPagamento folha = folhaBO.GetFolhaPagamento(int.Parse(Request.QueryString["folhaPagamentoUID"]));

                this.ProcessoSeletivoUID = folha.processoSeletivoUID;
                hdProcessoSeletivoUID.Value = folha.processoSeletivoUID.ToString();
                this.folhaPagamento = folha;

                MostrarContadores(folha);
                
                
                lbl1.Text = "Administração dos bolsistas do Instrumento de Seleção &raquo;" + folha.ProcessoSeletivo.InstrumentoSelecao.descricao;
                hpetapas.NavigateUrl = "~/webapp/folha/FrmListar.aspx?processoSeletivoUID=" + folha.processoSeletivoUID;

                VerificarPermissoesModulos();
            }
        }

        private void MostrarContadores(FolhaPagamento folha)
        {
            int ativos = 0;
            int inativos = 0;
            foreach (BolsistaFolhaPagamento bfp in folha.BolsistaFolhaPagamento.ToList())
            {
                if (bfp.ativo)
                    ativos++;
                else
                    inativos++;
            }

            lblBolsistasAtivos.Text = ativos.ToString();
            lblBolsistasInativos.Text = inativos.ToString();


            lblBolsistasPgtoRecusado.Text =
                (from bfp in folha.BolsistaFolhaPagamento where bfp.pagamentoRecusado == true select bfp)
                    .ToList()
                    .Count.ToString();


            lblPeriodoFolha.Text = folha.dataInicio.Value.ToShortDateString() +
                " até " + folha.dataFim.Value.ToShortDateString();
        }

        public List<vwBolsistasFolhaPagamento> PreecherGridView(String proreitoriaUID, int folhaPagamentoUID, String nomeBusca)
        {
            
            if (!String.IsNullOrEmpty(proreitoriaUID))
                this.ProReitoriaUID = int.Parse(proreitoriaUID);
            else
                this.ProReitoriaUID = null;

            BolsistaBO objBO = new BolsistaBO();
            List<vwBolsistasFolhaPagamento> registros = null;

            FolhaPagamentoBO folhaBO = new FolhaPagamentoBO();
            FolhaPagamento folha = folhaBO.GetFolhaPagamento(folhaPagamentoUID);

            registros = objBO.GetBolsistasFolhaPagamento(folhaPagamentoUID, nomeBusca);
            

            return registros;
        }


        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "pagamento")
            {
                BolsistaBO objBO = new BolsistaBO();
                objBO.AtualizarPagamento(int.Parse(e.CommandArgument.ToString()), int.Parse(Request.QueryString["folhaPagamentoUID"]));

                FolhaPagamentoBO folhaBO = new FolhaPagamentoBO();
                FolhaPagamento folha = folhaBO.GetFolhaPagamento(int.Parse(Request.QueryString["folhaPagamentoUID"]));

                this.ProcessoSeletivoUID = folha.processoSeletivoUID;
                                
                GdrEditais.DataBind();
            }

            if (e.CommandName == "recusarpagamento")
            {

                mdlPanelRecusa.Show();
                hdbolsistaUID.Value = e.CommandArgument.ToString();
                /*
                BolsistaBO objBO = new BolsistaBO();
                objBO.RecusarPagamento(int.Parse(e.CommandArgument.ToString()), int.Parse(Request.QueryString["folhaPagamentoUID"]));

                FolhaPagamentoBO folhaBO = new FolhaPagamentoBO();
                FolhaPagamento folha = folhaBO.GetFolhaPagamento(int.Parse(Request.QueryString["folhaPagamentoUID"]));

                this.ProcessoSeletivoUID = folha.processoSeletivoUID;

                PreecherGridView(Session["programaUID"].ToString(), int.Parse(Request.QueryString["folhaPagamentoUID"]), false);
                GdrEditais.DataBind();
                 */
            }
        }

      



        protected void lnkConsultar_Click(object sender, EventArgs e)
        {
            GdrEditais.DataBind();
        }

        protected void lnkMostrarTodos_Click(object sender, EventArgs e)
        {
            //   PreecherGridView(Session["programaUID"].ToString(), null, false);
        }

        public String VerificarSituacao(int bolsistaUID)
        {
            String situacao;

            BolsistaController controller = new BolsistaController();

            int folhaPagamentoUID = int.Parse(Request.QueryString["folhaPagamentoUID"]);

            bool ativo = controller.VerificarSituacaoBolsistaFolhaPagamento(bolsistaUID, folhaPagamentoUID);

            if (ativo)
                situacao = "Ativo";
            else
                situacao = "Inativo";

            return situacao;
        }

        protected void lnkNovoBolsista_Click(object sender, EventArgs e)
        {
            int folhaPagamentoUID = int.Parse(Request.QueryString["folhaPagamentoUID"]);

            FolhaPagamentoBO folhaBO = new FolhaPagamentoBO();
            FolhaPagamento folha = folhaBO.GetFolhaPagamento(folhaPagamentoUID);

            Response.Redirect("../processoSeletivo/FrmProcessoSeletivo.aspx?processoSeletivoUID=" + folha.processoSeletivoUID + "&folhaPagamentoUID=" + folhaPagamentoUID.ToString());
        }

        public String VerificarPermissaoAutorizacaoPagamento(Boolean pagamentoAutorizado)
        {
            String texto = "";
                        
            Session["administracao"] = "true";
            if (Session["permissao"].ToString().Contains("ADMINISTRADOR"))
            {
                texto = "";
            }
            else if (Session["permissao"].ToString().Contains("COORDENADOR"))
            {
                texto = "";
            }
            else
            { //FINANCEIRO
                if(!pagamentoAutorizado)
                    texto = "| Autorizar Pagamento";
            }

            return texto;
        }

        public String VerificarPermissaoRecusarPagamento(Boolean pagamentoRecusado)
        {
            String texto = "";

            Session["administracao"] = "true";
            if (Session["permissao"].ToString().Contains("ADMINISTRADOR"))
            {
                texto = "";
            }
            else if (Session["permissao"].ToString().Contains("COORDENADOR"))
            {
                texto = "";
            }
            else
            { //FINANCEIRO
                if (!pagamentoRecusado)
                {
                    texto = "| Pagamento Não Realizado";
                }
                else
                {
                    texto = "";                    
                }

            }

            return texto;
        }

        protected void VerificarPermissoesModulos()
        {
            Session["administracao"] = "true";
            if (Session["permissao"].ToString().Contains("ADMINISTRADOR"))
            {//ADMINISTRAÇÃO
                pnlAcoesCoordenador.Visible = false;

                if(this.folhaPagamento.etapa1 == 3 && this.folhaPagamento.etapa2 == 1)
                    PnlHomologacao.Visible = true;
            }
            else if (Session["permissao"].ToString().Contains("COORDENADOR"))
            {//COORDENADOR
                    if (this.folhaPagamento.etapa1 == 1)
                        pnlAcoesCoordenador.Visible = true;                
            }
            else
            { //FINANCEIRO
                lnkNovoBolsista.Visible = false;
                PnlHomologacao.Visible = false;
                pnlFinalizarPagamento.Visible = true;
            }
        }

        protected void btRecusar_Click(object sender, EventArgs e)
        {
            PnlParecer.Visible = true;
        }

        protected void btAceitar_Click(object sender, EventArgs e)
        {
            int folhaPagamentoUID = int.Parse(Request.QueryString["folhaPagamentoUID"]);

            FolhaPagamentoBO folhaBO = new FolhaPagamentoBO();
            folhaBO.AtualizarFolhaPagamento(folhaPagamentoUID, null, 3, null, null, null, false);

            FolhaPagamento folha = folhaBO.GetFolhaPagamento(folhaPagamentoUID);
            Response.Redirect("../folha/FrmListar.aspx?processoSeletivoUID=" + folha.processoSeletivoUID);
        }

        protected void btSalvar_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtParecer.Text))
            {
                int folhaPagamentoUID = int.Parse(Request.QueryString["folhaPagamentoUID"]);

                FolhaPagamentoBO folhaBO = new FolhaPagamentoBO();
                folhaBO.AtualizarFolhaPagamento(folhaPagamentoUID, null, 2, null, txtParecer.Text, null, false);
                                
                FolhaPagamento folha = folhaBO.GetFolhaPagamento(folhaPagamentoUID);
                Response.Redirect("../folha/FrmListar.aspx?processoSeletivoUID="+folha.processoSeletivoUID);
            }
        }

        protected void lnkFecharEtapa_Click(object sender, EventArgs e)
        {
            int folhaPagamentoUID = int.Parse(Request.QueryString["folhaPagamentoUID"]);
            FolhaPagamentoBO folhaBO = new FolhaPagamentoBO();
            folhaBO.AtualizarFolhaPagamento(folhaPagamentoUID, 3, 1, null, String.Empty, String.Empty, false);

            ScriptManager.RegisterStartupScript(Page, Page.GetType(),
                        Guid.NewGuid().ToString(),
                        "alert('Etapa finalizada com sucesso.');", true);

            pnlAcoesCoordenador.Visible = false;
            GdrEditais.DataBind();
        }

        public String LinkEditarBolsista(int bolsistaUID)
        {
            return "../bolsista/FrmEditar.aspx?bolsistaUID=" + bolsistaUID +
                    "&processoSeletivoUID=" + hdProcessoSeletivoUID.Value +
                    "&folhaPagamentoUID=" + Request.QueryString["folhaPagamentoUID"];
        }

        protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
        {             
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Object obj = DataBinder.Eval(e.Row.DataItem, "PagamentoRecusado");
                if(obj != null)
                {
                    bool pagamentoRecusado = (bool)obj;
                
                    if (pagamentoRecusado)
                    {
                        System.Drawing.Color color = System.Drawing.ColorTranslator.FromHtml(@"#FFC1C1");
                        
                        e.Row.BackColor = color;
                    }
                }

                Object obj2 = DataBinder.Eval(e.Row.DataItem, "PagamentoAutorizado");
                if (obj2 != null)
                {
                    bool pagamentoAprovado = (bool)obj2;

                    if (pagamentoAprovado)
                    {
                        System.Drawing.Color color = System.Drawing.ColorTranslator.FromHtml(@"#ADD8E6");

                        e.Row.BackColor = color;
                    }
                }

                Object obj3 = DataBinder.Eval(e.Row.DataItem, "Ativo");
                if (obj3 != null)
                {
                    bool ativo = (bool)obj3;

                    if (!ativo)
                    {
                        System.Drawing.Color color = System.Drawing.ColorTranslator.FromHtml(@"#F3F781");
                        e.Row.BackColor = color;
                    }

                }

            }        
        }

        protected void salvarParecer_Click(object sender, EventArgs e)
        {
            BolsistaBO objBO = new BolsistaBO();
            objBO.RecusarPagamento(int.Parse(hdbolsistaUID.Value), int.Parse(Request.QueryString["folhaPagamentoUID"]), txtParecerRecusa.Text);

            FolhaPagamentoBO folhaBO = new FolhaPagamentoBO();
            FolhaPagamento folha = folhaBO.GetFolhaPagamento(int.Parse(Request.QueryString["folhaPagamentoUID"]));

            this.ProcessoSeletivoUID = folha.processoSeletivoUID;
                        
            GdrEditais.DataBind();
        }

        protected void btGerarFolhaSuplementar_Click(object sender, EventArgs e)
        {
            int folhaPagamentoUID = int.Parse(Request.QueryString["folhaPagamentoUID"]);
            FolhaPagamentoBO folhaBO = new FolhaPagamentoBO();

            FolhaPagamento folha = folhaBO.GetFolhaPagamento(folhaPagamentoUID);

            folhaBO.AtualizarFolhaPagamento(folhaPagamentoUID, folha.etapa1, folha.etapa2, folha.etapa3, 
                folha.parecerE2, folha.parecerE3, true);
            
            Response.Redirect("FrmListar.aspx?folhaPagamentoUID=" + folhaPagamentoUID);
        }


        public String GetParecer(int bolsistaUID)
        {
            BolsistaController controller = new BolsistaController();
            BolsistaFolhaPagamento bfp = controller.GetBolsistaFolhaPagamento(bolsistaUID, 
                            int.Parse(Request.QueryString["folhaPagamentoUID"]));

            return bfp.parecer;
        }


    }

}