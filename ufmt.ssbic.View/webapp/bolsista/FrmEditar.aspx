﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webapp/MasterPage.Master" AutoEventWireup="true" CodeBehind="FrmEditar.aspx.cs" Inherits="ufmt.ssbic.View.webapp.bolsista.FrmEditar" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register src="../UCGetServidor.ascx" tagname="UCGetServidor" tagprefix="uc1" %>

<%@ Register src="UCDadosAluno.ascx" tagname="UCDadosAluno" tagprefix="uc2" %>

<%@ Register src="../../controles/UCDadosMonitor.ascx" tagname="UCDadosMonitor" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

  <h1>&nbsp;Editar Registro &raquo; 
        <asp:Label ID="lblTitulo" runat="server"></asp:Label>
        <asp:HiddenField ID="hddCodigoRegistro" runat="server" />
    </h1>
    <p>
    <asp:LinkButton ID="lnkVoltar" runat="server" onclick="lnkVoltar_Click">Voltar para a lista de bolsistas</asp:LinkButton>
    </p>
        <div id="containerMensagens">
                <asp:Panel ID="PanelMsgResultado" runat="server" Visible="false">
                    <asp:Label ID="lblMsgResultado" runat="server"></asp:Label>
                </asp:Panel>
        </div>

    <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" 
        Width="100%">
        <asp:TabPanel runat="server" HeaderText="Situação" ID="TabPanel1">
            <ContentTemplate><asp:Panel ID="Panel2" runat="server">
                <fieldset>
                    <legend><strong>Situação Atual</strong></legend>
                    <asp:Label ID="lblSituacaoBolsista" runat="server"></asp:Label>
                </fieldset>
                <fieldset>
                    <legend><strong>
                    <asp:LinkButton ID="lnkAlterarSituacaoBolsista" runat="server" 
                                onclick="lnkAlterarSituacaoBolsista_Click">Alterar Situação do Bolsista</asp:LinkButton>
                    </strong></legend>
                    
                    <asp:Panel ID="pnlSituacaoBolsista" runat="server" Visible="False">
                    <p>
                    <asp:RadioButtonList ID="rdoSituacaoBolsista" runat="server">
                        <asp:ListItem Selected="True" Value="1">Ativo</asp:ListItem>
                        <asp:ListItem Value="0">Inativo</asp:ListItem>
                    </asp:RadioButtonList>
                    </p>
                    <p>
                        Justifique a alteração da situação: <br />
                        <asp:TextBox ID="TextJustificativaSituacao" runat="server" TextMode="MultiLine" 
                            Height="69px" Width="424px"></asp:TextBox>
                    </p>
                    <p>
                        <asp:Button ID="btConfirmar" runat="server" Text="Confirmar Alteração" 
                                onclick="btConfirmar_Click" />
                    </p>
                    </asp:Panel>
               </fieldset>
                
               </asp:Panel>
            </ContentTemplate>
        </asp:TabPanel>

        <asp:TabPanel runat="server" HeaderText="Responsável" ID="responsavel"><ContentTemplate><asp:Panel ID="Panel1" runat="server"><uc1:UCGetServidor ID="UCGetServidor1" runat="server" /></asp:Panel></ContentTemplate></asp:TabPanel>

        <asp:TabPanel ID="infobanco" runat="server" HeaderText="Dados do Aluno / Informações Bancárias"><ContentTemplate><asp:Panel ID="Panel3" runat="server">
        <uc2:UCDadosAluno ID="UCDadosAluno2" runat="server" /></asp:Panel></ContentTemplate></asp:TabPanel>

    </asp:TabContainer>
        <asp:Panel ID="pnlMonitoria" Visible="false" runat="server">

            <uc3:UCDadosMonitor ID="UCDadosMonitor1" runat="server" />

        </asp:Panel>
    
</asp:Content>
