﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.DataAccess;
using ufmt.ssbic.Business;

namespace ufmt.ssbic.View.webapp.bolsista
{
    public partial class UCDadosAluno : System.Web.UI.UserControl
    {
        private BolsistaBO bolsistaBO;
        private AlunoBO alunoBO;
        public UCDadosAluno()
        {
            this.bolsistaBO = new BolsistaBO();
            this.alunoBO = new AlunoBO();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int bolsistaUID = int.Parse(Request.QueryString["bolsistaUID"]);

                Bolsista bolsista = this.bolsistaBO.GetBolsistaPorId(bolsistaUID);
                vwAlunoSiga aluno = this.alunoBO.GetAluno(decimal.Parse(bolsista.registroAluno));

                lblNomeAluno.Text = aluno.Nome;
                lblMatricula.Text = aluno.Matricula.ToString();
                lblCurso.Text = aluno.Curso;
                lblCampus.Text = aluno.Campus;
                lblCpf.Text = aluno.CPF;
                txtEmailAluno.Text = bolsista.email;

                if (bolsista.bancoUID != null)
                    ddlBanco.SelectedValue = bolsista.bancoUID.ToString();
               
                txtAgencia.Text = bolsista.agencia;
                txtContaCorrente.Text = bolsista.numeroConta;       
                
            }

        }

        protected void btnSalvarDadosAluno_Click(object sender, EventArgs e)
        {
            string msg = string.Empty;
            try
            {
                int bolsistaUID = int.Parse(Request.QueryString["bolsistaUID"]);
                string email = txtEmailAluno.Text;

                int? bancoUID = null;
                if(!ddlBanco.SelectedValue.Equals("0"))
                    bancoUID = int.Parse(ddlBanco.SelectedValue);

                string agencia = txtAgencia.Text;
                string conta = txtContaCorrente.Text;
                this.bolsistaBO.AtualizarBolsista(bolsistaUID, email, conta, bancoUID, agencia);
                msg = "Bolsista atualizado com sucesso.";
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('" + msg + "');", true);
        }

    }
}