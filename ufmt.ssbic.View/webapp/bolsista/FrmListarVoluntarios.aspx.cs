﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.View.session;

namespace ufmt.ssbic.View.webapp.bolsista
{
    public partial class FrmListarVoluntarios : VerificaSession
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btFiltrar_Click(object sender, EventArgs e)
        {
            grdVoluntarios.DataBind();
        }

        public String LinkEditarVoluntario(int monitorUID)
        {
            int processoSeletivoUID = int.Parse(Request.QueryString["processoSeletivoUID"]);
            int folhaPagamentoUID = int.Parse(Request.QueryString["folhaPagamentoUID"]);

            String url = "FrmEditarVoluntario.aspx?monitorUID=" + monitorUID +
                                                "&processoSeletivoUID="+processoSeletivoUID +
                                                "&folhaPagamentoUID="+folhaPagamentoUID;

            return url;
        }

        public string GetPeriodoAtuacao(String strperiodo)
        {
            string periodoAtuacao = "";

            int periodo = 0;
            if (int.TryParse(strperiodo, out periodo))
            {
                if (periodo == 1)
                    periodoAtuacao = "1º Semestre";
                else if (periodo == 2)
                    periodoAtuacao = "2º Semestre";
                else
                    periodoAtuacao = "Anual";
            }
            return periodoAtuacao;
        }
    }
}