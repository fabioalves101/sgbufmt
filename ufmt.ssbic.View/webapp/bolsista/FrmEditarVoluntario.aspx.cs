﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ufmt.ssbic.View.session;

namespace ufmt.ssbic.View.webapp.bolsista
{
    public partial class FrmEditarVoluntario : VerificaSession
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HiddenField hdUrlRedirect = (HiddenField)UCDadosMonitorVoluntario1.FindControl("hdUrlRedirect");
            hdUrlRedirect.Value = 
                "FrmListarVoluntarios.aspx?processoSeletivoUID="+Request.QueryString["processoSeletivoUID"]+
                                           "&folhaPagamentoUID=" + Request.QueryString["folhaPagamentoUID"];
        }
    }
}