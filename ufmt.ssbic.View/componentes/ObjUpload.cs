﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.IO;

namespace ufmt.ssbic.View.componentes
{

    public class ObjUpload
    {
        public String MsgErro { get; set; }
        public String NomeArquivo { get; set; }

    }

    public class Upload : System.Web.UI.Page
    {
        public ObjUpload UploadFile(FileUpload up, String maxTamanhoArquivoUpload, string tipoArquivoUpload, String diretorioUpload)
        {
            ObjUpload upA = new ObjUpload();
            upA.MsgErro = null;


            if (up.HasFile)
            {
                string[] extensoes = tipoArquivoUpload.Split(',');
                string tipoExtensoes = String.Empty;
                string Arquivo = up.FileName;
                string ext = System.IO.Path.GetExtension(up.FileName);
                bool extensaoValida = false;
                foreach (string extensao in extensoes)
                {
                    tipoExtensoes += " " + extensao.ToString();
                    if(ext.Trim()==extensao)
                        extensaoValida = true;
                }


                if (extensaoValida)
                {

                    if (up.PostedFile.ContentLength > Int32.Parse(maxTamanhoArquivoUpload))
                    {
                        //Arquivo ultrapassa o tamanho máximo permitido
                        upA.MsgErro = "Arquivo ultrapassa o tamanho máximo permitido";

                    }
                    else
                    {

                        string Nome = Guid.NewGuid().ToString();
                        string NomeC = Nome + ext;
                        string Caminho = diretorioUpload;
                        if (!File.Exists(Caminho + NomeC))
                        {
                            try
                            {
                                up.SaveAs(Caminho + NomeC);
                                //Arquivo Enviado";
                                upA.MsgErro = null;
                                upA.NomeArquivo = NomeC;
                            }
                            catch (Exception err)
                            {
                                //Erro na transmissão do arquivo
                                upA.MsgErro = "Não foi possível enviar o arquivo.  Por favor, entre em contato com a coordenação do evento";
                                //TODO: Enviar log de erro

                            }

                        }
                        else
                        {
                            //Arquivo Já Existe no Servidor
                            upA.MsgErro = "Arquivo já existe no servidor";
                        }
                    }
                }
                else
                {
                    //Formato do Arquivo Inválido
                    upA.MsgErro = "Formato do arquivo inválido.  Formato(s) Permitido(s): " + tipoExtensoes;
                }
            }

            return upA;



        }
    }
}