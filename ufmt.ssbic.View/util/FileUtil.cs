﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ufmt.ssbic.View.util
{
    public class FileUtil
    {
        public static void ForceDownload(HttpResponse response, String fileName, String filePhysicalPath)
        {
            System.IO.FileStream fs = null;
            fs = System.IO.File.Open(filePhysicalPath, System.IO.FileMode.Open);
            byte[] btFile = new byte[fs.Length];
            fs.Read(btFile, 0, Convert.ToInt32(fs.Length));
            fs.Close();
            response.AddHeader("Content-disposition", "attachment; filename=" +
                               fileName);
            response.ContentType = "application/octet-stream";
            response.BinaryWrite(btFile);
            response.End();
        }
    }
}