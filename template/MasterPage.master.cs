﻿using System;
using ufmt.patrimonio.bll;
using ufmt.sig.entity;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Culture = "pt-br";

        if (Session["UsuarioPatrimonio"] != null)
        {
            Usuario userMaster = (Usuario)Session["UsuarioPatrimonio"];
            AutenticacaoService aservMaster = new AutenticacaoService();
            
            if (!(aservMaster.VerificaPermissao(int.Parse(userMaster.UsuarioUID.ToString()), int.Parse(System.Configuration.ConfigurationManager.AppSettings["administrador"]))))
            {
                Response.Redirect(ResolveUrl("~") + "Login.aspx");
            }
            else
            {
                UnidadeService respUnidade = new UnidadeService();
                Unidade unidadeGlobal = respUnidade.getUnidadePorResponsavel(int.Parse(userMaster.Pessoa.PessoaUID.ToString()));
                
                if (unidadeGlobal != null)
                {
                    Session["Unidade"] = unidadeGlobal;
                    usuarioLogado.Text = userMaster.Pessoa.Nome;
                }
                else
                {
                   Response.Redirect(ResolveUrl("~") + "Login.aspx");
                }
            }
        }
        else
        {
            Response.Redirect(ResolveUrl("~") + "Login.aspx");
        }
        
    }
       
    protected void lbtSair_Click(object sender, EventArgs e)
    {
        Session.Clear();
        Response.Redirect(ResolveUrl("~") + "Login.aspx");
    }

    protected void lbtnRedefinirSenha_Click(object sender, EventArgs e)
    {
        Response.Redirect(ResolveUrl("~") + "administrador/acessos/RedefinirSenhaFRM.aspx");
    }
}
